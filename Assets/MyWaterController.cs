﻿using UnityEngine;
using System.Collections;

public class MyWaterController : MonoBehaviour {

	public Texture2D[] texturesForAnimation;
	public Texture2D[] normals;
	public int			framesPerSecond = 30;
	public int			framesPerSecondNormals = 30;
	public bool			animateNormals;
	public Vector2		move;
	private Vector2		_move;

	public 	AnimationCurve	foamMoveCurve;
	public	float			foamMoveTime;
	private float			timer = 0;
	private bool			foamForward = true;
	public	Vector2			foamMoveValues;
	public	bool			animateFoam;



	private Material _material;
	// Use this for initialization
	void Start () {
		_material = GetComponent<MeshRenderer> ().material;
	}

	int index;
	// Update is called once per frame
	void Update () {
		index = (int)( (Time.time * framesPerSecond) % texturesForAnimation.Length );
		_material.SetTexture("_Caustics", texturesForAnimation[ index ] );
		if (animateNormals) {
			_material.SetTexture ("_Bump", normals [(int)((Time.time * framesPerSecondNormals) % texturesForAnimation.Length)]);
		}
		_move += move / 1000f;
		_material.SetFloat( "_BendA",Mathf.Cos( ( Time.time * framesPerSecond/1000f )*0.5f ) - 0.5f + _move.x ); 
		_material.SetFloat( "_BendB",Mathf.Sin(	(  Time.time * framesPerSecond/1000f )*0.5f ) + 0.5f + _move.y );

		if (animateFoam) {
			timer += Time.deltaTime;

			if( foamForward )
			{
				float foamValue = Mathf.Lerp( foamMoveValues.x, foamMoveValues.y, foamMoveCurve.Evaluate( timer / foamMoveTime ) );
				_material.SetFloat( "_FoamSize", foamValue );
			}
			else
			{
				float foamValue = Mathf.Lerp( foamMoveValues.y, foamMoveValues.x, foamMoveCurve.Evaluate( ( timer / foamMoveTime ) ) );
				_material.SetFloat( "_FoamSize", foamValue );
			}

			if( timer >= foamMoveTime )
			{
				timer = 0;
				foamForward = !foamForward;
			}
		}
	}

#if UNITY_EDITOR
	public bool readFromFolder;
	public string loadPath;
	public bool sort;
	public bool sortTextures;
	public string replaceWord;
	void OnDrawGizmosSelected()
	{
		if (readFromFolder) {
			readFromFolder = false;
			Object[] textures = Resources.LoadAll( loadPath );
			normals = new Texture2D[ textures.Length ];
			for( int i = 0; i < normals.Length; i++ )
			{
				normals[ i ] = textures[ i ] as Texture2D;
			}
		}

		if (sort) {
		
			sort = false;
			Sort( normals );
		}

		if (sortTextures) {
		
			sortTextures = false;
			Sort( texturesForAnimation );
		}
	}

	void Sort( Texture2D[] source )
	{
		int k = 0;
		for ( int i = 0; i < source.Length; i++) {
		
			k = int.Parse( source[ i ].name.Replace( replaceWord,"" ) );
			for (int j = 0; j < i; j++) {
				
				if( k < int.Parse( source[ j ].name.Replace( replaceWord,"" ) ) )
				{
					Texture2D temp = source[ i ];
					source[ i ] = source[ j ];
					source[ j ]= temp;
					break;
				}
			}
		}
	}
#endif
}
