﻿using UnityEngine;
using System.Collections;

public class TestCOntroller : MonoBehaviour {

	[System.Serializable]
	public class TestObject
	{
		public string 		name;
		public GameObject	obj;
	}

	public TestObject[]		testObjects;

	public void EnableObject( string name )
	{
		foreach ( TestObject obj in testObjects) {
			if( obj.name == name )
			{
				obj.obj.SetActive( !obj.obj.activeSelf );
			}
		}
	}

	public bool removeMono;
	void OnDrawGizmosSelected()
	{
		if (removeMono) {
			removeMono = false;
			Debug.Log("123");
			RemoveMono( transform );
		}
	}

	private void RemoveMono( Transform parent )
	{
		MonoBehaviour[] scripts = parent.GetComponentsInChildren<MonoBehaviour> ();
		for (int i = 0; i < scripts.Length; i++) {
		
			DestroyImmediate( scripts[ i ] );
		}
	}
}
