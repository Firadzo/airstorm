Welcome To Smart Water 3d basic edition:

Smart water must always be installed in this order:
1- Basic
2- Extensions
3- Add-Ons

For example
1- Basic
2- PRO Extension
3- Mobile Extension
4- Underwater Add-Ons

TROUBLESHOOTING

1- the water is all black.
Check if the water mixer in the setup pannel is not all at 0.

2- I have an error message for all shaders saying that the shaders are not compatible with 4.0
You are in DX11 mode, Smart Water does not support DX11 (yet). Uncheck the DX11 button in the player's settings and restart you Unity 3D software.

3- I do not see any reflections.
Make sure that the "MirrorReflection.cs" script is attached to the water. If it is already the case, remove it and attach it again.

4- reflections are too slow and generates too many draw calls
There is nothing you can do, calculating the reflection means drawing your scene another time, it basically doubles the number of draw calls. 
If reflections are a problem, use another shader using a faked reflection texture.

5- When I load a demo there is nothing else than the water.
Be sure you have also installed all the necessary assets. For example with Palace of Orinthalian, this demo is too big, so you need to 
install it from the asset store and then load the SmartWater-Palace-PRO-REF demo for example. The same is working for all the 3rd party scenes demos.

User manual, Tutorials, Support and extras are available at http://www.smartwater3d.com
