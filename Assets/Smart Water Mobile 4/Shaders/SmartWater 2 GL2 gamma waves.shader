Shader "Smart Water/Mobile/GLES 2 waves" {
	Properties   
	{
		// colors
		_WaterColor ("WaterColor", Color) = (1,1,1,1)
		_SunColor ("Sun Color", Color) = (0.5, 0.5, 0.5, 1)		
		_CausticsColor ("Caustics Color", Color) = (0.5, 0.5, 0.5, 1)
		_UnderColor ("WaterColor", Color) = (1,1,1,1)
		// powers
		_RefractionPower ("Refraction Power", Float) = 0.05		
		_ReflectionPower ("Reflection Power", Float) = 0.05
		_CausticPower ("CausticPower", Float) = 1.0		
		_SunPower ("Sun Intensity", Range (0,2)) = 0.25
		_Shininess ("Shininess", Range (0.01, 1)) = 1
		//sun
		_SunPosition ("Sun Position", Vector) = (1, 1, 1,1)
		_SunSpread ("Sun Spread", Range (1,5000)) = 2000	
		_SunHalo ("SunHalo", Float) = 20		
		// textures	
		_ReflectionTex ("Reflection Texture (RGB)", 2D) = "white" {}	
		_Fresnel ("Fresnel (A) ", 2D) = "gray" {}
		_Bump ("Bump (RGB)", 2D) = "bump" {}
		_DepthMap ("Depth Map (A)", 2D) ="" {}
		// sizes
		_TextureSize ("Texture Size", Float) = 1
		_CausticSize ("Caustic Size", Float) = 1.0	
		_FoamSize ("Foam Size", Float) = 1.0
		_ReflectionSize ("Reflection Size", Float) = 1.0		
		// foam
		_FoamStrenght("Foam Strenght", Range(0.01, 1.0)) = 0.1	
		_Foam("Foam (RGB)", 2D) = "white" {}	
		_FoamOnOff ("Foam On Off", Float) = 1	
		_WavesOnOff ("Waves On Off", Float) = 1		

		_Caustics("Caustic Texture (RGB)", 2D) = "caustics" {}

		// passed from CPU
		_BendA ("BendA", Float) = 0.05
		_BendB ("BendB", Float) = 0.05
		_Mix ("Mix", Float) = 1.0	
		_MovX ("MovX", Float) = 1.0		
		_MovY ("MovY", Float) = 1.0											
					
		_DepthFactor ("Depth Factor", Range (0,4)) = 1
		_DistanceFogAdjust ("Distance Fog Adjust", Range (0,1)) = 0.1		
		_CoastalFade("Coastal Fade", Float) = 0.001
		
		_Transparency ("Transparency", Float) = 0.5		

		// vertex animation
		_GerstnerIntensity("Wave Intensity", Float) = 1.0
		_GAmplitude ("Wave Amplitude", Vector) = (1.3 ,1.35, 1.25, 1.25)
		_GFrequency ("Wave Frequency", Vector) = (1.3, 1.35, 1.25, 1.25)
		_GSteepness ("Wave Steepness", Vector) = (2.0, 2.0, 2.0, 1.0)
		_GSpeed ("Wave Speed", Vector) = (1.2, 1.375, 1.1, 1.5)
		_GDir ("Wave Direction", Vector) = (0.3 ,0.85, 0.85, 0.25)	
		BorderWave ("Border Wave Adjust", Vector) = (0.1 ,0.9, 0.5, 0.5)			
		
	}
SubShader {
	Tags { "Queue" = "Transparent-120" "IgnoreProjector"="True"  "DisableBatching"="True" "CanUseSpriteAtlas"="False"}  // water uses -120
	//Tags { "RenderType"="Opaque" }
	Blend SrcAlpha OneMinusSrcAlpha 
	//ZWrite Off
	LOD 200
 
CGPROGRAM

#pragma surface surf SM3DSpecularMobile vertex:vert approxview noforwardadd
#pragma target 2.0 
#pragma glsl

#pragma exclude_renderers flash

#include "UnityCG.cginc"
#include "Assets/Smart Water Basic 4/shaders/SM3DWaves.cginc"	
 


float4 _DepthMap_ST;


sampler2D _Refraction;
sampler2D _ReflectionTex;
sampler2D _Fresnel;
sampler2D _Bump;
sampler2D _MainTex;
sampler2D _MainTex1;
sampler2D _DepthMap;
sampler2D _Caustics;
sampler2D _Foam;

half4 _WaterColor;
half4 _SpecularColor;
half4 _CausticsColor;
half4 _SunColor ;
float _SunSpread;
float _Shininess;
half4 _UnderColor;
half4 _GDir;
float _SunPower;
float _BendA; 
float _BendB;
float _DepthFactor;
float _DistanceFogAdjust;
float _Mix;
float _CausticPower;
float _MovX;
float _MovY;
float _CausticSize;
float _FoamSize;
float _ReflectionSize;
float _SunHalo;
float _Transparency ;
float _RefractionPower;
float _ReflectionPower;
float _Disturbance;
float _CoastalFade;
float _SunLight;
float _Turbulance;
float _FoamStrenght;
float _FoamOnOff;
float _Gamma;
float _TextureSize;
float _WavesOnOff;
float _BorderWave;



float _WavesAmplitude;
float _WavesFrequency;
float _WavesStepness;
float _WavesSpeed;

struct Input {
	//half2	_DepthMap;
    //half2  	bumpTexCoord; 
	half4	depthuv;
	half4   viewVector;
	//float4 	offset;
    //half3   objSpaceNormal;	
  };

 void vert (inout appdata_full v, out Input o) 
{
    o.depthuv.zw = v.vertex.xz/float2(_TextureSize, _TextureSize);   	
	float3 n = mul((float3x3)_Object2World, v.normal).xyz;
    float3 binormal = cross(n, v.tangent) * v.tangent.w;
	half3 worldSpaceVertex = mul(_Object2World,(v.vertex)).xyz;
	o.viewVector.xyw = -normalize(worldSpaceVertex - _WorldSpaceCameraPos);
	float3x3 rotation = float3x3( v.tangent.xyz, binormal, v.normal );
	
    o.depthuv.xy = TRANSFORM_TEX(v.texcoord, _DepthMap);
	half4 fdepth=saturate(_BorderWave/o.depthuv);
	half4 fdepth1=fdepth*fdepth;
		
	
	if (_WavesOnOff) {	
		half3 vtxForAni = (worldSpaceVertex).xzz * 1.0; 	
		half3 nrml;
		half3 offsets;
		_GDir.x-=_MovX;_GDir.y+=_MovY;_GDir.z+=_MovX;
		GerstnerMobile (
			 offsets, nrml, v.vertex.xyz, vtxForAni, 			// offsets, nrml will be written
			_GAmplitude*_WavesAmplitude,					 	// amplitude
			_GFrequency*_WavesFrequency*fdepth,	 				// frequency
			_GSteepness*_WavesStepness*fdepth, 					// steepness
			_GSpeed*_WavesSpeed,								// speed
			_GDir												// direction # 1, 2
		);
		offsets *= (fdepth1);	
		v.vertex.xyz += offsets*fdepth;	
		o.viewVector.z = (offsets.y)*_FoamStrenght*0.2;
		//o.objSpaceNormal = 	nrml;
		
	}	
	
	float3 vd = ObjSpaceViewDir( v.vertex );
}
 
 

void surf (Input i, inout SurfaceOutput o)
{	
	// -------------------- depth map
	
	half4 depth = tex2D(_DepthMap, i.depthuv.xy )*_DepthFactor;	
	half4 fdepth=saturate(1/depth);
	half4 fdepth2=fdepth*fdepth;
	//float offset2=(exp((i.offset.y)*2)*0.030);
	//float turbulance=_Turbulance*(1-fdepth)*0.1;
	
	 
	// --------------------Displacement map animation 
	half4 buv = (half4(i.depthuv.z +_BendB,
	i.depthuv.w ,
	i.depthuv.z ,
	i.depthuv.w-_BendB
	)+fdepth);

	half3 tangentNormal0 = (tex2D(_Bump, buv.xy));
	half3 tangentNormal1 = (tex2D(_Bump, buv.zw)) ;
	half3 tangentNormal = saturate((tangentNormal0 + tangentNormal1)-0.5);	
	float2 bumpSampleOffset = tangentNormal.xy	;
	
	// -------------------- caustics
	half3 caustics = tex2D(_Caustics, bumpSampleOffset+	buv.xy*_CausticSize)*fdepth*_CausticsColor * _CausticPower;

	half3 diffuse1 = tex2D(_MainTex, bumpSampleOffset+buv.xy)*_WaterColor;
	half3 diffuse2 = tex2D(_MainTex1, bumpSampleOffset+buv.xy)*_WaterColor;	
	// -------------------- foam
	//half4 foam = tex2D(_Foam,i.depthuv*_FoamSize+tangentNormal.xy*_Turbulance)*fdepth2*_FoamStrenght;
	//half4 foam = tex2D(_Foam,buv.xy+bumpSampleOffset.xy*_FoamSize*_Turbulance)*fdepth2*_FoamStrenght;
	half4 foam = tex2D(_Foam,i.depthuv.zw*_FoamSize+tangentNormal.xy*_Turbulance)*_FoamStrenght*_FoamOnOff;
	
	// -------------------- Reflections 
	//half3 reflection = (tex2D(_ReflectionTex,i.depthuv.xy))*_ReflectionPower;		
	// -------------------- refraction / fresnel
	//half facing =  saturate(dot( i.viewVector,bumpSampleOffset));	
	//half fresnel = tex2D(_Fresnel, half2(facing, 0.5f)).b;		
	//half3 ref = (lerp (lerp (reflection,tangentNormal.y,_RefractionPower), reflection, fresnel));

	// -------------------- final result mixer
	o.Albedo=(diffuse2*_Mix)+ (diffuse1*(1-_Mix))
	+(caustics*fdepth
	+_UnderColor*0.2
	+ foam*fdepth*_FoamOnOff
	)*fdepth
	*_SunLight
	;
	o.Specular=(bumpSampleOffset.x+bumpSampleOffset.y)+tangentNormal.xyz;
	o.Normal=normalize(tangentNormal);

	//o.Alpha = saturate(lerp(_Transparency, (1-fdepth2),_CoastalFade*fdepth2)*_Transparency*(1.5-fdepth2));
	o.Alpha = saturate(lerp(_Transparency, 1.2-(fdepth),_CoastalFade*fdepth) + max(foam*foam*foam*fdepth2,_CoastalFade*fdepth2));
	//o.Albedo=pow(o.Albedo,_Gamma);
}


	
		#include "Assets/Smart Water Basic 4/shaders/SM3DLightning.cginc"
ENDCG


}

FallBack "Diffuse", 1
}



