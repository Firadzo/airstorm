// Unlit alpha-blended shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "MyWater" {
Properties {
	//_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	
	//_Transparency ("Transparency", Range (0,1)) = 0.5	
	
	//_WaterColor ("WaterColor", Color) = (1,1,1,1)
	
	_Bump ("Bump (RGB)", 2D) = "bump" {}
	
	_Caustics("Caustic Texture (RGB)", 2D) = "caustics" {}
	_CausticsColor ("Caustics Color", Color) = (0.5, 0.5, 0.5, 1)
	_CausticPower ("CausticPower", Float) = 1.0	
	
	_BendA ("BendA", Float) = 0.05
	_BendB ("BendB", Float) = 0.05	
		
	_DepthFactor ("Depth Factor", Range (0,2)) = 1
	_DepthAlpha ("Depth Alpha", Range (0,1)) = 1
	_DepthMap ("Depth Map (A)", 2D) ="" {}
	
	_TextureSize ("Texture Size", Float) = 1
	_CausticSize ("Caustic Size", Float) = 1.0	
	
	_FoamTexture ("Base (RGB)", 2D) = "white" {}
	_FoamSize ("Foam Size", float) = 1
	_FoamTransparency ("Foam Transparency", float) = 1
	_FoamDepth ("Foam Depth", Range (0,1) ) = 1
}

SubShader {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	//Tags { "Queue" = "Transparent-120" "IgnoreProjector"="True"  "DisableBatching"="True" "CanUseSpriteAtlas"="False"}
	LOD 200
	
	Blend SrcAlpha OneMinusSrcAlpha 
	
	Pass {  
		CGPROGRAM
// Upgrade NOTE: excluded shader from DX11 and Xbox360; has structs without semantics (struct v2f members alpha)
#pragma exclude_renderers d3d11 xbox360
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

//float4 _MainTex_ST;
float4 _DepthMap_ST;
			
//sampler2D _MainTex;
sampler2D 	_DepthMap;
sampler2D 	_Caustics;
sampler2D 	_Bump;
sampler2D	_FoamTexture;

//half4 _WaterColor;
half  _FoamDepth;
float _FoamSize;
half4 _CausticsColor;
half4 _DepthColor;
float _BendA; 
float _BendB;
float _DepthFactor;
float _CausticPower;
float _CausticSize;
//float _Transparency ;
float _TextureSize;
float _DepthAlpha;
float _FoamTransparency;

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				half2 bumpTexCoord : TEXCOORD0;
				half2 depthuv	:  	TEXCOORD1;
				half2 foamUV	:  	TEXCOORD2;
				half4 buv		: 	TEXCOORD3;
				//float alpha;
			};
				
			v2f vert (appdata_base v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				//o.bumpTexCoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.bumpTexCoord.xy = v.vertex.xz/float2( _TextureSize, _TextureSize );   	
				o.foamUV.xy =  v.vertex.xz/float2( _FoamSize, _FoamSize );  // v.texcoord / float2( _FoamSize, _FoamSize );
				o.depthuv = TRANSFORM_TEX(v.texcoord, _DepthMap);
				o.buv = half4(o.bumpTexCoord.x +  _BendB,
				o.bumpTexCoord.y +  _BendA ,
				o.bumpTexCoord.x +  _BendA ,
				o.bumpTexCoord.y +  _BendB
				);
				//half4 depth = tex2D(_DepthMap, i.depthuv ) * _DepthFactor;
				//o.alpha = _DepthMap.;
				return o;
			}
			// a, x x>=a
			// 1 fullTransparency
			fixed4 frag (v2f i) : COLOR
			{
				half4 depth = tex2D(_DepthMap, i.depthuv );
				//half4 fdepth=saturate(1/depth);
				//half4 fdepth2=fdepth*fdepth;
//				
				half4 buv = i.buv;
				half3 tangentNormal0 = (tex2D(_Bump, buv.xy));
				half3 tangentNormal1 = (tex2D(_Bump, buv.zw)) ;
				half3 tangentNormal = saturate((tangentNormal0 + tangentNormal1));	
				float2 bumpSampleOffset =  tangentNormal.xy;
				half depthMinusOne = (1 - depth.r );
				
				// -------------------- caustics
				half4 caustics = tex2D(_Caustics, bumpSampleOffset + buv.xy*_CausticSize ) * _CausticsColor;
				
//				buv = half4(i.foamUV.x +  _BendB,
//				i.foamUV.y +  _BendA ,
//				i.foamUV.x +  _BendA ,
//				i.foamUV.y +  _BendB
//				);
//				tangentNormal0 = (tex2D(_Bump, buv.xy));
//				tangentNormal1 = (tex2D(_Bump, buv.zw)) ;
//				tangentNormal = saturate((tangentNormal0 + tangentNormal1));	
//				bumpSampleOffset =  tangentNormal.xy;
				
				half4 foamColor = tex2D( _FoamTexture,// bumpSampleOffset + buv.xy * _FoamSize );
				i.foamUV );
				half alpha = //step( depthMinusOne, _FoamDepth ) * _Transparency
				lerp( _CausticsColor.a, _FoamTransparency * foamColor.r * depth.r , depthMinusOne );
				//step( 1f - ( depth.r ), 0 ) * _Transparency + step( 1f - ( depth.r ), 1 ) * _FoamTransparency * foamColor.r * (1 - depth.r );
				half3 rgbColor = lerp( caustics.rgb, foamColor.rgb, depthMinusOne );
				//step( 1f - ( depth.r ), 0 ) * caustics.rgb + step( 1f - ( depth.r ), 1 ) * foamColor.rgb * (1 - depth.r );
				fixed4 col = fixed4( rgbColor, alpha );
//				half foamDepthResult = step( depthMinusOne, _FoamDepth ) * col.a;
//				col.a =  foamDepthResult + step( foamDepthResult, 0 ) * depth.r;
				 //+ ( step( 1f - ( depth.r ), 1 ) * _FoamTransparency * depth.r * foamColor.r )  );
				//half4 fdepth2=fdepth*fdepth;
				//_Transparency * depth );//tex2D(_Caustics, i.bumpTexCoord  * _CausticSize );
				//col.a = ;//lerp( _Transparency, _DepthAlpha, 1f - ( depth.r ) );
				return col;
			}
		ENDCG
	}
}

}
