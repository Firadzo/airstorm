Shader "TEST_SHADER"
{
        Properties
        {
                _MainTex ("Base (RGB)", 2D) = "white" {}
                _Mask ("Mask (A)", 2D) = "white" {}
                _MaskColor ("Mask Color", Color) = (1,1,1,1)
                _MainColor ("Main Color", Color) = (1,1,1,1)
                _TextureSize("TextureSize", float ) = 1
               	_MaskSize("MaskSize", float ) = 1
                _InMaskTexture ("Base (RGB)", 2D) = "white" {}
                _AlphaOnBorder("AlphaOnBorder", float ) = 1
                _BorderThreshHold("BorderThreshHold", Range(0,1) ) = 1
        }
       
        SubShader
        {
                Tags { "RenderType"="Transparent"  }

                LOD 200
                Lighting Off
                Cull Off

                Pass
                {
                        Blend SrcAlpha OneMinusSrcAlpha
                        CGPROGRAM

                        #pragma vertex vert
                        #pragma fragment frag

                        #include "UnityCG.cginc"

                        
                        uniform sampler2D _MainTex;
                        uniform sampler2D _Mask;
                        
                        uniform float _MaskSize;
                        uniform fixed4 _MaskColor;
                       	uniform fixed4 _MainColor;
                       	
                       	uniform sampler2D _InMaskTexture;
                       	uniform float _TextureSize;
                       	uniform float _AlphaOnBorder;
                       	uniform float _BorderThreshHold;
                       
                        struct vertexInput
                        {
                                float4 vertex : POSITION;
                                half2 texcoord : TEXCOORD0;
                        };

                        struct fragmentInput
                        {
                                float4 position : SV_POSITION;
                                half2 texcoord : TEXCOORD0;
                                half2 maskOutline : TEXCOORD1;
                                half2 maskUVS : TEXCOORD2;
                        };
                       
                        fragmentInput vert(vertexInput i)
                        {
                                fragmentInput o;
                                o.position = mul(UNITY_MATRIX_MVP, i.vertex);
                                o.texcoord = i.texcoord;
                                o.maskUVS = i.texcoord/float2( _MaskSize, _MaskSize );
                                o.maskOutline = i.texcoord/float2( _TextureSize, _TextureSize );
                                return o;
                        }
                       
                        fixed4 frag(fragmentInput i) : COLOR
                        {
                                fixed4 col = tex2D( _MainTex, i.texcoord );  
                                fixed4 maskCol = tex2D( _Mask, i.maskUVS ); 
                                fixed4 inMaskColor = tex2D( _InMaskTexture, i.maskOutline);    
                                fixed4 result = fixed4( lerp(col.rgb, inMaskColor.rgb * _MaskColor.rgb, maskCol.r ) * _MainColor.rgb, 
                               //lerp( col.a, step( maskCol.r, _BorderThreshHold ) * _AlphaOnBorder, maskCol.r ) );
                                step( maskCol.r, 0 ) * col.a  + ( step( maskCol.r, _BorderThreshHold ) * _AlphaOnBorder  )  );
                               // result.a = _MaskColor.a;//lerp( col.a, _MaskColor.a, maskCol );                  
                                return result;
                        }
                        ENDCG
                }
        }
}
