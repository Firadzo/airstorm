Shader "Smart Water/Mobile/GL 1.1" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _SpecColor ("Specular Color", Color) = (1,1,1,1)
	_Emission ("Emmisive Color", Color) = (0,0,0,0)	
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}    
	_BumpTex ("Bump (RGB) Trans (A)", 2D) = "white" {}
	_Transparency ("Transparency", Range (0, 1) ) = 1	
	_Blend ("Blend", Range (0, 1) ) = 1

}

SubShader 
{
	Tags { "Queue" = "Transparent-120" "IgnoreProjector"="True"  "DisableBatching"="True" "CanUseSpriteAtlas"="False"}  // water uses -120
	//Tags { "RenderType"="Opaque" }
	Blend SrcAlpha OneMinusSrcAlpha 
	colormask RGBA
	LOD 200
	
	Color [_Color]
	Pass
	{		
		Material 
		{
            Diffuse [_Color]
            Ambient [_Color]
			Specular [_SpecColor] 
			Emission [_Emission]			
			
        }
		BindChannels {
            Bind "Vertex", vertex
            Bind "normal", normal
            Bind "texcoord", texcoord0 // main uses 1st uv
            Bind "texcoord1", texcoord1  // decal uses 2nd uv
         }

		//Lighting On
		SeparateSpecular On
		SetTexture [_MainTex ]
		{
			ConstantColor ( 0,0,0, [_Transparency])
			combine texture * primary, constant
			combine texture lerp (texture) previous
		}	
		SetTexture [_BumpTex]
		{
			ConstantColor (0,0,0, [_Transparency])
			//combine texture dot3 constant
			combine texture + previous, constant
			//combine texture lerp (previous) constant

		}				
	}

}

}