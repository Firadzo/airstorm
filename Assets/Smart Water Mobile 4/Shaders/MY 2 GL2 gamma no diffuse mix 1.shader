Shader "Smart Water/Mobile/MY_GLES 2 no diffuse mix"  
{ 
	Properties   
	{
		// colors
		_DepthColor ("DepthColor", Color) = (1,1,1,1)
		_WaterColor ("WaterColor", Color) = (1,1,1,1)
		_CausticsColor ("Caustics Color", Color) = (0.5, 0.5, 0.5, 1)
		// powers
		_CausticPower ("CausticPower", Float) = 1.0		
		_SunSpread ("Sun Spread", Range (1,5000)) = 2000
		_SunPower ("Sun Intensity", Range (0,2)) = 0.25
		// textures
		_MainTex ("Main Texture 1 (RGB)", 2D) = "white" {}
		//_Reflection ("Reflection Texture (RGB)", 2D) = "white" {}	
		//_Fresnel ("Fresnel (A) ", 2D) = "gray" {}
		_Bump ("Bump (RGB)", 2D) = "bump" {}
		_DepthMap ("Depth Map (A)", 2D) ="" {}
		// sizes
		_TextureSize ("Texture Size", Float) = 1
		_CausticSize ("Caustic Size", Float) = 1.0	
		_FoamSize ("Foam Size", Float) = 1.0
		//_ReflectionSize ("Reflection Size", Float) = 1.0		
		// foam
		_FoamStrenght("Foam Strenght", Range(0.01, 1.0)) = 0.1	
		_Foam("Foam (RGB)", 2D) = "white" {}	
		_Turbulance("Turbulance", Float) = 1.0				

		_Caustics("Caustic Texture (RGB)", 2D) = "caustics" {}

		// passed from CPU
		_BendA ("BendA", Float) = 0.05
		_BendB ("BendB", Float) = 0.05									
					
		_DepthFactor ("Depth Factor", Range (0,2)) = 1
		_DepthAlpha ("Depth Alpha", Range (0,1)) = 1

		_CoastalFade("Coastal Fade", Float) = 0.001
		
		_Transparency ("Transparency", Float) = 0.5		
		//_Gamma ("Gamma Ajustment", Float) = 2.2		
		
	}
SubShader {
	Tags { "Queue" = "Transparent-120" "IgnoreProjector"="True"  "DisableBatching"="True" "CanUseSpriteAtlas"="False"}  // water uses -120
	//Tags { "RenderType"="Opaque" }
	Blend SrcAlpha OneMinusSrcAlpha 
	//colormask RGBA
	LOD 200

CGPROGRAM

#pragma surface surf SM3DSpecularMobile alpha:fade vertex:vert  keepalpha noshadow approxview noforwardadd
#pragma target 2.0 
#pragma glsl

#include "UnityCG.cginc"



uniform float3 _SunPosition;
float4 _DepthMap_ST;
float4 _Reflection_ST;

sampler2D _Refraction;
sampler2D _Bump;
sampler2D _MainTex;
sampler2D _DepthMap;
sampler2D _Caustics;
sampler2D _Foam;

half4 _WaterColor;
half4 _CausticsColor;
half4 _DepthColor;
float _BendA; 
float _BendB;
float _DepthFactor;
float _DepthAlpha;
float _CausticPower;
float _CausticSize;
float _FoamSize;
float _ReflectionSize;
float _SunSpread;
float _SunPower;
float _SunLight;
float _Transparency ;
float _CoastalFade;
float _Turbulance;
float _FoamStrenght;
float _Gamma;
float _TextureSize;



struct Input {
	//half2	_DepthMap;
    half2  	bumpTexCoord; 
	half2	depthuv;	
  };

 void vert (inout appdata_tan v, out Input o) 
{
    // change the size of the main textures
    o.bumpTexCoord.xy = v.vertex.xz/float2(_TextureSize, _TextureSize);   	
	// distance from the camera
	//o.offset.x = 1-lerp(saturate(_Distance*(distance(_WorldSpaceCameraPos, mul(_Object2World, v.vertex)))),1,_Distance);
	o.depthuv = TRANSFORM_TEX(v.texcoord, _DepthMap);

}
  

//     fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
//         {
//             fixed4 c;
//             c.rgb = s.Albedo; 
//             c.a = s.Alpha;
//             return c;
//         }

void surf (Input i, inout SurfaceOutput o)
{	
	// -------------------- depth map
	half4 depth = tex2D(_DepthMap, i.depthuv ) * _DepthFactor;
	//half4 fdepth=lerp((1/depth),1.0,0.0001);	
	half4 fdepth=saturate(1/depth);
	half4 fdepth2=fdepth*fdepth;

	// --------------------Displacement map animation 
	half4 buv = half4(i.bumpTexCoord.x +  _BendB,
	i.bumpTexCoord.y+  _BendA ,
	i.bumpTexCoord.x+  _BendA ,
	i.bumpTexCoord.y+  _BendB
	);
	half3 tangentNormal0 = (tex2D(_Bump, buv.xy));
	half3 tangentNormal1 = (tex2D(_Bump, buv.zw)) ;
	half3 tangentNormal = saturate((tangentNormal0 + tangentNormal1));	
	float2 bumpSampleOffset =  tangentNormal.xy;
	
	// -------------------- caustics
	half3 caustics = tex2D(_Caustics, bumpSampleOffset+	buv.xy*_CausticSize) * _CausticsColor * _CausticPower;

	// -------------------- foam

	//half4 foam = tex2D(_Foam,buv.xy*_FoamSize+tangentNormal.xy*_Turbulance)*fdepth2*_FoamStrenght;
	//half4 foam = (tex2D(_Foam,buv.xy*_FoamSize+tangentNormal.xy*_Turbulance)*_FoamStrenght*2)*fdepth2;
	//half4 finalfoam=((lerp(foam*fdepth2, foam*fdepth2,1-_CoastalFade))*_FoamStrenght);

	// -------------------- final result mixer'
//	half4 tt  = (caustics * fdepth + _WaterColor
//	)*fdepth
//	*_SunLight;
	o.Albedo= ( caustics + _WaterColor )*_SunLight;
	//o.Albedo = lerp( o.Albedo, ( caustics * fdepth + fdepth + _WaterColor )*fdepth *_SunLight, 1f - depth.r  );
	//o.Specular=(bumpSampleOffset.x+bumpSampleOffset.y)+tangentNormal.xyz;
	o.Normal = normalize( tangentNormal );
	//o.Alpha = saturate(lerp(_Transparency, (1-fdepth2),_CoastalFade*fdepth2)*_Transparency*(1.5-fdepth2));
	o.Alpha = lerp( _Transparency, _DepthAlpha, 1f - ( depth.r )  ); // saturate(_Transparency * depth );//( _CoastalFade * depth ));
	//o.Alpha = saturate(lerp(_Transparency, (1-fdepth2),_CoastalFade*fdepth2)*_Transparency*(1.5-fdepth2));
	//o.Albedo=pow(o.Albedo,_Gamma);
}

	#include "Assets/Smart Water Basic 4/shaders/SM3DLightning.cginc"
ENDCG


}

FallBack "Diffuse", 1
}



