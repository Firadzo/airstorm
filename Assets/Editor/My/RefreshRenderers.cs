﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class RefreshRenderers : MonoBehaviour {

	/// <summary>
	/// Функция удаляет и добавляет снова меш рендереры для всех обьектов на сцене ( для отключения скрытых лайтмапов и активации батчинга )
	/// </summary>
	[MenuItem ("My/Refresh mesh renderers")]
	static void RefreshMeshRenderers()
	{
		MeshRenderer[] renderers = FindObjectsOfType<MeshRenderer> ();
		GameObject obj;
		Material mat;
		foreach (MeshRenderer render in renderers) {
			mat = render.sharedMaterial;
			obj = render.gameObject;
			DestroyImmediate( render );
			obj.AddComponent<MeshRenderer>().sharedMaterial = mat;
		}
	}
}
