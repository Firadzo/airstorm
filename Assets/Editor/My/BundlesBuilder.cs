﻿using UnityEngine;
using UnityEditor;
using com.shephertz.app42.paas.sdk.csharp;    
using com.shephertz.app42.paas.sdk.csharp.upload;
using System.Collections.Generic;

public class UploaderToApp42 {


	private static UploadService _uploadService;
	private static UploadService uploadService
	{
		get{
			if (_uploadService == null) {
				App42Log.SetDebug (true);        //Prints output in your editor console  
				App42API.Initialize ("6c91b2bef171795573128938a9316f647862d2d81f276e57c46d069f95a2745a", "95209d38c8ee577212898bd35c65ae3c6109f5398c64c418d4ea945ab013a7ea");
				_uploadService = App42API.BuildUploadService (); 
			}
			return _uploadService;
		}
	}

	static string GetSelectionPath()
	{
		Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.Assets ); 
		string path = System.IO.Path.Combine (Application.dataPath.Replace ( "Assets", "" ), AssetDatabase.GetAssetPath (selection [0]));
		return path;
	}

	static string GetSelectionName()
	{
		return System.IO.Path.GetFileName (GetSelectionPath () );
	}

	[MenuItem("Assets/UploadToStorage")]
	static void UploadToStorage () {
		//Debug.Log ( selection[ 0 ].name );
		//Debug.Log ( AssetDatabase.GetAssetPath ( selection [0] ) );
		//Debug.Log ( path );
		Debug.Log("Start uploading");
		uploadService.UploadFileForUser (GetSelectionName(), "bundle", GetSelectionPath(), "OTHER", System.DateTime.Now.ToString(), new UnityCallBack() );
	}

	[MenuItem("Assets/RemoveFromStorage")]
	static void RemoveFile( )
	{
		//string name = AssetDatabase.GetAssetPath ( selection [0] ).Split('/')[ AssetDatabase.GetAssetPath ( selection [0] ).Split('/').Length - 1 ] );
		Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.Assets ); 
		uploadService.RemoveFileByUser( GetSelectionName(), "bundle", new RemoveCallBack() );
	}

	static void RemoveFile( System.Action onRemoved )
	{
		uploadService.RemoveFileByUser( GetSelectionName(), "bundle", new RemoveCallBack( onRemoved ) );
	}

	[MenuItem("Assets/ReplcaOnStorage")]
	static void ReplaceFile( )
	{
		uploadService.GetFileByUser( GetSelectionName(), "bundle", new ReplaceCallBack() );
	}

	public class ReplaceCallBack : App42CallBack  
	{  
		public void OnSuccess(object response)  
		{  
			App42Response app42Response = (App42Response) response;   
			Debug.Log ("File Exists");
			RemoveFile ( UploadToStorage );
		}  
		
		public void OnException( System.Exception e)  
		{  
			App42Log.Console("Exception : " + e);  
		}  
	} 
	
	public class RemoveCallBack : App42CallBack  
	{  
		private System.Action onRemoved;
		public RemoveCallBack( System.Action _onRemoved )
		{
			onRemoved = _onRemoved;
		}
		public RemoveCallBack()
		{}

		public void OnSuccess(object response)  
		{  
			App42Response app42Response = (App42Response) response;   
			App42Log.Console("app42Response is : " + app42Response.ToString());
			if (onRemoved != null) {
				Debug.Log("File Removed");
				onRemoved();
			}
		}  
		
		public void OnException( System.Exception e)  
		{  
			App42Log.Console("Exception : " + e);  
		}  
	}  
}

public class BundlesBuilder : EditorWindow {

	[MenuItem ("My/Bundles")]
	static void ShowWindow()
	{
		BundlesBuilder window = (BundlesBuilder)EditorWindow.GetWindow (typeof (BundlesBuilder));
		window.Show();
	}

	private BuildTarget platform = BuildTarget.Android;
	private bool uncompressed = false;
	void OnGUI () {
		
		platform = (BuildTarget)EditorGUILayout.EnumPopup ( "Platform", platform );
		uncompressed = EditorGUILayout.Toggle ("Uncompressed", uncompressed );
		GUILayout.Space ( 5f );

		if (GUILayout.Button ("Build bunles", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
			BuildBundles();
		}

		if (GUILayout.Button ("Build bunles and upload", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
			
			UploadBundles( BuildBundles() );
			
		}

	}

	string BuildBundles()
	{
		string path = EditorUtility.SaveFilePanel ("Save Resource", "Assets/bundles/", "New Resource", "unity3d");
		
		if (path.Length != 0) {
			// Build the resource file from the active selection.
			Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
			
			foreach (object asset in selection) {
				string assetPath = AssetDatabase.GetAssetPath((UnityEngine.Object) asset);
			}
			
			BuildPipeline.BuildAssetBundle(Selection.activeObject, selection, path,
			                               ( uncompressed ?  BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.UncompressedAssetBundle : BuildAssetBundleOptions.CollectDependencies ), 
			                               platform );
			Selection.objects = selection;
		}
		return path;
	}

	void UploadBundles( string filePath )
	{
		if (System.IO.File.Exists (filePath)) {
			string fileName = System.IO.Path.GetFileName( filePath );  
			string userName = platform.ToString ();  
			string description = "File Description";      
			string fileType = "OTHER";   
			App42Log.SetDebug (true);        //Prints output in your editor console  
			App42API.Initialize ("6c91b2bef171795573128938a9316f647862d2d81f276e57c46d069f95a2745a", "95209d38c8ee577212898bd35c65ae3c6109f5398c64c418d4ea945ab013a7ea");  
			UploadService uploadService = App42API.BuildUploadService ();  
			uploadService.UploadFileForUser (fileName, userName, filePath, fileType, description, new UnityCallBack ());  
		} else {
		
			Debug.Log("File doesn't exists at path "+filePath);
		}

	}

}
public class UnityCallBack : App42CallBack  
{  
	public void OnSuccess(object response)  
	{  
		Debug.Log ("Sucess");
		Upload upload = (Upload) response;    
		IList<Upload.File>  fileList = upload.GetFileList();  
		for(int i=0; i < fileList.Count; i++)  
		{  
			App42Log.Console("fileName is " + fileList[i].GetName());  
			App42Log.Console("fileType is " + fileList[i].GetType());  
			App42Log.Console("fileUrl is " + fileList[i].GetUrl());  
			App42Log.Console("TinyUrl Is  : " + fileList[i].GetTinyUrl());  
			App42Log.Console("fileDescription is " + fileList[i].GetDescription());  
		}  
	}  
	
	public void OnException( System.Exception e)  
	{  
		Debug.Log ("ERROR");
		App42Log.Console("Exception : " + e.Message );  
	}  
} 

