﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class SetLightmapSize : MonoBehaviour {

	[MenuItem ("My/Lightmap Size 2048")]
	static void SetLMSize2048()
	{
		LightmapEditorSettings.maxAtlasWidth = 2048;
		LightmapEditorSettings.maxAtlasHeight = 2048;
	}

	[MenuItem ("My/Lightmap Size 4096")]
	static void SetLMSize4096()
	{
		LightmapEditorSettings.maxAtlasWidth = 4096;
		LightmapEditorSettings.maxAtlasHeight = 4096;
	}

	[MenuItem ("My/Debug Size")]
	static void DebugSize()
	{
		Debug.Log ( LightmapEditorSettings.maxAtlasWidth +"/"+LightmapEditorSettings.maxAtlasHeight);
	}
}
