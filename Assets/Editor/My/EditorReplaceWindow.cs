﻿using UnityEngine;
using UnityEditor;

public class EditorReplaceWindow : EditorWindow {

	[MenuItem ("My/EditorReplace")]
	static void ShowWindow()
	{
		EditorReplaceWindow window = (EditorReplaceWindow)EditorWindow.GetWindow (typeof (EditorReplaceWindow));
		window.Show();
	}
	Mesh source;
	Mesh replaceON;
	Transform transformParent;
	Material material;
	void OnGUI () {
		
		source = (Mesh)EditorGUILayout.ObjectField ( "Source", source, typeof(Mesh), true );
		GUILayout.Space ( 5f );
		replaceON = (Mesh)EditorGUILayout.ObjectField ( "Replace to", replaceON, typeof(Mesh), true );
		GUILayout.Space ( 5f );
		material = (Material)EditorGUILayout.ObjectField ( "Material", material, typeof(Material), true );
		GUILayout.Space ( 5f );
		if(GUILayout.Button( "ReplaceMeshes", GUILayout.ExpandWidth(true), GUILayout.ExpandWidth(true))) 
		{
			ReplaceWithMaterial( source, replaceON );
		}
		GUILayout.Space ( 5f );
		if(GUILayout.Button( "ReplaceMeshesWithMaterial", GUILayout.ExpandWidth(true), GUILayout.ExpandWidth(true))) 
		{
			ReplaceWithMaterial( source, replaceON, material );
		}
		GUILayout.Space ( 5f );

		if(GUILayout.Button( "SetMaterialForNewMeshes", GUILayout.ExpandWidth(true), GUILayout.ExpandWidth(true))) 
		{
			ReplaceWithMaterial( replaceON, null, material );
		}
		GUILayout.Space ( 5f );
		EditorGUILayout.BeginHorizontal ();
		transformParent = (Transform)EditorGUILayout.ObjectField ( "Parent", transformParent, typeof(Transform), true );
		if(GUILayout.Button( "Replace child meshes", GUILayout.ExpandWidth(true), GUILayout.ExpandWidth(true))) 
		{
			if( transformParent != null )
			{
				MeshFilter mf;
				MeshFilter[] filters = (MeshFilter[])FindObjectsOfType<MeshFilter>(  );
				foreach( Transform child in transformParent )
				{
					mf = child.GetComponent<MeshFilter>();
					if( mf != null )
					{
						foreach( MeshFilter filter in filters )
						{
							if( filter.name == child.name )
							{
								filter.sharedMesh = mf.sharedMesh;
								filter.GetComponent<MeshRenderer>().sharedMaterial = mf.GetComponent<MeshRenderer>().sharedMaterial;
							}
						}
					}
				}
			}
		}
		EditorGUILayout.EndHorizontal ();

	}

	private void ReplaceWithMaterial( Mesh from, Mesh to = null, Material newMat = null )
	{
		if (from == null ) {
			return;
		}
		MeshFilter[] filters = (MeshFilter[])FindObjectsOfType<MeshFilter>(  );
		for( int i = 0; i < filters.Length; i++ )
		{
			if( filters[ i ].sharedMesh == from )
			{
				if( to != null )
				{
					filters[ i ].sharedMesh = to;
				}
				if( newMat != null )
				{
					filters[ i ].GetComponent<MeshRenderer>().sharedMaterial = newMat;
				}
			}
		}
	}
}
