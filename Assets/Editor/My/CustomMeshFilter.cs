﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MeshFilter))]
[CanEditMultipleObjects]
public class CustomMeshFilter : Editor {

	public MeshFilter Target
	{
		get {  return (MeshFilter)target; }
	}

	public Transform transform
	{
		get{ return Target.transform; }
	}
	[SerializeField]
	static bool optionVisible = false;
	[SerializeField]
	Vector3 offset = Vector3.zero;
	Vector3 scale = Vector3.zero;
	public override void OnInspectorGUI() {
		DrawDefaultInspector ();
		GUILayout.Space ( 5f );
		if(GUILayout.Button( ( optionVisible? "Hide":"More" ), GUILayout.ExpandWidth(true), GUILayout.ExpandWidth(true))) 
		{
			optionVisible = !optionVisible;
		}
		if ( optionVisible ) {
			if (GUILayout.Button ("SaveMesh", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				MyEditorUtilities.SaveMesh (Target.mesh, EditorUtility.SaveFilePanel ("Save", "Assets/TEST", Target.mesh.name, "asset"));
			}
			if (GUILayout.Button ("SaveMeshWithScale", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				MyEditorUtilities.SaveMeshWithScale (Target.mesh, EditorUtility.SaveFilePanel ("Save", "Assets/TEST", Target.mesh.name, "asset"), transform);
			}

			if (GUILayout.Button ("ReacalculateNormals", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				Target.sharedMesh.RecalculateNormals ();
			}
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button ("ClearNormals", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				Vector3[] normals = new Vector3[0];
				Target.sharedMesh.normals = normals;
			}
			if (GUILayout.Button ("ClearUV1", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				Vector2[] uv1 = new Vector2[0];
				Target.sharedMesh.uv2 = uv1;
			}
			if (GUILayout.Button ("ClearUV2", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				Vector2[] uv2 = new Vector2[0];
				Target.sharedMesh.uv2 = uv2;
			}
			EditorGUILayout.EndHorizontal();

			if (GUILayout.Button ("DebugMeshUV", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				Debug.Log ("uv " + Target.sharedMesh.uv.Length);
				Debug.Log ("uv1 " + Target.sharedMesh.uv2.Length);
				Debug.Log ("uv2 " + Target.sharedMesh.uv2.Length);
				Debug.Log ("Normals " + Target.sharedMesh.normals.Length);
			}

			offset = EditorGUILayout.Vector3Field( "Offset ", offset );
			if (GUILayout.Button ("Apply offset", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				Mesh meshNew = Target.sharedMesh;
				Vector3[] vertices = meshNew.vertices;
				for( int j = 0; j < vertices.Length; j++ )
				{
					vertices[ j ].x += offset.x;
					vertices[ j ].y += offset.y;
					vertices[ j ].z += offset.z;
				}
				meshNew.vertices = vertices;
			}
			scale = EditorGUILayout.Vector3Field( "Mesh scale ", scale );
			if (GUILayout.Button ("Apply scale", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				Mesh meshNew = Target.sharedMesh;
				Vector3[] vertices = meshNew.vertices;
				for( int j = 0; j < vertices.Length; j++ )
				{
					vertices[ j ].x *= scale.x;
					vertices[ j ].y *= scale.y;
					vertices[ j ].z *= scale.z;
				}
				meshNew.vertices = vertices;
			}
		}
	}
}

public static class MyEditorUtilities
{
	/// <summary>
	/// Сохранение меша как ассета внутри проекта, если путь задан вне папки ассетов то сохранение не сработает
	/// </summary>
	/// <param name="source">Source.</param>
	/// <param name="path">Path.</param>
	public static void SaveMesh( Mesh source, string path )
	{
		if ( source == null || string.IsNullOrEmpty (path) || !path.Contains("Assets" ) ) {
			return;
		}
		path = path.Substring ( path.IndexOf("Assets") );
		Mesh meshNew = source;
		string meshName = path.Split ( '/' )[ path.Split ( '/' ).Length - 1 ].Replace ( ".asset","");
		Debug.Log ( meshName );
		meshNew.name = meshName;
		Debug.Log ( path );
		AssetDatabase.CreateAsset ( meshNew, path  );
		AssetDatabase.SaveAssets();
	}

	public static void SaveMeshWithScale( Mesh source, string path, Transform transform )
	{
		if ( source == null || string.IsNullOrEmpty (path) || !path.Contains("Assets" ) ) {
			return;
		}
		Debug.Log ( transform.localScale );
		Vector3 scale = transform.localScale;
		transform.localScale = Vector3.one;
		Mesh meshNew = source;
		Vector3[] vertices = meshNew.vertices;
		for( int j = 0; j < vertices.Length; j++ )
		{
			Debug.Log( vertices[ j ] +" before " );
			vertices[ j ].x *= scale.x;
			vertices[ j ].y *= scale.y;
			vertices[ j ].z *= scale.z;
			Debug.Log( vertices[ j ] );
		}
		meshNew.vertices = vertices;
		SaveMesh ( meshNew, path );
	}


	public static void SenterPivot( Mesh source )
	{
		if ( source == null ) {
			return;
		}
		Vector3[] vertices = source.vertices;
		Vector3 pivotPoint = Vector3.zero;
		for( int j = 0; j < vertices.Length; j++ )
		{
			pivotPoint += vertices[ j ];
		}
		pivotPoint /= (float)vertices.Length;
		Debug.Log ( pivotPoint );
	}

	//[CustomEditor(typeof(Mesh))]
	public class CustomMeshEditor : Editor {
		/*public override void OnInspectorGUI() {
			DrawDefaultInspector ();
			/*if(GUILayout.Button( "ClearNormals", GUILayout.ExpandWidth(true), GUILayout.ExpandWidth(true))) 
			{
				Vector3[] normals = new Vector3[0];
				( (Mesh)target ).normals = normals;;
			}
		}*/
	}
}


