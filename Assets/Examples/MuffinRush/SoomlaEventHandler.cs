using System;
using System.Collections.Generic;
using System.Collections;
using Soomla.Store;
using UnityEngine;

	/// This class contains functions that receive events that they are subscribed to.
public class SoomlaEventHandler  : MonoBehaviour
{
	public static SoomlaEventHandler instance;

	public static UILabel storeMoneyLbl;

	void  Start () {
		if (instance == null) {
			transform.parent = null;
			instance = this;
			storeMoneyLbl.text = SaveLoadParams.plMoney.ToString ();
		} else {
			Destroy (gameObject);
			return;
		}
		SoomlaStore.Initialize (new MyIStore ());
		//	StoreInventory.RefreshLocalInventory ();
		DontDestroyOnLoad (gameObject);

		StoreEvents.OnMarketPurchase += onMarketPurchase;
		StoreEvents.OnMarketRefund += onMarketRefund;
		StoreEvents.OnItemPurchased += onItemPurchased;
		StoreEvents.OnGoodEquipped += onGoodEquipped;
		StoreEvents.OnGoodUnEquipped += onGoodUnequipped;
		StoreEvents.OnGoodUpgrade += onGoodUpgrade;
		StoreEvents.OnBillingSupported += onBillingSupported;
		StoreEvents.OnBillingNotSupported += onBillingNotSupported;
		StoreEvents.OnMarketPurchaseStarted += onMarketPurchaseStarted;
		StoreEvents.OnItemPurchaseStarted += onItemPurchaseStarted;
		StoreEvents.OnCurrencyBalanceChanged += onCurrencyBalanceChanged;
		StoreEvents.OnGoodBalanceChanged += onGoodBalanceChanged;
		StoreEvents.OnMarketPurchaseCancelled += onMarketPurchaseCancelled;
		StoreEvents.OnRestoreTransactionsStarted += onRestoreTransactionsStarted;
		StoreEvents.OnRestoreTransactionsFinished += onRestoreTransactionsFinished;
		StoreEvents.OnSoomlaStoreInitialized += onSoomlaStoreInitialized;
		StoreEvents.OnUnexpectedStoreError += onStoreError;
#if UNITY_ANDROID && !UNITY_EDITOR
			StoreEvents.OnIabServiceStarted += onIabServiceStarted;
			StoreEvents.OnIabServiceStopped += onIabServiceStopped;
#endif
		if ( StoreInventory.GetItemBalance ( MyIStore.NO_ADS_ITEM ) > 0 ) {
			
			RemovedAds();
		}
		
		if ( StoreInventory.GetItemBalance ( MyIStore.MIG_ITEM ) > 0 ) {
			BuyMig();
		}
	}

	private void SetStoreLblText()
	{
		if (storeMoneyLbl != null) {
			storeMoneyLbl.text = SaveLoadParams.plMoney.ToString ();
		}
	}

		/// <summary>
		/// Handles a market purchase event.
		/// </summary>
		/// <param name="pvi">Purchasable virtual item.</param>
		/// <param name="purchaseToken">Purchase token.</param>
		public void onMarketPurchase(PurchasableVirtualItem pvi, string payload, Dictionary<string, string> extra) {
		CustomConsole.DebugText ( "Puchase" + pvi.Name +" "+pvi.ID);
		}

		/// <summary>
		/// Handles a market refund event.
		/// </summary>
		/// <param name="pvi">Purchasable virtual item.</param>
		public void onMarketRefund(PurchasableVirtualItem pvi) {

		}

	private void BuyMig()
	{
		PlayerPrefs.SetInt("migOpen", 1);
		GameObject obj = GameObject.Find("MainMenuConroller");
		if (obj) {
			obj.GetComponent<MainMenuConroller>().CheckMigBuyed();
		}
		CustomConsole.DebugText("MIG BOUGHT");
	}
	
	private void RemovedAds()
	{
		SaveLoadParams.RemoveAds();
		MyAdMob.RemoveBanner();
		CustomConsole.DebugText("ADS REMOVED");
	}

	private void onStoreError( int i )
	{
		CustomConsole.DebugText ("StoreError "+i);
	}

		/// <summary>
		/// Handles an item purchase event.
		/// </summary>
		/// <param name="pvi">Purchasable virtual item.</param>
		public void onItemPurchased(PurchasableVirtualItem pvi, string payload) 
		{
			CustomConsole.DebugText( "PURCHASED " + pvi.ItemId );
			switch (pvi.ItemId) {	
			case MyIStore.NO_ADS_ITEM:
				RemovedAds();
				break;

			case MyIStore.MIG_ITEM:
				BuyMig();
				break;
				}
		}

	
		/// <summary>
		/// Handles a good balance changed event.
		/// </summary>
		/// <param name="good">Virtual good whose balance has changed.</param>
		/// <param name="balance">Balance.</param>
		/// <param name="amountAdded">Amount added.</param>
		public void onGoodBalanceChanged(VirtualGood good, int balance, int amountAdded) {
			
		}

		/// <summary>
		/// Handles a currency balance changed event.
		/// </summary>
		/// <param name="virtualCurrency">Virtual currency whose balance has changed.</param>
		/// <param name="balance">Balance of the given virtual currency.</param>
		/// <param name="amountAdded">Amount added to the balance.</param>
		public void onCurrencyBalanceChanged(VirtualCurrency virtualCurrency, int balance, int amountAdded)
		{
			CustomConsole.DebugText ( "Currency balance changed"+ virtualCurrency.Name +" "+balance+" +"+amountAdded );
			SaveLoadParams.plMoney += amountAdded;
			SaveLoadParams.realMoney += amountAdded;
			if( PlayerScript.instance != null )
			{
				PlayerScript.instance.playerMoney = SaveLoadParams.plMoney;
			}
			SaveLoadParams.SaveMoney();
			PlayerSkills.AddStartLevelMoney( amountAdded );
			SetStoreLblText ();
		}

		/// <summary>
		/// Handles a good equipped event.
		/// </summary>
		/// <param name="good">Equippable virtual good.</param>
		public void onGoodEquipped(EquippableVG good) {

		}

		/// <summary>
		/// Handles a good unequipped event.
		/// </summary>
		/// <param name="good">Equippable virtual good.</param>
		public void onGoodUnequipped(EquippableVG good) {

		}

		/// <summary>
		/// Handles a good upgraded event.
		/// </summary>
		/// <param name="good">Virtual good that is being upgraded.</param>
		/// <param name="currentUpgrade">The current upgrade that the given virtual
		/// good is being upgraded to.</param>
		public void onGoodUpgrade(VirtualGood good, UpgradeVG currentUpgrade) {

		}

		/// <summary>
		/// Handles a billing supported event.
		/// </summary>
		public void onBillingSupported() {

		}

		/// <summary>
		/// Handles a billing NOT supported event.
		/// </summary>
		public void onBillingNotSupported() {

		}

		/// <summary>
		/// Handles a market purchase started event.
		/// </summary>
		/// <param name="pvi">Purchasable virtual item.</param>
		public void onMarketPurchaseStarted(PurchasableVirtualItem pvi) {

			CustomConsole.DebugText ( "Purchase start "+ pvi.Name +" "+pvi.ID );
		}

		/// <summary>
		/// Handles an item purchase started event.
		/// </summary>
		/// <param name="pvi">Purchasable virtual item.</param>
		public void onItemPurchaseStarted(PurchasableVirtualItem pvi) {

		}

		/// <summary>
		/// Handles an item purchase cancelled event.
		/// </summary>
		/// <param name="pvi">Purchasable virtual item.</param>
		public void onMarketPurchaseCancelled(PurchasableVirtualItem pvi) {
		CustomConsole.DebugText ( "Purchase canceled "+ pvi.Name +" "+pvi.ID );
		}


		/// <summary>
		/// Handles a restore Transactions process started event.
		/// </summary>
		public void onRestoreTransactionsStarted() {
		CustomConsole.DebugText ( "Restore transaction started");
		}

		/// <summary>
		/// Handles a restore transactions process finished event.
		/// </summary>
		/// <param name="success">If set to <c>true</c> success.</param>
		public void onRestoreTransactionsFinished(bool success) {
		CustomConsole.DebugText ( "Restore transaction finished "+success);
		}

		/// <summary>
		/// Handles a store controller initialized event.
		/// </summary>
		public void onSoomlaStoreInitialized() {
			
		}

#if UNITY_ANDROID && !UNITY_EDITOR
		public void onIabServiceStarted() {

		}
		public void onIabServiceStopped() {

		}
#endif
}
