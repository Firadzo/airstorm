using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Soomla.Store;

public class MyIStore : IStoreAssets {

		/// <summary>
		/// see parent.
		/// </summary>
		public int GetVersion() {
		return 3;
		}
		
		/// <summary>
		/// see parent.
		/// </summary>
		public VirtualCurrency[] GetCurrencies() {
		return new VirtualCurrency[]{COINS };
		}
		
		/// <summary>
		/// see parent.
		/// </summary>
		public VirtualGood[] GetGoods() {
		return new VirtualGood[] { MIG_PLANE_GOOD, NO_ADS_GOOD };
		}
		
		/// <summary>
		/// see parent.
		/// </summary>
		public VirtualCurrencyPack[] GetCurrencyPacks() {
		return new VirtualCurrencyPack[] {FIVE_THOUSANDS_COINS_PACK, SEVEN_THOUSANDS_COINS_PACK, TEN_THOUSANDS_COINS_PACK };
		}
		
		/// <summary>
		/// see parent.
		/// </summary>
		public VirtualCategory[] GetCategories() {
		return new VirtualCategory[]{GENERAL_CATEGORY};
		}
		
		public static VirtualCategory GENERAL_CATEGORY = new VirtualCategory(
		"General", new List<string>( new string[] { MIG, DISABLE_ADS } )
		);
		/** Static Final Members **/
	#if UNITY_ANDROID || UNITY_EDITOR 
		public const string FIVE_THOUSANDS_COINS    = "coin_1000";
		public const string SEVEN_THOUSANDS_COINS   = "coins_7000";
		public const string TEN_THOUSANDS_COINS     = "coins_10000";
		public const string MIG     				= "plane_mig";
		public const string DISABLE_ADS     		= "remove_ads";
	#elif UNITY_IPHONE
		public const string FIVE_THOUSANDS_COINS    = "COINS__5000";
		public const string SEVEN_THOUSANDS_COINS   = "COINS_7000";
		public const string TEN_THOUSANDS_COINS     = "COINS_10000";
		public const string MIG     				= "plane_mig";
		public const string DISABLE_ADS     		= "REMOVE_ADS";
	#elif UNITY_WP8
		public const string FIVE_THOUSANDS_COINS    = "coin_1000";
		public const string SEVEN_THOUSANDS_COINS   = "coins_7000";
		public const string TEN_THOUSANDS_COINS     = "coins_10000";
		public const string MIG     				= "plane_mig";
		public const string DISABLE_ADS     		= "remove_ads";
	#endif
		public const string	COINS_ID = "coins";
		public const string	MIG_ITEM = "mig";
		public const string	NO_ADS_ITEM = "no_ads";

		/** Virtual Currencies **/
		
		public static VirtualCurrency COINS = new VirtualCurrency(
			"Coins",										// name
			"",												// description
			COINS_ID							// item id
			);
		
		
	#region	/** Virtual Currency Packs **/
		
	public static VirtualCurrencyPack FIVE_THOUSANDS_COINS_PACK = new VirtualCurrencyPack(
			"5000 coins",                                   // name
			"Test refund of an item",                       // description
			FIVE_THOUSANDS_COINS,                                   // item id
			5000,												// number of currencies in the pack
			COINS_ID,                        // the currency associated with this pack
			new PurchaseWithMarket(FIVE_THOUSANDS_COINS, 0.99)
			);
		
	public static VirtualCurrencyPack SEVEN_THOUSANDS_COINS_PACK = new VirtualCurrencyPack(
		"7000 coins",                                   // name
		"Test refund of an item",                       // description
		SEVEN_THOUSANDS_COINS,                                   // item id
		7000,												// number of currencies in the pack
		COINS_ID,                        // the currency associated with this pack
		new PurchaseWithMarket(SEVEN_THOUSANDS_COINS, 1.3)
		);

	public static VirtualCurrencyPack TEN_THOUSANDS_COINS_PACK = new VirtualCurrencyPack(
		"10000 coins",                                   // name
		"Test refund of an item",                       // description
		TEN_THOUSANDS_COINS,                                   // item id
		10000,												// number of currencies in the pack
		COINS_ID,                        // the currency associated with this pack
		new PurchaseWithMarket(TEN_THOUSANDS_COINS, 1.8)
		);
#endregion
		/** LifeTimeVGs **/
		// Note: create non-consumable items using LifeTimeVG with PuchaseType of PurchaseWithMarket
		public static LifetimeVG NO_ADS_GOOD = new LifetimeVG(
			"No Ads", 														// name
			"No More Ads!",				 									// description
			NO_ADS_ITEM,													// item id
			new PurchaseWithMarket(DISABLE_ADS, 0.99));	// the way this virtual good is purchased

		public static VirtualGood MIG_PLANE_GOOD = new LifetimeVG(
		"Mig plane", 														// name
		"Buy MIG!",				 									// description
		MIG_ITEM,													// item id
		new PurchaseWithMarket( MIG, 0.99));	// the way this virtual good is purchased
}
