Shader "Smart Water 4/Basic/Basic 1 Flow" 
{ 
	Properties   
	{ 
		// colors
		_WaterColor ("WaterColor", Color) = (1,1,1,1)
		_SpecularColor ("Specular Color", Color) = (1,1,1,1)
		_SunColor ("Sun Color", Color) = (0.5, 0.5, 0.5, 1)		
		_CausticsColor ("Caustics Color", Color) = (0.5, 0.5, 0.5, 1)
		_UnderColor ("WaterColor", Color) = (1,1,1,1)
		// powers
		_RefractionPower ("Refraction Power", Float) = 0.05		
		_ReflectionPower ("Reflection Power", Float) = 0.05
		_CausticPower ("CausticPower", Float) = 1.0		
		_SunPower ("Sun Intensity", Range (0,2)) = 0.25
		_Shininess ("Shininess", Range (0.01, 1)) = 1
		//sun
		_SunPosition ("Sun Position", Vector) = (1, 1, 1,1)
		_SunSpread ("Sun Spread", Range (1,5000)) = 2000	
		_SunHalo ("SunHalo", Float) = 20		
		// textures
		_MainTex ("Main Texture 1 (RGB)", 2D) = "white" {}
		_MainTex1("Main Texture 2 (RGB)", 2D) = "white" {}		
		_ReflectionTex ("Reflection Texture (RGB)", 2D) = "white" {}	
		_Fresnel ("Fresnel (A) ", 2D) = "gray" {}
		_Bump ("Bump (RGB)", 2D) = "bump" {}
		_DepthMap ("Depth Map (A)", 2D) ="" {}
		// sizes
		_TextureSize ("Texture Size", Float) = 1
		_CausticSize ("Caustic Size", Float) = 1.0	
		_FoamSize ("Foam Size", Float) = 1.0
		_ReflectionSize ("Reflection Size", Float) = 1.0		
		// foam
		_FoamStrenght("Foam Strenght", Range(0.01, 1.0)) = 0.1	
		_Foam("Foam (RGB)", 2D) = "white" {}	
		_Turbulance("Turbulance", Float) = 1.0	
		_FoamOnOff ("Foam On Off", Float) = 1	
		_Turbulance("Turbulance", Float) = 1.0	
		_FoamOnOff ("Foam On Off", Float) = 1			
		_Bumpiness ("Bumpiness", Float) = 1			
		_Precision ("Ground Precision", Float) = 1			

		_Caustics("Caustic Texture (RGB)", 2D) = "caustics" {}

		// passed from CPU
		_BendA ("BendA", Float) = 0.05
		_BendB ("BendB", Float) = 0.05
		_Mix ("Mix", Float) = 1.0	
		_MovX ("MovX", Float) = 1.0		
		_MovY ("MovY", Float) = 1.0		
		_Flow ("Flow", Float) = 1.0			
		_Mix1 ("Mix 1", Float) = 1.0
		_Mix2 ("Mix 2", Float) = 1.0
		_Mix3 ("Mix 3", Float) = 1.0
		_Mix4 ("Mix 4", Float) = 1.0		
		_Distance ("Distance", Float) = 1.0		
				
		_DepthFactor ("Depth Factor", Range (0,4)) = 1
		_DistanceFogAdjust ("Distance Fog Adjust", Range (0,1)) = 0.1		
		_CoastalFade("Coastal Fade", Float) = 0.001
		
		_Transparency ("Transparency", Float) = 0.5		
		_Gamma ("Gamma Ajustment", Float) = 2.2			
		_GroundPower ("Ground Power", Float) = 1.0	
		_Strenght ("Ground Zoom", Float) = 1.0			
		
	}
SubShader {
	Tags { "Queue" = "Transparent-120" }  // water uses -120 
	//Tags { "RenderType"="Opaque"  }

	Blend SrcAlpha OneMinusSrcAlpha  
	ZWrite On
	//ZTest Always 
	//Cull Off  
	//Fog { Mode Off }
 
CGPROGRAM

#pragma surface surf SM3DSpecular2 vertex:vert noforwardadd halfasview 
#pragma target 3.0 
#pragma exclude_renderers flash
#pragma multi_compile_fog

#include "UnityCG.cginc"


float4 _DepthMap_ST;

sampler2D _Refraction;
sampler2D _ReflectionTex;
sampler2D _Fresnel;
sampler2D _Bump;
sampler2D _MainTex;
sampler2D _MainTex1;
sampler2D _DepthMap;
sampler2D _Caustics;
sampler2D _Foam;

half4 _WaterColor;
half4 _SpecularColor;
half4 _CausticsColor;
half4 _SunColor ;
float _SunSpread;
float _Shininess;
half4 _UnderColor;
float _SunPower;
float _BendA; 
float _BendB;
float _Flow;
float _DepthFactor;
float _DistanceFogAdjust;
float _Mix;
float _CausticPower;
float _MovX;
float _MovY;
float _CausticSize;
float _FoamSize;
float _ReflectionSize;
float _SunHalo;
float _Transparency ;
float _RefractionPower;
float _ReflectionPower;
float _Disturbance;
float _CoastalFade;
float _SunLight;
float _Turbulance;
float _FoamStrenght;
float _FoamOnOff;
float _Gamma;
float _TextureSize;
float _GroundPower;
float _Strenght;
float _Distance;
float _Bumpiness;
float _Precision;

float _Mix1;
float _Mix2;
float _Mix3;
float _Mix4;	

uniform float4 unity_FogParams;
uniform float4 unity_FogColor;
uniform sampler2D _CameraDepthTexture;


struct Input {		
    half4  	bumpTexCoord; 
    half3   objSpaceNormal;
    half3   viewVector;	
	float4 	offset;
  };
  
  #include "Assets/Smart Water Basic 4/shaders/SM3DLightning.cginc"

 void vert (inout appdata_full v, out Input o) 
{
	half3 worldSpaceVertex = mul(_Object2World,(v.vertex)).xyz;
	o.viewVector = normalize(worldSpaceVertex - _WorldSpaceCameraPos);
	o.bumpTexCoord.xy = v.vertex.xz/float2(_TextureSize, _TextureSize);
	float4 pos = mul(UNITY_MATRIX_MVP, v.vertex);
	
	#if UNITY_UV_STARTS_AT_TOP
		float scale = -1.0;
    #else
		float scale = 1.0;
    #endif

	half4 ver = mul(UNITY_MATRIX_MVP, v.vertex);
	o.objSpaceNormal = 	v.normal;	
    o.bumpTexCoord.zw = TRANSFORM_TEX(v.texcoord, _DepthMap);	
	
	float3 n = mul((float3x3)_Object2World, v.normal).xyz;
    float3 binormal = cross(n, v.tangent) * v.tangent.w;
	// distance from the camera
	o.offset=float4(0,0,0,0);
	o.offset.x = (_Distance*(sqrt(distance(_WorldSpaceCameraPos, mul(_Object2World, v.vertex)))));
    o.objSpaceNormal = 	v.normal;

}
 

void surf (Input i, inout SurfaceOutput o)
{	
	// -------------------- depth map
	;
	half4 depth = tex2D(_DepthMap, i.bumpTexCoord.zw )*_DepthFactor;
	half4 fdepth=saturate(1/depth);
	half4 fdepth2=fdepth*fdepth;
	half4 fdepth3=saturate(3/depth);
	//half4 fdepth4=saturate(4/depth);
	float turbulance=_Turbulance*(1-fdepth)*0.1;
	float dist=2-(clamp((lerp(i.offset.x,1,_Distance)),0,2));
	
	// --------------------Displacement map animation 
	half4 buv = half4(i.bumpTexCoord.x +_BendB-(fdepth3.x*_Flow),
	i.bumpTexCoord.y+_BendA+(fdepth3.y*_Flow) ,
	i.bumpTexCoord.x+_BendA-(fdepth3.x*_Flow) ,
	i.bumpTexCoord.y+_BendB+(fdepth3.y*_Flow)
	);
	half4 tangentNormal0 = float4(0,0,0,0);
	half4 tangentNormal1 = float4(0,0,0,0);
	tangentNormal0.xyz = (tex2D(_Bump, buv.xy)).xyz-0.5;
	tangentNormal1.xyz = (tex2D(_Bump, buv.zw)).xyz-0.5;
	
	half4 tangentNormal = (tangentNormal0 + tangentNormal1);
	float3 bumpSampleOffset = ((i.objSpaceNormal.xyz  - tangentNormal.xyz)); 
	
	// -------------------- Reflections 
	//float4 reflection = tex2Dproj(_ReflectionTex, UNITY_PROJ_COORD(i.grabPassPos+tangentNormal*_Flow))*_ReflectionPower;
	half3 reflection = (tex2D(_ReflectionTex,i.bumpTexCoord.zw*_Precision))*_ReflectionPower;	


	// -------------------- fake ocean animation
	half3 diffuse1 = saturate(tex2D(_MainTex, bumpSampleOffset+buv.xy)*_WaterColor*(0.8));
	half3 diffuse2 = saturate(tex2D(_MainTex1, bumpSampleOffset+buv.xy)*_WaterColor*(0.8))	;
	half3 diffusemix=(diffuse2*_Mix)+ (diffuse1*(1-_Mix));	
	
	// -------------------- caustics
	half3 caustics =(tex2D(_Caustics, bumpSampleOffset.xyz*0.1+buv.xy*_CausticSize)*_CausticsColor * _CausticPower)*fdepth;

	// -------------------- refraction / fresnel
	half facing =  saturate(dot( -i.viewVector,bumpSampleOffset));	
	half fresnel = tex2D(_Fresnel, half2(facing, 0.5f)).b;	
	half3 ref = saturate(lerp (lerp (reflection,tangentNormal.y*_Precision,tangentNormal.y*_RefractionPower), reflection*_SpecularColor, fresnel))*_RefractionPower;
	
		// -------------------- foam
	half4 foam1 = tex2D(_Foam,buv.xy+bumpSampleOffset.xy*_FoamSize*turbulance);
	half4 foam = saturate(foam1*_FoamStrenght*fdepth2);
	
	
	o.Normal=saturate(tangentNormal.xyz*_Bumpiness);	

	// -------------------- final result mixer 
	o.Albedo=
	(
	diffusemix *3
	+(_WaterColor*2+min(dist,0.5))*_Mix3
	+_SunColor*(1-dist)*0.1
	+ foam1*fdepth2*_FoamOnOff*_FoamStrenght*5		
	+ caustics*fdepth2
	+ (_UnderColor*fdepth3)*_Mix3
	+ (ref*(1-dist))*_Mix4
	)*_Mix1
	+ (lerp((foam)*fdepth2, (foam)*fdepth2*fdepth2,_CoastalFade)*0.4)*_FoamStrenght*_FoamOnOff
	;

	o.Specular=saturate(bumpSampleOffset.x+bumpSampleOffset.y)*tangentNormal.y;//*(1-fdepth3);
	o.Alpha = saturate(lerp(_Transparency, 1.2-(fdepth2),_CoastalFade*fdepth3) + max(foam*foam1*fdepth2,0.2));
	o.Albedo=pow(o.Albedo,_Gamma);

}



ENDCG


}

FallBack "Diffuse", 1
}


