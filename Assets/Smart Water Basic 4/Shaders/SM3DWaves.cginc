﻿
uniform float4 _GAmplitude;
uniform float4 _GFrequency;
uniform float4 _GSteepness; 									
uniform float4 _GSpeed;					
uniform float4 _GDirectionAB;		
uniform float4 _GDirectionCD;
uniform float _GerstnerIntensity;

// Waves adapted from Unity's Water Fx 4! Great work guy, very easy to adapt and modify.
inline half3 GerstnerOffsets (half2 xzVtx, half steepness, half amp, half freq, half speed, half2 dir) 
{
	half3 offsets;
	
	offsets.x =
		steepness * amp * dir.x *
		cos( freq * dot( dir, xzVtx ) + speed * _Time.x); 
		
	offsets.z =
		steepness * amp * dir.y *
		cos( freq * dot( dir, xzVtx ) + speed * _Time.x); 
		
	offsets.y = 
		amp * sin ( freq * dot( dir, xzVtx ) + speed * _Time.x);

	return offsets;			
}	

inline half3 GerstnerOffset4 (half2 xzVtx, half4 steepness, half4 amp, half4 freq, half4 speed, half4 dirAB, half4 dirCD,float fdepth1) 
{
	half3 offsets;
	
	half4 AB = steepness.xxyy * amp.xxyy * dirAB.xyzw;
	half4 CD = steepness.zzww * amp.zzww * dirCD.xyzw;
	
	half4 dotABCD = freq.xyzw * half4(dot(dirAB.xy, xzVtx), dot(dirAB.zw, xzVtx), dot(dirCD.xy, xzVtx), dot(dirCD.zw, xzVtx));
	half4 TIME = _Time.yyyy * speed;
	
	half4 COS = cos (dotABCD + TIME);
	half4 SIN = sin (dotABCD + TIME);
	
	offsets.x = dot(COS, half4(AB.xz, CD.xz))*fdepth1;
	offsets.z = dot(COS, half4(AB.yw, CD.yw))*fdepth1;
	offsets.y = dot(SIN, amp)*fdepth1;

	return offsets;			
}	

inline half3 GerstnerNormal (half2 xzVtx, half steepness, half amp, half freq, half speed, half2 dir) 
{
	half3 nrml = half3(0,0,0);
	
	nrml.x -=
		dir.x * (amp * freq) * 
		cos(freq * dot( dir, xzVtx ) + speed * _Time.x);
		
	nrml.z -=
		dir.y * (amp * freq) * 
		cos(freq * dot( dir, xzVtx ) + speed * _Time.x);	

	return nrml;			
}	

inline half3 GerstnerNormal4 (half2 xzVtx, half4 amp, half4 freq, half4 speed, half4 dirAB, half4 dirCD, float fdepth1) 
{
	half3 nrml = half3(0,2.0,0);
	
	half4 AB = freq.xxyy * amp.xxyy * dirAB.xyzw;
	half4 CD = freq.zzww * amp.zzww * dirCD.xyzw;
	
	half4 dotABCD = freq.xyzw * half4(dot(dirAB.xy, xzVtx), dot(dirAB.zw, xzVtx), dot(dirCD.xy, xzVtx), dot(dirCD.zw, xzVtx));
	half4 TIME = _Time.yyyy * speed;
	
	half4 COS = sin (dotABCD + TIME);
	
	nrml.x -= dot(COS, half4(AB.xz, CD.xz))*fdepth1;
	nrml.z -= dot(COS, half4(AB.yw, CD.yw))*	fdepth1;
	
	nrml.xz *= _GerstnerIntensity;
	nrml = normalize (nrml);

	return nrml;			
}	

inline void Gerstner (	out half3 offs, out half3 nrml,
				 half3 vtx, half3 tileableVtx, 
				 half4 amplitude, half4 frequency, half4 steepness, 
				 half4 speed, half4 directionAB, half4 directionCD,float fdepth1 ) 
{

		offs = GerstnerOffset4(tileableVtx.xz, steepness, amplitude, frequency, speed, directionAB, directionCD,fdepth1);
		nrml = GerstnerNormal4(tileableVtx.xz + offs.xz, amplitude, frequency, speed, directionAB, directionCD,fdepth1);		
					
}

inline void GerstnerMobile (	out half3 offs, out half3 nrml,
				 half3 vtx, half3 tileableVtx, 
				 half4 amplitude, half4 frequency, half4 steepness, 
				 half4 speed, half4 dir ) 
{

		offs = GerstnerOffsets(tileableVtx.xz, steepness, amplitude, frequency, speed, dir);
		nrml = GerstnerNormal(tileableVtx.xz + offs.xz, steepness, amplitude, frequency, speed, dir);		
					
}