﻿ struct SurfaceOutputSpecColor {
	half3 Albedo;
	half3 Normal;
	half3 Emission;
	half Specular;
	half3 GlossColor;
	half Alpha;              
   
};

	// old lightning model
 half4 LightingSM3DSpecular (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
        half3 h = normalize (lightDir + viewDir);
		half diff=dot (h, lightDir);

        float nh = dot (s.Normal, h);
        float spec =  pow (nh, 48*diff*_SunSpread);

        half4 c;
        c.rgb = (s.Albedo * _LightColor0.rgb +spec *_SunPower) * atten * s.Specular *2;
        c.a = s.Alpha;
        return c;
    }

half4 LightingSM3DSpecular2 (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
        half3 h = normalize ((lightDir + viewDir)+s.Specular);

        half diff = max (0, dot (s.Normal, lightDir));

        float nh = max (0, dot (s.Normal, h));
        float spec = pow(pow (nh, 2048*diff*_SunSpread),nh);//+exp(s.Specular*s.Specular);

        half4 c;
        c.rgb = (s.Albedo   + _LightColor0.rgb * spec  * _SunPower ) * (atten * 2 );
        c.a = s.Alpha;
        return c;
    }
	
half4 LightingSM3DSpecularMobile (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
        half3 h = normalize ((lightDir + viewDir)+s.Specular);

        //half diff = max (0, dot (s.Normal, lightDir));

        float nh = max (0, dot (s.Normal, h));
        float spec = pow (nh, 2048*_SunSpread);//+exp(s.Specular*s.Specular);

        half4 c;
        c.rgb = (s.Albedo   + _LightColor0.rgb * spec  * _SunPower ) * (atten );
        c.a = s.Alpha;
        return c;
    }
	
half4 LightingSM3DSpecularUnder (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
        half3 h = normalize ((lightDir + viewDir)+s.Specular);

        //half diff = max (0, dot (s.Normal, lightDir));

        float nh = max (0, dot (s.Normal, h));
        float spec = pow (nh, 48*_SunSpread);

        half4 c;
        c.rgb = (s.Albedo   + _LightColor0.rgb * spec  * _SunPower *2.21) * (atten*2 );
        c.a = s.Alpha;
        return c;
    }
	
half4 LightingSM3DSpecularPBR (SurfaceOutputSpecColor s, half3 lightDir, half3 viewDir, half atten) {
		#ifndef USING_DIRECTIONAL_LIGHT
		lightDir = normalize(lightDir);
		#endif
		
		viewDir = normalize(viewDir);
		half3 h = normalize (lightDir + viewDir);
		
		half diff = max (0, dot (s.Normal, lightDir));
		float nh = max (0, dot (s.Normal, h));
        float spec = pow(pow (nh, 2048*diff*_SunSpread),nh);//+exp(s.Specular*s.Specular);

        half4 c;
        c.rgb = (s.Albedo   + _LightColor0.rgb * spec  * _SunPower ) * (atten * 2 ); 
		c.a = s.Alpha + _LightColor0.a  * spec * atten ;
        return c;
    }
	
