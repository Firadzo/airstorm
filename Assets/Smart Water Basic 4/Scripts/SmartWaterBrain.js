#pragma strict
import System.IO;

@script ExecuteInEditMode

// ****  SmartWaterBrain  ****
// (c) Nicolas Choukroun All Rights Reserved.
// http://smartwater3d.com for support


// **************************************************
// IMPORTANT
// If you don't master programming, better not touch to this script.
// 
// **************************************************

public var watertype:int=1; //0=indie, 1=OpenGl 1.1 1 pass,  2= OpenGLes 1.1 2 pass, 3=OpenGLes 2.0, 4=Pro OpenGL 3 
public static var statusunderwater:boolean; //true = underwater, false = above water
public var mainTerrain:GameObject;

public var currentdepthmap:Texture2D;
public var colorspace:int;
public var antialias:int;
public var currentshader : Shader;
public var currentmaterial:Material;
public var currentmesh:Mesh;
public var currentundermaterial:Material;
public var storedmaterialwater:Material[];
public var storedmeshes:Mesh[];
public var storedmaterialunder:Material[];
@SerializeField
private var excurrentmaterial:Material;
public var autosetup:boolean;
public var depthmapmaxy:int;
public var shininess:float;
public var depthadjust:float;
public var flow:float;
public var groundprecision:float;
public var complex:float;
public var distance:float;
public var mix1:float;
public var mix2:float;
public var mix3:float;
public var mix4:float;

public var fixedypos:boolean;
public var foamonoff:int;
public var wavesonoff:int;
public var wavesamplitude:float;
public var wavesintensity:float;
public var wavesfrequency:float;
public var wavesstepness:float;
public var wavesspeed:float;

public var debugonoff:int;
public var materialindependant:boolean;
public var depthmapreso:int=1024;
public var adjustborders:float=1;
var delayx=10;
var delayxtemp=0;
public var gamma:float=2.2;

public var SmartWaterImage : Texture2D;
public var DepthMap : Texture2D;
// public var noiseTexture: Texture2D;
public var caustics : Texture[];
public var mainwater : Texture[];
public var framesPerSecond=10;

 // for each prefab, you have to set these.
public var mycam : Camera;
public var mysun : Light;
//  variables
static public var stepjump:float = 10.0; // water moving x speed
static public var myposy:float;
public var groundpower:float;
//static public var speedx = 15.0; // water moving x speed
//static public var speedy= 20.0;// water moving y speed

public var waterthickness = 1.0;
public var movex:float; // direction how the water moves
public var movey:float;
public var depthfactor:float;
private var movexi = 0.0;
private var moveyi= 0.0;

// all the textures needed
public var fresnelTexture : Texture2D;
public var foamTexture: Texture2D;
public var reflectionTexture : Texture2D;
static public var reflectionTexturePRO : RenderTexture;
// public var refractionTexture : Texture2D;
public var bumpTexture : Texture2D;
public var maintexTexture : Texture2D;
public var maintexTexture1 : Texture2D;
static public var causticsTexture : Texture2D;

// all the color needed
public var causticsColor : Color;
public var specularColor : Color;
public var mainColor : Color;
public var underColor : Color;

// transparency is set by the color level, the more bright the more transparent.
public var transparency  : float;
public var sunColor : Color;
public var refractionpower : float;
public var specular : float;
public var sunpower : float;
public var sunhalo : float;
public var sunspread : float;
public var causticpower : float ;
public var reflectionpower : float = 0.5;
public var causticsize : float = 0.5;
public var foamsize : float = 0.5;
public var reflectionsize : float = 0.5;
public var texturesize:float=1.0;
public var disturbance:float=1.0;
public var strenght:float;
public var foamstrenght:float;
public var turbulance:float;
public var coastalfade:float;
public var borderwave:float;
public var depthtransparency:float;

public var distancefogadjust : float = 0.1;
private var shader : Shader = null;
// do not touch, or only if you know why
private var savefogcolor:Color;
private var savefogdensity:float;
private var savefogstart:float;
private var savefogend:float;
private var saveskybox:Material;
private var status=true;
private var gali:float=1;
private var savescale:Vector3;
public	var index : int ;
public	var index1 : int ;
public	var indexf1 : float ;
static public	var posy:float;
public var bounce:int=0;
public var sw:int=0; 
public var calledfirsttime=0;
private var webtext:String="0.0";
private var maxnbrshaders:int=15;

private var pathsu : String[] = [
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2", 
"Materials Private/Under/SmartwaterUnderGL2",
"Materials Private/Under/SmartwaterUnderGL2",
"Materials Private/Under/SmartwaterUnderGL2",
"Materials Private/Under/SmartwaterUnderGL2",
"Materials Private/Under/SmartwaterUnderGL2"	
];

private var pathmaterial : String[] = [
"Materials Private/Mobile/SmartWaterPlaneMobile01", 
"Materials Private/Mobile/SmartWaterPlaneMobile02",  						
"Materials Private/Mobile/SmartWaterGL11-1Pass", 
"Materials Private/Mobile/SmartWaterPlaneMobile03",
"Materials Private/Basic/SmartWaterPlaneBasic3g", 					
"Materials Private/Basic/SmartWaterPlaneBasic4g",
"Materials Private/Cartoon/SmartWaterCartoon1",
"Materials Private/Pro/SmartWaterPlanePro1",
"Materials Private/Pro/SmartWaterPlanePro2",
"Materials Private/Pro/SmartWaterPlanePro3",
"Materials Private/Pro/SmartWaterPlanePro4",							
"Materials Private/Pro/SmartWaterPlanePro5",
"Materials Private/Pro/SmartWaterPlanePro6",
"Materials Private/Pro/SmartWaterPlanePro7",
"Materials Private/Pro/SmartWaterPlanePro8",
"Materials Private/Pro/SmartWaterPlanePro8",
"Materials Private/Pro/SmartWaterPlanePro8"
							
];

private var pathmeshes : String[] = [
"Meshes/Smartwater_Plane_16x16", 
"Meshes/Smartwater_Plane_24x24", 
"Meshes/Smartwater_Plane_32x32", 
"Meshes/SmartWater_plane_48x48", 
"Meshes/Smartwater_Plane_64x64", 			
"Meshes/Smartwater_Plane_96x96", 			
"Meshes/Smartwater_Plane_128x128",	
"Meshes/Smartwater_Plane_128x128",	
"Meshes/Smartwater_Plane_128x128"					
];	

// Phillips waves
/*
	public var m_mat:Material;
	public var m_numGridsX:int = 2;
	public var m_numGridsZ:int = 2;
	public var N:int = 64;
	public var m_length:float = 64;
	public var m_waveAmp:float = 0.0002f; // phillips spectrum parameter -- affects heights of waves
	public var m_windSpeed:Vector2 = new Vector2(32.0f,32.0f);
	var Nplus1:int;								
	var m_windDirection:Vector2 ;
	var m_fourier:FourierCPU;
	// for fast fourier transform
	var m_heightBuffer:Vector2[];
	var m_slopeBuffer:Vector4[]; 
	var m_displacementBuffer:Vector4[];
	var m_spectrum:Vector2[];
	var m_spectrum_conj:Vector2[];
	var m_position:Vector3[];
	var m_dispersionTable:float[];
	var m_fresnelLookUp:Texture2D ;
	var GRAVITY = 9.81;
*/	
#if UNITY_EDITOR
function Awake()
{

	CheckVersion();

}

 
 
function CheckVersion(){

		// check version number on the web
	var url = "http://www.smartwater3d.com/check_version.php";
	var web = new WWW (url);
	yield web;
	webtext=web.text;
	yield WaitForSeconds (60*10); // check version number every 10 min

}
#endif
 
function Start()
{
	myposy=transform.localPosition.y;
	gameObject.tag="SmartWater";

	if (autosetup)
	{
		switch(Application.platform)
		{
 #if UNITY_STANDALONE
			case RuntimePlatform.OSXPlayer:
			case RuntimePlatform.OSXEditor:
			case RuntimePlatform.OSXWebPlayer:
			case RuntimePlatform.OSXDashboardPlayer:
			case RuntimePlatform.WindowsPlayer:
			case RuntimePlatform.WindowsWebPlayer:
			case RuntimePlatform.LinuxPlayer:
			watertype=2; // smartwater GL 3
			break;
	

#endif

 #if UNITY_IPHONE
			case RuntimePlatform.IPhonePlayer:	
			
				switch (UnityEngine.iOS.Device.generation)
				{
					case UnityEngine.iOS.DeviceGeneration.iPhone:	//First generation device
					case UnityEngine.iOS.DeviceGeneration.iPhone3G:	//Second generation
					case UnityEngine.iOS.DeviceGeneration.iPhone3GS: //Third generation
					case UnityEngine.iOS.DeviceGeneration.iPodTouch1Gen:	//iPod Touch, first generation
					case UnityEngine.iOS.DeviceGeneration.iPodTouch2Gen:	//iPod Touch, second generation
					case UnityEngine.iOS.DeviceGeneration.iPad1Gen:	//iPad, first generation
					watertype=4;
					break;
					case UnityEngine.iOS.DeviceGeneration.iPodTouch3Gen:	//iPod Touch, third generation
					case UnityEngine.iOS.DeviceGeneration.iPhone4:	//Fourth generation
					case UnityEngine.iOS.DeviceGeneration.iPad2Gen:	//iPad, second generation
					case UnityEngine.iOS.DeviceGeneration.iPodTouch4Gen:	//iPod Touch, fourth generation
					case UnityEngine.iOS.DeviceGeneration.iPhone4S:
					case UnityEngine.iOS.DeviceGeneration.iPad3Gen:
					case UnityEngine.iOS.DeviceGeneration.iPhone5:
					case UnityEngine.iOS.DeviceGeneration.iPodTouch5Gen:
					case UnityEngine.iOS.DeviceGeneration.iPadMini1Gen:
					case UnityEngine.iOS.DeviceGeneration.iPad4Gen:
					case UnityEngine.iOS.DeviceGeneration.iPhoneUnknown:
					case UnityEngine.iOS.DeviceGeneration.iPadUnknown:
					case UnityEngine.iOS.DeviceGeneration.iPodTouchUnknown:
					case UnityEngine.iOS.DeviceGeneration.iPhone5C:
					case UnityEngine.iOS.DeviceGeneration.iPhone5S:
					case UnityEngine.iOS.DeviceGeneration.iPad5Gen:
					case UnityEngine.iOS.DeviceGeneration.iPadMini2Gen:
					watertype=0;
					break;

				}
			break;
	#endif		
#if UNITY_ANDROID
			case RuntimePlatform.Android:
			watertype=0; // smartwater iPhone openGL 2
			break;	
#endif		
		}
	
	}
	

	InitShaders();
	
}



function InitShaders()
{	

	storedmaterialwater = new Material[pathmaterial.length+1];
	storedmaterialunder = new Material[pathmaterial.length+1];	
	storedmeshes = new Mesh[pathmeshes.length+1];	

	var i=0;
	//Debug.Log("pathlenght="+pathmaterial.length);
	for(i=0;i<pathmaterial.length-1;i++)
	{
		storedmaterialwater[i]=UnityEngine.Resources.Load(pathmaterial[i], Material);
		//Debug.Log(i+"-exist:"+pathmaterial[i]+ " - responseload="+storedmaterialwater[i]);
	}
	for(i=0;i<pathmeshes.length-1;i++)
	{
		storedmeshes[i]=UnityEngine.Resources.Load(pathmeshes[i], Mesh);
		Debug.Log("storedmeshes : "+i+"-exist:"+pathmeshes[i]+ " - responseload="+storedmeshes[i]);
	}	
	if (!materialindependant) {
		for(i=0;i<pathsu.length-1;i++)
		{	
			storedmaterialunder[i]=UnityEngine.Resources.Load(pathsu[i], Material);
			//Debug.Log(i+"-under:"+pathsu[i]+"  result="+storedmaterialunder[i]);
		}
	}

	PrivateInit();
}

function PrivateInit()
{
	if (statusunderwater==true) 
	{	// we are underwater
		if (materialindependant) {
			// independant
			currentmaterial=currentundermaterial;
			currentshader=currentmaterial.shader as Shader;
		}
		else{

			currentmaterial=storedmaterialunder[watertype];
			currentshader=currentmaterial.shader as Shader;	
			currentmesh=storedmeshes[complex] as Mesh;			
			
		}
		
	}else{
		// we are OUT of the water
		if (materialindependant) 
		{
			if (excurrentmaterial==null) excurrentmaterial=storedmaterialwater[watertype];
			currentmaterial=excurrentmaterial;
			currentshader=currentmaterial.shader as Shader;
		}else {
			currentmaterial=storedmaterialwater[watertype] as Material;
			currentundermaterial=storedmaterialunder[watertype] as Material;
			//Debug.Log("Material selected is ="+watertype+" - "+storedmaterialwater[watertype]);
			currentshader=storedmaterialwater[watertype].shader as Shader;
			currentmesh=storedmeshes[complex] as Mesh;
		}
	}

}

// here we pass all the inspector real time value to the shader.
// some simple maths are done on the CPU to have a good balance Shader/CPU
function LateUpdate () 
{
	//InitShaders();
	if (watertype>=maxnbrshaders-1) watertype=maxnbrshaders-1;
	if (watertype<0) watertype=0;
	if (texturesize==0) texturesize=1000;


	if (calledfirsttime==0){	
		excurrentmaterial=currentmaterial;calledfirsttime=1;
		if (!materialindependant) currentmesh=storedmeshes[complex] as Mesh;
	}	
	//excurrentmaterial=currentmaterial;	

	
	// doing some maths for the real time texture animations
	movexi+=movex/10000;
	moveyi+=movey/10000;
	index= (Time.time * framesPerSecond) % caustics.Length;
	causticsTexture= caustics[index] as Texture2D; 
	index1= (Time.time * framesPerSecond ) % (mainwater.Length-1);
	maintexTexture= mainwater[index1] as Texture2D; 	

//	// this must stay in the Update function, it is used to animate the water
//	movexi+=movex/10000;
//	moveyi+=movey/10000;
//	
//	// texture animations
//	index= (Time.time * framesPerSecond) % caustics.Length;
//	causticsTexture= caustics[index] as Texture2D; 
//	index1= (Time.time * framesPerSecond/20) % (mainwater.Length-1);
//	maintexTexture= mainwater[index1] as Texture2D; 	
//	indexf1 = (Time.time * framesPerSecond/20)% 1;
//	maintexTexture1= mainwater[index1+1] as Texture2D;		
 
	// 'waves' if the y position is locked by the editor (option in Setup)
//	if (fixedypos)  {
//		posy= posy+Mathf.Sin(Time.time/4 );
//		transform.rotation.z=Mathf.Sin(Time.time )*0.0005;
//		transform.rotation.x=Mathf.Cos(Time.time )*0.0005;
//	}

	//Debug.Log("storedmaterialdebug="+storedmaterialwater[watertype].name);
	//Debug.Log("watertype=="+watertype);
	//Debug.Log("path=="+paths[watertype]);
		

	
	if (storedmaterialwater[watertype]==null)
	{
		InitShaders();
	}
	
	PrivateInit();
	
	

	if (watertype!=2)
	{
		if (watertype!=7) {
		currentmaterial.SetFloat("_Mix", indexf1);		
		currentmaterial.SetTexture("_MainTex", maintexTexture);
		currentmaterial.SetTexture("_MainTex1", maintexTexture1);		
		if (watertype>=9) {currentmaterial.SetTexture("_ReflectionTex", reflectionTexture);}	
		}
		//currentmaterial.SetTexture("_Refraction", refractionTexture);
		currentmaterial.SetTexture("_Caustics", causticsTexture);

		currentmaterial.SetTexture ("_Bump", bumpTexture);
		currentmaterial.SetTexture("_Fresnel", fresnelTexture);
		
		currentmaterial.SetColor("_WaterColor", mainColor);	
		currentmaterial.SetColor("_CausticsColor", causticsColor*mysun.color);			
		//currentmaterial.SetColor("_SpecularColor", specularColor*mysun.color*refcorrect);
		currentmaterial.SetColor("_SunColor", mysun.color*5);	
		currentmaterial.SetFloat("_SunSpread", 1-sunspread);				

		currentmaterial.SetFloat("_Transparency", transparency);			 
		currentmaterial.SetFloat("_Specular", specular);	
		currentmaterial.SetFloat("_MovX", movexi);		
		currentmaterial.SetFloat("_MovY", moveyi);		
		currentmaterial.SetFloat("_SunHalo",2-sunhalo);	

		currentmaterial.SetFloat("_CausticSize", causticsize);			
		currentmaterial.SetFloat("_FoamSize", foamsize);			
		currentmaterial.SetFloat("_ReflectionSize", reflectionsize*100);	
		
		currentmaterial.SetFloat("_TextureSize", texturesize);			
		currentmaterial.SetFloat("_RefractionPower", refractionpower);	
		currentmaterial.SetFloat("_ReflectionPower", reflectionpower);			

		currentmaterial.SetVector("_SunPosition", mysun.transform.position);
		currentmaterial.SetFloat("_SunPower", sunpower);		
		currentmaterial.SetFloat("_Shininess", shininess);
		currentmaterial.SetFloat("_CausticPower", causticpower*2);	
		currentmaterial.SetFloat( "_BendA",Mathf.Cos((Time.time * framesPerSecond/1000)*0.5)-0.5+movexi); 
		currentmaterial.SetFloat( "_BendB",Mathf.Sin((Time.time * framesPerSecond/1000)*0.5)+0.5+moveyi);			
		//currentmaterial.SetTextureOffset ("_DepthMap", Vector2(Mathf.Sin(Time.time )*5,Mathf.Cos(Time.time )*5));
		currentmaterial.SetTexture("_Foam", foamTexture);
		currentmaterial.SetFloat("_FoamStrenght", foamstrenght );			
		currentmaterial.SetFloat("_DepthFactor", depthfactor);
		currentmaterial.SetFloat("_DistanceFogAdjust", distancefogadjust);
		currentmaterial.SetColor("_UnderColor", underColor*4);
		currentmaterial.SetFloat("_Disturbance", disturbance/ 10000231 );
			
		currentmaterial.SetFloat("_Turbulance", turbulance );
		currentmaterial.SetVector("_CameraPosition", mycam.transform.position);
		currentmaterial.SetFloat("_CoastalFade", coastalfade/5 );	
		currentmaterial.SetFloat("_SunLight", mysun.intensity );
		currentmaterial.SetFloat("_FoamOnOff", foamonoff );	
		currentmaterial.SetFloat("_Flow", flow );
		currentmaterial.SetFloat("_Precision", groundprecision ); 
		currentmaterial.SetTexture("_DepthMap",currentdepthmap);
	
		//if (foamonoff==0) currentmaterial.SetColor("_UnderColor", underColor2);
		//else currentmaterial.SetColor("_UnderColor", underColor);		
	}
	if (statusunderwater==true) 
	{
		#if UNITY_EDITOR
		if (PlayerSettings.colorSpace!=ColorSpace.Gamma)  Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
		#endif
		currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
		currentmaterial.SetFloat("_SunSpread", (sunspread));
		currentmaterial.SetTexture("_DepthMap",currentdepthmap);
	}else{
	
		switch (watertype)
		{	
			case 0: // smart water mobile gamma 
			#if UNITY_EDITOR
			if (PlayerSettings.colorSpace!=ColorSpace.Gamma)  Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
			#endif
			currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
			currentmaterial.SetFloat("_SunSpread", (sunspread));
			
			break;	
			case 1: // smart water mobile no diffuse mix
			#if UNITY_EDITOR
			if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");	
			#endif
			currentmaterial.SetFloat("_FoamStrenght", foamstrenght);	
			currentmaterial.SetFloat("_SunSpread", (sunspread));	
			currentmaterial.SetFloat("_RefractionPower", refractionpower*0.5);	
			currentmaterial.SetFloat("_ReflectionPower", reflectionpower*0.1);	
			currentmaterial.SetFloat("_CoastalFade", coastalfade );	
			currentmaterial.SetFloat("_Turbulance", turbulance*0.1 );		
			break;		
			
			case 2: // smart water mobile Open GL 1.1
			#if UNITY_EDITOR
			if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");	
			#endif
		
				currentmaterial.SetTexture("_MainTex", maintexTexture);			
				currentmaterial.SetTexture ("_BumpTex",   causticsTexture);			
				currentmaterial.SetFloat("_Blend", transparency);		
				currentmaterial.SetFloat("_Transparency", transparency-1);	
				currentmaterial.SetColor("_Color", mainColor*mysun.color);
				currentmaterial.SetColor("_Emission", causticsColor*mysun.color);
				currentmaterial.SetColor("_SpecColor", specularColor*0.1);		
				currentmaterial.SetTextureScale ("_MainTex", Vector2(500-causticsize*50,500-causticsize*50));		
				currentmaterial.SetTextureScale ("_BumpTex", Vector2(500-causticsize*50,500-causticsize*50));	
			break;
			
			case 3: // smart water Mobile with Waves
			#if UNITY_EDITOR
			if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");	
			#endif
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght);	
				currentmaterial.SetFloat("_SunSpread", (sunspread));	
				currentmaterial.SetFloat("_RefractionPower", refractionpower*0.5);	
				currentmaterial.SetFloat("_ReflectionPower", reflectionpower*0.1);	
				currentmaterial.SetFloat("_CoastalFade", coastalfade );	
				currentmaterial.SetFloat("_Turbulance", turbulance*0.1 );
				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;
				currentmaterial.SetFloat("_MovX", movexi*0.2);		
				currentmaterial.SetFloat("_MovY", moveyi*0.2);	

				currentmaterial.SetFloat("_WavesOnOff", wavesonoff );

				currentmaterial.SetFloat("_WavesAmplitude", wavesamplitude*10 );	
				currentmaterial.SetFloat("_WavesFrequency", wavesfrequency*0.05 );
				currentmaterial.SetFloat("_WavesStepness", wavesstepness );		
				currentmaterial.SetFloat("_WavesSpeed", wavesspeed*5);		
				currentmaterial.SetFloat("_GerstnerIntensity", wavesintensity );
				currentmaterial.SetFloat("_BorderWave", borderwave*0.1 );				
			break;			
			
			
			case 4: // Smart Water Basic 4 Gamma Open GL 3
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght);	
				currentmaterial.SetFloat("_SunSpread", (1-sunspread));	
				currentmaterial.SetFloat("_Gamma", gamma );		
				currentmaterial.SetFloat("_GroundPower", groundpower );
				currentmaterial.SetFloat("_Strenght", strenght );

				currentmaterial.SetFloat("_Distance", (1/distance));	

				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;				
			break;
			
			case 5: // smart water Mobile Gamma Open GL 3 WAVES	


				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;
				currentmaterial.SetFloat("_MovX", movexi*0.2);		
				currentmaterial.SetFloat("_MovY", moveyi*0.2);	
				currentmaterial.SetFloat("_Gamma", gamma );	
				currentmaterial.SetFloat("_WavesOnOff", wavesonoff );

				currentmaterial.SetFloat("_WavesAmplitude", wavesamplitude*10 );	
				currentmaterial.SetFloat("_WavesFrequency", wavesfrequency*0.05 );
				currentmaterial.SetFloat("_WavesStepness", wavesstepness );		
				currentmaterial.SetFloat("_WavesSpeed", wavesspeed*0.2);		
				currentmaterial.SetFloat("_GerstnerIntensity", wavesintensity );
				currentmaterial.SetFloat("_BorderWave", borderwave );	

				currentmaterial.SetFloat("_Distance", (1/distance));					

				
			break;		

			case 6: // smart water Cartoon
				#if UNITY_EDITOR
				if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
				#endif
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
				currentmaterial.SetFloat("_SunSpread", (sunspread));
				currentmaterial.SetFloat("_Gamma", gamma );				
			break;

			case 7: // smart water PRO GL3  version 1
			 #if UNITY_EDITOR
				if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
			#endif
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
				currentmaterial.SetFloat("_SunSpread", (1-sunspread));	
				currentmaterial.SetFloat("_Gamma", gamma );		
				currentmaterial.SetFloat("_GroundPower", groundpower );
				currentmaterial.SetFloat("_Strenght", strenght );

				currentmaterial.SetFloat("_Distance", (1/distance));	

				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;			
				break;

			case 8: // smart water PRO GL3 version 2 no diffuse mix
			 #if UNITY_EDITOR
				if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
			#endif
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
				currentmaterial.SetFloat("_SunSpread", (1-sunspread));	
				currentmaterial.SetFloat("_Gamma", gamma );		
				currentmaterial.SetFloat("_GroundPower", groundpower );
				currentmaterial.SetFloat("_Strenght", strenght );

				currentmaterial.SetFloat("_Distance", (1/distance));	

				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;				
			
			break;
				
				
			case 9: // smart water PRO GL 3 Pro 3 reflection 
			 #if UNITY_EDITOR
				if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
			#endif	
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
				currentmaterial.SetFloat("_SunSpread", (1-sunspread));	
				currentmaterial.SetFloat("_Gamma", gamma );		
				currentmaterial.SetFloat("_GroundPower", groundpower );
				currentmaterial.SetFloat("_Strenght", strenght );
				currentmaterial.SetFloat("_Distance", (1/distance));	

				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;			
			
			break;		
			
			case 10: // smart water PRO GL 3 Pro 4 reflection no diffuse mix
			 #if UNITY_EDITOR
				if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
			#endif	
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
				currentmaterial.SetFloat("_SunSpread", (1-sunspread));	
				currentmaterial.SetFloat("_Gamma", gamma );		
				currentmaterial.SetFloat("_GroundPower", groundpower );
				currentmaterial.SetFloat("_Strenght", strenght );
				currentmaterial.SetFloat("_Distance", (1/distance));	

				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;
			
			break;		
			
			case 11: // smart water PRO GL 3 Pro 4 reflection no diffuse mix with Shadows
			 #if UNITY_EDITOR
				//if (PlayerSettings.colorSpace==ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Linear Colorspace to run this shader.");		
				if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
				#endif
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
				currentmaterial.SetFloat("_SunSpread", (1-sunspread));	
				currentmaterial.SetFloat("_Gamma", gamma );		
				currentmaterial.SetFloat("_GroundPower", groundpower );
				currentmaterial.SetFloat("_Strenght", strenght );
				currentmaterial.SetFloat("_Distance", (1/distance));	

				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;		
				break;	
				
			case 12: // smart water PRO PRO GL3 WAVES
			 #if UNITY_EDITOR
				if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
			#endif
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
				currentmaterial.SetFloat("_SunSpread", (1-sunspread));	
				currentmaterial.SetFloat("_Gamma", gamma );		
				currentmaterial.SetFloat("_GroundPower", groundpower );	
				currentmaterial.SetFloat("_Strenght", strenght );
				currentmaterial.SetFloat("_Flow", flow );				
				currentmaterial.SetFloat("_Distance", (1/distance));	

				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;
				currentmaterial.SetFloat("_MovX", movexi*0.2);		
				currentmaterial.SetFloat("_MovY", moveyi*0.2);	

				currentmaterial.SetFloat("_WavesOnOff", wavesonoff );

				currentmaterial.SetFloat("_WavesAmplitude", wavesamplitude );	
				currentmaterial.SetFloat("_WavesFrequency", wavesfrequency*0.01 );
				currentmaterial.SetFloat("_WavesStepness", wavesstepness*0.1 );		
				currentmaterial.SetFloat("_WavesSpeed", wavesspeed*0.2);		
				currentmaterial.SetFloat("_GerstnerIntensity", wavesintensity );	
				currentmaterial.SetFloat("_BorderWave", borderwave );			
				break;	
				
				case 13: // smart water PRO PRO GL3 WAVES with shadows
			 #if UNITY_EDITOR
				if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
			#endif
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
				currentmaterial.SetFloat("_SunSpread", (1-sunspread));	
				currentmaterial.SetFloat("_Gamma", gamma );		
				currentmaterial.SetFloat("_GroundPower", groundpower );	
				currentmaterial.SetFloat("_Strenght", strenght );
				currentmaterial.SetFloat("_Flow", flow );				
				currentmaterial.SetFloat("_Distance", (1/distance));	

				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;
				currentmaterial.SetFloat("_MovX", movexi*0.2);		
				currentmaterial.SetFloat("_MovY", moveyi*0.2);	

				currentmaterial.SetFloat("_WavesOnOff", wavesonoff );

				currentmaterial.SetFloat("_WavesAmplitude", wavesamplitude );	
				currentmaterial.SetFloat("_WavesFrequency", wavesfrequency*0.01 );
				currentmaterial.SetFloat("_WavesStepness", wavesstepness*0.1 );		
				currentmaterial.SetFloat("_WavesSpeed", wavesspeed*0.2);		
				currentmaterial.SetFloat("_GerstnerIntensity", wavesintensity );
				currentmaterial.SetFloat("_BorderWave", borderwave );
				break;
				
				case 14: // smart water PRO PRO GL3 WAVES with shadows
			 #if UNITY_EDITOR
				if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
			#endif
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
				currentmaterial.SetFloat("_SunSpread", (1-sunspread));	
				currentmaterial.SetFloat("_Gamma", gamma );		
				currentmaterial.SetFloat("_GroundPower", groundpower );	
				currentmaterial.SetFloat("_Strenght", strenght );
				currentmaterial.SetFloat("_Flow", flow );				
				currentmaterial.SetFloat("_Distance", (1/distance));	

				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;
				currentmaterial.SetFloat("_MovX", movexi*0.2);		
				currentmaterial.SetFloat("_MovY", moveyi*0.2);	

				currentmaterial.SetFloat("_WavesOnOff", wavesonoff );

				currentmaterial.SetFloat("_WavesAmplitude", wavesamplitude );	
				currentmaterial.SetFloat("_WavesFrequency", wavesfrequency*0.01 );
				currentmaterial.SetFloat("_WavesStepness", wavesstepness*0.1 );		
				currentmaterial.SetFloat("_WavesSpeed", wavesspeed*0.2);		
				currentmaterial.SetFloat("_GerstnerIntensity", wavesintensity );
				currentmaterial.SetFloat("_BorderWave", borderwave );	
				break;
				
		
				case 15: // smart water PRO PBR 
			 #if UNITY_EDITOR
				if (PlayerSettings.colorSpace!=ColorSpace.Gamma) Debug.Log("<color=red>Fatal error:</color> Player settings must be set in Gamma Colorspace to run this shader.");
			#endif
				currentmaterial.SetFloat("_SunPower", sunpower);	
				currentmaterial.SetFloat("_FoamStrenght", foamstrenght );	
				currentmaterial.SetFloat("_SunSpread", (1-sunspread));	
				currentmaterial.SetFloat("_Gamma", gamma );		
				currentmaterial.SetFloat("_GroundPower", groundpower );	
				currentmaterial.SetFloat("_Strenght", strenght );
				currentmaterial.SetFloat("_Flow", flow );				
				currentmaterial.SetFloat("_Distance", (1/distance));	

				currentmaterial.SetFloat("_Mix1", mix1*0.2)	;
				currentmaterial.SetFloat("_Mix2", mix2*0.2)	;
				currentmaterial.SetFloat("_Mix3", mix3*0.3)	;	
				currentmaterial.SetFloat("_Mix4", mix4*0.2)	;
				currentmaterial.SetFloat("_MovX", movexi*0.2);		
				currentmaterial.SetFloat("_MovY", moveyi*0.2);	

				currentmaterial.SetFloat("_WavesOnOff", wavesonoff );

				currentmaterial.SetFloat("_WavesAmplitude", wavesamplitude );	
				currentmaterial.SetFloat("_WavesFrequency", wavesfrequency*0.01 );
				currentmaterial.SetFloat("_WavesStepness", wavesstepness*0.1 );		
				currentmaterial.SetFloat("_WavesSpeed", wavesspeed*0.2);		
				currentmaterial.SetFloat("_GerstnerIntensity", wavesintensity );
				currentmaterial.SetFloat("_BorderWave", borderwave );
	
				break;
							
				
		}		
		
	}
	if (Application.isPlaying) 
	{	
		GetComponent.<Renderer>().material=currentmaterial;
		//if (!materialindependant) this.transform.GetComponent(MeshFilter).mesh = currentmesh;
		//this.transform.GetComponent(MeshCollider).sharedMesh = this.transform.GetComponent(MeshFilter).mesh;
		
	}
	else 
	{
		GetComponent.<Renderer>().sharedMaterial=currentmaterial;
		//if (!materialindependant) this.transform.GetComponent(MeshFilter).sharedMesh = currentmesh;		
		
	}
	//currentshader=currentmaterial.shader as Shader;
	// speed of the waves (across the borders.)	
	// if (!fixedypos) renderer.transform.localPosition.y=posy;
	
}

function OnDestroy ()
{
	//if (Application.isPlaying)	DestroyImmediate(renderer.material,true);		
	//else DestroyImmediate(renderer.sharedMaterial,true);

}


function GenerateDepthMap(path: String,layer:String,sizemap:int,move:float)
{
 #if UNITY_EDITOR && !UNITY_WEBPLAYER
	move=move*depthadjust;
	var DepthMap : Texture2D;
	
	var posy:float=transform.position.y;	
	var terrain=mainTerrain.GetComponent(Terrain);
	
	var origin:Vector3 = GetComponent.<Renderer>().bounds.min;
	var theend:Vector3 = GetComponent.<Renderer>().bounds.max;

	var size:Vector3 = GetComponent.<Renderer>().bounds.max - GetComponent.<Renderer>().bounds.min;
	if (debugonoff) Debug.Log("x="+origin+" z="+theend + " size="+size);
	//var col:int=_layer;

	var col:int=LayerMask.NameToLayer(layer);
	if (debugonoff) Debug.Log("name="+layer+"  |  layer="+col+"   | sizemap="+sizemap+ "  |  posy="+posy );
	var layerMask = 1  <<  col;
	var progressBar:float=0;
	EditorUtility.DisplayProgressBar("Generate Depthmap","name="+layer+"  |  layer="+col+"   | sizemap="+sizemap+ "  |  posy="+posy ,progressBar);	
	var res:float= sizemap;

	var aspectx : float= size.x / res ;
	var aspectz : float= size.z / res ;	
	if (debugonoff) Debug.Log("aspectx="+aspectx+"  aspectz="+aspectz);

	EditorUtility.DisplayProgressBar("Generate Depthmap","Create the texture",progressBar);
	// here we pre-calculate the depthmap
	DepthMap= new Texture2D (res,res,TextureFormat.RGB24, false);
	//DepthMap= new Texture2D (res,res, TextureFormat.ARGB32, false);
	DepthMap.wrapMode = TextureWrapMode.Clamp;
	var exdepth:float=0.0;	
	var depth2:float=0.0;
	var adjust: float=0.0;
	var depth1: float=0.0;

	var nbr:float=0;
	var x1:float=0;
	var z1:float=0;
	var x:int=0;
	var z:int=0;
	var hit : RaycastHit;
	var lowest: float =1000;
	var start: Vector3;

	var inc: float =100/res;  // for the progress bar
	EditorUtility.DisplayProgressBar("Generate Depthmap","You must have SmartWater Colliders layers",progressBar);
	transform.Translate(0,move,0);	
	posy-=move;
	// Debug.Log("inc="+inc);
	// get the lowest point
	for (x = 0; x < res; x+=1){
		x1=origin.x+(aspectx*x);
		for (z = 0; z < res; z+=1){
			z1=origin.z+(aspectz*z);	
			start=Vector3(x1, depthmapmaxy, z1);
			if (Physics.Raycast(start, Vector3.down, hit, Mathf.Infinity,layerMask))
			{
				if (hit.point.y<lowest) lowest=hit.point.y;
			}
			// if (x==res/2) Debug.Log("hit z="+z+"   hit="+hit.point.y + "  lowest="+lowest);
			//if (Input.GetKeyDown (KeyCode.Space)) return;
		
		}
		EditorUtility.DisplayProgressBar("Generate Depthmap","Calculate the lowest point: x="+x+"  layer="+layerMask+"   dephmapxy="+depthmapmaxy,progressBar); 
	}
	
	//lowest=lowest+posy; // max deep
	//move =Mathf.Abs(lowest/2);
	//move=2;
	EditorUtility.DisplayProgressBar("Generate Depthmap","Now drawing the depth map.",progressBar);
	//lowest= Mathf.Abs(posy-lowest);
	if (debugonoff) Debug.Log("lowest="+lowest);
	EditorUtility.ClearProgressBar();
	var yVelocity = 0.001;
	for (x = 0; x < res; x+=1){
		x1=origin.x+(aspectx*x);// exdepth=depth1=depth2=DepthMap.GetPixel(x, z).grayscale;
		for (z = 0; z < res; z+=1){
			if (Input.GetKeyDown ("space")) return;
			z1=origin.z+(aspectz*z);	
			start=Vector3(x1, depthmapmaxy, z1);
			if (Physics.Raycast(start, Vector3.down, hit, Mathf.Infinity,layerMask))
			{
				if (hit.point.y<=posy-move) {
					depth1=(posy-hit.point.y)/lowest;
					// depth1=Mathf.SmoothDamp(exdepth,depth1,yVelocity, 0.5);
					//if (PlayerSettings.colorSpace==ColorSpace.Gamma) depth1=Mathf.Pow(depth1,1/2.2);
					adjust=Mathf.Abs(depth1*depthadjust);
					//if (adjust==0.0) adjust=0.00001;
					DepthMap.SetPixel(x, z, Color(adjust,adjust,adjust,adjust));
					exdepth=depth1;
					// if (x==res/2) Debug.Log("adjust="+adjust);
					if (debugonoff && x%4<2.0 && z%4<2.0) Debug.DrawLine (start, hit.point,Color.green);
					//start.y=posy;
					//if (debug) Debug.DrawLine (start, hit.point,Color.blue);
					//Debug.Log("color="+adjust);
					//if (debug) Debug.DrawRay (Vector3(x, lowest*10, z), hit.point, Color.yellow);
				}else{
				// above the water
					depth1=(posy-hit.point.y)/lowest;
					adjust=0;
					depth2=Mathf.Lerp(exdepth,adjust,1.2);
					adjust=depth2;
					//adjust=Mathf.Abs(1-depth1*depthadjust);
					//if (depth2==0.0) depth2=0.00001;
					DepthMap.SetPixel(x, z, Color(adjust,adjust,adjust,adjust));
					exdepth=depth1;
					if (debugonoff && x%4<1 && z%4<1 ) Debug.DrawLine (start, hit.point,Color.yellow);
				}
			}
			else
			{
			 // no water, no collision
				depth1=(posy-hit.point.y)/lowest;
				adjust=1;
				//depth2=Mathf.Lerp(exdepth,depth1,0.5);
				//adjust=depth2;
				//adjust=Mathf.Abs(1-depth1*depthadjust);
				DepthMap.SetPixel(x, z, Color(adjust,adjust,adjust,adjust));
				exdepth=depth1;
				if (debugonoff && x%4<1 && z%4<1) Debug.DrawLine (start, Vector3(x1, posy, z1), Color.red);			
			}
				
			
		}
		
        // Update progressbar
		nbr+=inc; 
		if (nbr>100) nbr=100;
        var progressStr:String = parseInt(nbr)+"% done... x="+x;
	    EditorUtility.DisplayProgressBar("Calculating Depth Map", progressStr, nbr/100);
		// if (debugonoff) break;
		
	

	}
	// antialias
	var xp:float;var xp0:Color;var xp1:Color;var xp2:Color;var xp3:Color;var xp4:Color;var xp5:Color;var xp6:Color;var xp7:Color;var xp8:Color;var c:Color;

	for (var d=1;d < antialias;d+=1) {
		
	    EditorUtility.DisplayProgressBar("Antialiasing", "Please Wait", d/100);
		for (x = 1; x < res-1; x+=1){
			x1=origin.x+(aspectx*x);
			for (z = 1; z < res-1; z+=1){
				xp0=DepthMap.GetPixel(x, z); 
				xp1=DepthMap.GetPixel(x+d, z);
				xp2=DepthMap.GetPixel(x, z+d);
				xp3=DepthMap.GetPixel(x+d, z+d);
				xp4=DepthMap.GetPixel(x-d, z);
				xp5=DepthMap.GetPixel(x, z-d);
				xp6=DepthMap.GetPixel(x-d, z-d);
				xp7=DepthMap.GetPixel(x+d, z-d);
				xp8=DepthMap.GetPixel(x-d, z+d);
				c=(xp1+xp2+xp3+xp4+xp5+xp6+xp7+xp8+xp0+xp0)/10;
				adjust=c.b;
				DepthMap.SetPixel(x, z, Color(adjust,adjust,adjust,adjust));

			}
		}
	}

	if (debugonoff) Debug.Break();
	transform.Translate(0,-move,0);
	posy+=move;
	DepthMap.Apply();
	var sav = DepthMap.EncodeToPNG();
	System.IO.File.Delete(path);
	System.IO.File.WriteAllBytes(path, sav);
	EditorUtility.ClearProgressBar();	EditorUtility.ClearProgressBar();	EditorUtility.ClearProgressBar();
	currentdepthmap=AssetDatabase.LoadAssetAtPath("Assets/Smart Water Basic 4/DepthMaps/default.png", Material) as Texture2D;
#endif
	
	return DepthMap;
}

function OnGUI () {
//		GUI.Label (Rect (10, 10, 800, 20), "Water Type="+watertype);
//		GUI.Label (Rect (10, 20, 800, 40), "Mycam="+mycam.name);
//		//GUI.Label (Rect (10, 40, 800, 80), "currentmaterial="+currentmaterial.name);
//		GUI.Label (Rect (10, 50, 800, 100), "Stored="+storedmaterialwater[watertype]);	
		}

