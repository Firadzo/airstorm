//@script ExecuteInEditMode

/// This is a modified javascript conversion of the Standard Assets MouseLook script.
/// Also added is functionallity of using a key to look up, down, left and right in addition to the Mouse.
/// Everything is on by default. You will want to turn off/on stuff depending on what you're doing.
 
/// You can also modify the script to use the KeyLook functions to control an object's rotation.
/// Try using MouseXandY on an object. Actually it works as is but you'll want to clean it up ;)
 
/// As of this version the key and mouse fight if used at the same time (ie up key and down mouse jitters).
 
/// Minimum and Maximum values can be used to constrain the possible rotation
 
/// To make an FPS style character:
/// - Create a capsule.
/// - Add a rigid body to the capsule
/// - Add the MouseLookPlus script to the capsule.
///   -> Set the script's Axis to MouseX in the inspector. (You want to only turn character but not tilt it)
/// - Add FPSWalker script to the capsule
 
/// - Create a camera. Make the camera a child of the capsule. Reset it's transform.
/// - Add the MouseLookPlus script to the camera.
///   -> Set the script's Axis to MouseY in the inspector. (You want the camera to tilt up and down like a head. The character already turns.)
var cameraReference : Camera;
 
var sensitivityX = 1.0;
var sensitivityY = 1.0;

var minimumX = -360.0;
var maximumX = 360.0;

var minimumY = -360.0;
var maximumY = 360.0;

var rotationX = 1.0;
var rotationY = 1.0;

var lookSpeed = 1.0;

var SpeedX = 0.001;
var SpeedY = 0.001;

var StepX = 0.5;
var StepY = 0.5;

var MaxSpeedX = 1.0;
var MaxSpeedY = 1.0;

var savey;
static var underwater=1;

private var savefog: Color;

private var wasClicked = false;
private var running = false;




function start() {

     //cameraReference=camera.current;
}
 
    function Update ()
    {
		//cameraReference=camera.current;
		var fingerCount = 0;
		for (var touch : Touch in Input.touches) {
		if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled)
			fingerCount++;
		}

		/*if (fingerCount>1 || Input.GetMouseButtonDown(1) )
		{
				rigidbody.AddForce (Vector3.forward * 10000);
				Debug.Log("force");

		}
		*/
  

	   if (Input.GetMouseButton(0) || Input.GetMouseButton(1) ) 
	   {
				wasClicked = true;
		}
		else 
		{
			wasClicked = false;

			SpeedX=0;
			SpeedY=0;
		}
		if (Input.GetMouseButton(1)) {
			running = true;
		}
		else if (Input.GetMouseButton(1)) {
			running = false;
		}
		
		if (wasClicked)
		{

				// Move toward and away from the camera
			if (Input.GetAxis("Vertical"))
			{
				var translationZ = Input.GetAxis("Vertical");
				transform.localPosition += (cameraReference.transform.localRotation * Vector3(0,0,translationZ/underwater)*SpeedY);				
				if (running) SpeedY+=StepY/10; else SpeedY+=StepY/100;
		
			}
		   
			// Strafe the camera
			if (Input.GetAxis("Horizontal"))
			{
				var translationX = Input.GetAxis("Horizontal");
				transform.localPosition += (cameraReference.transform.localRotation * Vector3(translationX/underwater,0,0)*SpeedX);				
				if (running) SpeedX+=StepX/10; else SpeedX+=StepX/100;
			

			}       	
		   

			// Read the mouse input axis
			rotationX += Mathf.Lerp(Input.GetAxis("Mouse X") ,Input.GetAxis("Mouse X") * sensitivityX,Time.time);
			rotationY += Mathf.Lerp(Input.GetAxis("Mouse Y"),Input.GetAxis("Mouse Y") * sensitivityY,Time.time);
 
			// Call our Adjust to 360 degrees and clamp function
			Adjust360andClamp();
 
			// Most likely you wouldn't do this here unless you're controlling an object's rotation.
			// Call our look left and right function.
			KeyLookAround();
 
			// Call our look up and down function.
			KeyLookUp();
			
			if (SpeedX>MaxSpeedX/100) SpeedX=MaxSpeedX/100;
			if (SpeedY>MaxSpeedY/100) SpeedY=MaxSpeedY/100;
			if (SpeedX<-MaxSpeedX/100) SpeedX=-MaxSpeedX/100;
			if (SpeedY<-MaxSpeedY/100) SpeedY=-MaxSpeedY/100;
			//if (transform.localPosition.y>1000+savey) transform.localPosition.y=1000+savey;
			//if (transform.localPosition.y<-1000+savey) transform.localPosition.y=-1000+savey;

		}		
    }
 
    function KeyLookAround ()
    {
//      If you're not using it, you can delete this whole function.
//      Just be sure to delete where it's called in Update.
 
        // Call our Adjust to 360 degrees and clamp function
        Adjust360andClamp();
 
        // Transform our X angle
        transform.localRotation = Quaternion.AngleAxis (rotationX, Vector3.up);
    }
 
    function KeyLookUp ()
    {
        // Adjust for 360 degrees and clamp
        Adjust360andClamp();
 
        // Transform our Y angle, multiply so we don't loose our X transform
        transform.localRotation *= Quaternion.AngleAxis (rotationY, Vector3.left);
    }
 
    function Adjust360andClamp ()
    {
//      This prevents your rotation angle from going beyond 360 degrees and also
//      clamps the angle to the min and max values set in the Inspector.
 
        // During in-editor play, the Inspector won't show your angle properly due to
        // dealing with floating points. Uncomment this Debug line to see the angle in the console.
 
        // Debug.Log (rotationX);
 
        // Don't let our X go beyond 360 degrees + or -
        if (rotationX < -360)
        {
            rotationX += 360;
        }
        else if (rotationX > 360)
        {
            rotationX -= 360;
        }   
 
        // Don't let our Y go beyond 360 degrees + or -
        if (rotationY < -360)
        {
            rotationY += 360;
        }
        else if (rotationY > 360)
        {
            rotationY -= 360;
        }
 
        // Clamp our angles to the min and max set in the Inspector
        rotationX = Mathf.Clamp (rotationX, minimumX, maximumX);
        rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
    }
 
    function Start ()
    {
	
		savey=transform.localPosition.y;
        // Make the rigid body not change rotation
        if (GetComponent.<Rigidbody>())
        {
            GetComponent.<Rigidbody>().freezeRotation = true;
			

        }		
    }