﻿#pragma strict

var frameCount = 0;
var dt = 0.0;
var fps = 0.0;
var updateRate = 4.0;  // 4 updates per sec.
 
function Update()
{
    frameCount++;
    dt += Time.deltaTime;
    if (dt > 1.0/updateRate)
    {
        fps = frameCount / dt ;
        frameCount = 0;
        dt -= 1.0/updateRate;
    }
}

function OnGUI()
{
 GUI.Label (Rect (30, 30, 1000, 20),"FPS: " +fps);
}