
@script ExecuteInEditMode
 
public var projector:Projector ;

function Start()
{
	projector = GetComponent (Projector);
}

function Update()
{
	var daddy: Transform = transform.parent;
	if (!daddy) print("Object has no parent");
	var script: SmartWaterBrain = daddy.GetComponent(SmartWaterBrain);
	if (!script) print("Parent has no SmartWaterBrain script");
	projector.material.SetTexture("_MainTex",script.causticsTexture);	
	//projector.material.SetTextureScale("_MainTex", Vector2(script.causticsize*10,script.causticsize*10));
}