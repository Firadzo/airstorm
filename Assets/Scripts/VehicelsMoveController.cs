﻿using UnityEngine;
using System.Collections;

[ System.Serializable ]
public class VehicleMoveInfo
{
#if UNITY_EDITOR
	[HideInInspector]
	public string name;
#endif
	public Transform 		transf;
	public SplineVenicle[] 	VenicleMoveList;

	public void StartMove()
	{
		for( int i = 0; i < VenicleMoveList.Length; i++ )
		{
			if( VenicleMoveList[i] ) VenicleMoveList[i].StartMove();
		}
	}
}

public class VehicelsMoveController : MonoBehaviour {

	public VehicleMoveInfo[]	vehiclesMoveInf;
	private int					currIndex = 0;

	private Transform			cameraTransf;

	void Awake()
	{
		cameraTransf = Camera.main.transform;
	}

	void Update () 
	{
		if ( vehiclesMoveInf [currIndex].transf.position.x <= cameraTransf.position.x ) 
		{
			vehiclesMoveInf [currIndex].StartMove();
			currIndex ++;
			if( currIndex == vehiclesMoveInf.Length )
			{
				enabled = false;
			}
		}
	}

#if UNITY_EDITOR
	public bool sort;
	public bool collect;
	void OnDrawGizmosSelected()
	{
		if (sort) 
		{
			sort = false;
			VehicleMoveInfo info;
		//	float x = vehiclesMoveInf[ 0 ].transf.position.x;
			for( int i = 0; i < vehiclesMoveInf.Length; i++ )
			{
				for( int j = 0; j < vehiclesMoveInf.Length - 1; j++ )
				{
					if( vehiclesMoveInf[ j ].transf.position.x > vehiclesMoveInf[ j + 1 ].transf.position.x )
					{
						info = vehiclesMoveInf[ j ];
						vehiclesMoveInf[ j ] = vehiclesMoveInf[ j + 1 ];
						vehiclesMoveInf[ j + 1 ] = info;
					}
				}
			}
		}

		if (collect) 
		{
			collect = false;
			VenicleMoveTrigger[] v = FindObjectsOfType< VenicleMoveTrigger >();
			vehiclesMoveInf = new VehicleMoveInfo[ v.Length ];
			for( int i = 0; i < v.Length; i++ )
			{
				vehiclesMoveInf[ i ] = new VehicleMoveInfo();
				vehiclesMoveInf[ i ].transf = v[ i ].transform;
				vehiclesMoveInf[ i ].VenicleMoveList = v[ i ].VenicleMoveList;
			}
		}

		if (vehiclesMoveInf != null) 
		{
			for (int i = 0; i < vehiclesMoveInf.Length; i++) 
			{
				if( vehiclesMoveInf[ i ].transf != null )
				{
					vehiclesMoveInf [i].name = vehiclesMoveInf [i].transf.position.x +"";
				}
			}
		}
	}

#endif
}
