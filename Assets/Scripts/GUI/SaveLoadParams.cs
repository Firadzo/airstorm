using System;
using UnityEngine;

public class SaveLoadParams : MonoBehaviour {
    //New game and start params, continue params
    private const string playerDiffucylty = "playerDiff";
    public static int plDiff = -1;
    private static int plDiffDefaultValue = 0;
	public	static int	selectedDifficulti = 0;//значение этой переменной передается в игру только при старте новой игры.

    private const string playerSelectedPlane = "playerPlane";
    public static int plPlane = -1;
    private static int plPlaneDefaultValue = 0;
	public	static int	selectedPlane = 0;//значение этой переменной передается в игру только при старте новой игры.

    private const string completedLevels = "playerCompletedLevels";
    public static int plDoneLevels = -1;
    private static int plDoneDefaultValue = -1;

    //Options area
    private const string playerBombAutoFire = "bombAutoFire";
    public static int plBombAutoFire = -1;
    private static int plBombDefaultValue = 1;

    private const string playerConrolType = "playerConrolType";
    public static int plControl = -1;
    private static int plControlDefaultValue = 1; //1-touch, 0- motion

	private const string muteKey = "mute";
	public static bool mute = false;
	private static int muteDefValue = 0;

    private const string musicLevel = "musicVolume";
    public static float plMusicLevel = -1.0f;
    private static float plMusicLevelDefaultValue = 1f;

    private const string soundLevel = "soundVolume";
    public static float plSoundLevel = -1.0f;
    private static float plSoundLevelDefaultValue = 1f;

    private const string vibration = "vibration";
    public static int plVibration = -1;
    private static int plVibrationDefaultValue = 1;

    private const string money = "money";
    public static int 	plMoney = 0;
    private static int 	plMoneyDefaultValue = 0;//дается при первом входе

	private const string realMoneyKey = "realMoney";
	public static int 	realMoney = 0;
	private static int 	realMoneyDefaultValue = 0;//игровые деньги купленные за реал

	private const string moneySpentKey = "realMoneySpent";
	public static int 	realMoneySpent = 0;
	private static int 	spentMoneyDefaultValue = 0;//потраченные реальные деньги


	private const 	string 	playerLivesKey = "playerLives";
	public static 	int 	plLivesNumber = 0;
	private static 	int 	plLivesDefaultValue = 2;

	private const 	string 	firstLevelStartKey = "firstStart";
	public static 	int 	firstLevelStart = 0;
	private static 	int 	firstLevelStartDefValue = 0;

	private const 	string 	adsKey = "adsEnabled";
	private static 	int 	adsEnabled = 1;
	private static 	int 	adsDefaultValue = 1;

	public static 	int 	currLevel = 0;

    public static Matrix4x4 calibrationMatrix;


	void Awake()
	{
		//PlayerPrefs.DeleteAll();
		LoadParams();
	}

	public static void RemoveAds()
	{
		adsEnabled = 0;
		PlayerPrefs.SetInt( adsKey, adsEnabled );
	}

	public static bool AdsEnabled
	{
		get{ return adsEnabled == 1; }
	}

	public static void SetNewGameParams()
	{
		plLivesNumber = plLivesDefaultValue;
		plMoney = plMoneyDefaultValue + realMoney;
		plDoneLevels  = 0;
		SaveLoadParams.plPlane = selectedPlane;
		SaveLoadParams.plDiff = selectedDifficulti;
		SetFirstLevelStart( 1 );
		Save();
	}

	public static void SetDefaulLivesNumber()
	{
		plLivesNumber = plLivesDefaultValue;
		SaveLives();
	}

	public static void SaveLives()
	{
		PlayerPrefs.SetInt( playerLivesKey, plLivesNumber );
	}

	public static float GetMusicLevel()
	{
		mute = PlayerPrefs.GetInt( muteKey, muteDefValue ) == 1;
		AudioListener.volume = mute ? 0 : 1;
		plMusicLevel = PlayerPrefs.GetFloat( musicLevel,plMusicLevelDefaultValue );
		return plMusicLevel;
	}

    public static void LoadParams(){
        currentLevel();
        //LOAD Params
        //diff
		plDiff = PlayerPrefs.GetInt(playerDiffucylty, plDiffDefaultValue );
		//lives
		plLivesNumber = PlayerPrefs.GetInt( playerLivesKey, plLivesDefaultValue );
		//selplane
		plPlane = PlayerPrefs.GetInt(playerSelectedPlane, plPlaneDefaultValue );
        //complete lev
		plDoneLevels = PlayerPrefs.GetInt( completedLevels, plDoneDefaultValue );
        //auto Fire
		plBombAutoFire = PlayerPrefs.GetInt(playerBombAutoFire, plBombDefaultValue );
        //pl conrol
		plControl = PlayerPrefs.GetInt(playerConrolType, plControlDefaultValue );
       // Debug.Log("PL control LOAD " + plControl);
        //music
		plMusicLevel = PlayerPrefs.GetFloat( musicLevel,plMusicLevelDefaultValue );
        //sound
		plSoundLevel = PlayerPrefs.GetFloat(soundLevel, plSoundLevelDefaultValue );
        //vibration
		plVibration = PlayerPrefs.GetInt( vibration, plVibrationDefaultValue );
        //money
		plMoney = PlayerPrefs.GetInt(money, plMoneyDefaultValue );
		realMoney = PlayerPrefs.GetInt( realMoneyKey, realMoneyDefaultValue );
		realMoneySpent = PlayerPrefs.GetInt( moneySpentKey, spentMoneyDefaultValue );

		adsEnabled = PlayerPrefs.GetInt( adsKey, adsDefaultValue );
        //money on start level

		mute = PlayerPrefs.GetInt( muteKey, muteDefValue ) == 1;
		AudioListener.volume = mute ? 0 : 1;

		firstLevelStart = PlayerPrefs.GetInt( firstLevelStartKey, firstLevelStartDefValue );
        Debug.Log("Options loaded");
    }

	public static void SetFirstLevelStart( int value )
	{
		firstLevelStart = value;
		PlayerPrefs.SetInt( firstLevelStartKey, value );
	}

    public static void Save() {
        PlayerPrefs.SetInt(playerDiffucylty, plDiff);
        PlayerPrefs.SetInt(playerSelectedPlane, plPlane);
        PlayerPrefs.SetInt(completedLevels, plDoneLevels);
        PlayerPrefs.SetInt(playerBombAutoFire, plBombAutoFire);
        PlayerPrefs.SetInt(playerConrolType, plControl);
        PlayerPrefs.SetFloat(musicLevel, plMusicLevel);
        PlayerPrefs.SetFloat(soundLevel, plSoundLevel);
		PlayerPrefs.SetInt( muteKey, mute ? 1 : 0 );
        PlayerPrefs.SetInt(vibration, plVibration);
        PlayerPrefs.SetInt( money, plMoney );
		PlayerPrefs.SetInt( realMoneyKey, realMoney );
		PlayerPrefs.SetInt( moneySpentKey, realMoneySpent );
		SaveLives();
        Debug.Log("ALL saved");
    }

    public static void SaveOptions() {
        PlayerPrefs.SetInt(playerBombAutoFire, plBombAutoFire);
        PlayerPrefs.SetInt(playerConrolType, plControl);
        Debug.Log("SAVE CONROL " + plControl);
        PlayerPrefs.SetFloat(musicLevel, plMusicLevel);
        PlayerPrefs.SetFloat(soundLevel, plSoundLevel);
		PlayerPrefs.SetInt( muteKey, mute ? 1 : 0 );
        PlayerPrefs.SetInt(vibration, plVibration);
        Debug.Log("OPTIONS saved");
    }

    public static void SaveMoney() {
        PlayerPrefs.SetInt( money, plMoney );
		PlayerPrefs.SetInt( realMoneyKey, realMoney );
    }

    public static void currentLevel() {
        string name = LoadSceneFromBundle.currentScene;
        string[] currLevelName = name.Split('_');
        if (currLevelName.Length < 2) {
            Debug.Log("It's not game level");
        } else {
            currLevel = Convert.ToInt32(currLevelName[1]);
        }
    }

#if UNITY_EDITOR

	public bool clearPrefs = false;

	void OnDrawGizmosSelected()
	{
		if( clearPrefs )
		{
			clearPrefs  = false;
			PlayerPrefs.DeleteAll();
		}
	}
#endif
}
