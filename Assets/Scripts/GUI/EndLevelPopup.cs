using AnimationOrTween;
using UnityEngine;
using System.Collections;

public class EndLevelPopup : MonoBehaviour {
	
	public 	MyUIDialog	exitToMenuDialog;
	public 	MyUIDialog	noTapjoyConnectionDialog;
	public	MyUIDialog	noVideoAvaliableDialog;
	public 	MyUIDialog	liveForVideoDialog;
	public 	MyUIDialog	waitingVideoDialog;
	public GameObject 	LoadingScreen;
    public GameObject 	EndLevelScreen;
	public TweenPosition LooseAllLifesScreen;
    public GameObject 	SkillsPopup;
	public TweenPosition SettingsScreen;
    public GameObject 	PauseScreen;
    public GameObject 	inGameMenu;
	public TweenPosition storeMenu;

    public GameObject[] LevelNumbers;
    public UISprite[] EndScreenStars;

    const string mainMenuSceneName = "MainMenu";

    public int allEnemyCnt = 1;
    public int deadEnemyCnt = 1;

    public UILabel GetCoins;

    public int currentLevel = 0;
    public int levelsAllCount = 5;

    public UILabel DestroyPersents;

    public GameObject MainIcons;
	
    private bool 				coll = false;
    private TweenPositionAndLoadLevel loadLevel;
    private bool 				showEndScreen = false;
    private PlayerSkills 		skills;
    private AudioManager 		music;
	private SkillsPageSwitch	skillsPageSwitch;
    private FingerGestures 		_fingerGestures;

	public delegate void 		OnEscapeKey();
	private 	static OnEscapeKey	onEscape;
	private static OnEscapeKey	saved;
	public static EndLevelPopup	instance;

	//ждем видео заданное время, пытаемься показать видео по истечению заданных интервалов времени
	private float	currentCheckInterval;
	private float	videoEndTime;
	private bool 	checkForVideo;
	const float		TIME_TO_WAIT_FOR_VIDEO = 2f;
	const float		VIDEO_CHECK_INTERVAL = 0.25f;
	const int		MAX_LIVES_FOR_VIDEO = 2;
	private int		curLivesForVideo;

	public static void ReplaceOnEscapeKey( OnEscapeKey replace )
	{
		saved = onEscape;
		onEscape = replace;
	}

	public static void ReturnOnEscapeKey()
	{
		onEscape = saved;
	}

	public static bool CanShowDialog()
	{
		if( instance == null )
		{
			return true;
		}
		return ( Time.timeScale == 0 && SpawnPlayer.ComixDestroyed && !instance.LoadingScreen.activeSelf ); 
	}

	void Awake()
	{
		instance = this;
		curLivesForVideo = 0;
		onEscape = showPause;
	}

    private void Start() {
        _fingerGestures = FindObjectOfType<FingerGestures>();

        showEndScreen = false;
        //for ex, we complete 1 level, current level - 2
        currentLevel = SaveLoadParams.currLevel;
        loadLevel = LoadingScreen.GetComponent<TweenPositionAndLoadLevel>();

        GameObject skill = GameObject.Find("Skills");
        if (skill) {
            skills = skill.GetComponent<PlayerSkills>();
        } else {
            Debug.Log("Skills obj not found!");
        }

        for (int i = 0; i < LevelNumbers.Length; i++) {
			LevelNumbers[i].SetActive( false );
        }

        for (int j = 0; j < EndScreenStars.Length; j++) {
            EndScreenStars[j].alpha = 0;
        }

		music = AudioManager.instance;
		skillsPageSwitch = SkillsPopup.GetComponent<SkillsPageSwitch>();
    }

    private void Update() {
		CheckForVideo ();
		if( TutorialController.instance.tutorialStarted )
		{
			return;
		}
        if ( Input.GetKeyDown(KeyCode.Escape) ) {
			if( onEscape != null )
			{
				onEscape();
			}
        }
    } 

#if !UNITY_EDITOR
	void OnApplicationFocus(bool focusStatus) {
//		Debug.Log( focusStatus+" FOCUS" );
		if( !focusStatus )
		{
			if( !showPauseScreen && Time.timeScale == 1 && SpawnPlayer.ComixDestroyed && !LoadingScreen.activeSelf )
			{
				showPause();
			}
		}
	}

	void OnApplicationPause( bool pauseStatus  )
	{
		//Debug.Log( pauseStatus );
		if( pauseStatus )
		{
			if( !showPauseScreen && Time.timeScale == 1 && SpawnPlayer.ComixDestroyed && !LoadingScreen.activeSelf )
			{
				showPause();
			}
		}
	}
#endif


    public void endLevel() {
        if (coll) 
		{
            return;
        }
        coll = true;

        PlayerScript.instance.setShootDisable();
		PlayerScript.instance.setMoveDisable();

        LevelConroller.spawnDisabled = true;

        showEndLevel();
    }

    private bool _isEndLevel;

	private void BackToEndLevelUI()
	{
		TweenPosition tween = SkillsPopup.GetComponent<TweenPosition>();
		tween.Play(false);
		music.PlayMenusMusicInstant();
		EndLevelScreen.gameObject.SetActive (true);
		onEscape = exitToMenuDialog.Open;
	}

    private void showEndLevel() {
        if ( EndLevelScreen && !showEndScreen ) 
		{
			onEscape = exitToMenuDialog.Open;
			AudioManager.PauseGameSounds();
            if (_fingerGestures != null) 
			{
                _fingerGestures.gameObject.SetActive(false);
            }
            Time.timeScale = 0;
            inGameMenu.SetActive(false);
            music.PlayMenusMusicInstant();

            //EndLevelScreen.SetActive(true);

            showEndScreen = true;
			//ActivateNewPanel( EndLevelScreen );
            TweenPosition tween = EndLevelScreen.GetComponent<TweenPosition>();
            tween.enabled = true;
			tween.Play( true );

            float persentCompete = ((float) deadEnemyCnt / (float) allEnemyCnt) * 100f;

            if (allEnemyCnt == 0) {
                DestroyPersents.text = "0%";
            } else {
                DestroyPersents.text = "" + persentCompete.ToString("##") + "%";
            }

			int getCoins = 300;
            if ( persentCompete <= 60f ) {
                EndScreenStars[0].alpha = 1;
            } 
			else if ( persentCompete > 60f && persentCompete <= 85f ) 
			{
                EndScreenStars[0].alpha = 1;
                EndScreenStars[1].alpha = 1;
				getCoins = 500;
            } 
			else if ( persentCompete > 85f ) 
			{
                EndScreenStars[0].alpha = 1;
                EndScreenStars[1].alpha = 1;
                EndScreenStars[2].alpha = 1;
				getCoins = 800;
            }


            if (GetCoins) {
                GetCoins.text = getCoins.ToString();
            }

            SaveLoadParams.plMoney += getCoins;
			PlayerScript.instance.playerMoney = SaveLoadParams.plMoney;
            //Save done levels, scores and other
            SaveLoadParams.plDoneLevels = currentLevel;
            SaveLoadParams.Save();

            PlayerPrefs.SetInt("replay", -1);

            _isEndLevel = true;
        }
    }

    private void showEndGame() {
        GameObject t = GameObject.Find("LevelConroller");
        if (t) {
            t.GetComponent<SpawnPlayer>().ShowComixes(true);
        }
        EndLevelScreen.SetActive(false);
    }

    public void showMenu() {
        //LoadingScreen.SetActive(true);
		//SaveLoadParams.SaveMoney();
		skills.ReturnSkills( PlayerScript.instance.playerLives == 0 );
        Time.timeScale = 1;

        loadLevel.LoadLevel( mainMenuSceneName );

		//ActivateNewPanel( LooseAllLifesScreen );
		//LooseAllLifesScreen.to = new Vector3(1500, 0, 0);
		LooseAllLifesScreen.enabled = true;
		LooseAllLifesScreen.Play( false );
    }

    public bool storeOpen = false;

    public void openStore() {
		if( TutorialController.instance.tutorialStarted )
		{
			return;
		}
        storeOpen = !storeOpen;
		if( storeOpen )
		{
			MyTapjoy.CheckEarning = true;
			onEscape = openStore;
		}
		else
		{
			onEscape = closeSkillsPopup;
		}
		storeMenu.enabled = true;
		storeMenu.Play(storeOpen);
		storeMenu.gameObject.SetActive( true );

        GameObject obj = GameObject.Find("GoogleIAB");
        if (obj) {
            obj.GetComponent<GoogleIABEventListener>().Reset();
        }

		if( currentLevel == 5  && _isEndLevel )
		{
			return;
		}

        SkillsPopup.SetActive(!storeOpen);
//        Debug.Log(storeOpen);
    }

	public void LoadLevel( int number )
	{
		SaveLoadParams.SetFirstLevelStart( 1 );
		loadLevel.LoadLevel( "level_" + ( number ) );
		LevelNumbers[currentLevel].SetActive( true );
		//ActiveLevel();
	}

    public void nextLevel() {
        if (currentLevel + 1 > levelsAllCount) {
            showEndGame();
            return;
        }

        //LoadingScreen.SetActive(true);
		SaveLoadParams.SetFirstLevelStart( 1 );
		loadLevel.LoadLevel( "level_" + (currentLevel + 1 ) );
      	LevelNumbers[currentLevel].SetActive( true );
       // ActiveLevel();
    }

	#region game over
	private void ShowFailure()
	{
		CleaerEvents();
		noVideoAvaliableDialog.Open();
	}

	private bool addLifeClicked = false;//для защиты от ложного срабатывания
	private void CleaerEvents()
	{
		waitingVideoDialog.Close ();
		TapjoyUnity.Tapjoy.OnVideoComplete -= AddLife;
		AdColony.OnVideoFinished -= AddLife;
		TapjoyUnity.Tapjoy.OnVideoStart -= StopCheckForVideo;
		AdColony.OnVideoStarted -= StopCheckForVideo;
	}

	private void CheckForVideo()
	{
	if (checkForVideo) {
		
			if( Time.realtimeSinceStartup <= videoEndTime  )
			{
				if( Time.realtimeSinceStartup >= currentCheckInterval )
				{
					currentCheckInterval += VIDEO_CHECK_INTERVAL;
					ShowVideo();
				}
			}
			else
			{
				ShowFailure();
				checkForVideo = false;
			}
		}
	}

	private void ShowVideo()
	{
		MyTapjoy.ShowAddLifeVideo ();
		if (checkForVideo) {
			if (AdColony.IsVideoAvailable (ADCAdManager.VIDEO_ID)) {
				AdColony.ShowVideoAd (ADCAdManager.VIDEO_ID);
			}
		}
		if (checkForVideo) {
			MyTapjoy.ShowUnityADVideo (AddLife);
		}
	}

	void StopCheckForVideo()
	{
		checkForVideo = false;
	}

	private void ShowAdditionLifeVideo()
	{
		if( addLifeClicked ){ return; }
		addLifeClicked = true;
		liveForVideoDialog.Close();
		waitingVideoDialog.Open ();
		checkForVideo = true;
		videoEndTime = Time.realtimeSinceStartup + TIME_TO_WAIT_FOR_VIDEO;
		currentCheckInterval = Time.realtimeSinceStartup + VIDEO_CHECK_INTERVAL;
		TapjoyUnity.Tapjoy.OnVideoComplete += AddLife;
		AdColony.OnVideoFinished += AddLife;
		TapjoyUnity.Tapjoy.OnVideoStart += StopCheckForVideo;
		AdColony.OnVideoStarted += StopCheckForVideo;
		ShowVideo ();
		//AddLife();
	}

	public static void CallAddLife()
	{
		instance.AddLife ();
	}

	private void AddLife( bool videoFinished )
	{
		AddLife();
	}

	private void AddLife()
	{
		checkForVideo = false;
		CleaerEvents();
		onEscape = showPause;
		curLivesForVideo++;
		LooseAllLifesScreen.enabled = true;
		LooseAllLifesScreen.Play( false );
		AudioManager.ResumeGameSounds();
		AudioManager.instance.PlaySavedLevelMusic();
		Time.timeScale = 1;
		inGameMenu.SetActive( true );
		PlayerScript.instance.AddLifeAndRespawn();
	}
	
    public void ShowGameOver() {
		addLifeClicked = false;
        //LooseAllLifesScreen.SetActive(true);
		//ActivateNewPanel( LooseAllLifesScreen );
		onEscape = exitToMenuDialog.Open;
		noTapjoyConnectionDialog.Close();
		noVideoAvaliableDialog.Close();
		waitingVideoDialog.Close ();
		if( curLivesForVideo <  MAX_LIVES_FOR_VIDEO )
		{
			liveForVideoDialog.Open();
		}
		else
		{
			liveForVideoDialog.Close();
		}
		LooseAllLifesScreen.enabled = true;
		LooseAllLifesScreen.Play( true );
		AudioManager.PauseGameSounds();
        music.PlayMenusMusicInstant();
        //if (_fingerGestures != null) _fingerGestures.gameObject.SetActive(false);
        Time.timeScale = 0;
        inGameMenu.SetActive(false);
    }
#endregion
    public void replayLevel() {
        //LoadingScreen.SetActive(true);

		//LooseAllLifesScreen.to = new Vector3(1500, 0, 0);
		LooseAllLifesScreen.enabled = true;
		LooseAllLifesScreen.Play( false );

       LevelNumbers[currentLevel - 1].SetActive( true );

        //Level replays
       // SaveLoadParams.saveReplayMoney();

		skills.ReturnSkills( PlayerScript.instance.playerLives == 0 );

        loadLevel.LoadLevel( "level_" + currentLevel );
        //PlayerPrefs.SetInt("replay", 1);
        //if (_fingerGestures != null) _fingerGestures.gameObject.SetActive(false);
        Time.timeScale = 1;
        //inGameMenu.SetActive(true);
    }

    public void showSkillsPopup() {
		if( TutorialController.instance.tutorialStarted )
		{
			TutorialController.instance.SelectSkill();
		}
		float chance = Random.Range( 0, 100f );
		MyTapjoy.CheckEarning = true;
		if( chance <= 40f )
		{
			/*if( chance <= 5f )
			{
				MyTapjoy.ShowAddLifeVideo( null, null );
			}
			else*/
			{
				MyTapjoy.ShowInterstitial();
			}
		}
		MyAdMob.ShowBunner();
		onEscape = closeSkillsPopup;
        //SkillsPopup.SetActive(true);
		if( !showPauseScreen )
		{
			AudioManager.PauseGameSounds();
			music.SaveInGameMusic();
		}
		music.PlayerUpgradesMusic();

        PlayerScript.instance.setShootDisable();
		PlayerScript.instance.setMoveDisable();

		skillsPageSwitch.SetActiveFirstPage();
        TweenPosition tween = SkillsPopup.GetComponent<TweenPosition>();
        tween.enabled = true;
        tween.Play(true);
        if (_fingerGestures != null) {
            _fingerGestures.gameObject.SetActive(false);
        }
        Time.timeScale = 0;
        inGameMenu.SetActive(false);
        if (showPauseScreen) {
            PauseScreen.SetActive(false);
        }
        if (_isEndLevel) {
            EndLevelScreen.SetActive(false);
        }
    }

    public void closeSkillsPopup() {
		if( TutorialController.instance.tutorialStarted )
		{
			if( TutorialController.instance.tutorialPhase != ETutorialPhase.back )
			{
				return;
			}
			TutorialController.instance.EndTutorial();
		}
		MyTapjoy.CheckEarning = false;
		SaveLoadParams.SaveMoney();
		if (_isEndLevel) {
			BackToEndLevelUI();
            return;
        }

        //SkillsPopup.SetActive(false);
        ActiveLevel();
    }

    private void ActiveLevel() {
        if (_fingerGestures != null) {
            _fingerGestures.gameObject.SetActive(true);
        }
        if (skills != null) {
            skills.updatePlayerParams();
        }
        inGameMenu.SetActive(true);
		TweenPosition tween = SkillsPopup.GetComponent<TweenPosition>();
		tween.Play(false);
        if (showPauseScreen) 
		{
			onEscape = closePause;
            PauseScreen.SetActive(true);
			music.PlayPauseMusic();
        }
		else
		{   
			MyAdMob.HideBunner();
			onEscape = showPause;
			music.PlaySavedLevelMusic();
			PlayerScript.instance.setMoveEnable();
			PlayerScript.instance.SetShootEnable();
			Time.timeScale = 1;
			AudioManager.ResumeGameSounds();
		}
    }

    public void showSettings() {
        //SettingsScreen.SetActive(true);
		//ActivateNewPanel( SettingsScreen );
		onEscape = closeSettings;
		MyAdMob.HideBunner();
		SettingsScreen.enabled = true;
		SettingsScreen.Play(true);
    }

    public void closeSettings() {
		MyAdMob.ShowBunner();
        //SettingsScreen.SetActive(false);
		SettingsScreen.Play(false);
        SaveLoadParams.SaveOptions();
		AudioManager.ApplySoundsVolume();
		onEscape = closePause;
        //GameObject.Find("Player").GetComponent<PlayerConrrol>().LoadConrolParams();
    }

    public bool showPauseScreen = false;
    //private float _prevTimeScale;
    public void showPause() 
	{
		MyAdMob.ShowBunner();
		if( Random.Range( 0, 100f ) <= 40f )
		{
			MyTapjoy.ShowInterstitial();
		}
        //_prevTimeScale = Time.timeScale;
        //Time.timeScale = 0;
		AudioManager.PauseGameSounds();
		onEscape = closePause;
        //PauseScreen.SetActive(true);
        showPauseScreen = true;
        TweenPosition tween = PauseScreen.GetComponent<TweenPosition>();
        tween.enabled = true;
        tween.Play(true);
		PlayerScript.instance.setShootDisable();
		PlayerScript.instance.setMoveDisable();
		music.SaveInGameMusic();
        music.PlayPauseMusic();
        if (_fingerGestures != null) {
            _fingerGestures.gameObject.SetActive(false);
        }
        Time.timeScale = 0;
        inGameMenu.SetActive(false);
    }

    public void closePause() {
		MyAdMob.HideBunner();
		AudioManager.ResumeGameSounds();
		onEscape = showPause;
        showPauseScreen = false;
        TweenPosition tween = PauseScreen.GetComponent<TweenPosition>();
        tween.Play(false);
		PlayerScript.instance.setMoveEnable();
		PlayerScript.instance.SetShootEnable();

        music.PlaySavedLevelMusic();
        if (_fingerGestures != null) {
            _fingerGestures.gameObject.SetActive(true);
        }
        if (skills != null) {
            skills.updatePlayerParams();
        }
        Time.timeScale = 1;
        inGameMenu.SetActive(true);
        //PauseScreen.SetActive(false);

        //Time.timeScale = _prevTimeScale;
    }

    public void exitWithApplication() {
        Application.Quit();
#if UNITY_EDITOR
		Debug.Log("ExitGame");
#endif
    }
}
