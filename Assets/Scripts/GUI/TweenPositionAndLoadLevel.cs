﻿using UnityEngine;
using System.Collections;
using System;

[AddComponentMenu("NGUI/Tween/TweenPositionAndLoadLevel")]
public class TweenPositionAndLoadLevel : UITweener
{
	public Vector3 from;
	public Vector3 to;
	private string levelName = "";
	
	Transform mTrans;

	public Transform cachedTransform { get { if (mTrans == null) mTrans = transform; return mTrans; } }
	public Vector3 position { get { return cachedTransform.localPosition; } set { cachedTransform.localPosition = value; } }
	
	bool stateLoad = false;

	public void LoadLevel( string name )
	{
		levelName = name;
		enabled = true;
		Play ( true );
	}

	override protected void OnUpdate (float factor, bool isFinished) 
	{ 
		cachedTransform.localPosition = from * (1f - factor) + to * factor; 
		
		if(cachedTransform.localPosition == to && !stateLoad)
		{
			stateLoad = true;
			LoadSceneFromBundle.LoadScene( levelName );
           // AsyncOperation async = Application.LoadLevelAsync( levelName );
            //async.allowSceneActivation = true;
           // StartCoroutine(LoadLevelProgress(async));

		}
	}

    IEnumerator LoadLevelProgress(AsyncOperation async)
    {
        while (!async.isDone)
        {
            yield return null;
        }
    }

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public TweenPosition Begin (GameObject go, float duration, Vector3 pos)
	{
		TweenPosition comp = UITweener.Begin<TweenPosition>(go, duration);
		comp.from = comp.value;
		comp.to = pos;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
}