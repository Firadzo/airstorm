using UnityEngine;
using System.Collections;
[System.Serializable]
public class PlaneIconData
{
	public GameObject 			render;
	public RotatePlaneInMenu 	planeRotation;
	public GameObject			planeImage;

	public void UnSelect()
	{
		planeRotation.StopRotate();
		render.SetActive( false );
		planeImage.SetActive( true );
	}

	public void Select()
	{
		planeRotation.StartRotate();
		render.SetActive( true );
		planeImage.SetActive( false );
	}
}

public class GUISelectActive : MonoBehaviour {
    //public RotatePlaneInMenu Plane;
	public	PlaneIconData		planeIcon;
	//public 	RotatePlaneInMenu 	plane;
    public 	int writeValue;

   // public bool isPlane = false;
    public bool isMig = false;
    public MainMenuConroller menuConroller;

	static PlaneIconData	curSelection;

	void Awake()
	{
		curSelection = null;
	}

    /*void Start() {
        for (var i = 0; i < targets.Length; i++)
        {
            TweenAlpha tween2 = targets[i].GetComponent<TweenAlpha>();
            if (tween2) {
                tween2.enabled = false;
            }

            UISprite spr = targets[i].GetComponent<UISprite>();
            if (spr && spr.enabled) spr.enabled = false;
        }
    }*/

    void OnClick() 
    {
		//Debug.Log("click");
        if (isMig) {
            int check = PlayerPrefs.GetInt("migOpen", -1);
            if (check < 0)
			{
                menuConroller.openStore();
				return;
            }

        }

		if( curSelection != null )
		{
			curSelection.UnSelect(); 
		}
		curSelection = planeIcon;
		curSelection.Select();
		SaveLoadParams.selectedPlane = writeValue;
       /*Select diffulcty
        if (isPlane == false) {
            for (var i = 0; i < targets.Length; i++) {
                //UIImageButtonState target = targets[i].GetComponent<UIImageButtonState>();
               // target.setNormal();
                TweenAlpha tween2 = targets[i].GetComponent<TweenAlpha>();
                if(tween2)tween2.enabled = false;

                UISprite spr = targets[i].GetComponent<UISprite>();
                spr.enabled = false;
            }

            //UIImageButtonState currBtn = this.gameObject.GetComponent<UIImageButtonState>();
            //currBtn.OnPress();

            TweenAlpha tween = this.gameObject.GetComponent<TweenAlpha>();
            if(tween)tween.enabled = true;

            UISprite spr2 = this.gameObject.GetComponent<UISprite>();
            spr2.enabled = true;

        } else {*/
            //select Plane
            /*for ( int i = 0; i < targets.Length; i++ )
            {
               GameObject testTarget = GameObject.Find(targets[i].name + "_" + "Render");
				if(testTarget)testTarget.transform.GetChild(0).gameObject.SetActive(false);

                GameObject testImage = GameObject.Find(targets[i].name + "_" + "Image");
                if (testImage) testImage.transform.GetChild(0).gameObject.SetActive(true);
            }*/

          /*  GameObject render = GameObject.Find(this.gameObject.name + "_" + "Render");
			if (render) render.transform.GetChild(0).gameObject.SetActive(true);
            if(Plane)Plane.setToStart();

            GameObject image = GameObject.Find(this.gameObject.name + "_" + "Image");
			if (image) image.transform.GetChild(0).gameObject.SetActive(false);
			*/
    }
    
}
