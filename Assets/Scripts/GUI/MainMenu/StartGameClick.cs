using UnityEngine;
using System.Collections;

public class StartGameClick : MonoBehaviour {

    public GameObject[] planes;

    private void OnClick() 
    {
        for (var i = 0; i < planes.Length; i++)
        {
            GameObject Target = GameObject.Find(planes[i].name + "_" + "Render");
            if (Target) Target.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
