using UnityEngine;
using System.Collections;

public class LoadingLevelSelect : MonoBehaviour {

    public GameObject LoadingScreen;
    public GameObject[] LevelNumbers;

    public string LevelScenesKey = "level";

    public int levelsAllCount = 5;
    public int CompletedLevels = 0;

    private TweenPositionAndLoadLevel loadLevel;

    // Use this for initialization
    private void Start()
    {
        CompletedLevels = SaveLoadParams.plDoneLevels;
        loadLevel = LoadingScreen.GetComponent<TweenPositionAndLoadLevel>();
        for (int i = 0; i < LevelNumbers.Length; i++) {
            UISprite spr = LevelNumbers[i].GetComponent<UISprite>();
            spr.alpha = 0;
        }
    }

	public void newGame() 
    {
        loadLevel.LoadLevel( LevelScenesKey + "_1" );
	    UISprite spr = LevelNumbers[0].GetComponent<UISprite>();
	    spr.alpha = 1;

	    SaveLoadParams.plDoneLevels = 0;
        SaveLoadParams.Save();
	}

    public void continueGame() 
    {
        if(CompletedLevels <= 0) return;
		loadLevel.LoadLevel( LevelScenesKey + "_" + (CompletedLevels+1) );
        UISprite spr = LevelNumbers[CompletedLevels].GetComponent<UISprite>();
        spr.alpha = 1;
    }
	
	public void exitGame()
	{
		Application.Quit();	
	}
}
