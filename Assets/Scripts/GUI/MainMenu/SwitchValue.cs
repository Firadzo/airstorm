using UnityEngine;
using System.Collections;

public class SwitchValue : MonoBehaviour {	
	public bool button1Value = false;
	
	public GameObject Button1Active;
    public GameObject Button2Active;

	void Start()
	{
		if(SaveLoadParams.plControl == 0)
		{
			button1Value = true;
            if(Button1Active)Button1Active.SetActive(true);
            if(Button2Active)Button2Active.SetActive(false);
		}
		else
		{
			button1Value = false;
            Button1Active.SetActive(false);
            Button2Active.SetActive(true);
		}
	
	}
	
	void OnClick() {
		//Debug.Log("CLICK");
			if(button1Value)
			{
				button1Value = false;
	            Button1Active.SetActive(false);
	            Button2Active.SetActive(true);
				SaveLoadParams.plControl = 1;
			}
			else
			{
				button1Value = true;
	            Button1Active.SetActive(true);
	            Button2Active.SetActive(false);
				SaveLoadParams.plControl = 0;
			}
		//Debug.Log( SaveLoadParams.plControl );
	}
	
}
