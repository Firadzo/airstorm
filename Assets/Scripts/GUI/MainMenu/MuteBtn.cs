using UnityEngine;
using System.Collections;

public class MuteBtn : MonoBehaviour {
    public UISlider soundLevel;
    public UISlider musicLevel;

    public string onSpriteName;
    public string offSpriteName;
    private bool muted = false;

    private UISprite sprite;

	// Use this for initialization
	void Start () 
    {
        sprite = GetComponent<UISprite>();
	    if( SaveLoadParams.mute ) 
		{
			OffBtn();
	    } 
		else {
			OnBtn();
	    }
	}

	void Update()
	{
		if(  muted != SaveLoadParams.mute )
		{
			if( SaveLoadParams.mute )
			{
				OffBtn();
			}
			else
			{
				OnBtn();
			}
		}
	}

    public void OnBtn() {
        if (sprite) sprite.spriteName = onSpriteName;
        muted = false;
    }

    public void OffBtn() {
        if (sprite) sprite.spriteName = offSpriteName;
        muted = true;
    }

    private void OnClick()
    {
        if (muted) {

			OnBtn();
			SaveLoadParams.mute = false;
			AudioListener.volume = 1;
            SaveLoadParams.SaveOptions();
        } else {

			OffBtn();
			SaveLoadParams.mute = true;
			AudioListener.volume = 0;
			SaveLoadParams.SaveOptions();
        }
    }
}
