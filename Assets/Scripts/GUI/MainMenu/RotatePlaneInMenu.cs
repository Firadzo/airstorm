using UnityEngine;
using System.Collections;

public class RotatePlaneInMenu : MonoBehaviour {
	public float rotateSpeed = 0;
	public Vector3 angle = Vector3.zero;
    public Vector3 startPos;
    public Quaternion startRot;

	private bool rotate = false;
	// Use this for initialization
	void Start () {
	    startPos = this.transform.position;
	    startRot = this.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		if( rotate )
		{
        	transform.Rotate(angle * rotateSpeed * Time.deltaTime, Space.Self);
		}
	}

	public void StartRotate()
	{
		rotate = true;
	}

	public void StopRotate()
	{
		setToStart();
		rotate = false;
	}

    public void setToStart() {
        this.transform.position = startPos;
        this.transform.rotation = startRot;
    }
}
