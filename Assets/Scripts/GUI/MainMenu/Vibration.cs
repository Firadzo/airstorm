﻿using UnityEngine;
using System.Collections;

public class Vibration : MonoBehaviour {

    public string onSprName;
    public string offSprName;

    public UISprite onLed;
    public UISprite offLed;

    public bool button1Value = false;

    public GameObject Button1;

    UISprite sprite1;

    void Start()
    {
        sprite1 = Button1.GetComponent<UISprite>();

        if (SaveLoadParams.plVibration == 1)
        {
            button1Value = true;
            sprite1.spriteName = onSprName;
            onLed.alpha = 1;
            offLed.alpha = 0;
        }
        else
        {
            button1Value = false;
            sprite1.spriteName = offSprName;
            onLed.alpha = 0;
            offLed.alpha = 1;
        }
    }

    void OnClick()
    {
        if (button1Value)
        {
            button1Value = false;
            sprite1.spriteName = offSprName;
            SaveLoadParams.plVibration = 0;
            onLed.alpha = 0;
            offLed.alpha = 1;
        }
        else
        {
            button1Value = true;
            sprite1.spriteName = onSprName;
            onLed.alpha = 1;
            offLed.alpha = 0;
            SaveLoadParams.plVibration = 1;
        }
        Debug.Log("Vibr " + SaveLoadParams.plVibration);
    }
}
