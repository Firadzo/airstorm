using System;
using UnityEngine;
using System.Collections;

public class MainMenuConroller : MonoBehaviour {

	public	GameObject	mainMenuMusic;
    public 	GameObject SettingsScreen;
    private bool settingsOpen = false;

    public GameObject SelectPlaneScreen;
    public bool selectPlaneOpen = false;

    public GameObject LoadingScreen;

    public GameObject storeMenu;
    private bool storeOpen = false;

    public GameObject[] LevelNumbers;

    const string LEVEL_SCENE_KEY = "level_";

    public int levelsAllCount = 5;
    public int CompletedLevels = 0;

    private TweenPositionAndLoadLevel loadLevel;
	
    public 	GameObject 		buyMigLabel;
	public	UISprite		continueBtn;
	public	PlayerSkills	skills;
 

	void Awake()
	{
		if( MainMenuMusic.isNull )
		{
			Instantiate( mainMenuMusic );
		}
	}

    private void Start()
    {
        CompletedLevels = SaveLoadParams.plDoneLevels;
		continueBtn.alpha = SaveLoadParams.plDoneLevels < 0 ? 0.5f : 1f;
        loadLevel = LoadingScreen.GetComponent<TweenPositionAndLoadLevel>();
        for (int i = 0; i < LevelNumbers.Length; i++) {
			LevelNumbers[ i ].SetActive( false );
        }
        int check = PlayerPrefs.GetInt("Localization", -1);
        if (check < 0) {
            setSelectLocalized("0");
        } else {
            for (int i = 0; i < flags.Length; i++) {
                flags[i].GetComponentInChildren<UISprite>().alpha = 0.4f;
            }

            if (flags[check]) {
                flags[check].GetComponentInChildren<UISprite>().alpha = 1.0f;
            }
        }
        CheckMigBuyed();
		
		if( UnityEngine.Random.Range( 0, 100f )  <= 40f )
		{
			MyTapjoy.ShowInterstitial();
		}
    }

    public void CheckMigBuyed() {
        int check = PlayerPrefs.GetInt("migOpen", -1);
		buyMigLabel.SetActive( check < 0 );
    }

    public void openStore() {
        storeOpen = (!storeOpen);
		MyTapjoy.CheckEarning = storeOpen;
        TweenPosition tween = storeMenu.GetComponent<TweenPosition>();
        tween.enabled = true;
        tween.Play(storeOpen);
        openSelectPlane();
    } 
    
    public void openSettings() {
        settingsOpen = (!settingsOpen);
        TweenPosition tween = SettingsScreen.GetComponent<TweenPosition>();
        tween.enabled = true;
        tween.Play(settingsOpen);
        if (!settingsOpen) {
            SaveLoadParams.SaveOptions();
        }
		ShowBanner( !settingsOpen );
    } 
    
	private void ShowBanner( bool show )
	{
		if( show )
		{
			MyAdMob.ShowBunner();
		}
		else
		{
			MyAdMob.HideBunner();
		}
	}

	private void ShowOfferWall()
	{
		TapjoyUnity.Tapjoy.ShowOffers();
	}

    public void openSelectPlane() {
        selectPlaneOpen = (!selectPlaneOpen);
        TweenPosition tween = SelectPlaneScreen.GetComponent<TweenPosition>();
        tween.enabled = true;
        tween.Play(selectPlaneOpen);
		ShowBanner( !selectPlaneOpen );
    }

	public void newGame() 
	{
        loadLevel.LoadLevel( LEVEL_SCENE_KEY + "1" );
	    LevelNumbers[0].SetActive( true );
		skills.ClearSkills();
	    SaveLoadParams.SetNewGameParams();
	}

    public void continueGame() 
    {
        if( SaveLoadParams.plDoneLevels < 0 ) return;
		loadLevel.LoadLevel( LEVEL_SCENE_KEY + ( CompletedLevels >= 5? 5 : CompletedLevels+1 ) );
       	LevelNumbers[CompletedLevels].SetActive( true );
    }
	
	public void exitGame()
	{
        Debug.Log("exitGame");
        Application.Quit();	
	}

    //Debug Area
    public void LoadLevel(string num) {
		LoadLevel( Int32.Parse(num) );
    }

	public void LoadLevel( int lvl ) {
		loadLevel.LoadLevel( LEVEL_SCENE_KEY + lvl );
		LevelNumbers[( lvl - 1 )].SetActive( true );
	}

    public GameObject[] flags;
  //  private int currSelect = 0;

    private void setSelectLocalized( string num )
    {
        if (flags.Length <= 0)
        {
            return;
        }
        for (int i = 0; i < flags.Length; i++)
        {
            flags[i].GetComponentInChildren<UISprite>().alpha = 0.4f;
        }

        int num1 = Convert.ToInt32(num);

        if (flags[num1])
        {
            flags[num1].GetComponentInChildren<UISprite>().alpha = 1.0f;
        }

        PlayerPrefs.SetInt("Localization", num1);
		Localization.instance.currentLanguage = num1 == 0 ? "English" : "Russian";
    }

//#if UNITY_EDITOR

	void ClearPrefs()
	{
		PlayerPrefs.DeleteAll();
	}
//#endif

}
