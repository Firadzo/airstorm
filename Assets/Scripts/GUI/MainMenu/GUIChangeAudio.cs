using UnityEngine;
using System.Collections;

public class GUIChangeAudio : MonoBehaviour {
    public MuteBtn muteBtn;

	public bool isMusic = false;

	
	private UISlider _slider;
 
    void Start ()
    {
		_slider = gameObject.GetComponent<UISlider>();
        EventDelegate.Add(_slider.onChange, OnValueChange);
		//_slider.onValueChange += OnValueChange; 
		
		//music slider
		if(isMusic)
		{ 		 		
			_slider.value = SaveLoadParams.plMusicLevel;
			//AudioListener.volume = _slider.value;
		}
		else //sound in game slider
		{
			_slider.value = SaveLoadParams.plSoundLevel;
		}
		
    }
 
    void OnValueChange() {
        if (_slider.value > 0 && muteBtn)
        {
            muteBtn.OnBtn();
        } else if(muteBtn) {
            muteBtn.OffBtn();
        }

		if(isMusic)
		{
            //AudioListener.volume = _slider.value;
            SaveLoadParams.plMusicLevel = _slider.value;
			AudioManager.SetVolume( SaveLoadParams.plMusicLevel );
		}
		else
		{
            SaveLoadParams.plSoundLevel = _slider.value;
		}
    }
}
