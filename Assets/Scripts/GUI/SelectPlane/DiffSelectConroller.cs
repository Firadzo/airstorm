﻿using System;
using UnityEngine;
using System.Collections;

public class DiffSelectConroller : MonoBehaviour {

    public 	GameObject[] 	icons;
    public 	GameObject[] 	ruObj;
	public 	GameObject[] 	engObj;
	public 	GameObject		ruParent;
	public 	GameObject		engParent;

	private GameObject[] 	diffObj;
    public 	int 			currValue = 0;

	void Awake()
	{
		Localization.onLocalize += Localize;
	}

	void OnDestroy()
	{
		Localization.onLocalize -= Localize;
	}

    void Start() 
	{
		Localize( Localization.instance.currentLanguage );
        SelectDifficulty("1");    
    }

	void Localize( string language )
	{
		if( language ==  "Russian" )
		{
			ruParent.SetActive( true );
			engParent.SetActive( false );
			diffObj = ruObj;
		}
		else
		{
			engParent.SetActive( true );
			ruParent.SetActive( false );
			diffObj = engObj;
		}
		SelectDifficulty( currValue.ToString() );
	}

	private void SelectDifficulty( string value ) 
	{
        currValue = Convert.ToInt32(value);

        for ( int i = 0; i < diffObj.Length; i++ ) {
			TweenColor tween2 = diffObj[i].GetComponent<TweenColor>();
            if (tween2) tween2.enabled = false;

            UISprite spr = diffObj[i].GetComponent<UISprite>();
            spr.enabled = false;
            icons[i].GetComponent<UISprite>().enabled = false;

        }

		TweenColor tween = diffObj[currValue].GetComponent<TweenColor>();
        if (tween) tween.enabled = true;
        diffObj[currValue].GetComponent<UISprite>().enabled = true;
        icons[currValue].GetComponent<UISprite>().enabled = true;

        SaveLoadParams.selectedDifficulti = currValue;
    }
}
