// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class GUIInterface : MonoBehaviour {
//This script displays the health bar, and the score
public GUISkin mySkin; //The GUISkin we'll use, which holds info about the fonts and styles in the interface
public Texture2D HealthGreenTexture; //The texture of the green part of the health bar on the left
public string BriefingText; //The text to be displayed at the start of the level

private float HUDPosX = -70; //Used to animate the HUD popping from the left
private float MaxHealth = 0; //The maximum health value
public int Score = 0;
private GameObject Player; //The player's game object
private float BriefingPosX = Screen.width; //the position of the briefing text
	

public bool showFPS = false;
public float updateInterval = 0.5f;

private float accum = 0.0f; // FPS accumulated over the interval
private float frames = 0; // Frames drawn over the interval
private float timeleft; // Left time for current interval
	
void  Start (){
	Player = GameObject.Find("Player2"); //We set the player object
	if(Player)
	{
		PlayerScript control= Player.GetComponentInChildren<PlayerScript>();
		MaxHealth = control.Health; //Setting the maximum health value
	}
}

//Here we set an animate the GUI elements
void  OnGUI (){	
	if(showFPS)
	{
		timeleft -= Time.deltaTime;
	    accum += Time.timeScale/Time.deltaTime;
	    ++frames;
	   
		float fps = (accum/frames);
		GUI.Label ( new Rect( Screen.width * 0.7f - HUDPosX, 140, 240, 48), "FrameRate = " + fps.ToString("f2"));
	    // Interval ended - update GUI text and start new interval
	    if( timeleft <= 0.0f )
	    {   
			
	        timeleft = updateInterval;
	        accum = 0.0f;
	        frames = 0;
	    }
	}
		
		
	//Set the general GUI style we're going to use
	GUI.skin = mySkin;
	
	//At the start of the level, animate the entry of the GUI elements
	if ( Time.timeSinceLevelLoad < 1 )
	{
		HUDPosX -= (HUDPosX - 10)/16;
	}
	
	//Create a an empty box for the health bar base.
	GUI.Box ( new Rect( HUDPosX , 10, 300, 32), "", "HealthBarRed");
	
	//If the player object exists, do the following
	if ( Player )
	{
		//if the health bar green is within the bar limit, display it onscreen
		PlayerScript plControl= Player.GetComponentInChildren<PlayerScript>();
		
		if (plControl.Health > 0  )
		{
			//Create a box with the green health bar texture
			GUI.Box ( new Rect( HUDPosX + 4, 10, (plControl.Health/MaxHealth) * 300 - 8, 32), "", "HealthBarGreen");
		}
	}
	
	//Score 
	GUI.Label ( new Rect( Screen.width * 0.7f - HUDPosX, 5, 240, 48), Score.ToString(), "BarrageMeterGray" );
	
	//animate the interface entering teh screen
	if ( Time.timeSinceLevelLoad > 2 && Time.timeSinceLevelLoad < 5 )    
	{
		BriefingPosX -= BriefingPosX * 0.05f;
	}
	else if ( Time.timeSinceLevelLoad > 5 && Time.timeSinceLevelLoad < 8 )
	{
		BriefingPosX -= (BriefingPosX + Screen.width) * 0.05f;
	}	
	
	//Set a briefing label
	GUI.Label ( new Rect(Screen.width * 0.5f + BriefingPosX - 400,Screen.height * 0.5f,800,10), BriefingText);
}
}