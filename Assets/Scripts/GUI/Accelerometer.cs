﻿using UnityEngine;

public class Accelerometer : MonoBehaviour {
    public float 		speed = 10f;
    public float 		radius = 40f;
    public Vector3 		startPos;
	public Color		okColor;
	public UILabel		okLabel;
	private Transform	transf;

	public UISlider sensitivitySlider;
	public Vector2	sensitivityRange;


    private void Start() {
        startPos = transform.localPosition;
		transf = transform;
        LoadAcceleration();
		EventDelegate.Add(sensitivitySlider.onChange, OnValueChange);
	}
  
	void OnValueChange() {
		PlayerAcceleratorController.sensitivity  = Mathf.Lerp( sensitivityRange.x, sensitivityRange.y, sensitivitySlider.value );
		PlayerPrefs.SetFloat ("AccelSens", PlayerAcceleratorController.sensitivity );
	}

    void Update() {
		Vector3 result = startPos;
		if (SaveLoadParams.plControl != 0) {
			result.x = Mathf.Lerp (startPos.x - radius, startPos.x + radius, (PlayerAcceleratorController.accelerometerOffset.x + 1) / 2f);
			result.y = Mathf.Lerp (startPos.y - radius, startPos.y + radius, (PlayerAcceleratorController.accelerometerOffset.y + 1) / 2f);
		} else {

			result.x = Mathf.Lerp (startPos.x - radius, startPos.x + radius, (Input.acceleration.x + 1) / 2f);
			result.y = Mathf.Lerp (startPos.y - radius, startPos.y + radius, (Input.acceleration.y + 1) / 2f);
		}
		transf.localPosition = result;

    }
   
    void CalibrateAccelerometer () {
		PlayerAcceleratorController.accelerometerOffset = new Vector2( Input.acceleration.x, Input.acceleration.y );
		CustomConsole.DebugText ("Accelerometer offset change to "+ PlayerAcceleratorController.accelerometerOffset );
		SaveAcceleration();
    }

    void SaveAcceleration() {
		PlayerPrefs.SetFloat("Accelerometr_x", PlayerAcceleratorController.accelerometerOffset.x);
		PlayerPrefs.SetFloat("Accelerometr_y", PlayerAcceleratorController.accelerometerOffset.y);
    }

    private void LoadAcceleration() {
		PlayerAcceleratorController.accelerometerOffset.x = PlayerPrefs.GetFloat("Accelerometr_x", 0f);
		PlayerAcceleratorController.accelerometerOffset.y = PlayerPrefs.GetFloat("Accelerometr_y", 0f);
		PlayerAcceleratorController.sensitivity = PlayerPrefs.GetFloat ("AccelSens", 1f );
		sensitivitySlider.value = Mathf.InverseLerp ( sensitivityRange.x, sensitivityRange.y, PlayerAcceleratorController.sensitivity );
	}

}
