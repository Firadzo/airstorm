//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright � 2011-2012 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// Sample script showing how easy it is to implement a standard button that swaps sprites.
/// </summary>

[ExecuteInEditMode]

public class UIImageButtonState : MonoBehaviour
{
	public UISprite target;
	public string normalSprite;
	public string hoverSprite;
	public string pressedSprite;

	void OnEnable ()
	{
		if (target != null)
		{
			target.spriteName = UICamera.IsHighlighted(gameObject) ? hoverSprite : normalSprite;
		}
	}

	void Start ()
	{
		if (target == null) target = GetComponentInChildren<UISprite>();
	}

	public void OnPress()
	{
		if (enabled && target != null) 
        {
            target.spriteName = pressedSprite;
			target.MakePixelPerfect();
		}
	}

   public void setNormal() 
   {
       target.spriteName = normalSprite;
       target.MakePixelPerfect();
   }
}