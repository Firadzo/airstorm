﻿using UnityEngine;
using System.Collections;

public class MyUIDialog : MonoBehaviour {

	private bool opened;//для того что бы не возвращать делегат, если окно закрывается, но не открывалось ( было включено в эдиторе ) или уже было закрыто
	private static MyUIDialog 	nextDialog;
	private static MyUIDialog	current;
	private bool	escapeClose;

	public void Open(  )
	{
		Open ( true );
	}

	public void Open( bool closeOnEscape  )
	{
		escapeClose = closeOnEscape;
		if( current == null )
		{
			current = this;
		}
		else
		{
			if( gameObject.activeSelf ){ gameObject.SetActive( false ); }
			nextDialog = this;
			return;
		}
		if (closeOnEscape) {
			EndLevelPopup.ReplaceOnEscapeKey (Close);
		}
		gameObject.SetActive( true );
		opened = true;

	}

	public void Close()
	{
		current = null;
		if( nextDialog )
		{
			nextDialog.Open();
			nextDialog = null;
		}
		if( opened && escapeClose )
		{
			EndLevelPopup.ReturnOnEscapeKey();
		}
		gameObject.SetActive( false );
	}
}
