﻿using UnityEngine;
using System.Collections;

public class FPSCounter : MonoBehaviour {

	private UnityEngine.UI.Text 	label;
	private TextMesh	textMesh;
	public 	float		frequency;//частота обновления в секундах
	private float		timer;
	private int			frameCount = 0;
	private float 		accum   = 0; // FPS accumulated over the interval
	// Use this for initialization
	void Awake () {
		label = GetComponent<UnityEngine.UI.Text>();
		textMesh = GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		frameCount ++;
		accum += Time.timeScale/Time.deltaTime;
		if( timer >= frequency )
		{
			timer = 0;
			if( label != null )
			{
				label.text = Mathf.RoundToInt( accum / frameCount ) +"";
			}
			if( textMesh != null )
			{
				textMesh.text = Mathf.RoundToInt( accum/ frameCount ) +"";
			}
			frameCount = 0;
			accum = 0;
		}
	}
}
