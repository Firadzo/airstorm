﻿using UnityEngine;
using System.Collections;

public class comixController : MonoBehaviour {
    public bool itIsEndGame = false;
    public GameObject page1;
    public GameObject page2;
    private int curr_page = 0;

    public GameObject popupFuture;
    public GameObject popupBackToTime;
    public GameObject nextGameComing;
    private bool shownextGame = false;
    public GameObject creditsScreen;

	public bool isIpadResolution = false;

    private bool showFuture = false;
    private bool showBackToTime = false;
    private EndLevelPopup popup;

	const int PAGE_TIME = 10;

    void Start() 
    {
		UILabel[] texts = gameObject.GetComponentsInChildren<UILabel>();
		
		for(int i=0; i<texts.Length; i++)
		{
			texts[i].effectStyle = UILabel.Effect.Outline;
			texts[i].effectColor = new Color(69f/255f, 69f/255f, 69f/255f, 80f/255f);
			texts[i].effectDistance = new Vector2(0f, 0f);
		}

		if(!isIpadResolution)page2.SetActive(false);

        if(popupFuture)popupFuture.SetActive(false);
        if(popupBackToTime)popupBackToTime.SetActive(false);
        if(nextGameComing)nextGameComing.SetActive(false);
        if (creditsScreen)creditsScreen.SetActive(false);

        GameObject t = GameObject.Find("EndLevel");
        if (t) popup = t.GetComponent<EndLevelPopup>();

        GameObject music = GameObject.Find("AudioManager");
		music.SendMessage("PlayComixMusic", SendMessageOptions.DontRequireReceiver);

		Invoke("nextPage",PAGE_TIME);
    }

	void nextPage() {
        if (itIsEndGame) {
            if (GameObject.Find("SkipArea")) GameObject.Find("SkipArea").SetActive(false);
        }

		if(isIpadResolution)
		{
			SpawnPlayer pl = GameObject.Find("LevelConroller").GetComponent<SpawnPlayer>();
			pl.destroyComix();
			return;
		}

        if (curr_page != 0) {
            page2.SetActive(false);
            SpawnPlayer pl = GameObject.Find("LevelConroller").GetComponent<SpawnPlayer>();
            pl.destroyComix();
            return;
        }
	    page1.SetActive(false);
        page2.SetActive(true);
	    curr_page++;

		Invoke( "nextPage", PAGE_TIME );
	}

    void showChooseFuture()
    {
        if (popupFuture) {
            if (!showFuture) {
                showFuture = true;
                popupFuture.SetActive(true);
            } else {
                showFuture = false;
                popupFuture.SetActive(false);
            }
        }
    }

    void showChooseBackToTime()
    {
        if (popupBackToTime)
        {
            if (!showBackToTime)
            {
                showBackToTime = true;
                popupBackToTime.SetActive(true);
            }
            else
            {
                showBackToTime = false;
                popupBackToTime.SetActive(false);
            }
        }
    }

    void futureYes() {
        showChooseFuture();
        if (nextGameComing) 
        {
            if (!shownextGame)
            {
                shownextGame = true;
                nextGameComing.SetActive(true);
                StartCoroutine("showCreditsScreen");
            }
            else
            {
                shownextGame = false;
                nextGameComing.SetActive(false);
            }  
        }
        disableAll();
    }

    void backToTimeYes() {
        showChooseBackToTime();
        if ( nextGameComing )
        {
            if (!shownextGame)
            {
                shownextGame = true;
                nextGameComing.SetActive(true);
                StartCoroutine("showCreditsScreen");
            }
            else
            {
                shownextGame = false;
                nextGameComing.SetActive(false);
            }
        }
        disableAll();
    }

    IEnumerator showCreditsScreen()
    {
        yield return new WaitForSeconds(10);
        nextGameComing.SetActive(false);
        disableAll();
        showCredits();    
    }

    void disableAll() {
        page1.SetActive(false);
        page2.SetActive(false);
        if (GameObject.Find("BackComix")) GameObject.Find("BackComix").SetActive(false);
        if (GameObject.Find("Charter6")) GameObject.Find("Charter6").SetActive(false);
        if (GameObject.Find("TapToCont")) GameObject.Find("TapToCont").SetActive(false);
    }

    void showCredits() {
        StopCoroutine("showCreditsScreen");
		AudioManager.instance.PlayTitresMusic();
        creditsScreen.SetActive(true);
        nextGameComing.SetActive(false);
        /*for (int i = 0; i < creditsScreen.transform.childCount; i++) {
            if (creditsScreen.transform.GetChild(i).GetComponent<SplineAnimator>()) {
                creditsScreen.transform.GetChild(i).GetComponent<SplineAnimator>().moveEnablePause();
                Debug.Log(creditsScreen.transform.GetChild(i).name);
            }
        }*/
    }

    void showMainMenu() {
        popup.showMenu();
    }
}
