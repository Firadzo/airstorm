﻿using UnityEngine;
using System.Collections;

public class SkillsPageSwitch : MonoBehaviour {

    public 	GameObject 		page1;
    public 	GameObject 		page2;
	public	PlayerSkills	playerSkills;
    public 	bool 			page1Active = true;

    public UISprite page1Sprite;
    public UISprite page2Sprite;

    public UILabel moneyLabel;


	void OnEnable()
	{
		moneyLabel.text = SaveLoadParams.plMoney.ToString();
	}

	public void SetActiveFirstPage()
	{
		page1Active = false;
		setActivePage();
	}

    void setActivePage() 
	{
		moneyLabel.text = PlayerScript.instance.playerMoney.ToString();
		page1Active = !page1Active;
		page1.SetActive( page1Active );
		page2.SetActive( !page1Active );
        if (page1Active) 
		{
			if( !TutorialController.instance.tutorialStarted )
			{
				playerSkills.SelectCurrentGun();
			}
			page1Sprite.spriteName = "shooting_on";
			page2Sprite.spriteName = "speed_bomb_off";
        } 
		else 
		{
			playerSkills.SelectCurrentRocket();
			page1Sprite.spriteName = "shooting_of";
			page2Sprite.spriteName = "speed_bomb_on";
        }

    }

    /*void setActive(GameObject obj, bool active) 
    {
        UISprite[] spr = obj.GetComponentsInChildren<UISprite>();
        BoxCollider[] coll = obj.GetComponentsInChildren<BoxCollider>();
        UILabel[] labels = obj.GetComponentsInChildren<UILabel>();
        UITexture[] tex = obj.GetComponentsInChildren<UITexture>();

        for (int j = 0; j < coll.Length; j++)
        {
            coll[j].enabled = active;
        }

        for (int i = 0; i < spr.Length; i++) {
            spr[i].enabled = active;
        }

        for (int k = 0; k < labels.Length; k++) {
            labels[k].enabled = active;
        }

        for (int k = 0; k < tex.Length; k++)
        {
            tex[k].enabled = active;
        }
    }*/
}
