using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

//This class animates a gameobject along the spline at a specific speed.
public class SplineAnimator : MonoBehaviour
{
	public Spline spline;
	
	public float speed = 1f;
	public float offSet = 0f;
	public WrapMode wrapMode = WrapMode.Clamp;
	
	public float passedTime = 0f;
	public float angleRotation;
	
	//AutoRotate
	int angleTrigger = 0;
	int speedTrigger = 1;
	bool triggerRotate = false;
	bool right = false;
	
	RaycastHit hit;

	Vector3 rotateAroundPoint = Vector3.zero;
	Vector3 loopStartPos = Vector3.zero;
	float startAngle = 0;
	int checkLoopEndEnable = 3;//Skip frames ans enable check end loop

    public bool moveEnable = true;
    //private GameObject Player;

    public bool 			kamikadze = false;
	private bool 			useSplineData;
	private SplineDataCopy	splineData;
	public delegate	void 	OnTragectoryEnd ();
	public OnTragectoryEnd	onTrajectoryEnd;
	private Rigidbody		rigidBodyCached;

    private void Start() {
		rigidBodyCached = GetComponent<Rigidbody> ();
		//Player = PlayerScript.instance.gameObject;
		moveEnable = PlayerScript.instance.moveEnable;
		splineData = spline.GetComponent<SplineDataCopy> ();
		useSplineData = spline == null;
		if (useSplineData) {
			//Destroy( spline );
		}
		speed *= 1.25f;
    }

#if UNITY_EDITOR
	public bool emulateUpdate;
	public bool resetPosition;
	void OnDrawGizmosSelected()
	{
		if (emulateUpdate) {
			moveEnable = true;
			useSplineData = true;
		}
		if (resetPosition) {
			resetPosition = false;
			Reset();
		}
	}

#endif

	public void InitOnSpline()
	{
		if ( useSplineData ) 
		{
			posOnSplne = 0;
			splineData.CalculatePosAndRotation( posOnSplne );
			transform.position = splineData.calculatedPosition;
		}
		else 
		{
			posOnSplne = 0;
			transform.position = spline.GetPositionOnSpline(posOnSplne);
		}
	}

    void Update( )
    {
		#if UNITY_EDITOR
		if( emulateUpdate ){ return; }
		#endif
		if ( useSplineData ) 
		{
			UseSplineData();
		}
		else 
		{
			UseSpline();
		}
	}

	public void Reset()
	{
		passedTime = 0;
	}

	private void CheckRotateTrigger()
	{
		if(right)
		{
			angleTrigger -= (speedTrigger * 5);
			if( angleTrigger <= 0 )
			{
				triggerRotate = false;
				angleTrigger = 0;
				speedTrigger = 0;
				right = false;
			}
		}
		else
		{
			angleTrigger += (speedTrigger * 5);
			if ( angleTrigger >= 0 )
			{
				triggerRotate = false;
				angleTrigger = 0;
				speedTrigger = 0;
				right = false;
			}
		}
	}

	float posOnSplne;
	Vector3 pos;
	const float ROTATION_SPEED = 100f;
	private void UseSplineData()
	{
		//return;
		// moveEnable = Player.GetComponentInChildren<PlayerScript>().moveEnable;
		
		if ( !moveEnable ) return;
		
		passedTime += Time.deltaTime * speed;
		
		 posOnSplne = WrapValue( passedTime + offSet, 0f, 1f, wrapMode );

		splineData.CalculatePosAndRotation( posOnSplne );

		if( posOnSplne > 0.98f )
		{
			if( onTrajectoryEnd != null )
			{
				onTrajectoryEnd();
			}

			//Destroy(this.gameObject);
		}
		
		//if kamikadze - enable autopilot :-)
		if (kamikadze && posOnSplne > 0.98f && posOnSplne > 0)
		{
			gameObject.GetComponent<SplineAnimator>().enabled = false;
			gameObject.GetComponent<Kamikadze>().enabled = true;
			return;
		}
		
		pos = splineData.calculatedPosition;	

		Quaternion rot = splineData.calculatedRotation;
		Quaternion addHorizontalRot;
		if(triggerRotate)
		{	
			CheckRotateTrigger();
				
			addHorizontalRot = Quaternion.AngleAxis(angleTrigger, Vector3.forward);
			GetComponent<Rigidbody>().rotation = Quaternion.Slerp(GetComponent<Rigidbody>().rotation, (rot * addHorizontalRot), Time.deltaTime * 3f * speedTrigger);
		}
		else 
		{
				//Vector3 rotationDir = pos - transform.position;
			float angle = Vector3.Angle(pos - GetComponent<Rigidbody>().position, transform.forward);			
			addHorizontalRot = Quaternion.AngleAxis((-angle*3f), Vector3.forward);
			GetComponent<Rigidbody>().rotation = Quaternion.Slerp(GetComponent<Rigidbody>().rotation, (rot * addHorizontalRot), Time.deltaTime * 3f);
		}
			GetComponent<Rigidbody>().position = pos;
	}

	private void UseSpline()
	{
		//return;
		// moveEnable = Player.GetComponentInChildren<PlayerScript>().moveEnable;
		
		if (!spline || !moveEnable) return;
		
		passedTime += Time.deltaTime * speed;
		
		 posOnSplne = WrapValue( passedTime + offSet, 0f, 1f, wrapMode );
		
		if( posOnSplne > 0.98f)
		{
			if( onTrajectoryEnd != null )
			{
				onTrajectoryEnd();
			}
		}
		
		//if kamikadze - enable autopilot :-)
		if (kamikadze && posOnSplne > 0.98f && posOnSplne > 0)
		{
			enabled = false;
			gameObject.GetComponent<Kamikadze>().enabled = true;
			return;
		}
		
		pos =  spline.GetPositionOnSpline(posOnSplne);	
		
		//base rotation
		Quaternion rot = spline.GetOrientationOnSpline( WrapValue( passedTime + offSet, 0f, 1f, wrapMode ) );
		Quaternion addHorizontalRot;
		if(triggerRotate)
		{	
			CheckRotateTrigger();
				
			addHorizontalRot = Quaternion.AngleAxis(angleTrigger, Vector3.forward);
			GetComponent<Rigidbody>().rotation = Quaternion.Slerp(GetComponent<Rigidbody>().rotation, (rot * addHorizontalRot), Time.deltaTime * 3f * speedTrigger);
		}
		else 
		{
				//Vector3 rotationDir = pos - transform.position;
			float angle = Vector3.Angle(pos - GetComponent<Rigidbody>().position, transform.forward);			
			addHorizontalRot = Quaternion.AngleAxis((-angle*3f), Vector3.forward);
			GetComponent<Rigidbody>().rotation = Quaternion.Slerp(GetComponent<Rigidbody>().rotation, (rot * addHorizontalRot), Time.deltaTime * 3f);
		}
		GetComponent<Rigidbody>().position = pos;
		/*
		if(!triggerRotate || !loopRotate)
		{
		 	if (Physics.Raycast(transform.position, transform.forward, out hit, 200.0f)) 
			{
				//rotate around forward
	           if(hit.distance <= 8f && hit.transform.name == "RotateTrigger" && !triggerRotate && angleTrigger == 0f)
				{
					RotateTrigger rotTrigger = hit.transform.gameObject.GetComponent<RotateTrigger>();
					right = rotTrigger.right;
					angleTrigger = rotTrigger.countRotate*360*((right) ? 1 : -1);
					speedTrigger = rotTrigger.rotateSpeed;
					triggerRotate = true;
					return;
				}
	      		//Loop
	           if(hit.distance <= 8f && hit.transform.name == "LoopTrigger" && !loopRotate)
				{
					LoopTrigger lopTrigger = hit.transform.gameObject.GetComponent<LoopTrigger>();
					if(!lopTrigger.used)
					{
						rotateAroundPoint = (Vector3.up*lopTrigger.Radius + Vector3.forward*lopTrigger.Radius)+transform.position;
						transform.rotation = rot;
						loopStartPos = transform.position;
						loopRotate = true;	
						lopTrigger.used = true;
						checkLoopEndEnable = 10;
					}
					return;
				}
	        }
		}*/
		//Debug.DrawLine((Vector3.up*10 + Vector3.forward*10)+transform.position, transform.position);
	}
	
	private float WrapValue( float v, float start, float end, WrapMode wMode )
	{
		switch( wMode )
		{
		case WrapMode.Clamp:
		case WrapMode.ClampForever:
			return Mathf.Clamp( v, start, end );
		case WrapMode.Default:
		case WrapMode.Loop:
			return Mathf.Repeat( v, end - start ) + start;
		case WrapMode.PingPong:
			return Mathf.PingPong( v, end - start ) + start;
		default:
			return v;
		}
	}

    public void MoveEnableNow()
    {
        moveEnable = true;
    }

    public void MoveDisableNow()
    {
        moveEnable = false;
    }
}
