using UnityEngine;
using System.Collections;

public class AITrain : MonoBehaviour {
   
    public float trainSpeed = 0.1f;
    public Transform ExplosionEffect;

    public bool animationEnable = false;

    public Transform[] allObj;

    public Transform monetObj;
    public int monetCount = 1;
    private int monetChance = 0;

    private GameObject Player;
    private bool moveEnable = true;

	void Start () 
    {
        GetComponent<Animation>().Stop();

        foreach (AnimationState state in GetComponent<Animation>())
        {
            state.speed = trainSpeed;
        }

	    allObj = gameObject.GetComponentsInChildren<Transform>();
        Player = GameObject.Find("Player");
        if (Player) moveEnable = Player.GetComponentInChildren<PlayerScript>().moveEnable;

	    GameObject levConroller = GameObject.Find("LevelConroller");
        if (levConroller) {
            monetChance = levConroller.GetComponent<LevelConroller>().veniclesChancePer;
        }
	}

    private bool addPhysics = false;
	// Update is called once per frame
	void Update () 
    {
       if (!moveEnable) {
           GetComponent<Animation>().enabled = false;
           return;
       }

	   if (destroy) 
       {
           int rand = Random.Range(0, 100);

	       if (addPhysics) 
           {
	           for (int i = 0; i < allObj.Length; i++) 
               {
	               //Set default materials by flashing 
	               if (allObj[i] && allObj[i].transform) 
                   {
                       if (allObj[i].gameObject.GetComponent<AIZenitTurel>())
                       {
                           Transform eff = Instantiate(ExplosionEffect, allObj[i].gameObject.transform.position + (new Vector3(0, 1.6f, 0)), transform.rotation) as Transform;
                           Destroy(eff.gameObject, 3);
                           Destroy(allObj[i].gameObject);
                           continue;
                       }

	                   allObj[i].gameObject.AddComponent<Rigidbody>();
	                   allObj[i].gameObject.GetComponent<Rigidbody>().useGravity = false;
	                   allObj[i].gameObject.GetComponent<Rigidbody>().AddTorque(((rand < 51) ? transform.right : -transform.right) * 800);
	                   allObj[i].gameObject.GetComponent<Rigidbody>().AddForce(((rand < 51) ? transform.right : -transform.right) * 90);
                       allObj[i].gameObject.GetComponent<Rigidbody>().AddForce( transform.forward * 90);

                       if(allObj[i].gameObject.GetComponent<BoxCollider>())
                       {
                           StartCoroutine(Exposion(0.05f*i, allObj[i].gameObject.transform.position));
                       }
	               }
	           }

	           addPhysics = false;

	           addCoin();

               Destroy(gameObject, 4);
	       }

           foreach (AnimationState state in GetComponent<Animation>())
           {
               if (state.speed > 0)
               {
                   state.speed = (trainSpeed / 400);
               }
           }
	   }

        if (animationEnable) 
        {
            GetComponent<Animation>().enabled = true;
            GetComponent<Animation>().Play();
        }
	}

    IEnumerator Exposion(float time, Vector3 pos) 
    {
        yield return new WaitForSeconds(time);
        Transform eff = Instantiate(ExplosionEffect, pos + (new Vector3(0, 1.6f, 0)), transform.rotation) as Transform;
        Destroy(eff.gameObject, 3);
    }

    private bool destroy = false;

    public void DestroyTrain() 
    {
        if (destroy) return;

        destroy = true;
        addPhysics = true;

        for (int i = 0; i < allObj.Length; i++)
        {
            //Set default materials by flashing 
            if (allObj[i] && allObj[i].GetComponent<Renderer>()) 
            {
                allObj[i].GetComponent<Renderer>().material.SetColor("_Color", Color.black);
            }
        }
    }

    private void addCoin()
    {
        if (monetObj)
        {
            int rand = Random.Range(0, 100);
            if (rand <= monetChance)
            {
                for (int i = 0; i < monetCount; i++)
                {
                    Vector3 pos = new Vector3(Random.Range(-4, 4), transform.localPosition.y, Random.Range(-4, 4));
                    Instantiate(monetObj, transform.TransformPoint(pos), Quaternion.identity);
                }
            }
        }
    }

    public void MoveEnableNow()
    {
        moveEnable = true;
    }

    public void MoveDisableNow()
    {
        moveEnable = false;
    }
}
