using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SplineVenicle : MonoBehaviour {

	public Spline spline;
    public Vector3 RotationFIX = Vector3.zero;
    public float ANGLE_FIX = 90f;

	public float speed = 0.05f;
	public float offSet = 0f;
	public WrapMode wrapMode = WrapMode.Clamp;
	
	public float passedTime = 0f;
	
	public bool MoveEnable = false;
    public bool isPlane = false;


    //AutoRotate
    int angleTrigger = 0;
    int speedTrigger = 1;
    bool triggerRotate = false;
    bool right = false;

    RaycastHit hit;

    //Loop rotate
    bool loopRotate = false;
    Vector3 rotateAroundPoint = Vector3.zero;
    Vector3 loopStartPos = Vector3.zero;
    float startAngle = 0;
    int checkLoopEndEnable = 3;//Skip frames and enable check end loop
    public bool 			destroyAfterEnd = false;

    private bool 			moveEnable = true;
	public 	bool 			useSpline = true;
	public SplineDataCopy	splineData;
	private Rigidbody		rigidbodyCached;
	private Transform		transf;

    void Start()
    {
		rigidbodyCached = GetComponent<Rigidbody> ();
		if (rigidbodyCached == null) {
		
			rigidbodyCached = gameObject.AddComponent<Rigidbody>();
		}
		rigidbodyCached.isKinematic = true;
		if (spline != null) {
			splineData = spline.GetComponent<SplineDataCopy> ();
		}
		useSpline = splineData == null;
      	moveEnable = PlayerScript.instance.moveEnable;
		if (!useSpline) {
			Destroy( spline );
		}
    }


	float posOnSplne;
	void Update() {

		if (MoveEnable && moveEnable) {
			if( useSpline )
			{
				MoveOnSpline();
			}
			else
			{
				MoveOnSplineData();
			}
		}
		

		//transform.rotation = spline.GetOrientationOnSpline( WrapValue( passedTime + offSet, 0f, 1f, wrapMode ) );
	}
	 

	private void MoveOnSplineData()
	{
		if( !isPlane )
		{
			passedTime += Time.deltaTime * speed;
			
			posOnSplne = WrapValue(passedTime + offSet, 0f, 1f, wrapMode);
			
			if (destroyAfterEnd && posOnSplne > 0.98f)
			{
				Destroy(this.gameObject);
			}

			splineData.CalculatePosAndRotation( posOnSplne );

			rigidbodyCached.position = splineData.calculatedPosition;

			rigidbodyCached.rotation = Quaternion.Slerp(rigidbodyCached.rotation, splineData.calculatedRotation, Time.deltaTime * 3f);
			//transform.rotation = splineData.calculatedRotation;
		}
		else
		{
			if (!loopRotate) passedTime += Time.deltaTime * speed;
			//Debug.Log (WrapValue( passedTime + offSet, 0f, 1f, wrapMode ));
			
			posOnSplne = WrapValue(passedTime + offSet, 0f, 1f, wrapMode);
			
			if (destroyAfterEnd && posOnSplne > 0.98f)
			{
				Destroy(this.gameObject);
			}
			splineData.CalculatePosAndRotation( posOnSplne );
			Vector3 pos = splineData.calculatedPosition;
			
			//base rotation
			Quaternion rot = splineData.calculatedRotation;
			Quaternion addHorizontalRot;
			
			if (!loopRotate)
			{
				rigidbodyCached.position = pos;
				if (triggerRotate)
				{
					if (right)
					{
						angleTrigger -= (speedTrigger * 5);
					}
					else
					{
						angleTrigger += (speedTrigger * 5);
					}
					
					
					if (angleTrigger <= 0 && right)
					{
						triggerRotate = false;
						angleTrigger = 0;
						speedTrigger = 0;
						right = false;
					}
					else if (angleTrigger >= 0 && !right)
					{
						triggerRotate = false;
						angleTrigger = 0;
						speedTrigger = 0;
						right = false;
					}
					
					addHorizontalRot = Quaternion.AngleAxis(angleTrigger, Vector3.forward);
					rigidbodyCached.rotation = Quaternion.Slerp(rigidbodyCached.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f * speedTrigger);
				}
				else
				{
					Vector3 rotationDir = pos - rigidbodyCached.position;
					
					float angle = Vector3.Angle(pos - rigidbodyCached.position, transf.forward);
					//left rotation fix
					if (rotationDir.normalized.z < 0) 
					{
						angle = angle * -1 * 6f;
					} 
					else 
					{
						angle = angle * 6f;
					}
					
					addHorizontalRot = Quaternion.AngleAxis((angle), Vector3.forward);
					rigidbodyCached.rotation = Quaternion.Slerp(rigidbodyCached.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f);
				}
			}
			else
			{
				transf.RotateAround(rotateAroundPoint, -transf.right, 100f * Time.deltaTime);//Vector3.forward
				
				startAngle = Vector3.Angle(loopStartPos, rigidbodyCached.position);
				
				checkLoopEndEnable--;
				
				//360 graduses
				if (startAngle == 0f && checkLoopEndEnable < 0)
				{
					loopRotate = false;
					checkLoopEndEnable = 10;
					rigidbodyCached.position = loopStartPos;
				}
			}
		}
	}

	private void MoveOnSpline()
	{
		if(spline && !isPlane)
		{
			passedTime += Time.deltaTime * speed;
			
			posOnSplne = WrapValue(passedTime + offSet, 0f, 1f, wrapMode);
			
			if (destroyAfterEnd && posOnSplne > 0.98f)
			{
				Destroy(this.gameObject);
			}
			
			rigidbodyCached.position = spline.GetPositionOnSpline( posOnSplne );
			
			Quaternion rot = spline.GetOrientationOnSpline( posOnSplne );
			Quaternion addHorizontalRot = Quaternion.AngleAxis(ANGLE_FIX, RotationFIX);
			
			rigidbodyCached.rotation = Quaternion.Slerp(rigidbodyCached.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f);
		}
		else if (spline) 
		{
			if (!loopRotate) passedTime += Time.deltaTime * speed;
			//Debug.Log (WrapValue( passedTime + offSet, 0f, 1f, wrapMode ));
			
			posOnSplne = WrapValue(passedTime + offSet, 0f, 1f, wrapMode);
			
			if (destroyAfterEnd && posOnSplne > 0.98f)
			{
				Destroy(this.gameObject);
			}
			
			Vector3 pos = spline.GetPositionOnSpline(posOnSplne);
			
			//base rotation
			Quaternion rot = spline.GetOrientationOnSpline( posOnSplne );
			Quaternion addHorizontalRot;
			
			if (!loopRotate)
			{
				rigidbodyCached.position = pos;
				if (triggerRotate)
				{
					if (right)
					{
						angleTrigger -= (speedTrigger * 5);
					}
					else
					{
						angleTrigger += (speedTrigger * 5);
					}
					
					
					if (angleTrigger <= 0 && right)
					{
						triggerRotate = false;
						angleTrigger = 0;
						speedTrigger = 0;
						right = false;
					}
					else if (angleTrigger >= 0 && !right)
					{
						triggerRotate = false;
						angleTrigger = 0;
						speedTrigger = 0;
						right = false;
					}
					
					addHorizontalRot = Quaternion.AngleAxis(angleTrigger, Vector3.forward);
					rigidbodyCached.rotation = Quaternion.Slerp(rigidbodyCached.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f * speedTrigger);
				}
				else
				{
					Vector3 rotationDir = pos - rigidbodyCached.position;
					
					float angle = Vector3.Angle(pos - rigidbodyCached.position, transf.forward);
					//left rotation fix
					if (rotationDir.normalized.z < 0) 
					{
						angle = angle * -1 * 6f;
					} 
					else 
					{
						angle = angle * 6f;
					}
					
					addHorizontalRot = Quaternion.AngleAxis((angle), Vector3.forward);
					rigidbodyCached.rotation = Quaternion.Slerp(rigidbodyCached.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f);
				}
			}
			else
			{
				transf.RotateAround(rotateAroundPoint, -transf.right, 100f * Time.deltaTime);//Vector3.forward
				
				startAngle = Vector3.Angle(loopStartPos, rigidbodyCached.position);
				
				checkLoopEndEnable--;
				
				//360 graduses
				if (startAngle == 0f && checkLoopEndEnable < 0)
				{
					loopRotate = false;
					checkLoopEndEnable = 10;
					rigidbodyCached.position = loopStartPos;
				}
			}
		}
	}

	public Quaternion getRotation()
	{
		return rigidbodyCached.rotation;			
	}
	
	public void PauseMove()
	{
		MoveEnable = false;
       // if(spline)spline.gameObject.SetActive(false);
	}
	
	public void StartMove()
	{
       // if (spline)spline.gameObject.SetActive(true);
		MoveEnable = true;
	    moveEnable = true;
	}
	
	
	private float WrapValue( float v, float start, float end, WrapMode wMode )
	{
		switch( wMode )
		{
		case WrapMode.Clamp:
		case WrapMode.ClampForever:
			return Mathf.Clamp( v, start, end );
		case WrapMode.Default:
		case WrapMode.Loop:
			return Mathf.Repeat( v, end - start ) + start;
		case WrapMode.PingPong:
			return Mathf.PingPong( v, end - start ) + start;
		default:
			return v;
		}
	}

    public void MoveEnableNow()
    {
        moveEnable = true;
    }

    public void MoveDisableNow()
    {
        moveEnable = false;
    }
}
