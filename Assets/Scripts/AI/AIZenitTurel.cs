using UnityEngine;
using System.Collections;

public class AIZenitTurel : MonoBehaviour {
	//GUN
    public Transform LookTarget;
	public Transform Turret;
	public Transform tiltableGun;
    public Transform[] FirePos;

    //Destroy Section
    public Transform DestroyModel; //The effect created when the plane explodes

	//Params
    public float Health = 1; //health
    public float attackRadius = 250;
    public Transform ExplosionEffect; 
    public bool Crashed = false;

    public Transform 	BulletObject;
    public Transform 	MuzzleObject; 
    public int 			BulletSpeed = 5; 
    public float 		shotCoolDown = 0; 
	public float		attackInterval = 2;
    public int 			ShotsPerAttack = 3;

    public 	Transform 	monetObj;
    public 	int 		monetCount = 1;
    private int			monetChance = 0;

    private Transform 	BulletObjectCopy; 
    private float 		shotCooldownCount; 
    private float 		attackIntervalCount; 
    private int 		shotsPerAttackCount; 
	public 	bool		usePhysic;
    private EndLevelPopup popup;
	private Transform	transf;

#if UNITY_EDITOR
    public bool DRAWRadius = false;

    void OnDrawGizmos()
    {
        if (DRAWRadius)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, attackRadius);
        }
    }
#endif


    void Awake()
    {
		transf = transform;
        shotsPerAttackCount = ShotsPerAttack;
        attackIntervalCount = attackInterval;
        shotCooldownCount = shotCoolDown;
    }
	
	public bool shootEnable = true;

    void Start()
    {
        GameObject EndLevel = GameObject.Find("EndLevel");
        popup = EndLevel.GetComponent<EndLevelPopup>();
        popup.allEnemyCnt++;
		shootEnable = PlayerScript.instance.shootEnable;
		if ( LevelConroller.instance.currentDifficultyLevel == 0 )
        {
			Health = Health - (Health * LevelConroller.instance.easyLevel);
        }
		else if ( LevelConroller.instance.currentDifficultyLevel == 2 )
        {
			Health = Health + (Health * LevelConroller.instance.hardLevel);
        }
        monetChance = LevelConroller.instance.veniclesChancePer;
    }

	Vector3 posOnScreen;
    void Update()
    {
        if (Health > 0)
        {
			if(shootEnable)
			{
				posOnScreen = Camera.main.WorldToViewportPoint( transf.position );
				if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.x > 0 )
				{

					if(Turret)
					{
						LookTarget.rotation =  Quaternion.LookRotation( PlayerScript.instance.transform.position - LookTarget.position );
						//rotate turret
						Turret.eulerAngles = new Vector3(
							Turret.eulerAngles.x, 
							LookTarget.eulerAngles.y,
							Turret.eulerAngles.z);
					}
					
					if(tiltableGun)
					{
						//tilt gun
						
						tiltableGun.eulerAngles = new Vector3(
							LookTarget.eulerAngles.x,
							LookTarget.eulerAngles.y,
							tiltableGun.eulerAngles.z);
						
					}

					if( Vector3.Distance( transf.position, PlayerScript.getPosition ) < attackRadius ) 
                	{	      
				        if (attackIntervalCount > 0) {
				            attackIntervalCount	-= Time.deltaTime;
				        } else {
				           
				            if (shotCooldownCount > 0) {
				                shotCooldownCount -= Time.deltaTime;
				            } else {

	                            for (int i = 0; i < FirePos.Length; i++)
	                            {
	                                Shoot(FirePos[i]);
	                            }
					            //The number of shots per burst is set by ShotsPerAttack
								shotsPerAttackCount--;
				                if (shotsPerAttackCount == 0) 
								{
				                    shotsPerAttackCount = ShotsPerAttack;
				                    attackIntervalCount = attackInterval;
				                }
				            }
				        }
					}
				}
				else if( posOnScreen.y < 0 )
				{
					gameObject.SetActive( false );
				}
			}

        } 
        else 
        {
            if (!Crashed) 
            {
                popup.deadEnemyCnt++;
                Crashed = true;

                addCoin();

                if (ExplosionEffect) {
                    //Create an explosion
					EffectsManager.PlayEffect( ExplosionEffect.name, transf.position + (new Vector3(0, 10.6f, 0)), transf.rotation );
                }

                if (DestroyModel) {
					DestroyedObjectsManager.Spawn( DestroyModel.name, transf );
                }
                Destroy(gameObject);
            }
        }

    }
	
	
	private void SetDamageColor() {
	    Texture oldTextureTurret = new Texture();
	    if (Turret.GetComponent<Renderer>()) {
	        oldTextureTurret = Turret.GetComponent<Renderer>().material.mainTexture;
	        Turret.GetComponent<Renderer>().material = new Material(Shader.Find("Mobile/Vertex Colored"));
	        Turret.GetComponent<Renderer>().material.mainTexture = oldTextureTurret;
	    }

        Texture tiltableGunTexture = new Texture();
	    if (tiltableGun.GetComponent<Renderer>()) {
	        tiltableGunTexture = tiltableGun.GetComponent<Renderer>().material.mainTexture;
	        tiltableGun.GetComponent<Renderer>().material = new Material(Shader.Find("Mobile/Vertex Colored"));
	        tiltableGun.GetComponent<Renderer>().material.mainTexture = tiltableGunTexture;
	    }
	    StartCoroutine(setDefaultTextures(oldTextureTurret, tiltableGunTexture));	
	}
	
	IEnumerator setDefaultTextures(Texture oldTextureTurret, Texture tiltableGunTexture) {
        yield return new WaitForSeconds(0.1f);

	    if (Turret.GetComponent<Renderer>()) {
	        Turret.GetComponent<Renderer>().material = new Material(Shader.Find("Mobile/Diffuse"));
	        Turret.GetComponent<Renderer>().material.mainTexture = oldTextureTurret;
	    }

	    if (tiltableGun.GetComponent<Renderer>()) {
	        tiltableGun.GetComponent<Renderer>().material = new Material(Shader.Find("Mobile/Diffuse"));
	        tiltableGun.GetComponent<Renderer>().material.mainTexture = tiltableGunTexture;
	    }
	}
	

	public void applyDamage(float hp)
	{
		Health -= hp;	
	}

	private void TakeDamage( int damage )
	{
		Health -= damage;
		//SetDamageColor();
	}

	private void BridgeDestroy()
	{
		Health = 0;
	}
    //Shoot
    private void Shoot(Transform FirePos)
    {
        if (BulletObject)
        {
            //BulletObjectCopy.transform.Rotate(new Vector3(0,0,0));
			BulletBase bullet = BulletsManager.GetFreeBullet( BulletObject.name );
			Vector3 dir = PlayerScript.getPosition - Turret.position;
			if( bullet != null )
			{
				bullet.Launch( FirePos.position, Quaternion.identity, BulletSpeed * dir.normalized );
			}
            
            //BulletObjectCopy.transform.LookAt(Player.transform.position);
            if (MuzzleObject)
            {
				Instantiate(MuzzleObject, FirePos.position, Quaternion.identity );
            }
            shotCooldownCount = shotCoolDown;
        }
    }

    private void addCoin()
    {
        if (monetObj)
        {
            int rand = Random.Range(0, 100);
            if (rand <= monetChance)
            {
				Vector3 pos;
                for (int i = 0; i < monetCount; i++)
                {
					 pos = new Vector3(Random.Range(-4, 4), transf.localPosition.y, Random.Range(-4, 4));
					MedalsManager.Spawn( monetObj.name, transf.TransformPoint( pos ) );
                }
            }
        }
    }

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
