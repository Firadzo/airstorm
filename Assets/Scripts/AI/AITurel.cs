using UnityEngine;
using System.Collections;

public class AITurel : MonoBehaviour {
    //GUN  
    public Transform[] 	FirePos;
	public float 		rotationSpeed = 90.0f;
	public float 		tiltSpeed = 45.0f;
	public float  		minTilt = -70f;
	public float  		maxTilt = 10f;
	public Transform 	Turret;
	public MeshRenderer[] turretRenderers;
	public Transform 	tiltableGun;
	
	//Params
    public float Health = 1; //health
    public float attackRadius = 50;
	
    //Destroy Section
    public Transform ExplosionEffect; 
    public bool Crashed = false;

    public Transform 	BulletObject;
    public Transform 	MuzzleObject;
    public int 			BulletSpeed = 5;
    public float 		shotCooldown = 4; 
    public float 		attackInterval = 5;
    public int 			ShotsPerAttack = 3; 

    public 	Transform 	monetObj;
    public 	int 		monetCount = 1;
    private int 		monetChance = 0;

    private Transform 	BulletObjectCopy; 
    private float 		shotCoolDownCount; 
	private float 		attackIntervalCount;
    private int 		shotsPerAttackCount; 

    private EndLevelPopup popup;

    public bool DRAWRadius = false;

    void OnDrawGizmos()
    {
        if (DRAWRadius)
        {
            Gizmos.color = Color.grey;
            Gizmos.DrawWireSphere(transform.position, attackRadius);
        }
    }

    void Awake()
    {
        shotsPerAttackCount = ShotsPerAttack;
        attackIntervalCount = attackInterval;
        shotCoolDownCount 	= shotCooldown;
		if (GetComponent<Rigidbody> () == null) {
		
			Rigidbody r = gameObject.AddComponent<Rigidbody>();
			r.isKinematic = true;
		}
    }
	
	public bool shootEnable = true;

    void Start()
    {
		popup = EndLevelPopup.instance;
        popup.allEnemyCnt++;
		
		shootEnable = PlayerScript.instance.shootEnable;
		if (LevelConroller.instance.currentDifficultyLevel == 0)
       	{
			Health = Health - (Health * LevelConroller.instance.easyLevel);
        }
		else if (LevelConroller.instance.currentDifficultyLevel == 2)
        {
			Health = Health + (Health * LevelConroller.instance.hardLevel);
        }
		monetChance = LevelConroller.instance.veniclesChancePer;
    }

	Vector3 posOnScreen;
    void Update()
    {
        if (Health > 0)
        {
			//shootEnable = Player.GetComponentInChildren<PlayerScript>().shootEnable;
			
			if(shootEnable)
			{
				posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
				if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0 )
				{
					//rotate turret
					Vector3 dir = Turret.position - PlayerScript.getPosition;
					float angleToTarget = Mathf.Abs(90 - Vector3.Angle(dir, Turret.right));
					Turret.localEulerAngles = new Vector3(
						Turret.localEulerAngles.x,
						Turret.localEulerAngles.y + Mathf.Clamp(rotationSpeed * Time.fixedDeltaTime * Mathf.Sign(Vector3.Dot(Vector3.Cross(Turret.forward, dir), Turret.up)), -angleToTarget, angleToTarget),
						Turret.localEulerAngles.z);
					
					//tilt gun
					dir = tiltableGun.position - PlayerScript.getPosition;
					angleToTarget = Mathf.Abs(90 - Vector3.Angle(dir,tiltableGun.up));
					tiltableGun.localEulerAngles = new Vector3(
						tiltableGun.localRotation.eulerAngles.x + Mathf.Clamp(tiltSpeed * Time.fixedDeltaTime * Mathf.Sign(Vector3.Dot(Vector3.Cross(tiltableGun.forward, dir), tiltableGun.right)), -angleToTarget, angleToTarget),
						tiltableGun.localRotation.eulerAngles.y,
						tiltableGun.localRotation.eulerAngles.z);
					
					//limit gun tilt by margins
					if(tiltableGun.localEulerAngles.x > 180 && tiltableGun.localEulerAngles.x < Mathf.Repeat(minTilt,360))
						tiltableGun.localEulerAngles = new Vector3(minTilt, tiltableGun.localEulerAngles.y, tiltableGun.localEulerAngles.z);
					if(tiltableGun.localEulerAngles.x < 180 && tiltableGun.localEulerAngles.x > Mathf.Repeat(maxTilt,360))
						tiltableGun.localEulerAngles = new Vector3(maxTilt, tiltableGun.localEulerAngles.y, tiltableGun.localEulerAngles.z);
				   if (Vector3.Distance( transform.position, PlayerScript.getPosition ) < attackRadius) 
					{
						//Debug.DrawLine(ray.origin, hit.point);
			           // Debug.Log(hit.transform.name);
			            //if (hit.transform.tag == "Player" || hit.transform.name == "Player2") {
			                //First, we wait for a set amount of frame updates, and then start shooting a burst
			                if (attackIntervalCount > 0) {
			                    attackIntervalCount	-= Time.deltaTime;
			                } else {
			                    //The rate of fire is set by the value of ShotCooldown, and is set in frame updates
			                    if (shotCoolDownCount > 0) {
			                        //Decrease the cooldown counter
			                        shotCoolDownCount -= Time.deltaTime;
			                    } else {

                                    for (int i = 0; i < FirePos.Length; i++)
                                    {
                                        Shoot(FirePos[i]);
                                    }
				                    //The number of shots per burst is set by ShotsPerAttack
			
			                        shotsPerAttackCount--;
									if (shotsPerAttackCount == 0) 
									{
			                            shotsPerAttackCount = ShotsPerAttack;
			                    	    attackIntervalCount = attackInterval;
			                    	}
			                    }
			                }
						//}
					}	
				}
			}

        } else {
            if (!Crashed) {

                popup.deadEnemyCnt++;

                Crashed = true;

                /*MeshRenderer enbl = transform.Find("WarPlane").GetComponent<MeshRenderer>();
                enbl.enabled = false;

                MeshRenderer enbl2 = transform.Find("WarPlaneCrash").GetComponent<MeshRenderer>();
                enbl2.enabled = true;*/

                //Create an explosion
                if (ExplosionEffect) {
                    Instantiate(ExplosionEffect, transform.position + (new Vector3(0, 1.6f, 0)), transform.rotation);
                }
                Destroy(gameObject);

                addCoin();
                //GUIInterface scor = Camera.mainCamera.GetComponent<GUIInterface>();
                //scor.Score += 200;
  
            }
        }

    }
	
	private void TakeDamage( int damage )
	{
		Health -= damage;
		//SetDamageColor();
	}

	public void SetDamageColor()
	{
		Texture oldTextureTurret;
		for( int i = 0; i < turretRenderers.Length; i++ )
		{
			oldTextureTurret = turretRenderers[ i ].material.mainTexture;
			turretRenderers[ i ].material = new Material(Shader.Find("Mobile/Vertex Colored"));
			turretRenderers[ i ].material.mainTexture = oldTextureTurret;
		}
		StartCoroutine( setDefaultTextures() );	
	}
	
	IEnumerator setDefaultTextures( ) {
        yield return new WaitForSeconds(0.1f);	

		Texture oldTextureTurret;
		for( int i = 0; i < turretRenderers.Length; i++ )
		{
			oldTextureTurret = turretRenderers[ i ].material.mainTexture;
			turretRenderers[ i ].material = new Material(Shader.Find("Mobile/Diffuse"));
			turretRenderers[ i ].material.mainTexture = oldTextureTurret;
		}
    }
	

	public void applyDamage(float hp)
	{
		Health -= hp;	
	}

    //Shoot
    private void Shoot(Transform FirePos)
    {
        if (BulletObject)
        {
            BulletObjectCopy = Instantiate(BulletObject, FirePos.position, Quaternion.identity) as Transform;
			Vector3 dir = Turret.position - PlayerScript.getPosition;
            if (MuzzleObject)
            {
                Instantiate(MuzzleObject, BulletObjectCopy.position, BulletObjectCopy.rotation);
            }
            if (BulletObjectCopy.GetComponent<Rigidbody>())
            {
                BulletObjectCopy.GetComponent<Rigidbody>().AddForce(-dir * BulletSpeed);
            }
            shotCoolDownCount = shotCooldown;
        }
    }

    private void addCoin()
    {
        if (monetObj)
        {
            int rand = Random.Range(0, 100);
            if (rand <= monetChance)
            {
				Vector3 pos;
                for (int i = 0; i < monetCount; i++)
                {
					pos = new Vector3(Random.Range(-4, 4), transform.localPosition.y, Random.Range(-4, 4));
					MedalsManager.Spawn( monetObj.name, pos );
                }
            }
        }
    }

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
