using UnityEngine;
using System.Collections;

public class AIShip : MonoBehaviour {
    public float Health = 1; //health	
    public Transform destroyModel;

    public Material dissolveMaterial;
    private float maxDissolve = 3f;
    private bool dissolveEnabled = false;

    //Destroy Section
    public Transform ExplosionEffect;
    public bool Crashed = false;

    public bool itIsShip = false;
    public bool itIsAerostat = false;

    public Transform monetObj;
    public int monetCount = 1;
    private int monetChance = 0;

    private EndLevelPopup popup;
	private Transform		transf;

    private void Start() {
		transf = transform;
        GameObject EndLevel = GameObject.Find("EndLevel");
        popup = EndLevel.GetComponent<EndLevelPopup>();
        popup.allEnemyCnt++;
		if ( LevelConroller.instance.currentDifficultyLevel == 0 ) 
		{
			Health = Health - (Health * LevelConroller.instance.easyLevel);
		} 
		else if ( LevelConroller.instance.currentDifficultyLevel == 2 ) 
		{
			Health = Health + (Health * LevelConroller.instance.hardLevel);
        }
       monetChance = LevelConroller.instance.veniclesChancePer;
    }



    private void Update() {
        if ( itIsAerostat && Health <= 0 && dissolveEnabled ) 
		{
            if (maxDissolve >= 0f) 
			{
				acceleration += gravity * Time.deltaTime;
				transf.position += Vector3.down * acceleration * Time.deltaTime;
                maxDissolve -= Time.deltaTime * 2f;
                dissolveMaterial.SetFloat("_Range", maxDissolve);
                //Debug.Log(maxDissolve);
            } else {
                Destroy(gameObject);
            }
        }
    }

	const float gravity = 35f;
	float acceleration = 0;

   /* private void FixedUpdate() {
        if (Health < 0 && itIsShip) {
           // rigidbody.AddForceAtPosition(rigidbody.velocity * -0.1f, transform.TransformPoint(-500, 0, 0));
        }
    }*/

    public void applyDamage(float hp) {
        Health -= hp;
		CheckHealth ();
    }

	private void TakeDamage( int damage )
	{
		Health -= damage;
		CheckHealth ();
	}

	private void CheckHealth()
	{
		if (Health <= 0) {
			if (!Crashed) {
				
				Crashed = true;
				popup.deadEnemyCnt++;
				
				if (destroyModel) 
				{
					if( DestroyedObjectsManager.Spawn( destroyModel.name, transf.position, transf.rotation ) == null )
					{
						Instantiate(destroyModel, transf.position, transf.rotation);
					}
				}
				if (ExplosionEffect) {
					EffectsManager.PlayEffect ( ExplosionEffect.name, transf.position + (new Vector3(0, 10.6f, 0)), transf.rotation );
				}
				
				if (itIsShip) {
					Transform[] root = GetComponentsInChildren<Transform>();
					
					for (int d = 0; d < root.Length; d++) {
						GameObject part = root[d].gameObject;
						
						if (part.GetComponent<AITurel>()) {
							Destroy(part, 0.5f);
						}
						if (part.GetComponent<SubmarineTurel>()) {
							Destroy(part, 0.5f);
						}
						if (part.GetComponent<AIRockTorpedo>()) {
							if (!part.GetComponent<AIRockTorpedo>().waterPhase) {
								Destroy(part, 0.5f);
							}
						}
						
						if (part.GetComponent<Renderer>()) {
							part.GetComponent<Renderer>().material.SetColor("_Color", Color.black);
						}
					}
					SplineVenicle spline = gameObject.GetComponent<SplineVenicle>();
					if (spline) 
					{
						spline.spline = null;
						spline.enabled = false;
					}
					/*int rand = Random.Range(0, 100);
                    if (rand >= 50) 
					{
                        destroyByPhys();
                        return;
                    }*/
					
					
					
					gameObject.AddComponent<Rigidbody>();
					gameObject.GetComponent<Rigidbody>().mass = 15f;
					gameObject.GetComponent<Rigidbody>().useGravity = true;
					gameObject.GetComponent<Rigidbody>().isKinematic = false;
					gameObject.GetComponent<Rigidbody>().velocity = -transf.right.normalized * 40;
					Destroy( gameObject, 10f );
				}
				else if (itIsAerostat) 
				{
					if (dissolveMaterial) {
						int cnt = gameObject.GetComponent<Renderer>().materials.Length;
						Material[] matsNew = new Material[cnt];
						
						for (int i = 0; i < cnt; i++) {
							matsNew[i] = dissolveMaterial;
						}
						
						//gameObject.AddComponent<Rigidbody>();
						// gameObject.rigidbody.mass = 0.01f;
						// gameObject.rigidbody.useGravity = true;
						gameObject.GetComponent<Renderer>().materials = matsNew;
						
						SplineVenicle spline = gameObject.GetComponent<SplineVenicle>();
						if (spline)
						{
							spline.spline = null;
							spline.enabled = false;
						}
						
						dissolveEnabled = true;
						
					} else {
						Destroy(gameObject);
					}
				} 
				else {
					Destroy(gameObject);
				}
				
				addCoin();
			}
		}
	}

    public void BridgeDestroy() {
        if (itIsShip) {
            return;
        }

        Crashed = true;

        popup.deadEnemyCnt++;
		EffectsManager.PlayEffect ( ExplosionEffect.name, transf.position + (new Vector3(0, 1.6f, 0)), transf.rotation );

        if (destroyModel) 
		{
			Transform deathModel = Instantiate(destroyModel, transf.position, transf.rotation) as Transform;
            GameObject obj = deathModel.gameObject;
            obj.transform.parent = null;
            obj.AddComponent<Rigidbody>();
            obj.AddComponent<BoxCollider>();
            obj.GetComponent<Rigidbody>().mass = 150f;
            obj.GetComponent<Rigidbody>().useGravity = true;
            obj.GetComponent<Rigidbody>().AddForce(new Vector3(Random.value, Random.value, Random.value) * 2000f, ForceMode.Impulse);

            SplineVenicle spline = gameObject.GetComponent<SplineVenicle>();

            if (spline) {
                deathModel.rotation = spline.getRotation();
            }
            Destroy(obj, 10);
        } else {
            destroyByPhys();
        }

        Destroy(gameObject);
    }

    private void destroyByPhys() {
        Debug.Log("By physics");
        Transform[] root = GetComponentsInChildren<Transform>();

        for (int d = 0; d < root.Length; d++) {
            GameObject obj = root[d].gameObject;
            if (root[d].gameObject.GetComponentInChildren<RotatePlaneInMenu>()) {
                root[d].gameObject.GetComponentInChildren<RotatePlaneInMenu>().enabled = false;
            }
            obj.transform.parent = null;
            if (!obj.GetComponent<Rigidbody>()) {
                obj.AddComponent<Rigidbody>();
            }
            if (!obj.GetComponent<BoxCollider>()) {
                obj.AddComponent<BoxCollider>();
            }
            obj.GetComponent<Rigidbody>().mass = 35f;
            obj.GetComponent<Rigidbody>().useGravity = true;
            obj.layer = LayerMask.NameToLayer("NoCollisionLayer");
            obj.GetComponent<Rigidbody>().AddForce(new Vector3(Random.value, Random.value, Random.value).normalized * Random.Range(500f, 1700f), ForceMode.Impulse);
            Destroy(obj, 10);
        }
    }

    private void addCoin() {
        if (monetObj) {
            int rand = Random.Range(0, 100);
            if (rand <= monetChance) {
                for (int i = 0; i < monetCount; i++) {
						MedalsManager.Spawn( monetObj.name, transf.TransformPoint( new Vector3( Random.Range(-4, 4), transf.localPosition.y, Random.Range(-4, 4)) ) );
                }
            }
        }
    }
}
