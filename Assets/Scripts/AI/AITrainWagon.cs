using UnityEngine;
using System.Collections;

public class AITrainWagon : MonoBehaviour {
    public AITrain rootObject;

    public float Health = 1; //health

	// Use this for initialization
	void Start () 
    {
	    if (gameObject.transform.parent) 
        {
	        rootObject = gameObject.transform.parent.GetComponent<AITrain>();
	    }

        GameObject levConroller = GameObject.Find("LevelConroller");
        if (levConroller)
        {
            LevelConroller comp = levConroller.GetComponent<LevelConroller>();
            if (comp.currentDifficultyLevel == 0)
            {
                Health = Health - (Health * comp.easyLevel);
            }
            else if (comp.currentDifficultyLevel == 2)
            {
                Health = Health + (Health * comp.hardLevel);
            }
        }
	}


    public void applyDamage(float hp)
    {
        Health -= hp;
		if (Health < 0) 
		{
			if(rootObject)rootObject.DestroyTrain();
		}
    }
}
