using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISoldier : MonoBehaviour {
    public 	GameObject 		Muzzle;
    public	GameObject 		Bullet;
    public 	AnimationClip 	deathAnim;
    public 	Vector3 		shootOffset;
    public 	float 			Health = 10;
    public 	int 			BulletSpeed = 30;
    public 	float 			shot_Cooldown = 2.5f;
    private float 			shotCooldown;

    public List<AnimationState> Animations = new List<AnimationState>();
    public bool loopALL = false;
    public bool pauseAnimation = false;

    private int indexPlay;
	public float	attackRadius = 250f;

#if UNITY_EDITOR

	void OnDrawGizmosSelected()
	{
		Gizmos.DrawWireSphere( transform.position, attackRadius );
	}
#endif
    [Serializable]
    public class AnimationState {
        public AnimationClip animation;
        public float time_play = 0.0f;
        public Spline traectory;
        public bool isTractoryPlay = false;
    }

    private SplineVenicle traject;
    private GameObject Player;

    // Use this for initialization
    private void Start() {
        shotCooldown = shot_Cooldown;
        Player = GameObject.Find("Player");//Set player object

        traject = GetComponent<SplineVenicle>();

        StartCoroutine(playAnimation(0));

        GameObject levConroller = GameObject.Find("LevelConroller");
        if (levConroller)
        {
            LevelConroller comp = levConroller.GetComponent<LevelConroller>();
            if (comp.currentDifficultyLevel == 0)
            {
                Health = Health - (Health * comp.easyLevel);
            }
            else if (comp.currentDifficultyLevel == 2)
            {
                Health = Health + (Health * comp.hardLevel);
            }
        }
    }

	private void TakeDamage( int damage )
	{
		Health -= damage;
		CheckHealth ();
	}


    private IEnumerator playAnimation(int index) {
        if (!pauseAnimation) {
            indexPlay = index;

            if (indexPlay < Animations.Count && Animations[index].time_play > 0) //Play animation clips per time
            {
                if (Animations[index].isTractoryPlay) {
                    NextAnimation();
                    yield break;
                }

                if (Animations[index].traectory && traject) {
                    traject.spline = Animations[index].traectory;
                    Animations[index].isTractoryPlay = true;
                }

                GetComponent<Animation>().clip = Animations[index].animation;
                GetComponent<Animation>().Play();

                yield return new WaitForSeconds(Animations[index].time_play);

                NextAnimation();
            } else {
                if (GetComponent<Animation>().isPlaying) {
                    GetComponent<Animation>().Stop();
                }

                StopCoroutine("playAnimation");
            }
        } else {
            yield return new WaitForSeconds(4.0f);
            StopCoroutine("playAnimation");
            //Debug.Log("Move AI soilder paused");
            StartCoroutine(playAnimation(0));
        }
    }

    private void NextAnimation() {
        if ( indexPlay + 1 < Animations.Count ) 
		{
            if (traject) {
                traject.spline = null;
            }
            StopCoroutine("playAnimation");
            StartCoroutine(playAnimation(indexPlay + 1));
        } else {
            if (GetComponent<Animation>().isPlaying) {
                GetComponent<Animation>().Stop();
            }

            if (traject) {
                traject.spline = null;
            }

            StopCoroutine("playAnimation");

            //repeat play All animations
            if (loopALL) {
                StartCoroutine(playAnimation(0));
            }
        }
    }

    public void pauseAnim() {
        pauseAnimation = true;
        GetComponent<Animation>().Stop();
    }

    public void startAnim() {
        pauseAnimation = false;
        GetComponent<Animation>().Stop();
    }

    private bool destroy;

	Vector3 screenPos; 
	Vector3 toPlayer;
    private void Update() {
        if (!destroy && !pauseAnimation) {
			screenPos = Camera.main.WorldToViewportPoint(transform.position);//Vector3.Distance(Camera.main.WorldToScreenPoint(Player.transform.position), Camera.main.WorldToScreenPoint(transform.position));
			if ( screenPos.x > 0 && screenPos.x < 1 && screenPos.y < 1 && screenPos.y > 0 )
			{
				Vector3 dir = Player.transform.position - transform.position;
				/*toPlayer = transform.localEulerAngles;
				toPlayer.y = Quaternion.LookRotation( dir ).eulerAngles.y;
				transform.localEulerAngles = toPlayer;*/
				float angleToTarget = Mathf.Abs(90 - Vector3.Angle(dir, transform.right));
				transform.localEulerAngles = new Vector3(
					transform.localEulerAngles.x,
					transform.localEulerAngles.y + Mathf.Clamp(80 * Time.fixedDeltaTime * Mathf.Sign(Vector3.Dot(Vector3.Cross(transform.forward, dir), Vector3.up)), -angleToTarget, angleToTarget),
					transform.localEulerAngles.z);
				if( Vector3.Distance( transform.position, PlayerScript.getPosition ) <= attackRadius )
	            {
					if( (GetComponent<Animation>().clip.name == "soldat@shootdaigonal") || (GetComponent<Animation>().clip.name == "soldat@shootconor") || (GetComponent<Animation>().clip.name == "soldat@shootkoleno") )
					{
		                shotCooldown -= Time.deltaTime;
		                if (shotCooldown <= 0) 
						{
		                    shotCooldown = shot_Cooldown;
		        			if (Bullet) 
							{
								BulletBase bullet = BulletsManager.GetFreeBullet( Bullet.name );
								if( bullet != null )
								{
									Vector3 dir2 = ( Player.transform.position - transform.position ).normalized;
									bullet.Launch( transform.position + shootOffset, Quaternion.identity, dir2 * BulletSpeed );
								}
		                    }
		                }
					}
	            } 
			}
        }


    }

	private void CheckHealth()
	{
		if (Health <= 0 && !destroy) {
			destroy = true;
			StopCoroutine("playAnimation");
			GetComponent<Animation>().clip = deathAnim;
			GetComponent<Animation>().Play();
			Destroy(gameObject, 2);
		}
	}

    public void applyDamage(float hp) {
        Health -= hp;
		CheckHealth ();
    }

    public void ShootEnable() {
        startAnim();
    }

    public void ShootDisable() {
        pauseAnim();
    }
}
