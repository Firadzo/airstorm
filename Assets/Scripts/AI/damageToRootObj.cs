﻿using UnityEngine;
using System.Collections;

public class damageToRootObj : MonoBehaviour {
    private Transform rootObject;

	void Awake () {
        if (transform.root.GetComponent<BridgeDestroyTrigger>()) {
            rootObject = transform.root;
        }
        else if (transform.root.transform.root.GetComponent<BridgeDestroyTrigger>())
        {
            rootObject = transform.root.transform.root;
        } else {
            //Debug.Log("root object do not found");
        }
	}

    public void applyDamage(float hp) {
        if(rootObject)rootObject.SendMessage("applyDamage", hp, SendMessageOptions.DontRequireReceiver);
    }
}
