using UnityEngine;
using System.Collections;

public class LookToPlayer : MonoBehaviour {

	// Update is called once per frame
	void Update () {
		transform.LookAt( PlayerScript.instance.transform );
	}
}
