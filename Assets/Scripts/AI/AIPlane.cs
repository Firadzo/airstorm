using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;


public class AIPlane : MonoBehaviour {

	
	public class DefaultParams
	{
		public 	Transform	bulletObj;
		public 	int			bulletSpeed;
		public 	int 		defaultLayer;
		public 	string 		defaultTag;
		public	int			shotsPerAttack;
		public 	float 		attackInterval;
		public 	float		shotCooldown;
	}

	public AirStrike.TechniqueTypes techniqueType = AirStrike.TechniqueTypes.none;

    private Transform ModelBody;
    public Transform Shadow;
    //public float shadowHeight = 152.0f;
    public float Health = 1; //health

    //Destroy Section
    public Transform 	ExplosionEffect;
    private bool 		destroyed = false;
    private bool 		collDeadState = false;

    public Transform 	BulletObject;
	[HideInInspector]
	public float        bulletDamage;
    public gunLaunch 	launchType;
    public int		 	gunCount = 2;
    public int 			BulletSpeed = 70;
    public float 		ShotCoolDown = 20;
    public float 		AttackInterval = 70;
    public int 			ShotsPerAttack = 3;
    public int 			contactDamage = 300;
    public bool 		shootEnable = true;
	public Transform[] 	DeadFireContainer;

	public GameObject 	DeadFirePref;

	public float 		DeadFireTimeOut = 1;

    public Transform 	monetObj;
    public int 			monetCount = 1;
    public int 			monetChance = 0;

    private float 		attackIntervalCount;
    private int 		ShotsPerAttackCount;
    private float 		shotCooldown = 0;
    private EndLevelPopup popup;
    private Color 		defaultColor;
    private Vector3 	dir;

    public AudioClip 	bulletSound;
	public AudioClip 	fallingSound;
	private bool		seePlayer;
	private SplineAnimator	splineAnimator;
    
	public enum gunLaunch {
        SEEPLAYER,
        AUTO
    }

	public bool ready
	{
		get{ return !gameObj.activeSelf; }
	}

	private Transform 		transf;
	private GameObject 		gameObj;
	private Rigidbody		rigidBodyCached;
	private DefaultParams 	defaultParams;

	private void Awake()
	{
		transf = transform;
		gameObj = gameObject;
		gameObj.AddComponent<AudioSource>();
		gameObj.AddComponent<RegisterAudio>();
		rigidBodyCached = GetComponent<Rigidbody> ();
		splineAnimator = GetComponent<SplineAnimator> ();
		splineAnimator.onTrajectoryEnd += ResetPlane;
		GetComponent<AudioSource>().clip = bulletSound;
		GetComponent<AudioSource>().playOnAwake = false;
		ModelBody = transf.Find("Body").GetChild(0);
		if (ModelBody)
		{
			// defaultColor = ModelBody.renderer.material.GetColor("_Color");
		}
		defaultParams = new DefaultParams ();
		defaultParams.bulletObj = BulletObject;
		defaultParams.bulletSpeed = BulletSpeed;
		defaultParams = new DefaultParams ();
		defaultParams.attackInterval = AttackInterval;
		defaultParams.shotsPerAttack = ShotsPerAttack;
		defaultParams.shotCooldown = ShotCoolDown;
		defaultParams.defaultTag = transf.tag;
		defaultParams.defaultLayer = gameObj.layer;
		gameObj.SetActive (false);
	}

	private void ResetPlane()
	{
		splineAnimator.Reset ();
		collDeadState = false;
		transf.tag = defaultParams.defaultTag;
		AttackInterval = attackIntervalCount = defaultParams.attackInterval;
		ShotsPerAttack = ShotsPerAttackCount = defaultParams.shotsPerAttack;
		ShotCoolDown = defaultParams.shotCooldown;
		gameObj.layer = defaultParams.defaultLayer;
		BulletObject = defaultParams.bulletObj;
		BulletSpeed = defaultParams.bulletSpeed;
		GetComponent<AudioSource>().Stop ();
		GetComponent<AudioSource>().clip = bulletSound;
		gameObj.SetActive (false);
	}

	public void InitPlane( float attackInterval, float shotCd, int shotsPerAttack, Transform trajectory, float animatorSpeed )
	{
		destroyed = false;
		transf.parent = trajectory;
		splineAnimator.spline = trajectory.GetComponent<Spline> ();
		splineAnimator.speed = animatorSpeed;
		splineAnimator.enabled = true;
		splineAnimator.InitOnSpline ();
		EndLevelPopup.instance.allEnemyCnt++;
		shootEnable = PlayerScript.instance.shootEnable;
		if( attackInterval != 0 )
		{
			attackIntervalCount = AttackInterval = attackInterval;
		}
		else
		{
			attackIntervalCount = AttackInterval;
		}

		shotCooldown = 0;
		if( shotCd != 0 )
		{
			 ShotCoolDown = shotCd;
		}

		if( shotsPerAttack != 0 )
		{
			ShotsPerAttackCount = ShotsPerAttack = shotsPerAttack;
		}
		else
		{
			ShotsPerAttackCount = ShotsPerAttack;
		}
		rigidBodyCached.isKinematic = true;
		gameObj.SetActive (true);
		//Debug.Log( AttackInterval +"  "+attackInterval+"  "+AttackInterval * 60f );
	}

	private void TakeDamage( int damage )
	{
		Health -= damage;
		CheckOnDestroy ();
		//SetDamageColor();
	}

	public bool CheckOnScreen()
	{
		Vector3 screenPoint = Camera.main.WorldToScreenPoint( transf.position );
		return( screenPoint.x >= 0 && screenPoint.x <= Camera.main.pixelWidth 
		   && screenPoint.y >= 0 && screenPoint.y <= Camera.main.pixelHeight );
	}

    private void Update() {
        if (destroyed) {
            return;
        }
		if (setDefaultTexture) 
		{
			setBacktimer += Time.deltaTime;
			if( setBacktimer >= setBackTime )
			{
				SetDefaultTextures();
			}
		}
		//dotDebug = Vector3.Dot( PlayerScript.instance.transform.position - transform.position, transform.forward );
        if (shootEnable) {
            if (Health > 0) {
                //Shadow.transform.position = new Vector3(targetPoint.x, shadowHeight, targetPoint.z);
                //Shadow.transform.eulerAngles = new Vector3(90, 180+transform.eulerAngles.y, transform.eulerAngles.z);

               // Vector3 posOnScreen = Camera.mainCamera.WorldToViewportPoint(transform.position);

                //if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) 
                {
                    if (attackIntervalCount > 0) 
					{
                        attackIntervalCount -= Time.deltaTime;
						if( attackIntervalCount < 0 )
						{
							seePlayer = CheckOnScreen();
						}
                    } else {
						if( launchType == gunLaunch.SEEPLAYER )
						{
							if( !seePlayer )//РєР°Рє С‚РѕР»СЊРєРѕ РјРѕР¶РµРј СЃС‚СЂРµР»СЏС‚СЊ РґРµР»Р°РµРј Р·Р°Р»Рї, Р° РЅРµ РѕРґРёРЅ РІС‹СЃС‚СЂРµР»
							{
								seePlayer = CheckOnScreen();
							}
						}
						else
						{
							seePlayer = true;
						}
						if( seePlayer )
						{
	                        if ( shotCooldown > 0 ) 
							{
	                            //Decrease the cooldown counter
	                            shotCooldown -= Time.deltaTime;
	                        } 
							else 
							{
	                            if (gunCount == 1) 
								{
	                                Shoot(0f);
	                            } 
								else 
								{
	                                Shoot(-2.8f);
	                                Shoot(2.8f);
	                            }
	                             ShotsPerAttackCount--;
								if( ShotsPerAttackCount == 0 )
								{
	                                ShotsPerAttackCount = ShotsPerAttack;
	                                attackIntervalCount = AttackInterval;
									shotCooldown = 0;
	                            }
	                        }
						}
                    }
                }
            }
        }
    }

	private ParentedEffect[] _fires;
	private void CreateDeadFires()
	{
		_fires = new ParentedEffect[DeadFireContainer.Length];
		for(var i = 0; i < DeadFireContainer.Length; i++)
		{
			_fires[ i ] = ( ParentedEffect )EffectsManager.PlayEffect( DeadFirePref.name, DeadFireContainer[i] );
		}
	}

	private void CheckOnDestroy()
	{
		if ( Health <= 0 && !destroyed ) {
			destroyed = true;
			addCoin();
			//70 persents explosion, 30% - add physics and move down
			
			if ( Random.Range( 0, 100f ) > 70 ) {
				
				CreateDeadFires();
				
				//  ModelBody.renderer.material.SetColor("_Color", Color.black);
				splineAnimator.enabled = false;
				
				gameObj.tag = "Untagged";

				gameObj.layer = LayerMask.NameToLayer("EnemyDestroy");
				rigidBodyCached.useGravity = true;
				rigidBodyCached.isKinematic = false;
				rigidBodyCached.mass = 10;
				rigidBodyCached.velocity = transf.forward.normalized * 40;
				rigidBodyCached.constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
				collDeadState = true;
				GetComponent<AudioSource>().Stop();
				GetComponent<AudioSource>().clip = fallingSound;
				GetComponent<AudioSource>().Play();
				dir = (Random.Range(0, 1) == 1) ? Vector3.back : Vector3.forward;
				rigidBodyCached.AddForceAtPosition( GetComponent<Rigidbody>().velocity * -0.1f, transf.TransformPoint(0, -10, 0) );
				transf.Rotate (dir, 5, Space.Self);
			} else {
				//Create an explosion
				PlayExplosion();
			}


		    
		}
	}

    private void OnCollisionEnter(Collision collision) {
       // Debug.Log("Collision" + collision.gameObject.tag);

        if (collDeadState && ((collision.transform.tag == "LevelTerrain") || (collision.transform.tag == "Water")) || ( collision.transform.tag == "LevelHouse" )) {
            PlayExplosion();
            return;
        }
 
        if (collision.gameObject.CompareTag( "Player" ) ) {
            PlayExplosion();
			PlayerScript.instance.HitPlayer( contactDamage );
        }
    }

    private void PlayExplosion()
    {
		if (_fires != null) {
			for (int i = 0; i < _fires.Length; i++) {	
				_fires [i].Detach ();
			}
		}
		EndLevelPopup.instance.deadEnemyCnt++;
		EffectsManager.PlayEffect ( ExplosionEffect.name, transf.position + (new Vector3(0, 1.6f, 0)), transf.rotation );
		ResetPlane ();
    }



    public void applyDamage(float hp) {
        Health -= hp;
		CheckOnDestroy ();
    }

    //flashing after damage
    private void SetDamageColor() {
        if (Health > 0) {
            if (ModelBody) {
                ModelBody.GetComponent<Renderer>().material.SetColor("_Color", new Color(255f, 255f, 255f, 255f));
            }
			setBacktimer = 0;
			setDefaultTexture = true;
        }
    }

	private bool	setDefaultTexture;
	private float 	setBacktimer;
	const 	float	setBackTime = 0.1f;
    private void SetDefaultTextures() 
	{
		setDefaultTexture = false;
        if (ModelBody) {
            if (Health > 0) {
                ModelBody.GetComponent<Renderer>().material.SetColor("_Color", defaultColor);
            } else {
                ModelBody.GetComponent<Renderer>().material.SetColor("_Color", Color.black);
            }
        }
    }

    public Vector3 AddForce;
    //Shoot
    private void Shoot(float Offset) 
	{
		//audio.Play();
        if (BulletObject)
		{
            if (launchType == gunLaunch.AUTO)
            {
                //Debug.Log("transform.forward * BulletSpeed * 100 = " + transform.forward * BulletSpeed * 100);
				AddForce = transf.forward * BulletSpeed;
            }
            else
            {
               // var player = GameObject.Find("Player");
              //  if (player && Vector3.Dot(player.transform.position - transform.position, transform.forward) > 2)//СЃР°РјРѕР»РµС‚ РЅР°С…РѕРґРёС‚СЃСЏ РїРµСЂРµРґ СЃР°РјРѕР»РµС‚РѕРј РёРіСЂРѕРєР°
                {
					AddForce = ( PlayerScript.instance.transform.position - transf.position ).normalized * BulletSpeed;
                }
            }

			BulletBase bullet = BulletsManager.GetFreeBullet( BulletObject.name );
			if( bullet != null )
			{
				bullet.Launch( transf.position, transf.rotation, AddForce, bulletDamage );
			}
            shotCooldown = ShotCoolDown;
        }
    }

    private void addCoin() {
        if (monetObj) {
            int rand = Random.Range(0, 100);
            if (rand <= monetChance) {
				Vector3 pos;
                for (int i = 0; i < monetCount; i++) {
					pos = new Vector3(Random.Range(-4, 4), transf.localPosition.y, Random.Range(-4, 4));
					MedalsManager.Spawn( monetObj.name, transf.TransformPoint( pos ) );
                }
            }
        }
    }


    public void ShootEnable() {
        shootEnable = true;
    }

    public void ShootDisable() {
        shootEnable = false;
    }
}
