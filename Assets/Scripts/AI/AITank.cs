using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AITank : MonoBehaviour
{
	//Params
    public float 		Health = 1; //health
    public float 		attackRadius = 50;
	
	public bool 		BulletAIRocket = false;
	
	//GUN
	public float 		rotationSpeed = 90.0f;
	public float 		tiltSpeed = 45.0f;
	public float  		minTilt = -70f;
	public float  		maxTilt = 10f;
	public Transform 	Turret;
	public Transform 	tiltableGun;
	
	public Transform[] FirePos;
	
    //Destroy Section
	public Transform 	DestroyModel; 
    public Transform 	ExplosionEffect;
    public bool 		Crashed = false;

    //Bullet
    public Transform 	BulletObject; 
    public Transform 	MuzzleObject;
    public int 			BulletSpeed = 5;
    public float 		ShotCoolDown = 20; 
    public float 		AttackInterval = 70; 
    public int 			ShotsPerAttack = 3;

    public Transform 	monetObj;
    public int 			monetCount = 1;
    private int 		monetChance = 0;

    private float 		shotCooldown; 
	private float 		attackCooldown;
    private int 		ShotsPerAttackCount;

    private EndLevelPopup popup;
    private Vector3 	dir;
    private float 		angleToTarget;

    public bool 		shootEnable = true;
    public bool 		DRAWRadius = false;
	private Transform	transf;

    void OnDrawGizmos()
    {
        if (DRAWRadius)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, attackRadius);
        }
    }



    //Constructor
    void Awake()
    {
		transf = transform;
        ShotsPerAttackCount = ShotsPerAttack;
        attackCooldown = AttackInterval;
        shotCooldown = ShotCoolDown;
    }
	
    void Start()
    {
        GameObject EndLevel = GameObject.Find("EndLevel");
        popup = EndLevel.GetComponent<EndLevelPopup>();
        popup.allEnemyCnt++;
		
		shootEnable = PlayerScript.instance.shootEnable;
        if ( LevelConroller.instance.currentDifficultyLevel == 0 )
        {
			Health = Health - (Health * LevelConroller.instance.easyLevel);
        }
		else if ( LevelConroller.instance.currentDifficultyLevel == 2 )
      	{
			Health = Health + ( Health * LevelConroller.instance.hardLevel );
        }
		monetChance = LevelConroller.instance.veniclesChancePer;
    }

    public void BridgeDestroy()
    {
        popup.deadEnemyCnt++;
		EffectsManager.PlayEffect ( ExplosionEffect.name, transf.position + (new Vector3(0, 2.6f, 0)), transf.rotation );
		Transform deathModel = Instantiate(DestroyModel, transf.position, transf.rotation) as Transform;
        GameObject obj = deathModel.gameObject;
        obj.transform.parent = null;
        obj.AddComponent<Rigidbody>();
        obj.AddComponent<BoxCollider>();
        obj.GetComponent<Rigidbody>().mass = 150f;
        obj.GetComponent<Rigidbody>().useGravity = true;
        obj.GetComponent<Rigidbody>().AddForce(new Vector3( Random.value, Random.value, Random.value ) * 2000f, ForceMode.Impulse);

        SplineVenicle spline = gameObject.GetComponent<SplineVenicle>();

        if (spline)
        {
            deathModel.rotation = spline.getRotation();
        }

        shootEnable = false;

        Destroy(gameObject);
    }

	Vector3 posOnScreen;
    void Update()
    {  
        if (Health > 0)
        {
			if(shootEnable)
			{

				posOnScreen = Camera.main.WorldToViewportPoint(transf.position);
				if ( posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0 ) 
				{
					if(Turret)
					{
						//rotate turret
						dir = PlayerScript.instance.transform.position-Turret.position;
						angleToTarget = Mathf.Abs(90 - Vector3.Angle(dir,Turret.right));
						Turret.localEulerAngles = new Vector3(
							Turret.localEulerAngles.x,
							Turret.localEulerAngles.y + Mathf.Clamp(rotationSpeed * Time.fixedDeltaTime * Mathf.Sign(Vector3.Dot(Vector3.Cross(Turret.forward, dir), Turret.up)), -angleToTarget, angleToTarget),
							Turret.localEulerAngles.z);
					}
					
					if(tiltableGun)
					{
						//tilt gun
						dir = PlayerScript.instance.transform.position - tiltableGun.position;
						angleToTarget = Mathf.Abs(90 - Vector3.Angle(dir,tiltableGun.up));
						tiltableGun.localEulerAngles = new Vector3(
							tiltableGun.localRotation.eulerAngles.x + Mathf.Clamp(tiltSpeed * Time.fixedDeltaTime * Mathf.Sign(Vector3.Dot(Vector3.Cross(tiltableGun.forward, dir), tiltableGun.right)), -angleToTarget, angleToTarget),
							tiltableGun.localRotation.eulerAngles.y,
							tiltableGun.localRotation.eulerAngles.z);
					}
					
					if(tiltableGun.localEulerAngles.x > 180 && tiltableGun.localEulerAngles.x < Mathf.Repeat(minTilt,360))
						tiltableGun.localEulerAngles = new Vector3(minTilt, tiltableGun.localEulerAngles.y, tiltableGun.localEulerAngles.z);
					if(tiltableGun.localEulerAngles.x < 180 && tiltableGun.localEulerAngles.x > Mathf.Repeat(maxTilt,360))
						tiltableGun.localEulerAngles = new Vector3(maxTilt, tiltableGun.localEulerAngles.y, tiltableGun.localEulerAngles.z);

					if ( Vector3.Distance( transf.position, PlayerScript.instance.transform.position ) < attackRadius ) 
						{
			                if (attackCooldown > 0) 
		                    {
			                    attackCooldown -= Time.deltaTime;
			                } 
							else 
							{
			                    if (shotCooldown > 0) 
								{
			                        shotCooldown -= Time.deltaTime;
			                    } 
								else 
								{         
									for(int i = 0; i < FirePos.Length; i++)
									{
										Shoot(FirePos[i]);
									}
									ShotsPerAttackCount --;
			                        if (ShotsPerAttackCount == 0) 
									{
			                            ShotsPerAttackCount = ShotsPerAttack;
			                            attackCooldown 		= AttackInterval;
			                        }
			                    }
			                }
					}
	           }
			}

        }
    }
	
	public void applyDamage(float hp)
	{
		Health -= hp;	
		CheckHealth ();
	}

	
	private void TakeDamage( int damage )
	{
		Health -= damage;
		CheckHealth ();
	}

	private void CheckHealth()
	{
		if (!Crashed)
		{
			if( Health <= 0 )
			{
				Crashed = true;
				popup.deadEnemyCnt++;
				
				addCoin();
				//Create an explosion
				if(ExplosionEffect) EffectsManager.PlayEffect ( ExplosionEffect.name, transf.position + (new Vector3(0, 18f, 0)), transf.rotation );;
				
				//SplineVenicle spline = this.gameObject.GetComponent<SplineVenicle>();
				
				if (DestroyModel) {
					DestroyedObjectsManager.Spawn( DestroyModel.name, transf.position, transf.rotation );
				}
				
				Destroy(gameObject);
			}
		}
	}

    //Shoot
  	private void Shoot(Transform FirePos)
    {
        if (BulletObject)
        {
            Vector3 dir =  PlayerScript.getPosition - FirePos.position;
           // Debug.DrawRay(Turret.transform.position, -dir, Color.green, 50);
            if (MuzzleObject)
            {
				Instantiate(MuzzleObject, FirePos.position, Quaternion.LookRotation(-dir.normalized) );
            }	

			BulletBase bullet = BulletsManager.GetFreeBullet( BulletObject.name );
			if( bullet != null )
			{		
				bullet.Launch( FirePos.position, Quaternion.LookRotation( dir.normalized ), ( dir.normalized * BulletSpeed ) );
			}
            shotCooldown = ShotCoolDown;
        }
    }

    private void addCoin()
    {
        if (monetObj)
        {
            int rand = Random.Range(0, 100);
            if (rand <= monetChance)
            {
                for (int i = 0; i < monetCount; i++)
                {
					MedalsManager.Spawn( monetObj.name, transf.TransformPoint( new Vector3( Random.Range(-4, 4), transf.localPosition.y, Random.Range(-4, 4)) ) );
                }
            }
        }
    }

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
