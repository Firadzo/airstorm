using UnityEngine;
using System.Collections;

public class AIRocket : MonoBehaviour {
	
	public GameObject 	explosion;
	public int 			timeOut= 3;
    public float 		AILaunchTimeOut = 1;
	public int 			speed = 5; 
	public float 		Damage = 1;

    public bool AILaunchPause = false;

    private bool disablePause = false;
	private bool shootEnable = true;


	void  Start (){
		Invoke("KillSelf", timeOut);

		shootEnable = PlayerScript.instance.shootEnable;
        if ( LevelConroller.instance.currentDifficultyLevel == 0 )
        {
			Damage = Damage - (Damage * LevelConroller.instance.easyLevel);
        }
		else if ( LevelConroller.instance.currentDifficultyLevel == 2 )
        {
			Damage = Damage + (Damage * LevelConroller.instance.hardLevel);
        }
	}
	
	void Update()
	{
		if(shootEnable)
		{
            if (AILaunchPause)
            {
                transform.LookAt(new Vector3(0, 90, 0));
                if (!disablePause) {
                    disablePause = true;
                    StartCoroutine(disableAIPause());
                }
            } else {
                //Vector3 dir = Player.position - transform.position;
                //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.FromToRotation(transform.position.normalized, dir.normalized), Time.deltaTime * 300);
               transform.LookAt( PlayerScript.getPosition );
            }
            transform.Translate( speed * Vector3.forward * Time.deltaTime ); 
		}
		else
		{
			Destroy(gameObject);
		}
	}
	
    IEnumerator disableAIPause() {
        yield return new WaitForSeconds(AILaunchTimeOut);
        AILaunchPause = false;
    }

	void  OnCollisionEnter ( Collision collision  ) {
        if ( !collision.gameObject.CompareTag( "Player" ) ) return;

		if (collision.contacts.Length > 0) {
			ContactPoint contact = collision.contacts [0];
			Quaternion rotation = Quaternion.FromToRotation (Vector3.up, contact.normal);
			EffectsManager.PlayEffect (explosion.name, contact.point, rotation);
		} else {
			EffectsManager.PlayEffect (explosion.name, transform.position );
		}
		
        PlayerScript.instance.HitPlayer( Damage );
        		
		Kill ();    
	}
	
	void KillSelf()
	{
		EffectsManager.PlayEffect ( explosion.name, transform.position, Quaternion.identity );
		ParticleSystem trail = GetComponentInChildren<ParticleSystem>();
		if ( trail != null )
		{
			trail.transform.parent = null;
			trail.enableEmission = false;
			Destroy( trail.gameObject, 2f );
		}
		Destroy(gameObject);
	}
		
	void  Kill (){	
		/*(ParticleEmitter emitter= GetComponentInChildren<ParticleEmitter>();
		if (emitter) emitter.emit = false;	
		transform.DetachChildren();*/
		Destroy(gameObject);
		ParticleSystem trail = GetComponentInChildren<ParticleSystem>();
		if ( trail != null )
		{
			trail.transform.parent = null;
			trail.enableEmission = false;
			Destroy( trail.gameObject, 2f );
		}
	}

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }

}
