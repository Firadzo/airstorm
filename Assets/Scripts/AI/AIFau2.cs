﻿using UnityEngine;
using System.Collections;

public class AIFau2 : MonoBehaviour {
	public int attackRadius = 100;
    public GameObject explosion;
    public int timeOut = 3;
    public float AILaunchTimeOut = 1;
    public int speed = 5;
    public int rotSpeed = 5;
    public float Damage = 1;
    public int explosionRadius = 10;
    public bool shootEnable = true;

    public Transform monetObj;
    public int monetCount = 1;
    private int monetChance = 0;

    private bool disablePause = false;
    private bool enableAI = false;

    private int defSpeed;
    private bool launch = false;
    private bool explosionStart = false;
	private Transform transf;

#if UNITY_EDITOR

    void OnDrawGizmosSelected() {
         Gizmos.color = Color.blue;
         Gizmos.DrawWireSphere(transform.position, attackRadius);      
    }
#endif
   
    void Start()
    {
		transf = transform;
        shootEnable = PlayerScript.instance.shootEnable;
        enableAI = false;
        defSpeed = speed;
        speed = speed / 3;
        if ( LevelConroller.instance.currentDifficultyLevel == 0)
        {
			Damage = Damage - (Damage * LevelConroller.instance.easyLevel);
        }
		else if ( LevelConroller.instance.currentDifficultyLevel == 2 )
        {
			Damage = Damage + (Damage * LevelConroller.instance.hardLevel);
        }
		monetChance = LevelConroller.instance.veniclesChancePer;

    }
  
    void Update()
    {
        if (shootEnable) {
            if (explosionStart) return;

            if (!launch && Vector3.Distance( transf.position, PlayerScript.getPosition ) < attackRadius)
            {
                launch = true;
                Invoke("Explosion", timeOut);
            }

            if (shootEnable && launch) {
                if (!disablePause) {
                    disablePause = true;
                    StartCoroutine(disableAIPause());
                }

                if (enableAI) {
                    //transform.LookAt(Player);
					Vector3 dir = PlayerScript.getPosition - transf.position;
					transf.rotation = Quaternion.Slerp(transf.rotation, Quaternion.FromToRotation(transf.position.normalized, dir.normalized), Time.fixedDeltaTime * rotSpeed);
                    speed = defSpeed;
                }
				transf.Translate(speed * Vector3.forward * Time.fixedDeltaTime);
                
            }

            
			if (!explosionStart && Vector3.Distance(transf.position, PlayerScript.getPosition ) < 5) {
                Explosion();
                explosionStart = true;
            }
        }
    }

    IEnumerator disableAIPause()
    {
        yield return new WaitForSeconds(AILaunchTimeOut);
        enableAI = true;
    }

    void Explosion()
    {
        addCoin();

		Vector3 explosionPosition = transf.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPosition, explosionRadius);

        foreach (var hit in colliders)
        {
            if (!hit) continue;

			if ( hit.CompareTag( "Player" ) ) {
				float distance = Vector3.Distance(hit.transform.position, explosionPosition);

                float hitPoints = 1.0f - Mathf.Clamp01(distance / explosionRadius);
                hitPoints *= Damage;

                PlayerScript.instance.HitPlayer( hitPoints );
			}
        }
		EffectsManager.PlayEffect ( explosion.name, transf.position, Quaternion.identity );

        ParticleEmitter emitter = GetComponentInChildren<ParticleEmitter>();
        if (emitter) emitter.emit = false;
        Destroy(gameObject);
    }

	
    private void addCoin()
    {
        if (monetObj)
        {
            int rand = Random.Range(0, 100);
            if (rand <= monetChance)
            {
                for (int i = 0; i < monetCount; i++)
                {
					Vector3 pos = new Vector3(Random.Range(-4, 4), transf.localPosition.y, Random.Range(-4, 4));
					MedalsManager.Spawn( monetObj.name, transf.TransformPoint( pos ) );
                }
            }
        }
    }


    public void ShootEnable() {
        shootEnable = true;
    }

    public void ShootDisable() {
        shootEnable =  false;
    }
}
