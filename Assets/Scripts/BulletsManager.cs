﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BulletData
{
#if UNITY_EDITOR
	[HideInInspector]
	public	string		name;
#endif
	public 	GameObject	bulletPrefab;
	public	int			precreatedCount;

}

public class BulletsManager : MonoBehaviour {

	public BulletData[]		bulletsData;
	private Transform		transf;

	static BulletsManager		instance;
	static Dictionary< string, List< BulletBase > > bullets;

	public static Transform getTransform{ get { return instance.transf; } }


#if UNITY_EDITOR

	void OnDrawGizmosSelected()
	{
		if( bulletsData != null )
		{
			for( int i = 0; i < bulletsData.Length; i++ )
			{
				if( bulletsData[ i ].bulletPrefab != null )
				{
					bulletsData[ i ].name = bulletsData[ i ].bulletPrefab.name;
				}
			}
		}
	}
#endif

	void Awake () 
	{
		instance = this;
		transf = transform;
		bullets = new Dictionary<string, List<BulletBase>>();
		int j;
		for( int i = 0; i < bulletsData.Length; i++ )
		{
			if( !bullets.ContainsKey( bulletsData[ i ].bulletPrefab.name ) )
			{
				bullets.Add( bulletsData[ i ].bulletPrefab.name, new List<BulletBase>() );
			}
			for( j = 0; j < bulletsData[ i ].precreatedCount; j++ )
			{
				InstantiateBullet( i );
			}
		}
	}

	private BulletBase InstantiateBullet( int bulletIndex )
	{
		//if( bulletsData[ bulletIndex ].bulletPrefab.name == "ZenitBullet"){Debug.Log("Zenit");}
		BulletBase bullet = ( Instantiate( bulletsData[ bulletIndex ].bulletPrefab ) as GameObject ).GetComponent<BulletBase>();
		bullets[ bulletsData[ bulletIndex ].bulletPrefab.name ].Add( bullet );
		return bullet;
	}


	public static BulletBase GetFreeBullet( string bulletName )
	{
		if( bullets.ContainsKey( bulletName ) )
		{
			for( int i = 0; i < bullets[ bulletName ].Count; i++ )
			{
				if( bullets[ bulletName ][ i ].isReady )
				{
					return bullets[ bulletName ][ i ];
				}
			}
			int bulletIndex = GetBulletIndexByName( bulletName );
			if( bulletIndex != -1 )
			{
				return instance.InstantiateBullet( GetBulletIndexByName( bulletName ) );	 
			}
		}
		return null;
	}

	private static int GetBulletIndexByName( string bulletName )
	{
		for( int i = 0; i < instance.bulletsData.Length; i++ )
		{
			if( instance.bulletsData[ i ].bulletPrefab.name == bulletName )
			{
				return i;
			}
		}
		return -1;
	}
}
