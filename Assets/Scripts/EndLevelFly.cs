﻿using UnityEngine;
using System.Collections;

public class EndLevelFly : MonoBehaviour {
	public 	Transform	firstNode;
	public 	float		time;
	private float 		timer;

	public 	Vector3		cameraOffset;
	public 	Vector3		cameraAngles;
	public 	AnimationCurve	cameraSetCurve;
	public 	float		cameraSetTime;
	private float		cameraSetTimer;
	private Vector3 	cameraStartPos;
	private Vector3 	cameraStartAngles;

	public 	float		endLevelDelay;// время после исчезновения самолета пока движется камера
	public	float		endLevelCamMoveSpeed;

	private Spline		spline;
	private Transform	playerTransf;
	private bool		startFly;
	private bool		endFly;
	private Transform	cameraTransf;
	private Vector3 	startPos;
	static EndLevelFly instance;

	// Use this for initialization
	void Awake () {
		instance = this;
		startFly = false;
		cameraTransf = Camera.main.transform;
		spline = GetComponent<Spline>();
	}

	void Start()
	{
		playerTransf = PlayerScript.instance.transform;
		PlayerScript.instance.UseEndLevelSpline = true;
	}

	public static void Activate()
	{
		instance.startFly = true;
		instance.firstNode.position = instance.playerTransf.position;
		instance.cameraStartPos =  instance.cameraTransf.localPosition - instance.playerTransf.position;
		instance.cameraStartAngles = instance.cameraTransf.localEulerAngles;
	}

	float ratio;
	// Update is called once per frame
	Vector3 cameraMoveDirection;
	float z;
	bool parented;
	void Update () {
		if( startFly )
		{
			timer += Time.deltaTime;
			ratio = timer / time;
			playerTransf.position = spline.GetPositionOnSpline( ratio );
			playerTransf.rotation = spline.GetOrientationOnSpline( ratio );

			if( !parented )
			{
				cameraSetTimer += Time.deltaTime;
				//cameraTransf.localPosition = Vector3.Lerp( cameraStartPos, cameraOffset, cameraSetTimer / cameraSetTime );
				cameraTransf.position = playerTransf.position + Vector3.Lerp( cameraStartPos, cameraOffset, cameraSetCurve.Evaluate( cameraSetTimer / cameraSetTime ) );
				cameraTransf.localEulerAngles = Vector3.Lerp( cameraStartAngles, cameraAngles, cameraSetTimer / cameraSetTime );
				if( cameraSetTimer >= cameraSetTime )
				{
					//cameraTransf.parent = playerTransf;
					parented = true;
					//cameraTransf.localEulerAngles = cameraAngles;
					/*Vector3 t = cameraTransf.localPosition;
					t.x = 0;
					cameraTransf.localPosition = t;*/

				}
			}
			else
			{
//				Debug.LogError("::");
				cameraTransf.position = playerTransf.position + cameraOffset;
			}

			if( timer >= time )
			{
				endFly = true;
				startFly = false;
				timer = 0;
			}
		}
		else if( endFly )
		{
			timer += Time.deltaTime;
			cameraTransf.position += Vector3.right * endLevelCamMoveSpeed * Time.deltaTime;
			if( timer >= endLevelDelay )
			{
				endFly = false;
				EndLevelPopup.instance.endLevel();
			}
		}
	}
}
