using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {
	
	public GameObject 	Player;
    public GameObject 	playerMove;
	public int 			height = 0;
    public int 			xOffset = 0;

	public int startY = 0;
	//public int startZ = 0;
   // public float deltaZ = 15f;
    public int defaultHeight = 0;

    private Quaternion startRotation;
	private Transform	transf;
   // private float zOffset = 0;

    public bool playerIsConrol = false;

    private const float _Weight_Min = 0.1f;
    private const float _Weight_Max = 0.8f;

	void Awake()
	{
		transf = transform;
		//transf.eulerAngles = new Vector3 ();
	}

	void Start () {
	    defaultHeight = height;
       // startZ = (int)Player.transform.position.z;
        startY = (int)Player.transform.position.y;
		startRotation = transf.rotation;
	    //transform.position = new Vector3(Player.transform.position.x + xOffset, (startY + height), startZ);
	}


  //  private bool _f = false;
	
	void Update () {

	    UpdatePosition();
	}

    public void UpdatePosition()
    {
		if( PlayerScript.endLevelReached ){ return; }
		transf.position = new Vector3(playerMove.transform.position.x + xOffset, (startY + height), playerMove.transform.position.z); // + GetComponent<TBPinchZoom>().zoomAmount * transform.forward;
    }

    public void setToDefault() {
        height = defaultHeight;
		transf.rotation = startRotation;
    }

    public void setHeight(int value) {
        height = value;
    }

    public void setLeftRightMove(float value) {
        //transform.position = new Vector3(transform.position.x, transform.position.y, value);
        //zOffset = value;
    }



#if UNITY_EDITOR
	public bool check = false;
	public Transform checkingTransf;
	void OnDrawGizmosSelected()
	{
		if( check && checkingTransf != null )
		{
			check = false;
			Debug.Log( CheckPointVisibleToCam( checkingTransf.position ) );
		}

	}

	public bool CheckPointVisibleToCam( Vector3 point )
	{
		Vector3 screenPoint = Camera.main.WorldToScreenPoint( point );
		return( screenPoint.x >= 0 && screenPoint.x <= Camera.main.pixelWidth 
		       && screenPoint.y >= 0 && screenPoint.y <= Camera.main.pixelHeight );
	}
#endif
}
