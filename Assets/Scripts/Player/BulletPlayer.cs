using UnityEngine;

public class BulletPlayer : BulletBase {
    public Transform 	hitEffect;
    public AudioClip 	HitSound;
    public float 		Damage = 1;
    public float 		Speed = 1;

    public bool 		itIsDuga = false;
	static LayerMask 	layerMask;

	override protected void Awake()
	{
		layerMask = 1 << 14;
		base.Awake();
	}

     void Update() {
         if (shootEnable) {
             transf.Translate( Speed * Vector3.forward * Time.fixedDeltaTime );
             checkOnScreen();
             checkCollision();
         }
     }


	private Vector3 posOnScreen;
    private void checkOnScreen() {
		posOnScreen = Camera.main.WorldToViewportPoint( transf.position );
        if (posOnScreen.y > 1 || posOnScreen.x < 0 || posOnScreen.x > 1 ) {
			obj.SetActive( false );
        }
    }

    private void checkCollision() {
//		if (!userRaycast) {
//			return;
//		}
		posOnScreen = Camera.main.WorldToScreenPoint( transf.position );
		Ray ray = Camera.main.ScreenPointToRay(posOnScreen);
        var bullerRay = new Ray(ray.origin, ray.direction);

        RaycastHit hit;

        if (Physics.Raycast(bullerRay, out hit, 1000, layerMask))
        {
           // Debug.DrawLine(bullerRay.origin, hit.point, Color.red);

            if (hit.transform.CompareTag( "Enemy" ) ) {
                //  Debug.Log(hit.transform.name);
                if (hitEffect) {	 
					//create an explosionObject
					EffectsManager.PlayEffect( hitEffect.name, hit.point );
                }

                obj.SetActive( false ); //remove the object from the scene
				hit.transform.SendMessage( "TakeDamage", Damage, SendMessageOptions.DontRequireReceiver );
            }
        }
    }



    private void OnTriggerEnter(Collider collision) {
        if (!itIsDuga) {
            return;
        }
		if (collision.GetComponent<Collider>().CompareTag( "Bullet" ) || collision.GetComponent<Collider>().CompareTag( "Player" ) 
		    || collision.GetComponent<Collider>().CompareTag( "Rocket" ) || collision.GetComponent<Collider>().CompareTag( "Bomb" ) )
        {
            // Destroy(gameObject);
            return;
        }
        if (HitSound) {
            GetComponent<AudioSource>().PlayOneShot(HitSound);
        }

        //If we hit either an enemy or the player do the following...
        if (collision.GetComponent<Collider>().CompareTag( "Enemy" ) )
        {
            if (hitEffect)
            {
                //Instantiate(hitEffect, pos, rot); //create an explosionObject
            }

            //Destroy(gameObject); //remove the object from the scene

			collision.transform.SendMessage( "TakeDamage", Damage, SendMessageOptions.DontRequireReceiver );
        }
    }
    
	public override void Launch (Vector3 launchPos, Vector3 angle, float speed)
	{
		Speed = speed;
		base.Launch ( launchPos, angle, speed );
	}

	public override void Launch (Vector3 launchPos, Transform target, float speed)
	{
		Speed = speed;
		base.Launch ( launchPos, target, speed );
	}

	public override void Launch (Vector3 launchPos, Quaternion launchRot, float speed)
	{
		Speed = speed;
		base.Launch ( launchPos, launchRot, speed );
	}

    private bool shootEnable = true;
    public void ShootEnable() {
        shootEnable = true;
    }

    public void ShootDisable(){
        shootEnable = false;
    }
}