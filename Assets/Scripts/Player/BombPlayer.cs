using UnityEngine;
using System.Collections;

public class BombPlayer : MonoBehaviour {

    public float ExplosionOFFSET = 10.0f;
        
	public GameObject ExplosionEffect;
    public GameObject WaterExplosionEffect;
    public GameObject GroundEff;

	public float explosionRadius = 5.0f;
	///public float explosionPower= 10.0f;
	public float explosionDamage = 100.0f;
	public float explosionTime = 0.1f;
	public float destroyObjTime = 0.1f;

    private GameObject exposionEffect;
	// Use this for initialization
    void Start() {
      //  explosionRadius = explosionDamage / 2f;
        //StartCoroutine(Detonate(explosionTime));
        //destroyObjTime = explosionTime+0.1f;
        GetComponent<AudioSource>().minDistance = 400f;
    }

    //IEnumerator Detonate(float time) {
     //   yield return new WaitForSeconds(time);
      //  Explosion();		
//}
	/*void OnCollisionEnter (Collision collision  ){
		Debug.Log("Collision");

		Explosion();
		ContactPoint contact = collision.contacts[0];
		Quaternion rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);
	    Instantiate (ExplosionEffect, contact.point, rotation);
			
	}*/
	
	void OnTriggerEnter(Collider other) 
	{
		if(other.tag == "LevelTerrain" || other.tag == "Bridge"  || other.tag == "Enemy")
		{
			 Explosion(false);
			if( GroundEff ) EffectsManager.PlayEffect( GroundEff.name, new Vector3(transform.position.x, transform.position.y + ExplosionOFFSET, transform.position.z) );
		}

        if (other.tag == "Water") 
        {
            Explosion(true);
        }
    }

   

	public void Explosion(bool water) 
    {
	    if (water) 
        {
	        if (WaterExplosionEffect) 
            {
                exposionEffect = Instantiate(WaterExplosionEffect, new Vector3(transform.position.x, transform.position.y + ExplosionOFFSET, transform.position.z),
                    Quaternion.identity) as GameObject;
	        } 
            else 
            {
                if (ExplosionEffect)
                {
					EffectsManager.PlayEffect( ExplosionEffect.name, new Vector3(transform.position.x, transform.position.y + ExplosionOFFSET, transform.position.z ), Quaternion.identity );
                }
	        }
	    } 
        else
        {
            if (ExplosionEffect) 
            {
				EffectsManager.PlayEffect( ExplosionEffect.name, new Vector3(transform.position.x, transform.position.y + ExplosionOFFSET, transform.position.z ), Quaternion.identity );
            }
	    }
        Destroy(exposionEffect, 3);

	    Vector3 explosionPosition = transform.position;
		Collider[] colliders = Physics.OverlapSphere (explosionPosition, explosionRadius);
		
		foreach( Collider hit in colliders ) 
		{
			if (!hit) continue;
					
			//float distance= Vector3.Distance(hit.transform.position, explosionPosition);

    	   // float hitPoints = 1.0f - Mathf.Clamp01(distance / explosionRadius);
			//hitPoints *= explosionDamage;

			hit.gameObject.SendMessageUpwards("applyDamage", explosionDamage, SendMessageOptions.DontRequireReceiver);
			
		}
	
	    if (GetComponent<ParticleEmitter>()) 
		{
	        GetComponent<ParticleEmitter>().emit = true;
			//yield return new WaitForSeconds(0.5f);
			GetComponent<ParticleEmitter>().emit = false;
	    }

	    // destroy the explosion
        if (gameObject.transform.root) 
        {
            Destroy(gameObject.transform.root.gameObject, destroyObjTime);
        } 
        else 
        {
            Destroy(gameObject, destroyObjTime);
        }
	}


    public void ShootEnable() {
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
        gameObject.GetComponent<Rigidbody>().useGravity = true;
    }

    public void ShootDisable()
    {
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        gameObject.GetComponent<Rigidbody>().useGravity = false;
    }

#if UNITY_EDITOR

	void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere( transform.position, explosionRadius );
	}
#endif
}
