﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class PlayerMoveController : MonoBehaviour {

    private const float _SPEED_DEFAULT = 25f;
    public GameObject PlayerModel;
    public float Speed = _SPEED_DEFAULT;

    public AnimationCurve changeSpeedCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

    //public Collider Collider;
    // Use this for initialization

    // размеры границы по вертикали в рамках экрана в пределах которых может летать самолет
    public float _Hieght_Min = 0.2f;
    public float _Hieght_Max = 0.9f;

    // размеры границы по горизонтали в рамках экрана в пределах которых может летать самолет
    public float _Weight_Min = 0.2f;
    public float _Weight_Max = 0.8f;

	private float player_Height_Min;
	private float player_Height_Max;
	private float player_Width_Min;
	private float player_Width_Max;

    // размеры границы по горизонтали в пределах которых может перемещаться камера
    public float _Border_Horozontal_Min = 1190f;
    public float _Border_Horozontal_Max = 1230f;

    private GameObject _empty;

    private PlayerCamera PlayerCamera;

    private bool _canMoveByZ = false;
    private int _signZ = 0;

    private float _dlZ;

    private PlayerAcceleratorController _acceleratorController;
    private PlayerDragController 		_dragController;

    private Dictionary<GameObject,float> _rotateX;
    private Dictionary<GameObject,float> _rotateZ;

	public static PlayerMoveController	instance{ get; private set; }

	private Rigidbody	rigidBodyCach;
	private bool		paused;

#if UNITY_EDITOR
	Vector3 t;
	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		t = transform.position;
		Gizmos.DrawLine( new Vector3( t.x - 100f, t.y, _Border_Horozontal_Min ), new Vector3( t.x + 100f, t.y, _Border_Horozontal_Min ) );
		Gizmos.DrawLine( new Vector3( t.x - 100f, t.y, _Border_Horozontal_Max ), new Vector3( t.x + 100f, t.y, _Border_Horozontal_Max ) );
	}
#endif

    void Awake()
    {
		rigidBodyCach = GetComponent<Rigidbody> ();
		instance = this;
        //_box = GetComponentInChildren<BoxCollider>();
        _empty = new GameObject();

        _rotateX = new Dictionary<GameObject, float>();
        _rotateZ = new Dictionary<GameObject, float>();
		player_Height_Min = _Hieght_Min - 0.01f;
		player_Height_Max = _Hieght_Max + 0.01f;
		player_Width_Min = _Weight_Min - 0.01f;
		player_Width_Max = _Weight_Max + 0.01f;
    }

	void Start () {

	    if (PlayerCamera == null)
	    {
	        PlayerCamera = GameObject.FindObjectOfType<PlayerCamera>();
	    }

	    //if (Collider == null) Collider = GetComponentInChildren<Collider>();

        if (_acceleratorController == null) _acceleratorController = FindObjectOfType<PlayerAcceleratorController>();
	    if (_dragController == null) _dragController = FindObjectOfType<PlayerDragController>();
	}

	public float GetScreenCenter()
	{
		return ( _Border_Horozontal_Max + _Border_Horozontal_Min ) / 2f;
	}

    public void LoadConrolParams()
    {
        if (_acceleratorController != null && _dragController != null)
        {
            _acceleratorController.enabled = SaveLoadParams.plControl == 0;
            _dragController.enabled = !_acceleratorController.enabled;
        }

    }

	public static void Pause( bool pause )
	{
		instance.paused = pause;
	}

	public void SetSpeed( float value )
	{
		Speed = value * 200f;
	}

	// Update is called once per frame
	Vector3 tempPos;
	void LateUpdate ()
	{
		if (paused) {
			return;
		}
		if( PlayerScript.instance.moveEnable )
		{
	   	 	LoadConrolParams();
			tempPos = rigidBodyCach.position;
			tempPos.x += Speed * Time.deltaTime;
			tempPos.z += ( _canMoveByZ ? _dlZ : 0 ) * Time.deltaTime * PlayerScript.instance.HorizontalSpeed;
			rigidBodyCach.position = tempPos;
		}
	}

    public void Move(Transform player, float x, float y, float z)
    {
        if (player != null)
        {
            x = x + (Speed) * Time.deltaTime;
            player.transform.position = Vector3.MoveTowards(player.transform.position, new Vector3(x, y, z), 100f * Time.deltaTime);        

            //player.transform.position = Vector3.Lerp(player.transform.position, new Vector3(x, y, z), 100 * Time.smoothDeltaTime);
        }
    }

    public bool CheckPositionByX(Transform player, float x)
    {
        _empty.transform.position = player.position;

        x = x + (Speed) * Time.deltaTime;

        _empty.transform.Translate(x, 0f, 0f);

        var pos = Camera.main.WorldToViewportPoint(_empty.transform.position);       
        return pos.y > _Hieght_Min && pos.y < _Hieght_Max;
    }

    

    public bool CheckPositionByZ(Transform player, float z)
    {
        _empty.transform.position = player.position;
        _empty.transform.Translate(0f, 0f, z);
        var pos = Camera.main.WorldToViewportPoint(_empty.transform.position);

        _dlZ = 0;
        _canMoveByZ = true;
        if ((pos.x <= _Weight_Min || pos.x >= _Weight_Max) && Math.Sign(z) == _signZ)
        {
            _dlZ = -z;

            if (transform.position.z + _dlZ <= _Border_Horozontal_Min || transform.position.z + _dlZ >= _Border_Horozontal_Max)
                _canMoveByZ = false;
        }

        if (pos.x > _Weight_Min && pos.x < _Weight_Max)
            _signZ = Math.Sign(z);


        return !_canMoveByZ && (pos.x <= _Weight_Min || pos.x >= _Weight_Max) ? false : true;
    }


    public Vector3 CheckPosition(Vector3 position)
    {
        var checkPos = Camera.main.WorldToViewportPoint(position);
		checkPos = new Vector3(checkPos.x < player_Width_Min ? player_Width_Min : (checkPos.x > player_Width_Max ? player_Width_Max : checkPos.x), 
		                       checkPos.y < player_Height_Min ? player_Height_Min : (checkPos.y > player_Height_Max ? player_Height_Max : checkPos.y), 
		                       checkPos.z);

        position = Camera.main.ViewportToWorldPoint(checkPos);
        position = new Vector3(position.x, transform.position.y, position.z);

        return position;
    }

    public void CheckAndMoveCameraByHorizontal(Vector3 posTo)
    {
        _dlZ = 0;
        _canMoveByZ = false;

			if( posTo.x <= _Weight_Min )
			{
				_dlZ = 1f;

			}
			else if( posTo.x >= _Weight_Max )
			{
				_dlZ = -1f;
			}
            if ((_dlZ == 1 && transform.position.z + _dlZ < _Border_Horozontal_Max) 
			    || (transform.position.z + _dlZ > _Border_Horozontal_Min && _dlZ == -1))
			{
                _canMoveByZ = true;    
			}
    }

    public bool IsBorderLeft(Transform player, float z)
    {
        _empty.transform.position = player.position;
        _empty.transform.Translate(0f, 0f, z);
        var pos = Camera.main.WorldToViewportPoint(_empty.transform.position);

        return pos.x <= _Weight_Min;
    }

	public bool IsOnXBorder()
	{
		return true;//( pos.x >= _Weight_Max || pos.x <= _Weight_Min );
	}

    public bool IsBorderRight(Transform player, float z)
    {
        _empty.transform.position = player.position;
        _empty.transform.Translate(0f, 0f, z);
        var pos = Camera.main.WorldToViewportPoint(_empty.transform.position);

        return pos.x >= _Weight_Max;
    }

    public bool IsBorder(Transform player, float z)
    {
        _empty.transform.position = player.position;
        _empty.transform.Translate(0f, 0f, z);
        var pos = Camera.main.WorldToViewportPoint(_empty.transform.position);

        return pos.x <= _Weight_Min || pos.x >= _Weight_Max;
    }

    

    private const float _Rotation_Horizontal_Min = -70;
    private const float _Rotation_Horizontal_Max = 70;

    private const float _X_Horizontal_Max = 0.5f;

    private const float _Rotation_Vertical_Min = 0;
    private const float _Rotation_Vertical_Max = 10;

    private const float _Z_Vertical_Max = 0.4f;


    public void PlayerRotate(float x, float z, GameObject playerModel = null)
    {
        playerModel = playerModel == null ? PlayerModel : playerModel;

        if (!_rotateX.ContainsKey(playerModel))
            _rotateX.Add(playerModel,0);

        if (!_rotateZ.ContainsKey(playerModel))
            _rotateZ.Add(playerModel, 0);

        if (playerModel != null)
        {
            float newRotateX = x*(_Rotation_Horizontal_Max/_X_Horizontal_Max);
            _rotateX[playerModel] = newRotateX < _Rotation_Horizontal_Min
                ? _Rotation_Horizontal_Min
                : (newRotateX > _Rotation_Horizontal_Max ? _Rotation_Horizontal_Max : newRotateX);

            float newRotateZ = z * (_Rotation_Vertical_Max / _Z_Vertical_Max);
            _rotateZ[playerModel] = newRotateZ < _Rotation_Vertical_Min
                ? _Rotation_Vertical_Min
                : (newRotateZ > _Rotation_Vertical_Max ? _Rotation_Vertical_Max : newRotateZ);

            playerModel.transform.rotation = Quaternion.Euler(-_rotateX[playerModel],
                180f,
                _rotateZ[playerModel]);
           // Debug.Log("3 playerModel = " + playerModel);
           // Debug.Log("playerModel = " + playerModel + " _rotateX = " + _rotateX[playerModel] + " _rotateZ = " + _rotateZ[playerModel]);
        }
    }
}
