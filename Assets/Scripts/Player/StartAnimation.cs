using UnityEngine;
using System.Collections;

public class StartAnimation : MonoBehaviour {

    public Spline spline;
    public Vector3 RotationFIX = Vector3.zero;
    public float ANGLE_FIX = 90f;

    public float speed = 1f;
    public float offSet = 0f;
    public WrapMode wrapMode = WrapMode.Clamp;

    public float passedTime = 0f;

    public bool enable = false;

   // private GameObject Player;

    void Start() 
    {
   //     Player = GameObject.Find("Player"); ; //Set player object
    }

    void FixedUpdate()
    {
        if (!enable) return;
      
        if (spline)
        {
            float posOnSplne = WrapValue(passedTime + offSet, 0f, 1f, wrapMode);

            if (posOnSplne > 0.98f) 
            {
                enable = false;
                GameObject camera = GameObject.Find("AnimCamera");
                if(camera)Destroy(camera);

                GameObject Player = GameObject.Find("Player"); ; //Set player object
                PlayerScript scr = Player.GetComponentInChildren<PlayerScript>();
                scr.moveEnable = true;
                scr.shootEnable = true;

				LevelConroller.spawnDisabled = false;

                transform.parent = Player.transform;

                transform.localPosition = new Vector3(0,0,0);

                //Fix prefab rotation
                transform.localEulerAngles = new Vector3(0f, 90f, 0f);
                return;
            }

            passedTime += Time.fixedDeltaTime * speed;

            transform.position = spline.GetPositionOnSpline(WrapValue(passedTime + offSet, 0f, 1f, wrapMode));

            Quaternion rot = spline.GetOrientationOnSpline(WrapValue(passedTime + offSet, 0f, 1f, wrapMode));
            Quaternion addHorizontalRot = Quaternion.AngleAxis(ANGLE_FIX, RotationFIX);

            transform.rotation = Quaternion.Slerp(transform.rotation, (rot * addHorizontalRot), Time.fixedDeltaTime * 3f);
        }
    }

    private float WrapValue(float v, float start, float end, WrapMode wMode)
    {
        switch (wMode)
        {
            case WrapMode.Clamp:
            case WrapMode.ClampForever:
                return Mathf.Clamp(v, start, end);
            case WrapMode.Default:
            case WrapMode.Loop:
                return Mathf.Repeat(v, end - start) + start;
            case WrapMode.PingPong:
                return Mathf.PingPong(v, end - start) + start;
            default:
                return v;
        }
    }
}
