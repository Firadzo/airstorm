using UnityEngine;
using System.Collections;

public class PlayerShield : MonoBehaviour {
    public float Heath = 0;
    public float lifeTime = 10;
    public bool superShield = false;
    private bool setRespawnFlag = false;
    private bool _isEnabled;

	private GameObject	obj;

    public bool IsEnabled {
        get { return _isEnabled; }
    }

    void Start() 
    {
		obj = transform.GetChild( 0 ).gameObject;
        disable();
        superShield = false;
        setRespawnFlag = false;
    }

    // Update is called once per frame
	void Update () {
        //no-time disable only superShield
        if (Heath <= 0 && !setRespawnFlag) 
        {
            setRespawnFlag = true;
            if (superShield) 
			{
                superShield = false;
				disable();
                StopCoroutine("DisableShield");
            }
        }
	}

    public void EnableShield() {
        superShield = false;
        setRespawnFlag = false;
        StartCoroutine(DisableShield());
        enable();
    }

    public void EnableSuperShield()
    {
        setRespawnFlag = false;
        superShield = true;
        enable();
    }

    IEnumerator DisableShield()
    {
        yield return new WaitForSeconds(lifeTime);
        disable();
    }

    private void enable()
    {
        _isEnabled = true;
		obj.SetActive( true );
       // gameObject.collider.enabled = true;
    }

    private void disable()
    {
        _isEnabled = false;
		obj.SetActive( false );
       // gameObject.collider.enabled = false;
    }
}
