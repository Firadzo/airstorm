﻿using UnityEngine;
using System.Collections;

public class PlayerCameraController : MonoBehaviour {

    public PlayerScript PlayerScript;
    public PlayerMoveController PlayerMoveController;

    // Use this for initialization
	void Start () {

        if (PlayerScript == null) PlayerScript = GameObject.FindObjectOfType<PlayerScript>();
        if (PlayerMoveController == null) PlayerMoveController = GameObject.FindObjectOfType<PlayerMoveController>();

        if (PlayerScript != null)
        {
            transform.position = new Vector3(PlayerScript.transform.position.x, 1229 + 100, PlayerScript.transform.position.z);
        }
	}
	
	// Update is called once per frame
	void Update () {

        if (PlayerScript != null && PlayerMoveController != null)            
        {
            transform.position = new Vector3(PlayerMoveController.transform.position.x, PlayerMoveController.transform.position.y, transform.position.z);
        }
	
	}
}
