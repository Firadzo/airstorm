using UnityEngine;
using System.Collections;

public class TurelBullet : MonoBehaviour {
	public GameObject Target;
	public float speed = 5.0f;
	public float Damage = 1.0f;
	
	// Update is called once per frame
	void Update () 
	{
	    if (shootEnable) {
	        if (Target) {
	            transform.LookAt(Target.transform.position);
	        }
	        transform.Translate(speed * Vector3.forward * Time.fixedDeltaTime);

	        checkOnScreen();
	    }
	}
	
	void checkOnScreen()
	{
		Vector3 posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
		if(posOnScreen.y > 1) Destroy(gameObject);	
	}
	
	void  OnCollisionEnter(Collision collision)
	{
		if( !collision.gameObject.CompareTag( "Enemy" ) ) return;
		
		collision.transform.SendMessage( "TakeDamage", Damage, SendMessageOptions.DontRequireReceiver );
		Destroy(gameObject);  
	}

    private bool shootEnable = true;
    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }

}
