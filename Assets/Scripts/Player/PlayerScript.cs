﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {
    
	const float		ICON_ONCD_ALPHA = 0.3f;
	const float		ICON_READY_ALPHA = 0.6f;

	public 	float	invulnerabilityTime;
	private float	invulnerabilityTimer;
	private bool	invulnerable;
	//Turel
   // public Transform defaultTurel;
    private Transform currentTurel;
    public 	GameObject desantButton;
    public 	Transform desantPrefab;

    public float turelSpeed = -1; //set in skill
    public float turelDamage = -1; //set in skill
    public float turelDelay = -1; //set in skill
    public int	 turelLevel = -1; //set in skill

    public TUREL_TYPE currentTUREL; //set in skill

    public enum TUREL_TYPE {
        NONE,
        GUN_2_SHOT_SKILL,
        GUN_3_SHOT_SKILL,
        GUN_4_SHOT_LINE_SKILL,
        GUN_4_SHOT_NO_LINE_SKILL,
        GUN_7_SHOT_LINE_SKILL,
        GUN_7_SHOT_NO_LINE_SKILL,
        GUN_ALL_SHOT_SKILL,
        SUPER_SHOOT
    }

    public Transform[] Turels; //9 level turels, 0 - default, 1 - GUN_2_SHOT_SKILL...etc

    //SKILLS
	[HideInInspector]
	public float gunDamage = 50f;

    //AITurel
    public Transform TurelBullet;
    public float 	TurretSkillSpeed = -1; //auto turret
    public float 	TurretSkillDamage = -1; //auto turret
    private bool 	turretEnable = false;
    public float 	turretDelay = 0.5f;
    public float 	TurelCoolDown = 20;
	private float 	turelSpeedShotCoolDown;

    public void EnableAutoTurret() {
        turretEnable = true;
        if (turretDelay > 0) {
            TurelCoolDown = turretDelay;
        }
    }

    //Rocket
    public Transform RocketObject;
    public float 	Interval_NO_AI = 10;
    public float 	ROCKET_NO_AI_Speed = -1;
    public float 	ROCKET_NO_AI_Damage = -1;
    public bool 	rocketEnable = false;

    public void EnableNoAIRocket() {
        rocketEnable = true;
        StartCoroutine(addRocket());
    }

    public void DisableNoAIRocket() {
        rocketEnable = false;
    }

    public Transform RocketAIObject;
    public float 	Interval_AI = 20;
    public float 	ROCKET_AI_Speed = -1;
    public float 	ROCKET_AI_Damage = -1;
    public bool 	rocketAIEnable = false;

    public void EnableAIRocket() {
        rocketAIEnable = true;
        StartCoroutine(addAIRocket());
    }

    public void DisableAIRocket() {
        rocketAIEnable = false;
    }

    //Engine
    private float	verticalSpeed = -1;
	private float 	horizontalSpeed = -1;
	private float	planeSpeed;

	public float VerticalSpeed
	{
		get{ return verticalSpeed; }
	}

	public float HorizontalSpeed
	{
		get{ return horizontalSpeed; }
	}

	public void Spawn( float speed, float health, Transform playerDestroyPlane, PlayerShield shield )
	{
		maxHeath = _health = health;
		PlayerDestroyPlane = playerDestroyPlane;
		planeSpeed = speed;
		verticalSpeed = horizontalSpeed = speed;
		_shield = shield; 
		UpdateHealthBar ();
	}

	public void IncreaseVerticalSpeed( float procents )
	{
		verticalSpeed = planeSpeed + planeSpeed * procents;
	}

	public void IncreaseHorizontalSpeed( float procents )
	{
		verticalSpeed = planeSpeed + horizontalSpeed * procents;
	}

   // public float Super_UP_DOWN_Speed = -1;
  //  public float Super_RIGHT_LEFT_Speed = -1;
    /*private bool superSpeedEnabled = false;

    public void SuperSpeed() {
        superSpeedEnabled = true;
    }*/

    //Super shield
	[HideInInspector]
    public UISlider superShield;
    public float 	SuperShieldSkillHeath = -1;
    public float 	SuperShieldRespawnTime = 3;
    public int 		SuperShieldCount = 0;

    private UISprite backgroundSuperShield;
    private UILabel SuperShieldText;
    private bool 	superShieldOnCooldown = false;
   // private bool 	superShieldEnable = false;
    private float 	superShiledCooldownTimer = 0;

    public void SuperShield() {
        //superShieldEnable = true;
        superShield.gameObject.SetActive(true);
        SuperShieldText.text = SuperShieldCount.ToString();
        superShiledCooldownTimer = SuperShieldRespawnTime;
		superShield.value = 1;
        backgroundSuperShield.alpha = ICON_READY_ALPHA;
    }

    public void ActivateSuperShield() {
		if ( superShieldOnCooldown || crashed || _shield.IsEnabled ) {
            return;
        }
        superShieldOnCooldown = true;
		_shield.Heath = SuperShieldSkillHeath;
        _shield.EnableSuperShield();

        SuperShieldCount = (PlayerPrefs.GetInt("SuperShield", 0) - 1 < 0) ? 0 : PlayerPrefs.GetInt("SuperShield", 0) - 1;
        PlayerPrefs.SetInt("SuperShield", SuperShieldCount);
        superShiledCooldownTimer = SuperShieldRespawnTime;

        SuperShieldText.text = SuperShieldCount.ToString();

        if (SuperShieldCount <= 0) {
            superShiledCooldownTimer = SuperShieldRespawnTime;
			superShield.value = 1;
			backgroundSuperShield.alpha = ICON_READY_ALPHA;
            superShield.gameObject.SetActive(false);
        }
        updateSkills();
    }

	private MeshBlinker planeBlinker;
	public void SetPlaneBlinker()
	{
		planeBlinker = GetComponentInChildren<MeshBlinker>();
	}

    //superBomb
    public Transform SuperBombObject;
	//[HideInInspector]
    public UISlider superBomb;
    public int 		SuperBombCnt = 0;
    public float 	SuperBombRespawnTime = 3;
    public float 	SuperBombDamage = 50;
    public float 	SuperBombRadius = 200;

    private UISprite backgroundSuperBomb;
    private UILabel BombCount;
    private bool 	superBombRespawn = false;
    private bool 	superBombEnable = false;
    private float 	tmpSuperBombRespawnTime = 0;

    public void SuperBomb() {
        superBombEnable = true;
        superBomb.gameObject.SetActive(true);
        BombCount.text = SuperBombCnt.ToString();
        tmpSuperBombRespawnTime = SuperBombRespawnTime;
		superBomb.value = 1;
		backgroundSuperBomb.alpha = ICON_READY_ALPHA;
    }

    public void ActivateSuperBomb() {
		if (superBombRespawn || crashed ) {
            return;
        }

        superBombRespawn = true;
        addSuperBomb();

        SuperBombCnt = (PlayerPrefs.GetInt("SuperBomb", 0) - 1 < 0) ? 0 : PlayerPrefs.GetInt("SuperBomb", 0) - 1;
        PlayerPrefs.SetInt("SuperBomb", SuperBombCnt);
        tmpSuperBombRespawnTime = SuperBombRespawnTime;

        BombCount.text = SuperBombCnt.ToString();

        if (SuperBombCnt <= 0) {
            tmpSuperBombRespawnTime = SuperBombRespawnTime;
			superBomb.value = 1;
			backgroundSuperBomb.alpha = ICON_READY_ALPHA;
            superBomb.gameObject.SetActive(false);
        }

        updateSkills();
    }

    //super turel
    public float SuperTurelSpeed = 100;
    public float SuperTurelDamage = 5;
    private bool superShoot = false;

    public void SuperShoot() {
        superShoot = true;
    }

    public HelpPlanes HelpPlanes;
    public float HelpPlanesDamage = 10;
    public float HelpPlanesCooldown = 40;
    public float HelpPlanesSpeed = 40;

    public void EnableHelpPlanes() {
        if (HelpPlanes) {
			HelpPlanes.EnablePlanes( HelpPlanesDamage, HelpPlanesCooldown, HelpPlanesSpeed );
            //HelpPlanes.gameObject.SetActive(true);
        } else {
            Debug.Log("Help planes cannot be searched!");
        }
    }  

    public void DisableHelpPlanes() {
        if (HelpPlanes) {
            HelpPlanes.DisablePlanes();
        } else {
            Debug.Log("Help planes cannot be searched!");
        }
    }
    //end skills system

	public 	int 		playerLives;
    public 	float 		RespawnTime = 3.0f;
	[HideInInspector]
    public float maxHeath = 0; // temp variable for heath

    public int playerMoney = 0;

    public bool shootEnable = false; //Global value
    public bool moveEnable = false; //Global value
    private bool crashed = false;

    private Transform PlayerDestroyPlane;
	//[HideInInspector]
   // public Transform MuzzleObject; //The flash effect that appears at the mouth of a gun when it fires

    //GUI all
	[HideInInspector]
    public UISlider		Lifebar;
	[HideInInspector]
    public UILabel 		liveCounter;
	[HideInInspector]
    public UILabel 		scoreCounter;

    //Bomb and Torpedo
	[HideInInspector]
    public UISlider 	bomb;
	[HideInInspector]
    public UISlider 	torpedo;

	public	string[]	levelIndexesForTorpedo;
    private bool 		torpedoActive = false;
    public Transform 	TorpedoObject;
    public float 		TorpedoStartSpeed = 10;
	public float		torpedoCooldown;
    private UISprite 	backgroundTorpedo;

    public Transform 	BombObject;
    public float 		BombStartSpeed = 10;
    public float 		BombRotate = 40;
    //skill values
    public float 		bombRespawnTime = 3;
    public float 		bombDamage = 1;
	public float 		bombRadius = 5;
    public float 		bombSize = 1;

    private float 		tmpBombRespawnTime = 0;

    private bool 		respawnBombTorpedo = false;
    private UISprite 	backgroundBomb;


    //Shield
	[HideInInspector]
    public UISlider 	shield;

    public float 		ShieldSkillHeath = -1;
	[HideInInspector]
    public float 		ShieldSkillRespawnTime = 3;
	[HideInInspector]
    public float 		ShieldSkillTime = -1;

	private PlayerShield _shield;
    private bool 		shieldOnCoolDown = false;

    private UISprite 	backgroundShield;
    private float 		shieldRespawnTimer = 0;
    //private bool 		ShieldSkillEnable = false;

	//private BoxCollider	planeCollider;
	private bool		useEndLevelSpline;
	public	bool 		UseEndLevelSpline
	{
		set{ useEndLevelSpline = value; }
		get{ return useEndLevelSpline; }
	}
	private bool		endLevel;
	private bool		endLevelFly;
	public	float		endLevelFlyOffset;
	private float		endLevelFlyPos;//позиция на которую улетает самолет при окончании уровня, расчитывается от позиции камеры
	public 	float		speedGainTime;//время разгона
	private	float		speedGainTimer;

	public	AnimationCurve	endLevelFlySpeed;

	public	static PlayerScript	instance;

	private Transform			transf;


	public static bool endLevelReached
	{
		get { return instance.endLevel; }
	}

	public static Vector3 getPosition
	{
		get { return instance.transf.position; }
	}

    private void Awake()
    { 
		instance = this;
		transf 	= 	transform;
		//planeCollider = GetComponent<BoxCollider>();
    }

    public void EnableShieldSkill() {
        //ShieldSkillEnable = true;
        shield.gameObject.SetActive(true);
    }
	
    public void activateShield() {
        if (shieldOnCoolDown) {
            return;
        }
        shieldOnCoolDown 	= true;
        shieldRespawnTimer 	= ShieldSkillRespawnTime;
        _shield.Heath 		= ShieldSkillHeath;
        _shield.lifeTime 	= ShieldSkillTime;
        _shield.EnableShield();
    }

	public static void Kill()
	{
		instance._health = 0;
		instance.playerLives = 0;
	}

	public static bool debugInvulnerability;
	public void HitPlayer( float damage )
	{
		if (SaveLoadParams.plVibration == 1)
		{
			#if UNITY_ANDROID
			Handheld.Vibrate();
			#endif
		}
		if( invulnerable || debugInvulnerability )
		{
			return;
		}
		if( _shield && _shield.IsEnabled )
		{
			_shield.Heath -= damage;
			return;
		}
		_health -= damage;
		UpdateHealthBar ();
	}

    private float _health;
    public float Health {
        get { return _health; }
        set
        {
            _health = value;
			UpdateHealthBar();
        }
    }

    //RepairSkill
	[HideInInspector]
    public UISlider repairePlane;
    public int RepairSkillCount = -1;
    public float RepairHeath = 50;
    public float RepairSkillRespawnTime = 3;

    private UISprite backgroundRepaire;
    private UILabel RepaireCount;
    private bool repairSkillRespawn = false;
    private float tmpRepairSkillRespawnTime = 0;

    public void RepairSkillEnable() {
        //activateRepaire();    
        tmpRepairSkillRespawnTime = RepairSkillRespawnTime;
		repairePlane.value = 1;
		backgroundRepaire.alpha = ICON_READY_ALPHA;
        repairSkillRespawn = false;
        RepaireCount.text = RepairSkillCount.ToString();
        repairePlane.gameObject.SetActive(true);
    }

    //33 процента от здоровья на уровне
    public void activateRepaire() {
        if ( Health == maxHeath || crashed ) {
            return;
        }

        if ( repairSkillRespawn ) {
            return;
        }

        repairSkillRespawn = true;
        tmpRepairSkillRespawnTime = RepairSkillRespawnTime;
        RepairHeath = maxHeath * 0.33f;

        if ( RepairHeath + Health > maxHeath ) 
		{
            _health = maxHeath;
        } 
		else 
		{
			_health += RepairHeath;
        }

		UpdateHealthBar ();

        RepairSkillCount = PlayerPrefs.GetInt("RepairSkill", 0) - 1;
        PlayerPrefs.SetInt("RepairSkill", ((RepairSkillCount < 0) ? 0 : RepairSkillCount));
        RepaireCount.text = RepairSkillCount.ToString();

        if (RepairSkillCount <= 0) {
            tmpRepairSkillRespawnTime = RepairSkillRespawnTime;
			repairePlane.value = 1;
			backgroundRepaire.alpha = ICON_READY_ALPHA;
            repairSkillRespawn = false;
            repairePlane.gameObject.SetActive(false);
        }

        updateSkills();
    }

    

    private bool prevMoveEnable = false;
    private bool prevShootEnable = false;
    private UISprite hpBack;

    private void Start() {

		for( int i = 0; i < levelIndexesForTorpedo.Length; i++ )
		{
			if( LoadSceneFromBundle.currentScene.ToLower() == levelIndexesForTorpedo[ i ].ToLower() )
			{
				torpedoActive = true;
				break;
			}
		}
      //  maxHeath = Health;
        //GameObject LevelCnt = GameObject.Find("LevelConroller");

        tmpBombRespawnTime = torpedoActive ? torpedoCooldown : bombRespawnTime;
      //  shieldRespawnTimer = ShieldSkillRespawnTime;

        if (torpedoActive) {
            backgroundTorpedo = torpedo.gameObject.transform.FindChild("Background").GetComponent<UISprite>();
        }
        backgroundBomb = bomb.gameObject.transform.FindChild("Background").GetComponent<UISprite>();

        backgroundShield = shield.gameObject.transform.FindChild("Background").GetComponent<UISprite>();
        backgroundRepaire = repairePlane.gameObject.transform.FindChild("Background").GetComponent<UISprite>();
        repairePlane.gameObject.SetActive(false);
        backgroundSuperBomb = superBomb.gameObject.transform.FindChild("Background").GetComponent<UISprite>();
        backgroundSuperShield = superShield.gameObject.transform.FindChild("Background").GetComponent<UISprite>();

        RepaireCount = repairePlane.gameObject.transform.FindChild("Count").GetComponent<UILabel>();
        BombCount = superBomb.gameObject.transform.FindChild("Count").GetComponent<UILabel>();
        SuperShieldText = superShield.gameObject.transform.FindChild("Count").GetComponent<UILabel>();

        //check shield skill avariable
        if (PlayerPrefs.GetInt("ShieldSkill") > 0) {
            EnableShieldSkill();
        } else {
            shield.gameObject.SetActive(false);
        }

        if (PlayerPrefs.GetInt("RepairSkill") > 0) {
            RepairSkillEnable();
        } else {
            repairePlane.gameObject.SetActive(false);
        }

        if (PlayerPrefs.GetInt("SuperBomb") > 0) {
            SuperBomb();
        } else {
            superBomb.gameObject.SetActive(false);
        }

        if (PlayerPrefs.GetInt("SuperShield") > 0) {
            SuperShield();
        } else {
            superShield.gameObject.SetActive(false);
        }

        DisableHelpPlanes();//disable help planes, it's enabled if it's possible after skills update

        prevMoveEnable = moveEnable;
        prevShootEnable = shootEnable;

        if (desantButton) {
            desantButton.SetActive(false);
        } else {
            Debug.Log("Desant button no assigned");
        }

        //if level replays - set money count on first chance plays level
       /* if (SaveLoadParams.plStartLevelMoney > 0) {
            playerMoney = SaveLoadParams.plStartLevelMoney;
        } else {*/
            //start money on 1 level
			playerMoney = SaveLoadParams.plMoney;//(SaveLoadParams.currLevel == 1) ? SaveLoadParams.plMoney + levelConroller.startMoney : SaveLoadParams.plMoney;
        //}

       // SaveLoadParams.plStartLevelMoney = playerMoney; //temp value, if level replayed - this value is saved

        if (Lifebar && Lifebar.foregroundWidget) {
            hpBack = Lifebar.foregroundWidget.GetComponent<UISprite>();
        }

		superShield.value = 1f;
		backgroundSuperShield.alpha = ICON_READY_ALPHA;

		superBomb.value = 1f;
		backgroundSuperBomb.alpha = ICON_READY_ALPHA;

		repairePlane.value = 1f;
		backgroundRepaire.alpha = ICON_READY_ALPHA;

		shield.value = 1f;
		backgroundShield.alpha = ICON_READY_ALPHA;

		
		if (! torpedoActive ) 
		{
			torpedo.gameObject.SetActive(false);
		} 
		else 
		{
			bomb.gameObject.SetActive(false);
		}

		if (torpedo) torpedo.value = 1f;

		if (backgroundTorpedo) backgroundTorpedo.alpha = ICON_READY_ALPHA;

		bomb.value = 1f;
		backgroundBomb.alpha = ICON_READY_ALPHA;    
    }

	public float 	timeToPickCoins;
	private float 	timerToPickCoins;
	private bool 	bossDefeated;

	public void BossDefeted()
	{
		Debug.Log(" boss defeated ");
		bossDefeated = true;
		timerToPickCoins = 0;
		invulnerable = true;
		invulnerabilityTimer = -1f;// что бы не выключить неузвимость по таймеру
		planeBlinker.StopBlink();
	}

	private void StartEndLevelFly()
	{
		if( !endLevel )
		{
			DisableAIRocket();
			DisableNoAIRocket();
			currentTurel.gameObject.SetActive( false );
			speedGainTimer = 0;
			endLevelFly = true;
			endLevel = true;
			endLevelFlyPos = Camera.main.transform.position.x + endLevelFlyOffset;
			invulnerable = true;
			invulnerabilityTimer = -1f;// что бы не выключить неузвимость по таймеру
			planeBlinker.StopBlink();
			if( useEndLevelSpline )
			{
				EndLevelFly.Activate();
			}
		}
	}

	const string END_LEVEL_TAG = "endLevel";
	void OnTriggerEnter( Collider other )
	{
		if( other.CompareTag( END_LEVEL_TAG ) )
		{
			if( !bossDefeated )
			{
				StartEndLevelFly();
			}
		}
	}


	private void UpdateHealthBar()
	{
		if (Lifebar) 
		{
			if( hpBack != null )
			{
				maxHeath = maxHeath < Health ? Health : maxHeath;
				float hpPersents = Health / maxHeath;
				if (hpPersents > 0.6f) {
					hpBack.spriteName = "hpGreen";
				}
				if (hpPersents < 0.6f && hpPersents > 0.2f) {
					hpBack.spriteName = "hpYellow";
				}
				else if (hpPersents < 0.2f) {
					hpBack.spriteName = "hpRed";
				}
				Lifebar.value = hpPersents;
			}
		}
	}

	private void UpdateBars() {
		
		if (liveCounter) {
			liveCounter.text = playerLives.ToString();
		}
		
		if (scoreCounter) {
			scoreCounter.text = playerMoney.ToString();
			SaveLoadParams.plMoney = playerMoney;
		}
		
		//superShield
		if (superShieldOnCooldown) {
			backgroundSuperShield.alpha = ICON_ONCD_ALPHA;
			
			superShiledCooldownTimer -= Time.deltaTime;
			superShield.value = (SuperShieldRespawnTime - superShiledCooldownTimer) / SuperShieldRespawnTime;
			
			if (superShiledCooldownTimer < 0) {
				superShiledCooldownTimer = SuperShieldRespawnTime;
				superShield.value = 1f;
				backgroundSuperShield.alpha = ICON_READY_ALPHA;
				superShieldOnCooldown = false;
			}
		}
		
		//superBomb
		if (superBombRespawn) {
			backgroundSuperBomb.alpha = ICON_ONCD_ALPHA;
			
			tmpSuperBombRespawnTime -= Time.deltaTime;
			superBomb.value = (SuperBombRespawnTime - tmpSuperBombRespawnTime) / SuperBombRespawnTime;
			
			if (tmpSuperBombRespawnTime < 0) {
				tmpSuperBombRespawnTime = SuperBombRespawnTime;
				superBomb.value = 1f;
				backgroundSuperBomb.alpha = ICON_READY_ALPHA;
				superBombRespawn = false;
			}
		}
		
		//repaire
		if (repairSkillRespawn) {
			backgroundRepaire.alpha = ICON_ONCD_ALPHA;
			
			tmpRepairSkillRespawnTime -= Time.deltaTime;
			repairePlane.value = (RepairSkillRespawnTime - tmpRepairSkillRespawnTime) / RepairSkillRespawnTime;
			
			if (tmpRepairSkillRespawnTime < 0) {
				tmpRepairSkillRespawnTime = RepairSkillRespawnTime;
				repairePlane.value = 1f;
				backgroundRepaire.alpha = ICON_READY_ALPHA;
				repairSkillRespawn = false;
			}
		}
		
		//shield
		if (shieldOnCoolDown) {
			backgroundShield.alpha = ICON_ONCD_ALPHA;
			
			shieldRespawnTimer -= Time.deltaTime;
			shield.value = (ShieldSkillRespawnTime - shieldRespawnTimer) / ShieldSkillRespawnTime;
			
			if (shieldRespawnTimer <= 0) {
				shieldRespawnTimer = ShieldSkillRespawnTime;
				shield.value = 1f;
				backgroundShield.alpha = ICON_READY_ALPHA;
				shieldOnCoolDown = false;
				Debug.Log( ShieldSkillRespawnTime );
			}
		}
		
		//bomb
		if (respawnBombTorpedo) {
			if (torpedoActive) {
				backgroundTorpedo.alpha = ICON_ONCD_ALPHA;
			} else {
				backgroundBomb.alpha = ICON_ONCD_ALPHA;
			}
			
			tmpBombRespawnTime -= Time.deltaTime;
			
			if (torpedoActive) {
				torpedo.value = ( torpedoCooldown - tmpBombRespawnTime ) / torpedoCooldown;
			} else {
				bomb.value = (bombRespawnTime - tmpBombRespawnTime) / bombRespawnTime;
			}
			
			if (tmpBombRespawnTime < 0) {
				tmpBombRespawnTime = bombRespawnTime;
				
				if (torpedoActive) {
					tmpBombRespawnTime = torpedoCooldown;
					torpedo.value = 1f;
					backgroundTorpedo.alpha = ICON_READY_ALPHA;
				} else {
					tmpBombRespawnTime = bombRespawnTime;
					bomb.value = 1f;
					backgroundBomb.alpha = ICON_READY_ALPHA;
				}
				
				respawnBombTorpedo = false;
			}
		}
	}

	Vector3 temp;
    void Update() {
		if( endLevelFly )
		{
			if( useEndLevelSpline ){ return; }
			if( transform.position.x >= endLevelFlyPos )
			{
				endLevelFly = false;
				FindObjectOfType<EndLevelPopup>().SendMessage("endLevel", SendMessageOptions.RequireReceiver);
			}
			else
			{
				speedGainTimer += Time.deltaTime;

				temp = transform.position;
				temp.x += endLevelFlySpeed.Evaluate( speedGainTime / speedGainTimer ) * Time.deltaTime;
				transform.position = temp;
			}
			return;
		}
		if( bossDefeated )
		{
			timerToPickCoins += Time.deltaTime;
			if( timerToPickCoins >= timeToPickCoins )
			{
				bossDefeated = false;
				StartEndLevelFly();
			}
		}

        if (moveEnable != prevMoveEnable) {
            if (moveEnable) {
                setMoveEnable();
            } else {
                setMoveDisable();
            }
        }

        if (shootEnable != prevShootEnable) {
            if (shootEnable) {
                SetShootEnable();
            } else {
                setShootDisable();
            }
        }

		if( invulnerable )
		{
			if( invulnerabilityTimer > 0 )
			{
				invulnerabilityTimer -= Time.deltaTime;
				if( invulnerabilityTimer <= 0 )
				{
					invulnerable = false;
					planeBlinker.StopBlink();
				}
			}
		}
		
		if (Health > 0) 
		{
            if (shootEnable && turretEnable) {
                if (turelSpeedShotCoolDown > 0) {
                    turelSpeedShotCoolDown -= Time.deltaTime;
                } else {
                    AddBulletAI();
                }
            }
        } 
		else {
            if (!crashed) {
                crashed = true;
				PlayerMoveController.Pause( true );
               Instantiate(PlayerDestroyPlane, transform.position, transform.rotation);
                //Rigidbody CopyRigit = plCopy.GetComponent<Rigidbody>();
              //  plCopy.GetComponentInChildren<Rigidbody>().AddForce(transform.right * 500);

                Transform child = gameObject.transform;

                for (int d = 0; d < child.childCount; d++) 
				{
                    child.GetChild(d).gameObject.SetActive(false);
                }

                setMoveEnable();
                SetShootEnable();
				
				playerLives --;
				int bonusLifesNumber  = PlayerPrefs.GetInt( "AddLife", 0 );
				if( bonusLifesNumber > 0 )
				{
					bonusLifesNumber --;
					PlayerPrefs.SetInt( "AddLife", bonusLifesNumber );
				}
				SaveLoadParams.plLivesNumber = playerLives;
				SaveLoadParams.SaveLives();
            }
        }
		UpdateBars ();
    }

	public void AddLife()
	{
		playerLives++;
		SaveLoadParams.plLivesNumber = playerLives;
		SaveLoadParams.SaveLives();
	}

	public void AddLifeAndRespawn()
	{
		AddLife();
		Respawn();
	}

    //respawn area
    public void StartRespawn() {
        //Debug.Log("Respawn timer start");
        StartCoroutine(RespawnPlayer());
    }

    private IEnumerator RespawnPlayer() {
        yield return new WaitForSeconds(RespawnTime);
        if (playerLives <= 0) 
		{
            GameObject end_popup = GameObject.Find("EndLevel");
            var popup = end_popup.GetComponent<EndLevelPopup>();
            popup.ShowGameOver();
        } 
		else 
		{
			Respawn();
		}
	}

	private void Respawn()
	{
		PlayerMoveController.Pause (false);
		setMoveEnable();
		SetShootEnable();
		
		for (int d = 0; d < transf.childCount; d++) {
			transf.GetChild(d).gameObject.SetActive(true);
			/*if (transf.GetChild(d).gameObject.GetComponent<Turel>()) {
					Turel tur = transf.GetChild(d).gameObject.GetComponent<Turel>();
                    tur.shootEnable = true;
                }*/
		}
		currentTurel.GetComponent<Turel>().ShootEnable();
		Health = maxHeath;
		crashed = false;
		BecomeInvulnerable();
	}
	
	public void StartGame()
	{
		SetShootEnable();
		setMoveEnable();
		BecomeInvulnerable();
		Vector3 t = transf.position;
		t.x = Camera.main.transform.position.x + 50f;
		transf.position = t;
	}

	private void BecomeInvulnerable()
	{
		invulnerable = true;
		invulnerabilityTimer = invulnerabilityTime;
		planeBlinker.StartBlink();
	}
    //Weapon

    //bomb area
    public void addBomb() {
        if (crashed) return;
        if ( torpedoActive ) 
		{
            addTorpedo();
            return;
        }
        if ( BombObject && !respawnBombTorpedo ) //moveEnable
        {
			ThrowBomb( BombObject, bombDamage, bombSize, bombRadius );
            //Create a copy of a bullet from the library. Also move the bullet a little forward, so it doesn't shoot straight out of the plane's body//            Transform BombObjectCopy = Instantiate(BombObject, new Vector3(transform.position.x, transform.position.y - 10, transform.position.z), transform.rotation) as Transform;
            respawnBombTorpedo = true;
        }
    }

    private void addSuperBomb() {
        if (crashed) return;

        if (superBombEnable && SuperBombCnt > 0 && SuperBombObject) {
            //Create a copy of a bullet from the library. Also move the bullet a little forward, so it doesn't shoot straight out of the plane's body
			ThrowBomb( SuperBombObject, SuperBombDamage, 0, SuperBombRadius );
        }
    }

	private BombPlayer ThrowBomb( Transform bombObj, float _bombDamage, float scale, float expRadius )
	{
		Transform BombObjectCopy = Instantiate( bombObj, transform.position, transform.rotation ) as Transform;
		
		//Move the source of the bullet a little forward
		BombObjectCopy.transform.Translate(Vector3.right * 10.5f, Space.Self);
		
		//Vector3 posOnScreen = Camera.main.WorldToScreenPoint(transform.position);
		//Ray ray = Camera.main.ScreenPointToRay(posOnScreen);
		//Ray bullerRay = new Ray( BombObjectCopy.transform.position, ray.direction );
		Rigidbody body = BombObjectCopy.GetComponentInChildren<Rigidbody>();
		body.AddForce(Vector3.right * BombStartSpeed * 100);
		body.AddTorque(-Vector3.forward * BombRotate);
		
		BombPlayer bomb = BombObjectCopy.GetComponentInChildren<BombPlayer>();
		bomb.explosionDamage = SuperBombDamage;
		bomb.explosionRadius = expRadius;
		if( scale != 0 )
		{
			BombObjectCopy.localScale *= scale;
		}
		return bomb;
	}

    //Rocket
    private IEnumerator addRocket() {
        yield return new WaitForSeconds(Interval_NO_AI);

        StopCoroutine("addRocket");
        StartCoroutine("addRocket");
        
        if ( RocketObject && rocketEnable && shootEnable && !crashed ) {
            //Spawn 2 rockets
            for (int i = 0; i < 2; i++) {
				Vector3 spawnPos = transf.position;
				spawnPos.y -= 2.5f;//прячем под самолет
				Transform RocketObjectCopy = Instantiate(RocketObject, spawnPos, transform.rotation) as Transform;
                RocketPlayer rocket = RocketObjectCopy.GetComponent<RocketPlayer>();
                rocket.Damage = ROCKET_NO_AI_Damage;
                RocketObjectCopy.transform.Translate(Vector3.forward * ((i == 0) ? -4.5f : 4.5f));
                RocketObjectCopy.Rotate(new Vector3(0, 90, 0));
                RocketObjectCopy.GetComponent<Rigidbody>().velocity = transform.right * ROCKET_NO_AI_Speed;
            }
        }
    }

    private IEnumerator addAIRocket() {
        yield return new WaitForSeconds(Interval_AI);

        StopCoroutine("addAIRocket");
        StartCoroutine("addAIRocket");

        if (RocketAIObject && rocketAIEnable && shootEnable && !crashed)
        {
           // List<GameObject> allEnemies = new List<GameObject>();
			List<GameObject> leftEnemies = new List<GameObject>();
			List<GameObject> rightEnemies = new List<GameObject>();

            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

            //check on screen
            for (int i = 0; i < enemies.Length; i++) {
                if (enemies[i].GetComponent<BossPart>() && !enemies[i].GetComponent<BossPart>().avariable) {
                    continue;
                }
				Vector3 posOnScreen = Camera.main.WorldToViewportPoint(enemies[i].transform.position);
                if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) {
                    //allEnemies.Add(enemies[i]);
					if( enemies[i].transform.position.z > transf.position.z )
					{
						leftEnemies.Add( enemies[i] );
					}
					else if( enemies[i].transform.position.z < transf.position.z )
					{
						rightEnemies.Add( enemies[i] );
					}
					else
					{
						leftEnemies.Add( enemies[i] );
						rightEnemies.Add( enemies[i] );
					}
                }
            }
           // allEnemies.Reverse();
            //Spawn 2 rockets
            //if (allEnemies.Count != 0)
            {
                for (int k = 0; k < 2; k++) 
                {
					Vector3 spawnPos = transf.position;
					spawnPos.y -= 2.5f;//прячем под самолет
					Transform RocketAICopy = Instantiate(RocketAIObject, spawnPos, transform.rotation) as Transform;

                    PlayerAIRocket rocket = RocketAICopy.GetComponent<PlayerAIRocket>();
                    rocket.Damage = ROCKET_AI_Damage;
                    rocket.speed = ROCKET_AI_Speed;
					//Debug.Log( rightEnemies.Count+"  "+leftEnemies.Count );
					if( k == 0 )//правая
					{
						RocketAICopy.transform.Translate( Vector3.forward * -4.5f );
						if( rightEnemies.Count > 0 )
						{
							rocket.Target = rightEnemies[ Random.Range( 0, rightEnemies.Count ) ];
						}
						else if( leftEnemies.Count > 0 )
						{
							rocket.Target = leftEnemies[ Random.Range( 0, leftEnemies.Count ) ];
						}
					}
					else//левая
					{
						RocketAICopy.transform.Translate(Vector3.forward * 4.5f );
						if( leftEnemies.Count > 0 )
						{
							rocket.Target = leftEnemies[ Random.Range( 0, leftEnemies.Count ) ];
						}
						else if( rightEnemies.Count > 0 )
						{
							rocket.Target = rightEnemies[ Random.Range( 0, rightEnemies.Count ) ];
						}
					}

                  /*  if (allEnemies.Count > 0 && k <= allEnemies.Count-1)
                    {
                        rocket.Target = allEnemies[k].gameObject;
                        //if (allEnemies.Count == 1) {
                        //    rocket.Target = allEnemies[0];
                        //} else if (allEnemies[k]) {
                            
                        //}
                    }*/
                   // RocketAICopy.transform.Translate(Vector3.forward * ((k == 0) ? -4.5f : 4.5f));//0 правая 1 левая ракета.
                    RocketAICopy.Rotate(new Vector3(0, 90, 0));
                    //RocketAICopy.rigidbody.rigidbody.AddForce(transform.right * ROCKET_NO_AI_Speed * 100);
                    //RocketAICopy.transform.Translate(Vector3.left * ((k == 0) ? -4.5f : 4.5f));
                    //RocketAICopy.rigidbody.rigidbody.AddForce(transform.forward * 1 * 100);
                }
            }
        }
    }

    //torpedo area
    private void addTorpedo() {
        if(crashed) return;

        if (TorpedoObject && shootEnable && !respawnBombTorpedo) {
            var TorpedoObjectCopy = Instantiate(TorpedoObject, transform.position, Quaternion.identity) as Transform;
            TorpedoObjectCopy.Translate(Vector3.forward * 5.0f, Space.Self);
            TorpedoObjectCopy.Rotate(new Vector3(0, 90, 0));
            //Vector3 posOnScreen = Camera.mainCamera.WorldToScreenPoint(transform.position);
            // Ray ray = Camera.mainCamera.ScreenPointToRay(posOnScreen);
            // Ray bullerRay = new Ray(TorpedoObjectCopy.transform.position, ray.direction);

            TorpedoObjectCopy.GetComponent<Rigidbody>().AddForce(Vector3.right * TorpedoStartSpeed * 100); //(bullerRay.direction + TorpedoObjectCopy.forward)
            respawnBombTorpedo = true;
        }
    }

    //Bullet
    private void AddBulletAI() {
        if (crashed) return;
        if (TurelBullet) {
            var allEnemies = new List<GameObject>();

            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

            //check on screen
            for (int i = 0; i < enemies.Length; i++) {
                Vector3 posOnScreen = Camera.main.WorldToViewportPoint(enemies[i].transform.position);

                if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) {
                    allEnemies.Add(enemies[i]);
                }
            }

			BulletBase bullet = BulletsManager.GetFreeBullet( TurelBullet.name );
			if( bullet != null )
			{
				//Vector3 startAngle = new Vector3(0, 90f, 0);
				if (allEnemies.Count > 0)
				{
					bullet.Launch( transform.position, allEnemies[0].transform, TurretSkillSpeed * 3f );
				}
				else
				{
					bullet.Launch( transform.position, new Vector3(0, 90f, 0), TurretSkillSpeed * 3f );
				}

			}

           /* BulletObjectCopy.Rotate();
            */
            turelSpeedShotCoolDown = TurelCoolDown;
        }
    }

    public void SpawnTurel() {
        if (currentTurel != null) {
            Destroy(currentTurel.gameObject);
        }

        int spawnIndex = -1;
        switch (currentTUREL) {
            case TUREL_TYPE.GUN_2_SHOT_SKILL:
                if (Turels[0]) {
                    spawnIndex = 0;
                }
                break;
            case TUREL_TYPE.GUN_3_SHOT_SKILL:
                if (Turels[1]) {
                    spawnIndex = 1;
                }
                break;
            case TUREL_TYPE.GUN_4_SHOT_LINE_SKILL:
                if (Turels[2]) {
                    spawnIndex = 2;
                }
                break;
            case TUREL_TYPE.GUN_4_SHOT_NO_LINE_SKILL:
                if (Turels[3]) {
                    spawnIndex = 3;
                }
                break;
            case TUREL_TYPE.GUN_7_SHOT_LINE_SKILL:
                if (Turels[4]) {
                    spawnIndex = 4;
                }
                break;
            case TUREL_TYPE.GUN_7_SHOT_NO_LINE_SKILL:
                if (Turels[5]) {
                    spawnIndex = 5;
                }
                break;
            case TUREL_TYPE.GUN_ALL_SHOT_SKILL:
                if (Turels[6]) {
                    spawnIndex = 6;
                }
                break;
            case TUREL_TYPE.SUPER_SHOOT:
                if (Turels[7]) {
                    spawnIndex = 7;
                }
                break;
        }
        Debug.Log("spawnIndex = " + spawnIndex + " currentTUREL = " + currentTUREL + "  " + (int)TUREL_TYPE.SUPER_SHOOT);
        if (spawnIndex >= 0) 
		{
            currentTurel = Instantiate(Turels[spawnIndex], transform.TransformPoint( new Vector3(-8, -0.5f, 0) ), Quaternion.Euler(0,90,0)) as Transform;
            currentTurel.parent = gameObject.transform;
            currentTurel.Translate(Vector3.forward * 10, Space.Self);
            //Set turel Params
            Turel turelParams = currentTurel.GetComponent<Turel>();
            turelParams.currentTurelIndex = turelLevel;

            if (turelParams) {
                if (superShoot) {
                    turelParams.BulletSpeed = SuperTurelSpeed;
                    if (turelDelay > 0) {
						turelParams.Init( turelDelay );
					}
					turelParams.setDamage(SuperTurelDamage);
                } else {
                    turelParams.BulletSpeed = (turelSpeed > 0) ? turelSpeed : turelParams.BulletSpeed;
                    if (turelDelay > 0) {
						turelParams.Init( turelDelay );
					}
					turelParams.setDamage((turelDamage > 0) ? turelDamage : turelParams.getDamage());
                }
            }
        } 
        //else {
        //    currentTurel = Instantiate(defaultTurel, transform.TransformPoint(new Vector3(-8, 0, 0)), gameObject.transform.rotation) as Transform;
        //    currentTurel.parent = gameObject.transform;
        //}
        //Debug.Log("SpawnTurelUpdate" + spawnIndex);
    }

    public void updateSkills() {
        GameObject skillsObj = GameObject.Find("Skills");
        PlayerSkills skills = skillsObj.GetComponent<PlayerSkills>();
        skills.UpdateSkills();
    }

    public void setMoveEnable() {
		//Debug.Log ("Move enable");
        GameObject[] obj = (GameObject[]) GameObject.FindObjectsOfType(typeof (GameObject));

        for (int i = 0; i < obj.Length; i++) {
            obj[i].SendMessage("MoveEnableNow", SendMessageOptions.DontRequireReceiver);
        }
        moveEnable = true;
        prevMoveEnable = true;
    }

    public void setMoveDisable() {
		//Debug.Log ("Move disable");
        GameObject[] obj = (GameObject[]) GameObject.FindObjectsOfType(typeof (GameObject));

        for (int i = 0; i < obj.Length; i++) {
            obj[i].SendMessage("MoveDisableNow", SendMessageOptions.DontRequireReceiver);
        }
        moveEnable = false;
        prevMoveEnable = false;
    }

    public void SetShootEnable() {
		//Debug.Log ("Shoot enable");
        GameObject[] obj = (GameObject[]) GameObject.FindObjectsOfType(typeof (GameObject));

        for (int i = 0; i < obj.Length; i++) {
            obj[i].SendMessage("ShootEnable", SendMessageOptions.DontRequireReceiver);
        }
		//currentTurel.GetComponent<Turel>().ShootEnable();
        shootEnable = true;
        prevShootEnable = shootEnable;
    }

    public void setShootDisable() {
		//Debug.Log ("Shoot disable");
        GameObject[] obj = (GameObject[]) GameObject.FindObjectsOfType(typeof (GameObject));

        for (int i = 0; i < obj.Length; i++) {
            obj[i].SendMessage("ShootDisable", SendMessageOptions.DontRequireReceiver);
        }
		//currentTurel.GetComponent<Turel>().ShootDisable();
        shootEnable = false;
        prevShootEnable = shootEnable;
    }

    private Transform tempTorpedo;

    public void DesantEnable() {
        //Debug.Log("Enable Desant");
        if (desantButton) {
            desantButton.SetActive(true);
        }

        if (torpedoActive) {
            torpedo.transform.gameObject.SetActive(false);
            tempTorpedo = TorpedoObject;
            TorpedoObject = desantPrefab;
        } else {
            bomb.transform.gameObject.SetActive(false);
            tempTorpedo = BombObject;
            BombObject = desantPrefab;
        }
      
    }

    public void DesantDisable() {
        if (desantButton) {
            desantButton.SetActive(false);
        }

        if (torpedoActive)
        {
            torpedo.transform.gameObject.SetActive(true);
            TorpedoObject = tempTorpedo;
        }
        else
        {
            bomb.transform.gameObject.SetActive(true);
            BombObject = tempTorpedo;
        }
    }

    void OnParticleCollision(GameObject other) {
        Debug.Log(other.name);
    }
}