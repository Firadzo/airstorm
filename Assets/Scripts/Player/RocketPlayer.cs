using UnityEngine;
using System.Collections;

public class RocketPlayer : MonoBehaviour {

    public float Damage = 1; //How much damage a bullet causes
    public Transform exlosionEffect; //the effect to be displayed when this object hits an enemy
	private ParticleEmitter emitter;
	private Transform		transf;
	// Use this for initialization
	void Awake () {
		emitter = GetComponentInChildren<ParticleEmitter>();
		transf = transform;
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if (shootEnable) {
	        checkOnScreen();
	        //checkCollision();
	    }
	}

	Vector3 posOnScreen ;
	void  checkOnScreen()
    {
		posOnScreen = Camera.main.WorldToViewportPoint( transf.position );
        if (posOnScreen.y > 1) 
        {
            if (emitter) emitter.emit = false;
			transf.DetachChildren();

            Destroy(gameObject);
        }
    }

	void OnTriggerEnter( Collider other )
	{
		Debug.Log ( other.gameObject );
		other.SendMessage( "TakeDamage", Damage, SendMessageOptions.DontRequireReceiver );
		if (exlosionEffect)  EffectsManager.PlayEffect( exlosionEffect.name, transf.position, Quaternion.identity );
		Destroy(gameObject);
	}

	void OnCollisionEnter( Collision other )
	{
		Debug.Log ( other.gameObject +" Collision");
		other.gameObject.SendMessage( "TakeDamage", Damage, SendMessageOptions.DontRequireReceiver );
		if (exlosionEffect)  EffectsManager.PlayEffect( exlosionEffect.name, transf.position, Quaternion.identity );
		Destroy(gameObject);
	}

    void checkCollision()
    {
		posOnScreen = Camera.main.WorldToScreenPoint( transf.position );
		Ray ray = Camera.main.ScreenPointToRay(posOnScreen);
		Ray bullerRay = new Ray( transf.position, ray.direction );

        RaycastHit hit;

        if (Physics.Raycast(bullerRay, out hit, 1000f, LayerMask.NameToLayer("Bullet")) || Physics.Raycast(ray, out hit, 1000f))
        {
            if ( hit.transform.CompareTag( "Enemy" ) )
            {
				if (exlosionEffect)  EffectsManager.PlayEffect( exlosionEffect.name, hit.point, Quaternion.identity );//create an explosionObject
                Destroy(gameObject); //remove the object from the scene

				hit.transform.SendMessage( "TakeDamage", Damage, SendMessageOptions.DontRequireReceiver );
                //hit.transform.gameObject.renderer.material.color = Color.blue;
                //Add to the player's score
                //GUIInterface scor2 = Camera.mainCamera.GetComponent<GUIInterface>();
                //scor2.Score += 20;
            }
        }
    }

    private bool shootEnable = true;
    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
