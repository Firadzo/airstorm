using UnityEngine;
using System.Collections;

public class PlayerAIRocket : MonoBehaviour {
    private bool startExplode = false;

    private Transform _target;
	private Transform transf;
    public GameObject Target {
        set
        {
            _target = value.transform;
            //Invoke("LookAtTarget",0.1f);
        }
    }
    public	 GameObject 	explosion;
    public 	float 		timeOut = 3.0f;
    public 	float 		speed = 5.0f;
	public	float		explosionRadius;
    public 	float 		Damage = 1.0f;
    public 	bool 		onScreen = false;
	public 	float		rotationSpeed;
	public	float		startFlyTime;//время прямого полета
	private float		startFlyTimer;

	//const 	float		CENTER_OFFSET = 3f;

#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		//Vector3 t = transform.position;
		//t.z += CENTER_OFFSET;
		Gizmos.DrawWireSphere( transform.position, explosionRadius );
	}
#endif

	void Awake()
	{
		startFlyTimer = startFlyTime;
		transf = transform;
	}

    private void Start() {
        Destroy(gameObject, timeOut);
    }

    private void Update() {

        if (shootEnable && !startExplode)
        {
			if ( _target && _target.gameObject.activeSelf ) {
				if( startFlyTimer > 0 )
				{
					startFlyTimer -= Time.deltaTime;
				}
				else
				{
					transf.rotation = Quaternion.Slerp( transf.rotation, Quaternion.LookRotation( _target.position - transf.position )
				                                      , rotationSpeed * Time.deltaTime );
				}
				if (Vector3.Distance( _target.position, transf.position ) <= 5) {
                    ExplodeRocket();
                }
               // LookAtTarget();
            }

			transf.Translate( speed * Vector3.forward * Time.deltaTime );
        }
        checkOnScreen();
    }

	Vector3 posOnScreen;
    void checkOnScreen()
    {
		posOnScreen = Camera.main.WorldToViewportPoint( transf.position );
        if (posOnScreen.y > 1) {
            ParticleEmitter emitter = GetComponentInChildren<ParticleEmitter>();
            if (emitter) emitter.emit = false;
			transf.DetachChildren();

            Destroy(gameObject);
        }
    }

    private void ExplodeRocket() {
		startExplode = true;
		//Vector3 center = transform.position;
		//center.z += CENTER_OFFSET;
		Collider[] hitColliders = Physics.OverlapSphere( transf.position, explosionRadius );

		foreach ( Collider hit in hitColliders ) {
            if (!hit) continue;

            if (hit.transform.CompareTag( "Enemy" ) )
            {
				hit.transform.SendMessage( "TakeDamage", Damage, SendMessageOptions.DontRequireReceiver );
            }
        }

        if (explosion) {
			EffectsManager.PlayEffect( explosion.name, transf.position, Quaternion.identity );
        }
        // Stop emitting particles in any children
        ParticleEmitter emitter = GetComponentInChildren<ParticleEmitter>();
        if (emitter) {
            emitter.emit = false;
			Destroy( emitter.gameObject, 3f );
        }
        // Detach children - We do this to detach the trail rendererer which should be set up to auto destruct
		transf.DetachChildren();

        Destroy(gameObject);
    }

	void OnTriggerEnter( Collider other )
	{
		if( !startExplode )
		{
			ExplodeRocket();
		}
	}
	
	void OnCollisionEnter( Collision other )
	{
		if( !startExplode )
		{
			ExplodeRocket();
		}
	}

    private bool shootEnable = true;

    public void ShootEnable() {
        shootEnable = true;
    }

    public void ShootDisable() {
        shootEnable = false;
    }
}
