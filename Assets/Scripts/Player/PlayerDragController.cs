﻿using System;
using UnityEngine;
using System.Collections;

public class PlayerDragController : MonoBehaviour
{
    public PlayerMoveController MoveController;
    public float 				DragPlaneOffset = 0.0f;

    private bool 				_dragging;
    private FingerGestures.Finger draggingFinger;

    private float 				_dlX, _dlZ;

    RaycastHit 					_hitTap;
	RaycastHit					tempTap;
    RaycastHit					_prevHitTap;

    private bool 				moving;
	private bool				isTap;
	private float 				_rotateX;//, _rotateZ;

    private int 				_sign = 0;
    private PlayerScript 		_player;
	private Vector2 			speed;

	//скорость наклона при перемещении самолета влево/вправо
	public	float 				SpeedRotationByHorizonatl = 1f;

	//скорость возвращения самолета в горизонтальное положение
	public 	float 				InstantaneousSpeedRotationByHorizonatl = 10f;
	public	float				minDistToStartRot;				

    private void Awake()
    {
        _player = GetComponent<PlayerScript>();
    }

    // Use this for initialization
    private void Start()
    {
        if (MoveController == null) MoveController = GameObject.FindObjectOfType<PlayerMoveController>();

        _prevHitTap = new RaycastHit();

        _prevHitTap.point = transform.position;
    }
	
	//private Vector3 pos;

    private void OnFingerDown(FingerDownEvent @event)
    {
		UICamera.Raycast( @event.Position, out tempTap );
		if (tempTap.transform == null)
		{
			isTap = true;
			CheckTapPos( @event.Position );
		}
    }
    
    private void OnDrag( Vector2 pos )// DragGesture gesture )
    {
			//Debug.Log( gesture.DeltaMove );
			//pos = gesture.Position;
		CheckTapPos( pos );
		isTap = false;
    }

	private void CheckTapPos( Vector2 tapPos )
	{
		//UICamera.Raycast( tapPos, out tempTap );
		//if (tempTap.transform == null)
		{
			if ( Physics.Raycast( Camera.main.ScreenPointToRay( tapPos ), out tempTap,( 1 << LayerMask.NameToLayer("moveCollision")  ) ) )
			{
				_hitTap = tempTap;
				moving = true;
			}
		}
	}

	Vector3 endPoint;
	float dlz;
    private void LateUpdate()
    {
		if( PlayerScript.endLevelReached )
		{  
			if( PlayerScript.instance.UseEndLevelSpline )
			{  
				MoveController.PlayerModel.transform.localEulerAngles = new Vector3( 0, 90f, 0 );
				return; 
			}
			_rotateX = Mathf.Lerp( _rotateX, 0f, InstantaneousSpeedRotationByHorizonatl * Time.deltaTime );
			MoveController.PlayerRotate( _rotateX, 0 );
			return; 
		}

        if (moving)
        {
			speed  = new Vector2( _player.HorizontalSpeed, _player.VerticalSpeed );
            Vector3 view = Camera.main.WorldToViewportPoint(_hitTap.point);
            //Debug.Log("view = " + view);

			endPoint = MoveController.CheckPosition(new Vector3(_hitTap.point.x + DragPlaneOffset * view.y + (MoveController.Speed) * Time.deltaTime, transform.position.y, _hitTap.point.z));
			//endPoint.x += DragPlaneOffset;
			//endPoint.y = transform.position.y;
			transform.position = MoveController.CheckPosition(Vector3.MoveTowards(transform.position, endPoint, speed.magnitude * Time.deltaTime));
			if( isTap )
			{ 
				dlz = Vector3.Normalize( endPoint - transform.position ).z; 
			}
			else
			{
				dlz = _hitTap.point.z - _prevHitTap.point.z;
			}

			if( System.Math.Abs(dlz) >= minDistToStartRot )
			{
            	_sign = Math.Sign(dlz) > 0 ? 1 : Math.Sign(dlz) < 0 ? -1 : _sign;
            	_rotateX = Mathf.Lerp(_rotateX, (dlz == 0 ? 0 : (_sign == -1 ? -1 : 1)), ( dlz == 0 ? InstantaneousSpeedRotationByHorizonatl : SpeedRotationByHorizonatl) * Time.deltaTime);
			}
			else
			{
				_rotateX = Mathf.Lerp(_rotateX, 0f, InstantaneousSpeedRotationByHorizonatl * Time.deltaTime);
			}
			//Debug.Log( _rotateX );
            MoveController.PlayerRotate(_rotateX, 0);

            MoveController.CheckAndMoveCameraByHorizontal(view);

            _prevHitTap = _hitTap;

            if ( (float)Math.Floor( 10 * Vector3.Distance(transform.position, endPoint) / 10 ) <= 0 )
            {
                moving = false;
            }
        }
        else
        {
            _rotateX = Mathf.Lerp(_rotateX, 0f, InstantaneousSpeedRotationByHorizonatl * Time.deltaTime);
            
			MoveController.CheckAndMoveCameraByHorizontal( Camera.main.WorldToViewportPoint( transform.position ) );
            MoveController.Move( transform, transform.position.x, transform.position.y, transform.position.z);

            MoveController.PlayerRotate(_rotateX, 0);
        }
    }

    public float SpeedRotate
    {
        get { return _rotateX; }
    }
}
