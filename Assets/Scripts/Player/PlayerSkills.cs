using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using ProtoBuf;
using AirstormSaves;

public enum EPlayerSkills{  shield, gunSkill, engine, repair, autoTurret, rocket, helpPlanes, superSkill, bomb, addLife  }
public class PlayerSkills : MonoBehaviour {
    public List<GUNSKILLS> GunSkills = new List<GUNSKILLS>();
    //public Dictionary<string, int> backSkills = new Dictionary<string, int>();
   // private List<SavedBackValues> backSkills = new List<SavedBackValues>();

    public UILabel currPriceLabel;
	public UILabel storeMoneyLbl;

	const 	string	TUTORIAL_SKILL = "GUN_2_SHOT_SKILL";
	const 	string	TUTORIAL_SKILL_2 = "GUN_3_SHOT_SKILL";
	const	string	CUR_GUN_KEY = "ActiveGun";
	private string	currentGunName;

	#region GUNSKILLS
	[Serializable]
	public class OpenIdLevel
	{
		public int SkillID;
		public int NeedSubLevel;
	}
	
	
	[Serializable]
	public class SubLevels {
		#if UNITY_EDITOR
		[HideInInspector]
		public string name;
		#endif
		public int money = 1000;
		public float damage = 0.0f; //works in rocket skill too
		public float speed = 0.0f; //works in rocket skill too, works in engine skills
		//work only in shiels skills
		public float heath = 0.0f; //if repaire skill - how to add heath
		public float gunDelay = 0.0f; //delay between two shots 
	}
	[Serializable]
	public class GUNSKILLS {
		#if UNITY_EDITOR
		[HideInInspector]
		public string	hName;
#endif
        public Transform GUIObject;
        public string name = "";
        public int id = 0;

        //c какого уровня открывается данный скилл
        public int 						need_sub_level_to_open = 3;
        //какие скиллы открывает
        public OpenIdLevel[] 			open_id;
        //c какого уровня открывать следующие скиллы
        public 	int 					open_id_this_subLevel = 3;

        public 	PlayerScript.TUREL_TYPE type;

        public 	List<SubLevels> 		subLevel = new List<SubLevels>();
		public	EPlayerSkills			skillType;

        public 	bool GunSkillActive = false; //Current weapon
        public 	bool avariable 		= false;
      //  public 	bool haveLevel = false;
        public 	bool Selected 		= false;

		private GameObject			selectFrame;
		private GameObject			selectIcon;
		private UISprite[] 			subLevelGUI;

		public void GetSelectionGUI()
		{
			selectIcon 	= GUIObject.FindChild("SelectSpr").gameObject;
			selectFrame = GUIObject.FindChild("ActiveGun").gameObject;
			Transform skillGUI 	= GUIObject.FindChild("SubLevel");
			subLevelGUI 	= skillGUI.GetComponentsInChildren<UISprite>();  //GetComponentsInChildren<UISprite>();
			//Debug.Log( name+"  "+subLevelGUI.Length );
		}

		public void SetSkillLevelGUI( int level )
		{
			for( int i = 0; i < subLevelGUI.Length; i++ )
			{
				if( i < level )
				{
					subLevelGUI[ i ].enabled = true;
				}
				else
				{
					subLevelGUI[ i ].enabled = false;
				}
			}
		}

		public void Select( bool select )
		{
			selectFrame.SetActive( select );
			selectIcon.SetActive( select );
			Selected = select;
		}

		public void EnableFrame( bool enable )
		{
			selectFrame.SetActive( enable );
		}

		public void EnableSelectionIcon( bool enable )
		{
			selectIcon.SetActive( enable );
		}
    }
	#endregion

#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		if( GunSkills != null )
		{
			for( int i = 0; i < GunSkills.Count; i++ )
			{
				GunSkills[ i ].hName = GunSkills[ i ].name;
				if( GunSkills[ i ].subLevel != null )
				{
					for( int j = 0; j < GunSkills[ i ].subLevel.Count; j++ )
					{
						GunSkills[ i ].subLevel[ j ].name = "Money "+GunSkills[ i ].subLevel[ j ].money+
															" Damage "+GunSkills[ i ].subLevel[ j ].damage+
															" Speed "+GunSkills[ i ].subLevel[ j ].speed+
															" Health "+GunSkills[ i ].subLevel[ j ].heath+
															" Delay "+GunSkills[ i ].subLevel[ j ].gunDelay;
					}
				}
			}
		}
	}
#endif


	// Use this for initialization
	private void Start() {
		InitSkills();
		//Выдаем самолеты помошники на втором уровне
		if( LoadSceneFromBundle.currentScene == "level_2" )
		{
			int currentLvl = PlayerPrefs.GetInt( "HelpPlanes", 1 );
			PlayerPrefs.SetInt("HelpPlanes", ( currentLvl > 1 ? currentLvl : 1 ) );
		}
		StartCoroutine( setSkillsParams() );
	}
	
	private IEnumerator setSkillsParams() {
		yield return new WaitForSeconds(0.01f);
		//updatePlayerParams();
		
		//HideGreenBorde();
		currentGunName = PlayerPrefs.GetString( CUR_GUN_KEY, defaultWeapon );
		if( currentGunName == defaultWeapon )
		{
			SetDefaultGun();
		}
		else
		{
			setSelectSkill( currentGunName );
		}

		//on-off skills selector update
		setSelectOnOff();
		SaveStartLevelSkills();
	}

	private void SaveStartLevelSkills()
	{
		if( SaveLoadParams.firstLevelStart == 0 )
		{ 
			return; 
		}
		else 
		{
			SaveLoadParams.SetFirstLevelStart( 0 );
		}
		AirstormSaves.AirstormSaves startLevelData  = new AirstormSaves.AirstormSaves();
		startLevelData.saves = new List<AirstormSaves.PlayerSaveData>(); 

		for (int i = 0; i < GunSkills.Count; i++) {
			AddSaveData( GunSkills[i].name, ref startLevelData );
				//addBackSkill( GunSkills[i].name, PlayerPrefs.GetInt( GunSkills[i].name, 0 ) );
			AddSaveData( GunSkills[i].name + "_avariable", ref startLevelData );
			//addBackSkill( GunSkills[i].name + "_avariable", PlayerPrefs.GetInt( GunSkills[i].name + "_avariable", 0 ) );
			if( GunSkills[ i ].skillType == EPlayerSkills.gunSkill )
			{
				AddSaveData( GunSkills[i].name + "_GunSkillActive", ref startLevelData );
				//addBackSkill( GunSkills[i].name + "_GunSkillActive", PlayerPrefs.GetInt( GunSkills[i].name + "_GunSkillActive", 0 ) );
			}
		}
		AddSaveData( "rocket_currActive", ref startLevelData );
		AddSaveData( "SuperSpeed", ref startLevelData );
		AddSaveData( "SuperShield", ref startLevelData );
		AddSaveData( "SuperBomb", ref startLevelData );
		AddSaveData( "SuperShoot", ref startLevelData );
		AddSaveData( "SuperShoot_GunSkillActive", ref startLevelData );
		AddSaveData( "SuperShieldActive", ref startLevelData );
		AddSaveData( "money", ref startLevelData );
		AddSaveData( "realMoney", ref startLevelData );
		AddSaveData( "playerLives", ref startLevelData );
		AddSaveData( CUR_GUN_KEY, currentGunName, ref startLevelData );
//#if UNITY_ANDROID && !UNITY_EDITOR

//#else
		string path = Path.Combine(Application.persistentDataPath,"StartLevelData.s");
		FileStream fs = new FileStream( path, FileMode.OpenOrCreate, FileAccess.Write );
		AirstormSerializer serializer = new AirstormSerializer();
		serializer.Serialize( fs, startLevelData );
		fs.SetLength( fs.Position );
		//ProtoBuf.Serializer.Serialize<AirstormSaves.AirstormSaves>( fs, startLevelData );
		fs.Close();

//#endif
	}

	public static void AddStartLevelMoney( int money )
	{
		string path = Path.Combine(Application.persistentDataPath,"StartLevelData.s");
		FileStream fs = new FileStream( path, FileMode.OpenOrCreate, FileAccess.Read );
		AirstormSerializer serializer = new AirstormSerializer();
		AirstormSaves.AirstormSaves startLevelSaves = serializer.Deserialize( fs, null, typeof ( AirstormSaves.AirstormSaves ) ) as AirstormSaves.AirstormSaves;//ProtoBuf.Serializer.Deserialize<AirstormSaves.AirstormSaves>( fs );
		fs.Close();
		if( startLevelSaves != null && startLevelSaves.saves != null )
		{
			for( int i = 0; i < startLevelSaves.saves.Count; i++ )
			{
				if( startLevelSaves.saves[ i ].name == "money" )
				{
					startLevelSaves.saves[ i ].value = ( int.Parse( startLevelSaves.saves[ i ].value ) + money ).ToString();
				}
				else if( startLevelSaves.saves[ i ].name == "realMoney" )
				{
					startLevelSaves.saves[ i ].value = ( int.Parse( startLevelSaves.saves[ i ].value ) + money ).ToString();
				}
			}
		}
		fs = new FileStream( path, FileMode.OpenOrCreate, FileAccess.Write );
		serializer.Serialize( fs, startLevelSaves );
		fs.SetLength( fs.Position );
		fs.Close();
	}
	
	public void ReturnSkills( bool resetLives ) 
	{
		string path = Path.Combine(Application.persistentDataPath,"StartLevelData.s");
		FileStream fs = new FileStream( path, FileMode.OpenOrCreate, FileAccess.Read );
		AirstormSerializer serializer = new AirstormSerializer();
		AirstormSaves.AirstormSaves startLevelSaves = serializer.Deserialize( fs, null, typeof ( AirstormSaves.AirstormSaves ) ) as AirstormSaves.AirstormSaves;//ProtoBuf.Serializer.Deserialize<AirstormSaves.AirstormSaves>( fs );
		fs.Close();
		if( startLevelSaves != null && startLevelSaves.saves != null )
		{
			for( int i = 0; i < startLevelSaves.saves.Count; i++ )
			{
				if( startLevelSaves.saves[ i ].name == CUR_GUN_KEY )
				{
					PlayerPrefs.SetString( startLevelSaves.saves[ i ].name, startLevelSaves.saves[ i ].value );
				}
				else
				{
					PlayerPrefs.SetInt( startLevelSaves.saves[ i ].name, int.Parse( startLevelSaves.saves[ i ].value ) );
				}
			}
		}
		if( resetLives )
		{
			PlayerPrefs.SetInt( "AddLife", 0 );
			SaveLoadParams.SetDefaulLivesNumber();
		}

		/*PlayerPrefs.SetString( CUR_GUN_KEY, savedSelectedGun );
        foreach ( SavedBackValues obj in backSkills ) {
			PlayerPrefs.SetInt( obj.key, obj.value );
        }*/
	}

	private void AddSaveData( string name, string value, ref AirstormSaves.AirstormSaves saveData )
	{
		AirstormSaves.PlayerSaveData addData = new AirstormSaves.PlayerSaveData();
		addData.name = name;
		addData.value = value;
		saveData.saves.Add( addData );
	}

	private void AddSaveData( string name, ref AirstormSaves.AirstormSaves saveData )
	{
		AddSaveData( name, PlayerPrefs.GetInt( name, 0 ).ToString(), ref saveData );
	}


	/*#if UNITY_ANDROID

	private IEnumerator SaveStartLevelData( string path )
	{
		WWW www = new WWW( path);
		yield return www;
		www.
	}
	
	#else*/

	public void ClearSkills() {
		for (int i = 0; i < GunSkills.Count; i++) {

            //Debug.Log("GunSkills[i].name = " + GunSkills[i].name);
            PlayerPrefs.SetInt(GunSkills[i].name, 0);
            PlayerPrefs.SetInt(GunSkills[i].name + "_avariable", 0);
			if( GunSkills[ i ].skillType == EPlayerSkills.gunSkill )
			{
            	PlayerPrefs.SetInt(GunSkills[i].name + "_GunSkillActive", 0);
			}
        }

        PlayerPrefs.SetInt("rocket_currActive", 0);
        PlayerPrefs.SetInt("SuperSpeed", 0);
        PlayerPrefs.SetInt("SuperShield", 1 );
        PlayerPrefs.SetInt("SuperBomb", 1 );
        PlayerPrefs.SetInt("SuperShoot", 0);
		PlayerPrefs.SetInt("SuperShoot_GunSkillActive", 0);
        PlayerPrefs.SetInt("SuperShieldActive", 0);
		PlayerPrefs.SetString( CUR_GUN_KEY, defaultWeapon );
        //PlayerPrefs.SetInt("money", 0);
        //PlayerPrefs.SetInt("moneyLevelStart", 0);
    }

    public void InitSkills() {
        for (int i = 0; i < GunSkills.Count; i++) 
		{
            Transform obj = GunSkills[i].GUIObject;

          /*  if (GunSkills[i].name == "Engine_RIGHT_LEFT" || GunSkills[i].name == "EngineUP_DOWN")
            {
                int level = PlayerPrefs.GetInt(GunSkills[i].name, 0);
                PlayerPrefs.SetInt(GunSkills[i].name, level > 2 ? level : 3 );
			}*/
			GunSkills[ i ].GetSelectionGUI();
            UIButtonMessage mess = obj.GetComponent<UIButtonMessage>();
            if (mess) {
                mess.target = gameObject;
                mess.functionName = "setSelectSkill";
                mess.SendValue = GunSkills[i].name;
            }
        }
    }

    public void updatePlayerParams() {
        //Load and update params of the player
		//Debug.Log("updateParams");
		UpdateSkills();
        for (int i = 0; i < GunSkills.Count; i++) {
			int completeSkill = PlayerPrefs.GetInt( GunSkills[ i ].name, -1 );
			int subLevelLenght = completeSkill - 1;

			#region Super skills
			if ( GunSkills[i].skillType == EPlayerSkills.superSkill ) {
                if (subLevelLenght < 0) {
                    subLevelLenght = 0;
                }

                switch (GunSkills[i].name) {
                    /*case "SuperSpeed":
                        int check = PlayerPrefs.GetInt("SuperSpeed");
                        if (check > 0) 
						{
							PlayerScript.instance.SuperSpeed();
                        }
                        break;*/
                    case "SuperShield":
                        //if shield has been destroyed in the latest game, no activate it before buy new
                        int checkEnable = PlayerPrefs.GetInt("SuperShield");
                        if (checkEnable > 0) {
                            //Player.SuperShieldSkillHeath = GunSkills[i].subLevel[subLevelLenght].heath;
							PlayerScript.instance.SuperShieldCount = checkEnable;
							PlayerScript.instance.SuperShield();
                        }
                        break;
                    case "SuperBomb":
                        int bombCheck = PlayerPrefs.GetInt("SuperBomb");
                        if (bombCheck > 0) {
                            //Player.SuperBombRadius = GunSkills[i].subLevel[subLevelLenght].speed;
                            //Player.SuperBombDamage = GunSkills[i].subLevel[subLevelLenght].damage;
							PlayerScript.instance.SuperBombCnt = bombCheck;
							PlayerScript.instance.SuperBomb();
                        }
                        break;
                        case "SuperShoot":
                        int checkWeaponActive = PlayerPrefs.GetInt("SuperShoot_GunSkillActive", -1);
                        if(checkWeaponActive > 0)
                        {
                            cleanActiveGun();
                            GunSkills[i].GunSkillActive = true;
							PlayerScript.instance.currentTUREL = GunSkills[i].type;
							PlayerScript.instance.SuperShoot();
							PlayerScript.instance.SpawnTurel();
                           // UISprite SelectSpr = GunSkills[i].GUIObject.FindChild("ActiveGun").GetComponent<UISprite>();
                            //SelectSpr.alpha = 1;
                        } else {
                            //UISprite SelectSpr = GunSkills[i].GUIObject.FindChild("ActiveGun").GetComponent<UISprite>();
                            //SelectSpr.alpha = 0;
                        }
                    break;
                }
            }
			#endregion

            if (completeSkill < 0 || !GunSkills[i].avariable || subLevelLenght < 0) {
                continue;
            }

            if ( GunSkills[i].skillType == EPlayerSkills.shield ) {
				PlayerScript.instance.ShieldSkillRespawnTime 	= GunSkills[i].subLevel[subLevelLenght].gunDelay;
				PlayerScript.instance.ShieldSkillTime 			= GunSkills[i].subLevel[subLevelLenght].heath;
				PlayerScript.instance.EnableShieldSkill();
            } 
			else if (GunSkills[i].skillType == EPlayerSkills.engine ) {
                if (GunSkills[i].name == "EngineUP_DOWN") {
					PlayerScript.instance.IncreaseVerticalSpeed( GunSkills[i].subLevel[subLevelLenght].speed / 100f );
                }

                if (GunSkills[i].name == "Engine_RIGHT_LEFT") {
					PlayerScript.instance.IncreaseHorizontalSpeed( GunSkills[i].subLevel[subLevelLenght].speed / 100f );
                }
            } 
			else if (GunSkills[i].skillType == EPlayerSkills.repair ) //check working
            {
                //Player.RepairSkillCount = (int)GunSkills[i].subLevel[subLevelLenght].heath;
				PlayerScript.instance.RepairSkillCount = PlayerPrefs.GetInt("RepairSkill", 0);
				PlayerScript.instance.RepairSkillEnable();
            } 
			else if ( GunSkills[i].skillType == EPlayerSkills.gunSkill ) {

                int checkWeaponActive = PlayerPrefs.GetInt(GunSkills[i].name + "_GunSkillActive", 1);
                if (checkWeaponActive > 0) 
				{
                    GunSkills[i].GunSkillActive = true;

					PlayerScript.instance.turelLevel = PlayerPrefs.GetInt(GunSkills[i].name, 0) - 1;
					PlayerScript.instance.currentTUREL = GunSkills[i].type;
					PlayerScript.instance.turelSpeed 	= GunSkills[i].subLevel[subLevelLenght].speed;
					PlayerScript.instance.turelDamage 	= GunSkills[i].subLevel[subLevelLenght].damage;
					PlayerScript.instance.turelDelay 	= GunSkills[i].subLevel[subLevelLenght].gunDelay;
					PlayerScript.instance.SpawnTurel();
                }
            } 
			else if (GunSkills[i].skillType == EPlayerSkills.autoTurret ) 
			{
                 	PlayerScript.instance.TurretSkillDamage = GunSkills[i].subLevel[subLevelLenght].damage;
					PlayerScript.instance.TurretSkillSpeed = GunSkills[i].subLevel[subLevelLenght].speed;
					PlayerScript.instance.turretDelay = GunSkills[i].subLevel[subLevelLenght].gunDelay;
					PlayerScript.instance.EnableAutoTurret();
            } 
			else if (GunSkills[i].skillType == EPlayerSkills.rocket ) {
                int currentActive = PlayerPrefs.GetInt("rocket_currActive", 0);

                if ((GunSkills[i].name == "ROCKET_AI") && (currentActive == 1)) {
					PlayerScript.instance.ROCKET_AI_Damage = GunSkills[i].subLevel[subLevelLenght].damage;
					PlayerScript.instance.ROCKET_AI_Speed = GunSkills[i].subLevel[subLevelLenght].speed;
					PlayerScript.instance.Interval_AI = GunSkills[i].subLevel[subLevelLenght].gunDelay;
					PlayerScript.instance.EnableAIRocket();
					PlayerScript.instance.DisableNoAIRocket();
                    Debug.Log("Player.EnableAIRocket()");
				}
                else if ((GunSkills[i].name == "ROCKET_NO_AI") && (currentActive == 2)) {
					PlayerScript.instance.ROCKET_NO_AI_Damage = GunSkills[i].subLevel[subLevelLenght].damage;
					PlayerScript.instance.ROCKET_NO_AI_Speed = GunSkills[i].subLevel[subLevelLenght].speed;
					PlayerScript.instance.Interval_NO_AI = GunSkills[i].subLevel[subLevelLenght].gunDelay;
					PlayerScript.instance.EnableNoAIRocket();
					PlayerScript.instance.DisableAIRocket();
                    Debug.Log("Player.EnableNoAIRocket()");
                }
                else if(currentActive == 0) 
				{
					PlayerScript.instance.DisableNoAIRocket();
					PlayerScript.instance.DisableAIRocket();
                }
            } 
			else if (GunSkills[i].skillType == EPlayerSkills.bomb ) {
				PlayerScript.instance.bombDamage		= GunSkills[i].subLevel[subLevelLenght].damage; //damage, persents (10, 20, etc)
				PlayerScript.instance.bombRespawnTime 	= GunSkills[i].subLevel[subLevelLenght].gunDelay; //Bomb respawn time, seconds
				PlayerScript.instance.bombSize			= GunSkills[i].subLevel[subLevelLenght].heath; //bomb size, 1.2f - increase size on 20%
				PlayerScript.instance.bombRadius 		= GunSkills[i].subLevel[subLevelLenght].speed;
            }
            else if (GunSkills[i].skillType == EPlayerSkills.helpPlanes )
            {
				PlayerScript.instance.HelpPlanesDamage 	= (int)GunSkills[i].subLevel[subLevelLenght].damage; //damage, persents (10, 20, etc)
				PlayerScript.instance.HelpPlanesCooldown 	= GunSkills[i].subLevel[subLevelLenght].gunDelay; 
				PlayerScript.instance.HelpPlanesSpeed 		= GunSkills[i].subLevel[subLevelLenght].speed;
				PlayerScript.instance.EnableHelpPlanes();
            }
        }
    }

    private void HideGreenBorde()
    {
        for (int i = 0; i < GunSkills.Count; i++)
        {
			GunSkills[ i ].EnableFrame( false );
        }
    }

	public void SelectCurrentGun()
	{
		setSelectSkill( currentGunName );
	}

	public void SelectCurrentRocket()
	{
		int currentActive = PlayerPrefs.GetInt("rocket_currActive", 0);
		if( currentActive == 2 )
		{
			setSelectSkill( "ROCKET_NO_AI");
		}
		else if( currentActive == 1 )
		{
			setSelectSkill( "ROCKET_AI");
		}
	}

    //select skill
    private string _prevSellectedSkillName;
    public void setSelectSkill(string name) {

		if( TutorialController.instance.tutorialStarted )
		{
			if( TutorialController.instance.tutorialPhase == ETutorialPhase.select &&( name == TUTORIAL_SKILL || name == TUTORIAL_SKILL_2 ) )
			{
				TutorialController.instance.BuyArrow();
			}
			else
			{
				return;
			}
		}
        for (int i = 0; i < GunSkills.Count; i++) {
            //Debug.Log(GunSkills[i].name + " " + name);

            if (GunSkills[i].name == name && GunSkills[i].avariable && _prevSellectedSkillName != name)
            {
				Debug.Log("skill name = " + name);
                _prevSellectedSkillName = name;

                cleanSelectSkill();
                GunSkills[i].Select( true );

                //set active gun of selection
                if ( GunSkills[i].skillType == EPlayerSkills.gunSkill && GunSkills[i].avariable )
                {
                    setActiveGun();
                }

                int checkLevel = PlayerPrefs.GetInt(GunSkills[i].name, -1);
                if (checkLevel <= 5 && checkLevel >= 0)
                {
                    currPriceLabel.text = GunSkills[i].subLevel[checkLevel].money.ToString();
                }
                else
                {
					currPriceLabel.text ="-";
                    Debug.Log("Check Level" + checkLevel);
                }
                enableRockets(GunSkills[i]);
            }
            else
            {
				//if( GunSkills[ i ].name == currentGunName ){ Debug.Log( GunSkills[ i ].avariable );}
                if ( _prevSellectedSkillName != GunSkills[i].name )
					GunSkills[ i ].EnableFrame( false );
            }
        }
        updatePlayerParams();
    }

    private void enableRockets( GUNSKILLS skill ) {
		//Debug.Log(" enable "+ skill.name );
        int completeSkill = PlayerPrefs.GetInt(skill.name, -1);
        if (completeSkill > 0) {
           // int check = PlayerPrefs.GetInt("rocket_currActive");

            if (skill.name == "ROCKET_AI") {
                PlayerPrefs.SetInt("rocket_currActive", 1);
                //addBackSkill("rocket_currActive", 1);
				setSelectOnOff();
            } else if (skill.name == "ROCKET_NO_AI") {
                PlayerPrefs.SetInt("rocket_currActive", 2);
               // addBackSkill("rocket_currActive", 2);
				setSelectOnOff();
            }

            //setSelectOnOff();
        }
    }

    private void setSelectOnOff() {
        for (int i = 0; i < GunSkills.Count; i++) {
            if (GunSkills[i].name == "ROCKET_AI" || GunSkills[i].name == "ROCKET_NO_AI") 
			{
                int currentActive = PlayerPrefs.GetInt("rocket_currActive", 0);

                if (((currentActive == 2) && (GunSkills[i].name == "ROCKET_NO_AI")) ||
                    ((currentActive == 1) && (GunSkills[i].name == "ROCKET_AI"))) {
					GunSkills[i].EnableFrame( true );
					GunSkills[i].EnableSelectionIcon( true );
                } else {
					GunSkills[i].EnableFrame( false );
					GunSkills[i].EnableSelectionIcon( false );
                }
            }
        }
    }

    private void cleanSelectSkill() {

        for (int i = 0; i < GunSkills.Count; i++) {
            if (GunSkills[i].Selected) {
				GunSkills[i].Select( false );
            }
        }
    }

    public void setActiveGun() {
        bool cleanOld = false;

        for (int i = 0; i < GunSkills.Count; i++) {
			//if( !GunSkills[i].Selected && GunSkills[ i ].name == currentGunName ){ Debug.Log("TTT"); }
            if (GunSkills[i].Selected && GunSkills[i].skillType == EPlayerSkills.gunSkill ) 
			{
                int completeSkill = PlayerPrefs.GetInt(GunSkills[i].name, -1);
                int subLevelLenght = completeSkill - 1;

                if (completeSkill < 0 || !GunSkills[i].avariable || subLevelLenght < 0) {
                } 
				else {
                    GunSkills[i].GunSkillActive = true;
                    PlayerPrefs.SetInt(GunSkills[i].name + "_GunSkillActive", 2);
					currentGunName = GunSkills[i].name;
					PlayerPrefs.SetString( CUR_GUN_KEY, currentGunName );
                    if (GunSkills[i].name != defaultWeapon) {
                       // addBackSkill(GunSkills[i].name + "_GunSkillActive", 1); ///do not touch default gun! :-)
                    }
					GunSkills[ i ].EnableFrame( true );
                    cleanOld = true;
                }
				//break;
            }
        }

        if (cleanOld) {
            cleanActiveGun();
        }

        //updatePlayerParams();
    }

    private void cleanActiveGun() {
        for (int k = 0; k < GunSkills.Count; k++) {
            if (GunSkills[k].GunSkillActive && GunSkills[k].skillType == EPlayerSkills.gunSkill && !GunSkills[k].Selected) {
                GunSkills[k].GunSkillActive = false;
                PlayerPrefs.SetInt(GunSkills[k].name + "_GunSkillActive", 0);
                //addBackSkill(GunSkills[k].name + "_GunSkillActive", 0);
				GunSkills[ k ].EnableFrame( false );
            }
        }
    }

    public void addLevelSubSkill() {
        //Load or Update skills tree
        //Check points
        //check avaliable!

       // Debug.Log("addLevelSubSkill");

        for (int i = 0; i < GunSkills.Count; i++) {
            if ( GunSkills[i].Selected && GunSkills[i].avariable ) 
			{
				if( TutorialController.instance.tutorialStarted )
				{
					if( TutorialController.instance.tutorialPhase == ETutorialPhase.buy )
					{
						TutorialController.instance.ClickBack();
					}
					else
					{
						return;
					}

				}
                int checkLevel = PlayerPrefs.GetInt(GunSkills[i].name, 0);

                Debug.Log(GunSkills[i].name + " checkLevel = " + checkLevel);

				if (checkLevel < 6 && GunSkills[i].subLevel[checkLevel].money <=  PlayerScript.instance.playerMoney ) {

					if ( GunSkills[i].skillType != EPlayerSkills.superSkill && ( checkLevel >= 6) ) 
					{
                        Debug.Log("Max level");
                        break;
                    }
					//Снимаем деньги за покупку
					PlayerScript.instance.playerMoney -= GunSkills[i].subLevel[checkLevel].money;
					SaveLoadParams.plMoney =  PlayerScript.instance.playerMoney;
					if( SaveLoadParams.realMoney > SaveLoadParams.plMoney )
					{
						SaveLoadParams.realMoney = SaveLoadParams.plMoney;
					}
					storeMoneyLbl.text =  PlayerScript.instance.playerMoney.ToString();
					//

					if( GunSkills[ i ].skillType == EPlayerSkills.addLife )
					{
						PlayerScript.instance.AddLife();
					}
                    //Check sub-level
					int checkSubLevel = checkLevel + 1;
					
					Debug.Log(GunSkills[i].name + " checkSubLevel = " + checkSubLevel);

                    //if sublevel + 1 < 6 - add level
                    if (checkSubLevel <= 6) {
                        PlayerPrefs.SetInt(GunSkills[i].name, checkSubLevel);
                        //addBackSkill(GunSkills[i].name, checkSubLevel);
                        enableRockets(GunSkills[i]);
						if( GunSkills[ i ].skillType == EPlayerSkills.gunSkill )
						{
							setActiveGun();
						}
                    } else {
                        break;
                    }

                    //if sub level > openNextIdSubLevel, open new skills
                    //if (checkSubLevel == GunSkills[i].open_id_this_subLevel) {
                    for (int k = 0; k < GunSkills.Count; k++)
                    {
                        for (int j = 0; j < GunSkills[i].open_id.Length; j++)
                        {
							//Проверка на открытие другого скила, при прокачке текущего
                            if (!GunSkills[k].avariable && GunSkills[k].id == GunSkills[i].open_id[j].SkillID && checkSubLevel == GunSkills[i].open_id[j].NeedSubLevel)
                            {
                                PlayerPrefs.SetInt(GunSkills[k].name + "_avariable", 1);
                                //addBackSkill(GunSkills[k].name + "_avariable", 1);
                                GunSkills[k].avariable = true;
                            }
                        }
                    }
                    //}
				} else if (checkLevel < 6 && GunSkills[i].subLevel[checkLevel].money >  PlayerScript.instance.playerMoney) {
                    GameObject obj = GameObject.Find("EndLevel");
                    if (obj) {
                        obj.GetComponent<EndLevelPopup>().openStore();
                    }
                }
            }            
        }

        updatePlayerParams();
        //updateSkills();
    }

	//установка гуи уровня прокачки скила.
	//прозрачности иконка скила в соответствии с его доступностью.
	// цена на скил в соответсвии с его уровнем.
    public void UpdateSkills() {
        for (int i = 0; i < GunSkills.Count; i++) {

            int completeSkill = PlayerPrefs.GetInt(GunSkills[i].name, 0);

			GunSkills[ i ].SetSkillLevelGUI( completeSkill );

            //skill avaliable
            //var skill = GunSkills[i].GUIObject.FindChild("Background").GetComponent<UISprite>(); 
			UISprite skill = GunSkills[i].GUIObject.GetComponent<UISprite>();

			if ( PlayerPrefs.GetInt(GunSkills[i].name + "_avariable", 0) == 1) {
                skill.alpha = 1f;
                GunSkills[i].avariable = true;
            }

            //if sub level < 1
            if (completeSkill == 0 && GunSkills[i].avariable == false) {
                skill.alpha = 0.5f;
            }

            UILabel price = GunSkills[i].GUIObject.FindChild("Price").GetComponent<UILabel>();
            /*if (GunSkills[i].SuperSkill && GunSkills[i].name == "SuperBomb") 
            {
                price.text = PlayerPrefs.GetInt("SuperBomb").ToString();
            } else {*/
//            /int indexPrice = (completeSkill + 1 < 6) ? completeSkill + 1 : completeSkill;
            if (GunSkills[i].subLevel.Count > 0 && completeSkill >= 0 && completeSkill < 6) {
                price.text = GunSkills[i].subLevel[completeSkill].money.ToString();

                //UISprite SelectSpr = GunSkills[i].GUIObject.FindChild("SelectSpr").GetComponent<UISprite>();
				if (GunSkills[i].Selected ) {
                    currPriceLabel.text = GunSkills[i].subLevel[completeSkill].money.ToString();
                }
            }
			if (GunSkills[i].Selected && completeSkill == 6 ) {
				currPriceLabel.text = "-";
			}
            //}

            if ( !GunSkills[i].Selected	) 
			{
				GunSkills[ i ].EnableSelectionIcon( false );
            }

            //if (GunSkills[i].itIsGunSkill && !GunSkills[i].GunSkillActive)
            //{
            //    UISprite SelectSpr = GunSkills[i].GUIObject.FindChild("ActiveGun").GetComponent<UISprite>();
            //    SelectSpr.alpha = 0;
            //}

            
        }
    }

    public string defaultWeapon = "GUN_2_SHOT_SKILL";
	const string defaultWeapon2 = "GUN_3_SHOT_SKILL";
    private void SetDefaultGun() 
	{
        //Spawn default turel
            int currSubLevel = PlayerPrefs.GetInt( defaultWeapon, 1 );
            PlayerPrefs.SetInt(defaultWeapon, ( currSubLevel > 1 ? currSubLevel : 1 ) );
			currSubLevel = PlayerPrefs.GetInt( defaultWeapon2, 1 );
			PlayerPrefs.SetInt(defaultWeapon2, ( currSubLevel > 1 ? currSubLevel : 1 ) );
            PlayerPrefs.SetInt(defaultWeapon + "_GunSkillActive", 1);
            PlayerPrefs.SetInt(defaultWeapon + "_avariable", 1);
            setSelectSkill(defaultWeapon);
    }


    private bool CheckSuperSkills(GUNSKILLS obj) {
        //bool isSuperSkill = false;
//
//        switch (obj.name) {
//            case "SuperSpeed":
//#if UNITY_ANDROID
//                GoogleIAB.purchaseProduct("super_speed");
//#endif
//               // isSuperSkill = true;
//                break;
//            case "SuperShield":
//#if UNITY_ANDROID
//                int checkSubLevel = PlayerPrefs.GetInt("SuperShield") + 1;
//                if (checkSubLevel <= 6) {
//                    GoogleIAB.purchaseProduct("super_shied");
//                }
//#endif
//                //isSuperSkill = true;
//                break;
//            case "SuperBomb":
//#if UNITY_ANDROID
//                int checkSubLev = PlayerPrefs.GetInt("SuperBomb") + 1;
//                if (checkSubLev <= 6) {
//                    GoogleIAB.purchaseProduct("super_bomb");
//                }
//#endif
//               // isSuperSkill = true;
//                break;
//            case "SuperShoot":
//#if UNITY_ANDROID
//                GoogleIAB.purchaseProduct("super_shoot");
//#endif
//                //isSuperSkill = true;
//                break;
//        }
      	 return false;
    }

    public void buySuperShoot() {
        PlayerPrefs.SetInt("SuperShoot", 1);
        PlayerPrefs.SetInt("SuperShoot" + "_GunSkillActive", 1);
        PlayerPrefs.SetInt("SuperShoot" + "_avariable", 1);
        for (int i = 0; i < GunSkills.Count; i++) {
            if ("SuperShoot" == GunSkills[i].name) {
                GunSkills[i].Selected = true;
            }
        }
        setActiveGun();
    }

   

   /* 
     private class SavedBackValues {
        public string key;
        public int value;
    }
    
     private void addBackSkill(string key, int value) {
        SavedBackValues val = new SavedBackValues();
        val.key = key;
        val.value = value;
        backSkills.Add(val);
    }*/

}
