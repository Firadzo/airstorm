﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class HelpPlanes : MonoBehaviour {
    public GameObject plane1;
    public GameObject plane2;

    public 	float BulletSpeed = 5; //bullet's speed when shot
	public 	float ShotCoolDown = 20; //How long to wait between each new bullet shot
    private float shotCooldown;

    public Transform FirePos1;
    public Transform FirePos2;
    public GameObject BulletObject; //default bullet (witnout collider and rigitbody)
	
    public bool shootEnable = false;
	private bool planesEnabled;

    private PlayerMoveController _moveController;

    private PlayerScript _playerScript;

    void Awake() {
        _moveController = FindObjectOfType<PlayerMoveController>();
        _playerScript = FindObjectOfType<PlayerScript>();

    }

    void Update() {
		if( planesEnabled )
		{
	        if (shootEnable) {  
	            if (shotCooldown > 0) {
	                shotCooldown -= Time.deltaTime;
	            } else {
	                addBullet(FirePos1, BulletObject);
	                addBullet(FirePos2, BulletObject);
	            }
	        }

	        if (_playerScript && _moveController)
	        {
	            plane1.transform.rotation = _moveController.PlayerModel.transform.rotation;
	            plane2.transform.rotation = _moveController.PlayerModel.transform.rotation;
	        }
		}
    }

    private void addBullet(Transform FirePos, GameObject bullet) {
        
        if (FirePos && bullet) {
			BulletBase _bullet = BulletsManager.GetFreeBullet( bullet.name );
			if( _bullet != null )
			{
				_bullet.Launch( FirePos.position, Quaternion.Euler(0, FirePos.eulerAngles.y, 0), BulletSpeed );
			}
            //if (BulletObjectCopy.rigidbody) {
           //     BulletObjectCopy.rigidbody.AddForce(FirePos.forward * BulletSpeed * 100);
            shotCooldown = ShotCoolDown;
        }
    }

	public void EnablePlanes( float damage, float shootCooldown, float bulletSpeed )
	{
		//GameObject levConroller = GameObject.Find("LevelConroller");
		//SpawnPlayer spawnScript = levConroller.GetComponent<SpawnPlayer>();
		BulletPlayer bullet = BulletObject.GetComponent<BulletPlayer>();
		bullet.Damage = damage;//spawnScript.getStartDamage() * damage;

		ShotCoolDown	= shootCooldown;
		shotCooldown 	= shootCooldown;

		BulletSpeed = bulletSpeed;

		plane1.SetActive( true );
		plane2.SetActive( true );
		planesEnabled = true;
	}

	public void DisablePlanes()
	{
		plane1.SetActive( false );
		plane2.SetActive( false );
		planesEnabled = false; 
	}

    public void ShootEnable(){
        shootEnable = true;
        Debug.Log("help planes shoot enable");
    }

    public void ShootDisable(){
        shootEnable = false;
        Debug.Log("help planes shoot disable");
    }

    /*public void setDamage( float damage ) 
	{
        GameObject levConroller = GameObject.Find("LevelConroller");
        SpawnPlayer spawnScript = levConroller.GetComponent<SpawnPlayer>();
        BulletPlayer bullet = BulletObject.GetComponent<BulletPlayer>();
        bullet.Damage = spawnScript.getStartDamage() * damage;
    }*/

    public float getDamage() 
	{
        if (BulletObject.GetComponent<BulletPlayer>()) {
            return BulletObject.GetComponent<BulletPlayer>().Damage;
        }
        return 0.0f;
    }
	/*
    public void setShootCooldawn( float shootCooldawn ) 
	{
		ShotCoolDown	= shootCooldawn;
        shotCooldown = shootCooldawn;
    }*/
}
