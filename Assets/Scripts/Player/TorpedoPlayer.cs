using UnityEngine;
using System.Collections;

public class TorpedoPlayer : MonoBehaviour {

    public bool waterState = false;//false - air, true - water
    public float torpedoSpeed = 20.0f;

    public 	GameObject 	ExplosionEffect;
    public 	GameObject 	WaterExplosionEffect;
	public	Transform	smokeTrail;

    public 	float 		explosionRadius = 5.0f;
    public 	float 		explosionPower = 10.0f;
    public 	float 		explosionDamage = 100.0f;
    public 	float 		explosionTime = 0.1f;
    public 	float 		destroyObjTime = 0.1f;

    public bool waterSwitch = false;


#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		Gizmos.DrawWireSphere( transform.position, explosionRadius );	
	}
#endif

    // Use this for initialization
    void Start() 
    {
        //Player = GameObject.Find("Player").transform;

        waterState = false;
        waterSwitch = false;

        ParticleEmitter emitter = GetComponentInChildren<ParticleEmitter>();
        if (emitter) emitter.emit = false;
    }

   // private Transform water;

    void Update() 
    {
        if (shootEnable) {

            if (waterState) {
                transform.Translate(new Vector3(0, 0, 1) * Time.deltaTime * torpedoSpeed);
            }

            checkOnScreen();
        }
    }

    void checkOnScreen()
    {
		Vector3 posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
        if (posOnScreen.y > 1) 
        {
            ParticleEmitter emitter = GetComponentInChildren<ParticleEmitter>();
            if (emitter) {
                emitter.emit = false;
                emitter.transform.parent = null;
            }
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other) 
    { 
		if ( other.CompareTag( "Water" ) || other.CompareTag("SmartWater") ) 
        {
            waterState = true;
			if (waterState && !waterSwitch) {
				gameObject.GetComponent<Rigidbody>().useGravity = false;
				gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY;
				
				waterSwitch = true;
				smokeTrail.GetComponent<ParticleEmitter>().emit = true;

				Vector3 pos = smokeTrail.position;
				pos.y = other.transform.position.y + 0.3f;
				smokeTrail.position = pos;
				//gameObject.rigidbody.AddForce(transform.forward * torpedoSpeed * 100);
			}
          //  water = other.transform;
        }

		if (other.CompareTag( "LevelTerrain" ) || other.CompareTag( "Bridge" ) || other.CompareTag( "Enemy" ) )
        {
			Explosion( waterState );
        }
    }

    public void Explosion(bool waterExposion)
    {
        if (waterExposion) 
        {
            Instantiate(WaterExplosionEffect, transform.position, Quaternion.identity);
        } else {
            Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
        }

        Vector3 explosionPosition = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPosition, explosionRadius);

        foreach (var hit in colliders)
        {
            if (!hit) continue;

            //float distance = Vector3.Distance(hit.transform.position, explosionPosition);
            //float hitPoints = 1.0f - Mathf.Clamp01(distance / explosionRadius);
           // hitPoints *= explosionDamage;
			hit.gameObject.SendMessageUpwards("applyDamage", explosionDamage, SendMessageOptions.DontRequireReceiver);
        }

		smokeTrail.parent = null;
		smokeTrail.GetComponent<ParticleEmitter>().emit = false;
        // destroy the explosion
        Destroy(gameObject, destroyObjTime);
    }

    private bool shootEnable = true;
    public void ShootEnable()
    {
        shootEnable = true;
        if (!waterState) {
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            gameObject.GetComponent<Rigidbody>().useGravity = true;
        }
    }

    public void ShootDisable()
    {
        shootEnable = false;
        if (!waterState) {
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            gameObject.GetComponent<Rigidbody>().useGravity = false;
        }
    }
}
