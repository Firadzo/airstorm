using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Turel : MonoBehaviour {

    public 	float BulletSpeed = 5; //bullet's speed when shot
    private float shotCoolDown = 20; //How long to wait between each new bullet shot
    private float shootTimer;

    public bool debugRedraw = false;
    //public List<Guns> defaultBullet;

    public List<Guns> turels = new List<Guns>();
    public int currentTurelIndex = 0;

	public void Init( float delay )
	{
		shotCoolDown = delay;
		shootTimer = shotCoolDown;
	}


    [System.Serializable]
    public class Guns {
        public List<Gun> turel = new List<Gun>();
    }

    [System.Serializable]
    public class Gun {
        public Transform FirePos;
        public GameObject BulletObject; //default bullet (without collider and rigidbody)
    }

    //Constructor
   /* public Turel() {
        shootCooldown = ShotCoolDown;
    }*/

    //private GameObject Player;
    private bool shootEnable = false;

    void Start() {
		shootEnable = PlayerScript.instance.shootEnable;
    }



    private void Update() {
        if (shootEnable) {
            if (shootTimer > 0) {
                shootTimer -= Time.deltaTime;
            } else {
                currentTurelIndex = currentTurelIndex > turels.Count - 1 ? turels.Count - 1 : currentTurelIndex;
                for (int i = 0; i < turels[currentTurelIndex].turel.Count; i++)
				{
                    AddBullet(turels[currentTurelIndex].turel[i].FirePos, turels[currentTurelIndex].turel[i].BulletObject);
                }
            }
        }
    }

    //Bullet
    private void AddBullet(Transform FirePos, GameObject bullet) {
        if (FirePos && bullet) {
           
			BulletBase freebullet = BulletsManager.GetFreeBullet( bullet.name );
			if( freebullet != null )
			{
				freebullet.Launch( FirePos.position, Quaternion.Euler(0, FirePos.eulerAngles.y, 0), BulletSpeed );
			}

            shootTimer = shotCoolDown;
        }
    }

    public void ShootEnable(){
        shootEnable = true;
       // Debug.Log("main turel enable");
    }

    public void ShootDisable(){
        shootEnable = false;
     //   Debug.Log("main turel disable");
    }

    public void setBossBullet(GameObject bullet) {
        currentTurelIndex = currentTurelIndex > turels.Count - 1 ? turels.Count - 1 : currentTurelIndex;
        for (int i = 0; i < turels[currentTurelIndex].turel.Count; i++)
        {
            turels[currentTurelIndex].turel[i].BulletObject = bullet;
        }
    }

    /*public void setDefault() {
        for (int i = 0; i < turels[currentTurelIndex].turel.Count; i++)
        {
            turels[currentTurelIndex].turel[i].BulletObject = defaultBullet[currentTurelIndex].turel[i].BulletObject;
        }
    }*/

    /*public void OnApplicationQuit() {
        for (int i = 0; i < turels[currentTurelIndex].turel.Count; i++)
        {
            turels[currentTurelIndex].turel[i].BulletObject = defaultBullet[currentTurelIndex].turel[i].BulletObject;
        }
    }*/

    public void setDamage(float damage) {
        GameObject levConroller = GameObject.Find("LevelConroller");
        SpawnPlayer spawnScript = levConroller.GetComponent<SpawnPlayer>();
        currentTurelIndex = currentTurelIndex > turels.Count - 1 ? turels.Count - 1 : currentTurelIndex;
        for (int i = 0; i < turels[currentTurelIndex].turel.Count; i++)
        {
            if (turels[currentTurelIndex].turel[i].BulletObject && turels[currentTurelIndex].turel[i].BulletObject.GetComponent<BulletPlayer>()) {
                BulletPlayer bullet = turels[currentTurelIndex].turel[i].BulletObject.GetComponent<BulletPlayer>();
                Debug.Log(" start" + spawnScript.gameObject);
				bullet.Damage = damage;// spawnScript.getStartDamage() * damage;
            }
        }
    }

    public float getDamage() {
        currentTurelIndex = currentTurelIndex > turels.Count - 1 ? turels.Count - 1 : currentTurelIndex;
        if (turels[currentTurelIndex].turel[0].BulletObject && turels[currentTurelIndex].turel[0].BulletObject.GetComponent<BulletPlayer>())
        {
            return turels[currentTurelIndex].turel[0].BulletObject.GetComponent<BulletPlayer>().Damage;
        }
        return 0.0f;
    }

	# if UNITY_EDITOR
	private void OnDrawGizmos() {
		Gizmos.DrawSphere(transform.position, 0.4f);
		for (int j = 0; j < turels.Count; j++) {
			for (int i = 0; i < turels[j].turel.Count; i++)
			{
				if (turels[j].turel[i].FirePos) {
					Gizmos.DrawSphere(turels[j].turel[i].FirePos.transform.position, 0.4f);
					Gizmos.DrawLine(transform.position, turels[j].turel[i].FirePos.transform.position);
				}
			}
		}
	}

	//для быстрой смены одного типа пуль на другой
	public bool 		changeBullet;
	public string 		oldBulletName;
	public	GameObject	newBullet;
	void OnDrawGizmosSelected()
	{
		if( changeBullet )
		{
			changeBullet = false;
			for( int i =0; i < turels.Count; i ++)
			{
				for( int j = 0; j < turels[ i ].turel.Count; j++ )
				{
					if( turels[ i ].turel[ j ].BulletObject.name == oldBulletName )
					{
						turels[ i ].turel[ j ].BulletObject = newBullet;
					}
				}
			}
		}
	}
	#endif
}
