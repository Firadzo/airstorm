using UnityEngine;
using System.Collections;

public class PlayerDestroy : MonoBehaviour {
	
	public 	Transform 	ExplosionEffect;
	const 	float		startVelocty 	= 40f;
	const 	float		angularVelocity = 5f;
    void Awake() {
        Invoke("Respawn", 10);
		GetComponent<AudioSource>().volume = SaveLoadParams.plSoundLevel;
		GetComponent<Rigidbody>().velocity =  Vector3.right * startVelocty;
		GetComponent<Rigidbody>().angularVelocity = Vector3.right * angularVelocity;
    }

	void OnTriggerEnter(Collider other) 
	{
		if( other.tag == "LevelTerrain" || other.tag == "Water" ) {
		    Respawn();
		}
    }

    void Respawn() {
        EffectsManager.PlayEffect( ExplosionEffect.name, transform.position, transform.rotation );
        PlayerScript.instance.StartRespawn();

        Destroy(gameObject);
    }
}
