﻿using System;
using UnityEngine;
using System.Collections;

public class PlayerAcceleratorController : MonoBehaviour {

    public PlayerMoveController MoveController;

    private float _accelX = 0;
    private float _accelY = 0;
    private float _speedUpX = 0;

    private float _middelValueAcellY = -0.5f;
    private float _rotateX;

    private float ay, ax;

    //скорость торможения самолета по горизонтали
    public float SpeedSlowdownByHorizontal = 5f;


    //скорость торможения самолета по вертикали
    public float SpeedSlowdownByVertical = 2f;

    //скорость наклона при перемещении самолета влево/вправо
    const float SpeedRotationByHorizonatl = 1.8f;


    //скорость возвращения самолета в горизонтальное положение
	const float returnToHorizontalRotSpeed = 8f;

    private PlayerScript _player;
	private Transform 	transf;

    private void Awake()
    {
		_player = GetComponent<PlayerScript>();
		transf = transform;

    }

	
	void Start () {
		
		if (MoveController == null) MoveController = GameObject.FindObjectOfType<PlayerMoveController>();
		
	}

	public 	static 	float	accelThreshold 	= 0.2f;
	public 	static 	Vector2	accelerometerOffset = Vector2.zero;
	public	static 	float	sensitivity 	= 1f;

	private float GetAccelerometerValue( float value, float offset )
	{
		value -= offset;
		if ( value > accelThreshold ) {
			return Mathf.Clamp( value * sensitivity, -1f, 1f );
		} 
		else if( value < -accelThreshold ) {
			return Mathf.Clamp( -value * sensitivity, -1f, 1f );
		}
		return 0;
	}

    // Update is called once per frame
	void Update () {

		if( PlayerScript.endLevelReached )
		{ 
			if( PlayerScript.instance.UseEndLevelSpline )
			{  
				MoveController.PlayerModel.transform.localEulerAngles = new Vector3( 0, 90f, 0 );
				return; 
			}
			_rotateX = Mathf.Lerp( _rotateX, 0f, returnToHorizontalRotSpeed * Time.deltaTime );
			MoveController.PlayerRotate( _rotateX, 0 );
			return; 
		}

	    if (MoveController != null)
        {
#if UNITY_EDITOR
			ay = GetAccelerometerValue( Input.GetAxis("Vertical"), accelerometerOffset.y );
			ax = -GetAccelerometerValue( Input.GetAxis("Horizontal"), accelerometerOffset.x  );
#else
			ay = GetAccelerometerValue( Input.acceleration.y, accelerometerOffset.y );
			ax = -GetAccelerometerValue( Input.acceleration.x, accelerometerOffset.x );
#endif

			Vector3 targetPos = transf.position;

			if( ay != 0 || ax != 0  )
			{
				//Vector3 currentFrameSpeed = 
				targetPos = transf.position + new Vector3( _player.VerticalSpeed * Time.deltaTime * ay, 0, _player.HorizontalSpeed * ax * Time.deltaTime );
				MoveController.CheckAndMoveCameraByHorizontal( Camera.main.WorldToViewportPoint( targetPos ) );//с учетом позиции в следующем кадре при той же скорости движения
				targetPos = MoveController.CheckPosition( targetPos );
			}
			MoveController.Move( transf, targetPos.x, targetPos.y, targetPos.z );
			_rotateX = Mathf.Lerp( _rotateX, ( ax >= -0.2f && ax <= 0.2f ? 0 : ax ), ( ax == 0 ? returnToHorizontalRotSpeed : SpeedRotationByHorizonatl ) * Time.deltaTime);
			MoveController.PlayerRotate( _rotateX, 0);
			MoveController.CheckAndMoveCameraByHorizontal( Camera.main.WorldToViewportPoint( transform.position ) );
        }
	}

    public float SpeedRotate {
        get { return -_rotateX; }
    }
}
