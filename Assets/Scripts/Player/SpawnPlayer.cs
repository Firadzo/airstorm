using UnityEngine;
using System.Collections;

public class SpawnPlayer : MonoBehaviour {
    public Transform Player;
	public Transform CamPlayerMove;

    //player spawn parametres
    //il2
    //spidfire
    //gloster
    //mig-9
   
    public GameObject[] PlayerPlanes;
	public GameObject[] shields;
    public float[] startHP;
    public float[] startSpeed;
    //public float[] startDamage;
    public Transform[] PlayerCrachedPlanes;

    public Spline StartAnimation;
    public Vector3 animCameraPosition = new Vector3(20, 7, -0.69f);
    public Vector3 animCameraRotation = new Vector3(0, 270, 0);

    public GameObject InGameMenu;
    public GameObject ComixArhor; 
    public string[] currComixes;

    public float timeShowComix = 10.0f;

	[HideInInspector]
    public int typeSpawn = 0;
   // private int difficultyLevel = 0;
    private Transform Player2;
    private GameObject modelPl;
    private PlayerScript playerScr;
    private GameObject comix;
    private GameObject levelConroller;
    private PlayerCamera cam;
    private int cameraMask = 0;

    private static bool comixDestroyed = false;

	public static bool ComixDestroyed
	{
		get{ return comixDestroyed; }
	}
    //private bool comixEnable2 = false;

    public bool spawned = false;

	void Awake()
	{
		//SaveLoadParams.LoadParams();
	}

	void Start() {

		LevelConroller.spawnDisabled = true;

        if (PlayerPrefs.GetInt("replay", -1) <= 0) {
            //spawnPlayer();
        }
		Localization.LocalizeAll();
   /*     int loc = PlayerPrefs.GetInt("Localization", 0);
	    Localization locObj = FindObjectOfType<Localization>();

	    if (locObj != null)
	    {
	        if (loc == 0)
	        {
	            locObj.currentLanguage = "English";
	        }
	        else if (loc == 1)
	        {
	            locObj.currentLanguage = "Russian";
	        }
	    }*/

	}

    /*public void spawnPlayer(LevelConroller.UnitLoadStats playerStats) {
        //SaveLoadParams.LoadParams();

        if (PlayerPrefs.GetInt("replay", -1) > 0) {
            PlayerPrefs.SetInt("replay", -1);
        }
        //typeSpawn = SaveLoadParams.plPlane;
       // difficultyLevel = SaveLoadParams.plDiff;

        Debug.Log("Spawn player" + typeSpawn);

        //Set camera to player 
        cam = Camera.main.GetComponent<PlayerCamera>();
        cam.Player = Player.gameObject;
        cam.playerMove = CamPlayerMove.gameObject;

        if (Player)
        {
            playerScr = Player.GetComponentInChildren<PlayerScript>();

            modelPl = Instantiate(PlayerPlanes[typeSpawn], Player.transform.position, Quaternion.identity) as GameObject;

            playerScr.PlayerDestroyPlane = PlayerCrachedPlanes[typeSpawn];
			playerScr.Health 	= playerStats.health;
			playerScr.maxHeath 	= playerStats.health;
			playerScr.gunDamage = playerStats.damage;
			playerScr.playerLives = SaveLoadParams.plLivesNumber;
            PlayerMoveController controlParams = GameObject.FindObjectOfType<PlayerMoveController>();
            //controlParams.DefaultVerticalSpeed = startSpeed[typeSpawn];
            //controlParams.DefaultHorizontalSpeed = startSpeed[typeSpawn];
            controlParams.PlayerModel = modelPl;

            if (modelPl != null && !StartAnimation)
            {
                modelPl.transform.parent = Player.transform;
            }
        }

        levelConroller = GameObject.Find("LevelConroller");

        showComixes(false);

        if (StartAnimation && comixEnable2 == false)
        {
            disableMoveAndSpawn();
            showStartAnimation();
        }
    }*/

	public void spawnPlayer() {
		//SaveLoadParams.LoadParams();
		if (PlayerPrefs.GetInt("replay", -1) > 0) {
			PlayerPrefs.SetInt("replay", -1);
		}
		typeSpawn = SaveLoadParams.plPlane;
		// difficultyLevel = SaveLoadParams.plDiff;
		
		//Debug.Log("Spawn player" + typeSpawn);
		
		//Set camera to player 
		cam = Camera.main.GetComponent<PlayerCamera>();
		cam.Player 		= Player.gameObject;
		cam.playerMove = CamPlayerMove.gameObject;
		
		if (Player)
		{
			playerScr = Player.GetComponentInChildren<PlayerScript>();
			
			modelPl = Instantiate(PlayerPlanes[typeSpawn], Player.transform.position, Quaternion.identity) as GameObject;
			GameObject shield = Instantiate( shields[typeSpawn], Player.transform.position, Quaternion.identity) as GameObject;
			playerScr.Spawn( startSpeed[ typeSpawn ], startHP[ typeSpawn ], PlayerCrachedPlanes[ typeSpawn ], shield.GetComponent<PlayerShield>() );
			playerScr.playerLives = SaveLoadParams.plLivesNumber;
			PlayerMoveController controlParams = GameObject.FindObjectOfType<PlayerMoveController>();
			//controlParams.DefaultVerticalSpeed = startSpeed[typeSpawn];
			//controlParams.DefaultHorizontalSpeed = startSpeed[typeSpawn];
			controlParams.PlayerModel = modelPl;
			
			if (modelPl != null && !StartAnimation)
			{
				modelPl.transform.parent = Player.transform;
				shield.transform.parent = modelPl.transform;
			}
			playerScr.SetPlaneBlinker();
		}
		
		levelConroller = GameObject.Find("LevelConroller");
		MyAdMob.HideBunner();
		ShowComixes(false);
		
		if ( StartAnimation )
		{
			showStartAnimation();
		}

		/*if( !comixEnable2 )
		{
			DisableMoveAndSpawn();
		}*/
	}

    /*public float getStartDamage() {
        return startDamage[typeSpawn];
    }*/

    public void ShowComixes(bool endComix) 
    {
    	if (ComixArhor)
    	{
			//disable spawn and move, time to show comix))
			DisableMoveAndSpawn();

	        if (endComix) 
	        {
	            cameraMask = cam.GetComponent<Camera>().cullingMask;
	            cam.GetComponent<Camera>().cullingMask = 0;

				GameObject currentCommix = Resources.Load<GameObject>("Comixes/"+currComixes[currComixes.Length-1] ) as GameObject;

				comix = Instantiate( currentCommix, ComixArhor.transform.position, Quaternion.identity ) as GameObject;
	            comix.transform.parent = ComixArhor.transform;
	            comix.transform.localScale = new Vector3(1, 1, 1);

	            if (InGameMenu) InGameMenu.SetActive( false );
	            return;
	        }

	        int currentLevel = SaveLoadParams.currLevel - 1;//(SaveLoadParams.plDoneLevels - 1 < 0) ? 0 : SaveLoadParams.plDoneLevels - 1;

	        if ( currComixes.Length > 0 )
	        {
				cameraMask = cam.GetComponent<Camera>().cullingMask;
				cam.GetComponent<Camera>().cullingMask = 0;
				GameObject currentCommix = Resources.Load<GameObject>("Comixes/"+currComixes[ currentLevel ] ) as GameObject;
				comix = Instantiate( currentCommix, ComixArhor.transform.position, Quaternion.identity) as GameObject;
	            comix.transform.parent = ComixArhor.transform;
	            comix.transform.localScale = new Vector3(1, 1, 1);
	            //UIButtonMessage msg = comix.GetComponentInChildren<UIButtonMessage>();
	            //msg.target = levelConroller;

				if (InGameMenu) InGameMenu.SetActive( false );
				//comixEnable2 = true;
				comixDestroyed = false;
	        }
	        else
	        {
	            Debug.Log("No comix on level");
	        }

	       StartCoroutine("ClearStartComix");
		}

    }

    IEnumerator ClearStartComix() 
    {
        yield return new WaitForSeconds(timeShowComix);
        if(!comixDestroyed)destroyComix();
    }

    public void destroyComix() 
    {
        StopCoroutine("ClearStartComix");

        Destroy(comix);
		Resources.UnloadUnusedAssets ();
        comixDestroyed = true;

        if (InGameMenu) InGameMenu.SetActive( true );

        cam.GetComponent<Camera>().cullingMask = cameraMask;

        if (StartAnimation)
        {
            showStartAnimation();
        }
        else
        {
			//Debug.Log("Move enable");
			playerScr.StartGame();
            LevelConroller.spawnDisabled = false;
        }

		AudioManager.instance.playLevelMusic();
    }

    void DisableMoveAndSpawn() 
    {
        playerScr.setMoveDisable();
        playerScr.setShootDisable();
 
		LevelConroller.spawnDisabled = true;
    }

    void showStartAnimation() 
    {
        StartAnimation startanim = modelPl.AddComponent<StartAnimation>();
        startanim.ANGLE_FIX = 90;
        startanim.RotationFIX = new Vector3(0, 1, 0);
        startanim.wrapMode = WrapMode.Once;
        startanim.speed = 0.05f;
        startanim.enable = true;
        startanim.spline = StartAnimation;
        

        GameObject camera = new GameObject("AnimCamera");
        camera.AddComponent<Camera>();
        camera.transform.parent = modelPl.transform;
        camera.transform.localEulerAngles = animCameraRotation;
        camera.transform.localPosition = animCameraPosition;
        
    }
}
