using UnityEngine;
using System.Collections;

public class DownloadObbExampleTest : MonoBehaviour {
	
	private string expPath;
	private string logtxt;
//	private bool alreadyLogged = false;
	private string nextScene = "mainMenu";
	private bool downloadStarted;
	//public float delay;


	// || UNITY_ANDROID
	void Awake()
	{
		//#if UNITY_EDITOR
		//Application.LoadLevel( nextScene );
		//#endif

		StartCoroutine(loadLevel());
	}
	

	void log( string t )
	{
		logtxt += t + "\n";
		print("MYLOG " + t);
	}
	/*
	void OnGUI()
	{
		string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
		if ( mainPath == null && !downloadStarted )
		{
			//GUI.Label(new Rect( Screen.width- ( Screen.width * 0.75f ), Screen.height - ( Screen.height- 0.5f ), 430, 60), "The game needs to download  content. It's recommanded to use WIFI connection.");
			if (GUI.Button(new Rect( Screen.width * 0.05f, Screen.height * 0.6f, Screen.width * 0.9f, Screen.height * 0.1f ), 
			               "Need download content.Start Download"))
			{
				downloadStarted = true;
				GooglePlayDownloader.FetchOBB();
			}
		}
		if (GUI.Button(new Rect( Screen.width * 0.05f, Screen.height * 0.3f, Screen.width * 0.9f, Screen.height * 0.1f ), 
		               "Load Scene"))
		{
			Application.LoadLevel( nextScene );
		}
		/*if ( Application.platform == RuntimePlatform.Android )
		{
			//Application.LoadLevel( nextScene );
			//return;
		}
		
		expPath = GooglePlayDownloader.GetExpansionFilePath();
		if (expPath == null)
		{
			GUI.Label(new Rect(10, 10, Screen.width-10, 20), "External storage is not available!");
		}
		else
		{
			string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
			//string patchPath = GooglePlayDownloader.GetPatchOBBPath(expPath);
			if( alreadyLogged == false )
			{
				alreadyLogged = true;
				log( "expPath = "  + expPath );
				log( "Main = "  + mainPath );
				log( "Main = " + mainPath.Substring(expPath.Length));
				
				if (mainPath != null)
					StartCoroutine(loadLevel());
				
			}
			//GUI.Label(new Rect(10, 10, Screen.width-10, Screen.height-10), logtxt );
			
			if (mainPath == null)
			{
				GUI.Label(new Rect(Screen.width-600, Screen.height-230, 430, 60), "The game needs to download  content. It's recommanded to use WIFI connection.");
				if (GUI.Button(new Rect(Screen.width-500, Screen.height-170, 250, 60), "Start Download"))
				{
					GooglePlayDownloader.FetchOBB();
					StartCoroutine(loadLevel());
				}
			}
			
		}*/
		
	//}

	protected IEnumerator loadLevel() 
	{ 
		/*string mainPath;
		do
		{
			yield return new WaitForSeconds(0.5f);
			mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);	
		}
		while( mainPath == null );*/
		yield return new WaitForSeconds(3f);
		Application.LoadLevel(nextScene);
		/*if( downloadStarted == false )
		{
			downloadStarted = true;
			
			string uri = "file://" + mainPath;
			log("downloading " + uri);
			WWW www = WWW.LoadFromCacheOrDownload(uri , 0);		
			// Wait for download to complete
			yield return www;
			
			if (www.error != null)
			{
				log ("wwww error " + www.error);
			}
			else
			{
				Application.LoadLevel(nextScene);
			}
		}*/
	}
}
