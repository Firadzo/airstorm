using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Soomla.Store;

public class GoogleIABEventListener : MonoBehaviour {

    public UILabel storeMoneyLbl;

    private void Awake() 
	{
		MyTapjoy.SetStoreLabel( storeMoneyLbl );
		SoomlaEventHandler.storeMoneyLbl = storeMoneyLbl;
    }

	private void ShowOfferWall()
	{
		TapjoyUnity.Tapjoy.ShowOffers();
	}

    public void buyProduct(string id) 
	{
		switch( id )
		{
		case "FIVE_THOUSANDS_COINS":
			SoomlaStore.BuyMarketItem ( MyIStore.FIVE_THOUSANDS_COINS, "" );
			break;
		case "SEVEN_THOUSANDS_COINS":
			SoomlaStore.BuyMarketItem ( MyIStore.SEVEN_THOUSANDS_COINS, "" );
			break;
		case "TEN_THOUSANDS_COINS":
			SoomlaStore.BuyMarketItem ( MyIStore.TEN_THOUSANDS_COINS, "" );
			break;
		case "MIG":
			SoomlaStore.BuyMarketItem ( MyIStore.MIG, "" );
			break;
		case "DISABLE_ADS":
			SoomlaStore.BuyMarketItem ( MyIStore.DISABLE_ADS, "" );
			break;

		}

    }

    public void Reset() {
		storeMoneyLbl.text = SaveLoadParams.plMoney.ToString();//(startValue + toValue) + "";
        //animLabel = false;
    }
}


