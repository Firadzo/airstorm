﻿using UnityEngine;
using System.Collections;

public class EndLevelTrigger : MonoBehaviour
{

    private EndLevelPopup _endLevelPopup;

    private void Start()
    {
        _endLevelPopup = FindObjectOfType<EndLevelPopup>();
        Debug.Log("EndLevelTrigger Start");
    }

    void FixedUpdate()
	{
		transform.position = new Vector3(this.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
	}

    void OnTriggerEnter(Collider collider) 
    {
        Debug.Log("OnTriggerEnter collision.gameObject = " + collider.gameObject);
        if ( collider.tag == "Player" )
        {
            if (_endLevelPopup)
                _endLevelPopup.SendMessage("endLevel", SendMessageOptions.RequireReceiver);
        }
    }
}
