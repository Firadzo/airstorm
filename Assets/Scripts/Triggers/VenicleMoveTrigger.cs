using UnityEngine;
using System.Collections;

public class VenicleMoveTrigger : MonoBehaviour {
	
	public SplineVenicle[] VenicleMoveList;
	//public int TriggerId = 0;

	static Transform	mainCamera;


	void Awake()
	{
		if ( mainCamera == null ) 
		{
			mainCamera = Camera.main.transform;
		}
		Debug.Log ( transform.position );
	}

	void Start()
	{	
		for( int i = 0; i < VenicleMoveList.Length; i++ )
		{
			if ( VenicleMoveList[i] ) {
			    VenicleMoveList[i].PauseMove();
			}
		}
	}


	void Update() {
		if ( mainCamera.position.x >= transform.position.x ) 
		{
            Collison();
        }
	}


	void Collison() {
		for( int i = 0; i < VenicleMoveList.Length; i++ )
		{
			if( VenicleMoveList[i] ) VenicleMoveList[i].StartMove();
		}
        Destroy(gameObject);
	}
}
