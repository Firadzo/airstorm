using UnityEngine;
using System.Collections;

public class SoildersMoveTrigger : MonoBehaviour {

    public GameObject SoildersAnimRoot;
    public bool used = false;

    public SplineVenicle[] SoildersAnim;
    public AISoldier[] Soilder;
    private int cnt = 0;

    void Start() 
    {
        SoildersAnim = SoildersAnimRoot.GetComponentsInChildren<SplineVenicle>();
        Soilder = SoildersAnimRoot.GetComponentsInChildren<AISoldier>();

        Spline[] splines = SoildersAnimRoot.GetComponentsInChildren<Spline>();
        for (int k = 0; k < splines.Length; k++)
        {
            splines[k].gameObject.SetActive(false);
        }

        for (int i = 0; i < SoildersAnim.Length; i++)
        {
            if (SoildersAnim[i])
            {   
                SoildersAnim[i].PauseMove();
            }
        }

        for (int j = 0; j < Soilder.Length; j++)
        {
            if (Soilder[j]) Soilder[j].pauseAnim();
        }

        if (gameObject.GetComponent<BoxCollider>()) {
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }

    void FixedUpdate()
    {
        if (Camera.main.transform.position.x >= transform.position.x)
        {
            Collison(); 
        }

        if (cnt > 2) return;
        cnt++;
		transform.position = new Vector3(this.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
    }

    void Collison()
    {
        if (!used)
        {
            used = true;
        }
        else
        {
            return;
        }
       

        Spline[] splines = SoildersAnimRoot.GetComponentsInChildren<Spline>();
        for (int k = 0; k < splines.Length; k++)
        {
            splines[k].gameObject.SetActive(false);
        }

        for (int i = 0; i < SoildersAnim.Length; i++)
        {
            if (SoildersAnim[i])
            {
                SoildersAnim[i].StartMove();
//                Debug.Log("SoildersAnim[i].StartMove()");
            }
        }

        for (int j = 0; j < Soilder.Length; j++)
        {
            if (Soilder[j]) Soilder[j].startAnim();
        }
        Destroy(gameObject);
    }
}
