﻿using UnityEngine;
using System.Collections;

public class VehicleObserverBridge : MonoBehaviour
{

    public BridgeDestroyTrigger BridgeDestroyTrigger;

    public Animator Animator;

    public SplineVenicle SplineVenicle;

	// Use this for initialization
	void Start ()
	{
        if (!BridgeDestroyTrigger)
            BridgeDestroyTrigger = FindObjectOfType<BridgeDestroyTrigger>();

	    if (BridgeDestroyTrigger)
	        BridgeDestroyTrigger.OnDestroyed += OnDestroyedHandler;

	    if (!SplineVenicle)
	        SplineVenicle = GetComponent<SplineVenicle>();

        if (!Animator)
            Animator = GetComponent<Animator>();
	}


    private void OnDestroyedHandler()
    {
        if (Animator)
        {
            Animator.enabled = false;
        }

        if (SplineVenicle)
            SplineVenicle.enabled = false;
    }
}
