using UnityEngine;
using System.Collections;

public class TrainMoveTrigger : MonoBehaviour {

    public GameObject Train;
	private Transform	cameraTransf;

	void Awake()
	{
		cameraTransf = Camera.main.transform;
	}

    void LateUpdate() {
		if ( cameraTransf.position.x >= transform.position.x) {
            Collison();
        }
    }

    void Collison()
    {

        if (Train) 
        {
            AITrain train = Train.GetComponent<AITrain>();
            train.animationEnable = true;
        }
        Destroy(gameObject);
    }
}
