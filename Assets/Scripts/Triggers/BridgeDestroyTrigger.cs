using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class BridgeDestroyTrigger : MonoBehaviour
{
    private Action 		_onDestroyed;

    public event Action OnDestroyed {
        add { _onDestroyed += value; }
        remove { _onDestroyed -= value; }
    }

    public float 	bridgeDestroyMessageRadius = 100;
	public int 		health = 1;
	public Transform ExplosionEffect;
	
	public bool destroyState = false;
    public bool useAutoPhysAnim = false;
    public bool physAddCollider = false;
    public int physForceValue = 2000;

    public Transform monetObj;
    public int monetCount = 3;
	
	public void applyDamage(float hp)
	{
		health -= (int)hp;	
		if(health <= 0 && !destroyState)
		{
			destroyState = true;
			EffectsManager.PlayEffect ( ExplosionEffect.name, transform.position + (new Vector3(0, 1.6f, 0)), transform.rotation );
			destroyBridge();
		}
	}

#if UNITY_EDITOR

	void OnDrawGizmosSelected()
	{
		Gizmos.DrawWireSphere( transform.position, bridgeDestroyMessageRadius );
	}
#endif
	
	void destroyBridge() {
        if (useAutoPhysAnim) {
            Transform[] root = GetComponentsInChildren<Transform>();

            for (int d = 0; d < root.Length; d++)
            {
                GameObject obj = root[d].gameObject;
                obj.transform.parent = null;
                obj.AddComponent<Rigidbody>();
                if (physAddCollider) {
                    obj.AddComponent<BoxCollider>();
                   // obj.layer = LayerMask.NameToLayer("No")
                }
                obj.GetComponent<Rigidbody>().mass = 1;
                obj.GetComponent<Rigidbody>().useGravity = true;
                Vector3 vect = new Vector3( Random.Range( -1f, 1f ), Random.Range( -1f, 1f ), Random.Range( -1f, 1f ) );
                obj.GetComponent<Rigidbody>().AddForce(vect * physForceValue, ForceMode.Impulse);
                Destroy(obj, 10);
            }   
        } else {
            if (GetComponent<Animation>()) {
                GetComponent<Animation>().Play();
            }
            else if (transform.GetChild(0) && transform.GetChild(0).GetComponent<Animation>()) 
            {
                transform.GetChild(0).GetComponent<Animation>().Play();
            }
            else
            {
                Debug.Log("No animation find and no destroy by physics");
            }
        }


        Collider[] colliders = Physics.OverlapSphere( transform.position, bridgeDestroyMessageRadius );

        for (int i = 0; i < colliders.Length; i++) {
            colliders[i].gameObject.SendMessage("BridgeDestroy", SendMessageOptions.DontRequireReceiver);
        }

	    if (_onDestroyed != null) _onDestroyed();
	}

    private void addCoin()
    {
        if (monetObj)
        {     
            for (int i = 0; i < monetCount; i++)
            {
                Vector3 pos = new Vector3(Random.Range(-4, 4), transform.localPosition.y, Random.Range(-4, 4));
				MedalsManager.Spawn( monetObj.name, transform.TransformPoint( pos ) );
            }     
        }
    }
}
