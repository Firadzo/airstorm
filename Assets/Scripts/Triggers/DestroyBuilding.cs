using UnityEngine;
using System.Collections;

public class DestroyBuilding : MonoBehaviour {

	public float Health = 10.0f; //health
	public GameObject ExplosionEffect; //The effect created when the plane explodes
	public GameObject destroyedModel;
	
	public bool destroyState = false;

    public GameObject[] effectParts;
	public float explosionForse = 25;

    public Transform monetObj;
    public int monetCount = 1;
    private int monetChance = 0;

    public AudioClip destroyEff;

	public GameObject DeadFirePref;

    void Awake() 
    {
        gameObject.AddComponent<AudioSource>();
        GetComponent<AudioSource>().volume = SaveLoadParams.plSoundLevel;
        GetComponent<AudioSource>().rolloffMode = AudioRolloffMode.Linear;
        GetComponent<AudioSource>().minDistance = 400f;
        GetComponent<AudioSource>().clip = destroyEff;
    }

	void Start()
	{
		return;
		monetChance = LevelConroller.instance.housesChancePer;
		//DestroyedObjectsManager.Spawn( destroyedModel.name, transform.position, transform.rotation );
		//gameObject.SetActive (false);
	}

	public void applyDamage(float hp)
	{
		Health -= hp;	
		if(Health <= 0 && !destroyState)
		{
			GetComponent<AudioSource>().Play();
			
			destroyState = true;
			
			//Quaternion rot = new Quaternion(0, transform.rotation.y, 0, 0);
			if (ExplosionEffect) {
				EffectsManager.PlayEffect( ExplosionEffect.name, transform.position + new Vector3(0, 20, 0), transform.rotation );
			}
			
			if (destroyedModel) {
				DestroyedObjectsManager.Spawn( destroyedModel.name, transform.position, transform.rotation );
				//obj.transform.eulerAngles = new Vector3(0, gameObject.transform.eulerAngles.y, 0);
			}
			
			if( DeadFirePref != null )
			{
				Transform fire = ((GameObject)Instantiate(DeadFirePref, transform.position, Quaternion.identity)).transform;
				fire.Rotate(new Vector3(-90,0,0));
			}
			
			//destroyedModel
			Destroy(gameObject);
			
			addCoin();
			
			if (effectParts.Length > 0) 
			{
				int countParts = Random.Range(3, effectParts.Length);
				
				for (int i = 0; i < countParts; i++) 
				{
					int index = Random.Range(0, effectParts.Length - 1);	
					Vector3 dir = Random.insideUnitCircle;
					Vector3 movementVec = new Vector3(dir.y, transform.position.normalized.y*15, dir.x);
					//Debug.DrawRay(transform.position, new Vector3(dir.y, transform.position.normalized.y*15, dir.x)*50, Color.red);		
					GameObject part = Instantiate(effectParts[index], transform.position, Quaternion.identity) as GameObject;
					part.GetComponent<Rigidbody>().AddForce(movementVec * explosionForse* 100);	
					Destroy(part, 3);	
				}
			}
		}
	}

    private void addCoin() {
        if (monetObj) {  
            int rand = Random.Range(0, 100);
            if (rand <= monetChance) {
                for (int i = 0; i < monetCount; i++) {
					MedalsManager.Spawn( monetObj.name, transform.TransformPoint( new Vector3( Random.Range(-4, 4), transform.localPosition.y, Random.Range(-4, 4)) ) );
                }
            }
        }
    }
}
