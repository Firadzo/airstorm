﻿using UnityEngine;
using System.Collections;

public class BulletBase : MonoBehaviour {


	protected 	GameObject 	obj;
	protected	Transform	transf;
	protected 	Rigidbody	rigidBodyCached;

	public static bool	userRaycast = true;
	
	protected virtual void Awake()
	{
		rigidBodyCached = GetComponent<Rigidbody>();
		transf 	= transform;
		transf.parent = BulletsManager.getTransform;
		obj = gameObject;
		if (GetComponent<AudioSource>()) {
			GetComponent<AudioSource>().playOnAwake = false;
		}
		obj.SetActive( false );
	}

	public virtual bool isReady
	{ 
		get { return !obj.activeSelf; }
	}
	
	public virtual void Launch( Vector3 launchPos, Quaternion launchRot, Vector3 direction, float damage )
	{
		//Launch( launchPos, launchRot, direction );
	}

	public virtual void Launch( Vector3 launchPos, Quaternion launchRot, Vector3 direction )
	{
		transf.position = launchPos;
		transf.rotation = launchRot;
		rigidBodyCached.velocity = Vector3.zero;
		rigidBodyCached.isKinematic = false;
		obj.SetActive( true );
		rigidBodyCached.AddForce( direction );
	}

	public virtual void Launch( Vector3 launchPos, Quaternion launchRot )
	{
		transf.position = launchPos;
		transf.rotation = launchRot;
		obj.SetActive( true );
	}

	public virtual void Launch( Vector3 launchPos, Quaternion launchRot, float speed )
	{
		Launch( launchPos, launchRot );
	}

	public virtual void Launch( Vector3 launchPos, Vector3 launchAngle, float speed )
	{
		transf.position = launchPos;
		transf.eulerAngles = launchAngle;
		obj.SetActive( true );
	}

	public virtual void Launch( Vector3 launchPos, Transform target, float speed )
	{
		transf.position = launchPos;
		transf.LookAt( target );
		obj.SetActive( true );
	}
}
