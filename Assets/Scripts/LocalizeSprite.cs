﻿using UnityEngine;
using System.Collections;

public class LocalizeSprite : MonoBehaviour {
	public	UISprite			engBtn;
	public	GameObject			disableOnEng;
	public 	UISprite			ruBtn;
	public	GameObject			disableOnRu;

	// Use this for initialization
	void Awake () 
	{
		OnLocalize( PlayerPrefs.GetString("Language", "English") );
		Localization.AddToLocalization( this );
	}


	void OpenStore()
	{
		EndLevelPopup.instance.openStore();
	}

	public void OnLocalize( string language )
	{
		if( language == "Russian" )
		{
			engBtn.enabled = false;
			ruBtn.enabled = true;
			if( disableOnEng )
			{
				disableOnEng.SetActive( true );
			}
			if( disableOnRu )
			{
				disableOnRu.SetActive( false );
			}

		}
		else
		{
			engBtn.enabled = true;
			ruBtn.enabled = false;
			if( disableOnEng )
			{
				disableOnEng.SetActive( false );
			}
			if( disableOnRu )
			{
				disableOnRu.SetActive( true );
			}
		}
	}
}
