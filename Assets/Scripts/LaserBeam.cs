﻿using UnityEngine;
using System.Collections;

public class LaserBeam : MonoBehaviour {
	public 	float 		damagePerSec;
	public 	float		scaleFactor;
	public	Transform	hitPlayerEffect;
	public 	Transform	beamTransform;

	private bool		damagePlayer;
	private Transform	transf;

	void Awake()
	{
		transf  = transform;
	}
	

	void OnTriggerEnter( Collider other )
	{
		damagePlayer = true;
		hitPlayerEffect.gameObject.SetActive( true );
		//Debug.Log( other.tag + " enter " );
	}

	void OnTriggerExit( Collider other )
	{
		damagePlayer = false;
		Vector3 temp = beamTransform.localScale;
		temp.y = 1f;
		beamTransform.localScale = temp;
		hitPlayerEffect.gameObject.SetActive( false );
		temp = beamTransform.localEulerAngles;
		temp = Vector3.zero;
		temp.x = 90f;
		beamTransform.localEulerAngles = temp;
		//Debug.Log( other.tag + " leave " );
	}

	float length;
	// Update is called once per frame
	void Update () 
	{

		if( damagePlayer )
		{
			Quaternion rot = Quaternion.LookRotation( ( PlayerScript.getPosition - transf.position ).normalized );
			Vector3 temp = beamTransform.eulerAngles;
			temp.x = 90f + rot.eulerAngles.x;
			//transf.eulerAngles = temp;
			beamTransform.eulerAngles = temp;
			PlayerScript.instance.HitPlayer( damagePerSec * Time.deltaTime );
			float x = Mathf.Abs( PlayerScript.getPosition.x - transf.position.x );
			float z = Mathf.Abs( PlayerScript.getPosition.z - transf.position.z );
			length = Mathf.Sqrt( x * x + z * z );

			temp = hitPlayerEffect.localPosition;
			temp.z = length; //+ transf.localPosition.z;
			hitPlayerEffect.localPosition = temp;

			temp = hitPlayerEffect.position;
			temp.y = PlayerScript.getPosition.y + 0.25f;
			hitPlayerEffect.position = temp;

			temp = beamTransform.localScale;
			temp.y = length / scaleFactor;
			beamTransform.localScale = temp;

		}
	}
}
