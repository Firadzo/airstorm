﻿using UnityEngine;
using System.Collections;

public class MainMenuMusic : MonoBehaviour {

	const string mainMenuSceneName = "MainMenu";
	static MainMenuMusic instance;	
	private AudioSource	audioSource;

	private bool playMusic
	{
		get { return ( LoadSceneFromBundle.currentScene == "MainMenu" || string.IsNullOrEmpty( LoadSceneFromBundle.currentScene ) ); }
	}

	public static bool isNull
	{
		get{ return ( instance == null );}
	}

	void Awake () {
		if( instance != null )
		{
			if( instance != this )
			{
				Destroy( gameObject );
				return;
			}
		}
		else
		{
			instance = this;
		}

		audioSource = GetComponent<AudioSource>();
		audioSource.volume = SaveLoadParams.GetMusicLevel() * AudioManager.GLOBAL_MUSIC_LEVEL;
		DontDestroyOnLoad( gameObject );
		if( !playMusic  )
		{
			audioSource.Stop();
		}
	}

	void Start() 
	{

	}

	void Update()
	{
		if (playMusic) 
		{
			if (!audioSource.isPlaying) {
				audioSource.Play ();
			}
			audioSource.volume = SaveLoadParams.plMusicLevel * AudioManager.GLOBAL_MUSIC_LEVEL;
		} 
		else {
			if( audioSource.isPlaying )
			{
				audioSource.Stop();
			}
		}

	}

}
