using UnityEngine;
using System.Collections;

public class BillboardScript : MonoBehaviour
{
	public Vector3 rotateAngle = new Vector3( -90f, 0, 0 );
    public void Update()
    {
        transform.LookAt(Camera.main.transform.position);
		transform.Rotate( rotateAngle );
    }

}

