﻿using UnityEngine;
using System.Collections;

public class ResolutionFix : MonoBehaviour {

	const float DEFAULT_HEIGHT 			= 1280f;
	const float	DEFAULT_WIDTH 	 		= 800f;
	public Transform[]					fixPos;
	private static float				scaleFactor;
	//const float	DEFAULT_MANUAL_HEIGHT 	= 760f;

	//private UIRoot	uiRoot;

	// Use this for initialization
	void Awake () {
		//uiRoot = GetComponent<UIRoot>();
		float defaultScreenRatio = DEFAULT_WIDTH / DEFAULT_HEIGHT;
		//Debug.Log( Screen.width+" "+Screen.height );
		float curScreenRatio = (float)Screen.width / Screen.height;
		//Debug.Log( defaultScreenRatio +"  "+curScreenRatio );

		scaleFactor = curScreenRatio / defaultScreenRatio;
		//Debug.Log( scaleFactor );
	//	uiRoot.manualHeight = Mathf.RoundToInt( DEFAULT_MANUAL_HEIGHT * scaleFactor );
		transform.localScale  *= scaleFactor;
		//Debug.Log( scaleFactor );

	}


	void Start()
	{
		if( fixPos != null && scaleFactor > 1 )
		{
			//Vector3 pos;
			for( int i = 0; i < fixPos.Length; i++ )
			{
				//pos =	fixPos[ i ].localPosition;
			//pos.y += pos.y - ( pos.y * scaleFactor );
				//fixPos[ i ].localPosition = pos;
				fixPos[ i ].localScale -= Vector3.one * scaleFactor - Vector3.one;
			}
		}
	}


#if UNITY_EDITOR
	public bool apply;

	void Update()
	{
		if( apply )
		{
			apply = false;
			Awake ();
		}
	}
#endif

}
