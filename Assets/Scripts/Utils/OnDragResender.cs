﻿using UnityEngine;
using System.Collections;

public class OnDragResender : MonoBehaviour {
	public GameObject	reciever;
	
	void OnDrag (Vector2 delta)
	{
		reciever.SendMessage( "OnDrag", delta, SendMessageOptions.DontRequireReceiver );
	}

	void OnPress ( bool press )
	{
		reciever.SendMessage( "OnPress", press, SendMessageOptions.DontRequireReceiver );
	}
}
