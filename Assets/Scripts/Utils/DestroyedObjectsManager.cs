﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class DestroyedModelData
{
#if UNITY_EDITOR
	[HideInInspector]
	public string name;
#endif
	public GameObject 	prefab;
	public int			precreatedCount;
	public string[]		levelsForSpawn;


	public bool CreateOnThisLevel( string levelName )
	{
		if (levelsForSpawn == null) {
			return  true;
		} else if (levelsForSpawn.Length == 0) {
			return true;
		}
		foreach( string lvlName in levelsForSpawn )
		{
			if( lvlName ==  levelName )
			{
				return true;
			}
		}
		return false;
	}

	public bool CreateOnThisLevel()
	{
		if (levelsForSpawn == null) {
			return  true;
		} else if (levelsForSpawn.Length == 0) {
			return true;
		}
		foreach( string lvlName in levelsForSpawn )
		{
			if( lvlName ==  LoadSceneFromBundle.currentScene )
			{
				return true;
			}
		}
		return false;
	}
}

public class DestroyedObject
{
	public GameObject 	obj;
	public Transform	transf;

	public bool ready
	{
		get{ return !obj.activeSelf; }
	}

	public DestroyedObject( GameObject _obj )
	{
		obj = _obj;
		transf = obj.transform;
		obj.SetActive (false);
	}

	public GameObject Spawn( Vector3 pos, Quaternion rotation )
	{
		transf.position =  pos;
		transf.rotation =  rotation;
		obj.SetActive (true);
		return obj;
	}

	public GameObject Spawn( Transform target )
	{
		transf.localScale = target.localScale;
		return Spawn (target.position, target.rotation);
	}
}

public class DestroyedObjectsManager : MonoBehaviour {

	public DestroyedModelData[]	modelsData;

	private static Dictionary< string, List<DestroyedObject> >	destrouedObjects;
	private static DestroyedObjectsManager	instance;
	// Use this for initialization
	void Awake () {
		instance = this;
		destrouedObjects = new Dictionary< string, List<DestroyedObject> > ();
		Transform transf = transform;
		GameObject obj;
		for (int i = 0; i < modelsData.Length; i++) 
		{
			if( modelsData[ i ].CreateOnThisLevel() )
			{
				if( !destrouedObjects.ContainsKey( modelsData[ i ].prefab.name ) )
				{
					destrouedObjects.Add( modelsData[ i ].prefab.name, new List<DestroyedObject>() );
				}
				for ( int j = 0; j < modelsData[ i ].precreatedCount; j++ )
				{
					obj = Instantiate( modelsData[ i ].prefab ) as GameObject;
					obj.transform.parent = transf;
					destrouedObjects[ modelsData[ i ].prefab.name ].Add( new DestroyedObject( obj ) );
				}
			}
		}
	}

	public static GameObject Spawn( string name, Vector3 pos, Quaternion rotation )
	{
		DestroyedObject destroyedObj = GetFreeObject (name);
		if (destroyedObj != null ) {
			return destroyedObj.Spawn( pos, rotation );
		}
		return null;
	}

	public static GameObject Spawn( string name, Transform transf )
	{
		DestroyedObject destroyedObj = GetFreeObject (name);
		if ( destroyedObj != null ) {
			return destroyedObj.Spawn( transf );
		}
		return null;
	}

	private static DestroyedObject GetFreeObject( string name )
	{
		if ( !destrouedObjects.ContainsKey (name) ) {
			Debug.Log( name +" NONE!" );
			return null;
		}
		for (int i = 0; i < destrouedObjects[ name ].Count; i++) 
		{
			if( destrouedObjects[ name ][ i ].ready )
			{
				return destrouedObjects[ name ][ i ];
			}
		}
		return instance.CreateObject ( name );
	}

	private DestroyedObject CreateObject( string name )
	{
		for (int i = 0; i < modelsData.Length; i++) {
		
			if( modelsData[ i ].prefab.name == name )
			{
				GameObject obj = Instantiate( modelsData[ i ].prefab ) as GameObject;
				obj.transform.parent = transform;
				DestroyedObject destObj = new DestroyedObject( obj );
				destrouedObjects[ modelsData[ i ].prefab.name ].Add( destObj );
				return destObj;
			}
		}
		return null;
	}


#if UNITY_EDITOR
	public bool export;
	public string forLevel;
	void OnDrawGizmosSelected()
	{
		if (modelsData != null) {
		
			for( int k = 0; k < modelsData.Length; k++ )
			{
				if( modelsData[ k ].prefab == null ){
					modelsData[ k ].name = "NULL";
					continue;
				}
				modelsData[ k ].name = modelsData[ k ].prefab.name;
				if( modelsData[ k ].levelsForSpawn != null )
				{
					string levels = "";
					foreach( string s in modelsData[ k ].levelsForSpawn )
					{
						levels += "  "+s ;
					}
					modelsData[ k ].name += levels;
				}
			}
		}

		if (export) {
			export = false;
			GameObject _obj = new GameObject("DestroyedObjectsManage_"+forLevel);
			DestroyedObjectsManager manager = _obj.AddComponent<DestroyedObjectsManager>();
			List<DestroyedModelData> data = new List<DestroyedModelData>();
			foreach( DestroyedModelData modelDat in modelsData )
			{
				if( modelDat.CreateOnThisLevel( forLevel ) )
				{
					data.Add( modelDat );
				}
			}
			manager.modelsData = data.ToArray();
		}
	}

#endif
}
