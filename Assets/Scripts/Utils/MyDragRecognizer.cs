﻿using UnityEngine;
using System.Collections;

public class MyDragRecognizer : MonoBehaviour {

	public float		minThreshold;
	public float		minTime;
	private float		timer;
	public GameObject	reciever;
	const string		MESSAGE = "OnDrag";
	private Vector2		startPos;
	private int			fingerID = -1;
	// Update is called once per frame

	void Awake()
	{
		reciever = GameObject.FindGameObjectWithTag("Player");
	}

	RaycastHit hit;
	private void SetNewTouch()
	{
		fingerID = -1;
		for( i = 0; i < Input.touches.Length; i++ )
		{
			if( Input.touches[ i ].phase == TouchPhase.Began 
			   || Input.touches[ i ].phase == TouchPhase.Moved 
			   || Input.touches[ i ].phase == TouchPhase.Stationary )
			{
				if( UICamera.Raycast( Input.touches[ i ].position, out hit ) )
				{
					continue;
				}
				fingerID = Input.touches[ i ].fingerId;
				startPos = Input.touches[ i ].position; 
				timer = 0;
				startDrag = false;
				return;
			}
		}
	}

	int i;
	private bool startDrag;
	void Update () {
#if UNITY_EDITOR

		if( Input.GetMouseButtonDown( 0 ) )
		{
			startPos = Input.mousePosition;
			startDrag = false;
			timer = 0;
		}
		else if( Input.GetMouseButton( 0 ) )
		{
			timer += Time.deltaTime;
			if( startDrag )
			{
				reciever.SendMessage( MESSAGE, (Vector2)Input.mousePosition );
			}
			else
			{
				if( Mathf.Abs( Input.mousePosition.x - startPos.x ) >= minThreshold 
				   || Mathf.Abs( Input.mousePosition.y - startPos.y ) >= minThreshold ||
				   timer >= minTime )
				{
					//Debug.Log( " drag " );
					startDrag = true;
					reciever.SendMessage( MESSAGE, (Vector2)Input.mousePosition );
				}
			//lastPos = Input.mousePosition;
			}
	
		}
#else
		if( fingerID == -1 )
		{
			SetNewTouch();
		}
		else
		{
			timer += Time.deltaTime;
			for( i = 0; i < Input.touches.Length; i++ )
			{
				if( Input.touches[ i ].fingerId == fingerID )
				{
					if( !startDrag )
					{
						if( Mathf.Abs( Input.touches[ i ].position.x - startPos.x ) >= minThreshold 
						   || Mathf.Abs( Input.touches[ i ].position.y - startPos.y ) >= minThreshold ||
							   timer >= minTime )
						{
							startDrag = true;
							//Debug.Log( " drag " );
							reciever.SendMessage( MESSAGE, Input.touches[ i ].position );
						}
					}
					else
					{
						reciever.SendMessage( MESSAGE, Input.touches[ i ].position );
					}
					//lastPos = Input.touches[ i ].position;
					if( Input.touches[ i ].phase == TouchPhase.Canceled || Input.touches[ i ].phase == TouchPhase.Ended  )
					{
						SetNewTouch();
					}
					return;
				}
			}
			SetNewTouch();
		}
		#endif
	}

}
