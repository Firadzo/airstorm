﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    public Transform target;
    public bool oneDirectionOnly;
    public float minX = -5;
    public float maxX = 5;

    void Update()
    {
        float deltaX = target.position.x - transform.position.x;
        float deltaY = target.position.y - transform.position.y;
        if (!oneDirectionOnly || deltaX > 0.0f)
        {
            transform.Translate(deltaX * Time.deltaTime, deltaY * Time.deltaTime, 0.0f);
        }
        if (transform.position.x < minX) transform.Translate(minX - transform.position.x, transform.position.y, 0);
        if (transform.position.x > maxX) transform.Translate(maxX - transform.position.x, transform.position.y, 0);
    }
}
