﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class LevelPart
{
	public Vector3		startPos;
	public Vector3		endPos;
	public List<GameObject>	partObjects;
	public bool 		first;
#if UNITY_EDITOR
	public Vector3		screenPos;
	public bool			drawDeguSelection;
	public Color		debugColor;
#endif		

	private bool 	active;
	private bool	checkToActivate = true;

	public void Init()
	{
		checkToActivate = !first;
		active = first;
		EnablePart ( first );
	}

	private void EnablePart( bool enable )
	{
		foreach ( GameObject obj in partObjects ) {
			if( obj!= null )
			{
				obj.SetActive( enable );
			}
		}
	}

	public void CheckState()
	{
		if (active) {
			float pos = Camera.main.WorldToViewportPoint ( endPos ).y;
			if (pos < 0) {
				EnablePart (false);
				active = false;
			}
		
		} else if( checkToActivate )
		{
			float pos = Camera.main.WorldToViewportPoint ( startPos ).y;
			//Debug.Log( pos );
			if( pos >= 0.8f && pos < 1.1f )
			{
				EnablePart ( true );
				checkToActivate = false;
				active = true;
			}

		}
	}

}

public class LevelLoadManager : MonoBehaviour {

	public LevelPart[] 	levelParts;

	private Transform 	camTransf;
	private float		halfCamHeight;
	public Vector3 		screenPoint;
	private int 		curLvlPartIndex;

	void Awake()
	{
		camTransf = Camera.main.transform;
		Vector3 pos = Camera.main.ScreenToWorldPoint ( screenPoint );
		halfCamHeight = pos.x - camTransf.position.x;
		Debug.Log ( halfCamHeight );
		curLvlPartIndex = 0;
		foreach (LevelPart lvlPart in levelParts) {
			lvlPart.Init();
		}

	}


	void Update()
	{
		foreach (LevelPart lvlPart in levelParts) {
			lvlPart.CheckState();
		}
	}

	#if UNITY_EDITOR

	//public float debugY;
	//public float debugWidth;
	public Transform parent;
	public bool setupToObjectsForLvlPart;

	void OnDrawGizmosSelected()
	{

		//Gizmos.DrawSphere (Camera.main.ViewportToWorldPoint (screePoint), 15f);

		if( setupToObjectsForLvlPart )
		{
			setupToObjectsForLvlPart = false;
			foreach( LevelPart lvlPart in levelParts )
			{
				lvlPart.partObjects.Clear();
				SetupObjects( parent, lvlPart );
			}
		}

		if (levelParts != null) {
		
			foreach( LevelPart lvlPart in levelParts )
			{
				lvlPart.screenPos = Camera.main.WorldToViewportPoint( lvlPart.startPos );
				Gizmos.color = lvlPart.debugColor;
				Gizmos.DrawSphere( lvlPart.startPos, 15f );
				Gizmos.DrawSphere( lvlPart.endPos, 15f );
				Gizmos.DrawLine( new Vector3( lvlPart.startPos.x, Camera.main.transform.position.y, Camera.main.transform.position.z ), 
				                new Vector3( lvlPart.endPos.x, Camera.main.transform.position.y, Camera.main.transform.position.z ) );

				if( lvlPart.drawDeguSelection )
				{
					foreach( GameObject obj in lvlPart.partObjects )
					{
						Gizmos.DrawSphere( obj.transform.position, 5f );
					}
				}
			}
		}
	}


	private void SetupObjects( Transform tr, LevelPart lvlPart )
	{
		if (tr.GetComponents<Component> ().Length > 1) {
			if (tr.position.x >= lvlPart.startPos.x && tr.position.x <= lvlPart.endPos.x) {
				lvlPart.partObjects.Add ( tr.gameObject );
			}
		}
		if (tr.childCount > 0) {
		
			for( int i = 0; i < tr.childCount; i++ )
			{
				SetupObjects( tr.GetChild( i ), lvlPart );
			}
		}
	}
#endif

}
