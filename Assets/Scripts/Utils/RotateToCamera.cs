using UnityEngine;
using System.Collections;

public class RotateToCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    GameObject pl = GameObject.Find("Player2");
		transform.rotation = Quaternion.LookRotation(Camera.main.transform.position) * pl.transform.rotation;
	}   
}
