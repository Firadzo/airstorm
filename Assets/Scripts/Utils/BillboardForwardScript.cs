using UnityEngine;
using System.Collections;

public class BillboardForwardScript : MonoBehaviour
{
	private Transform 	transf;
	public 	bool		isQuad;	

	void Awake()
	{
		transf = transform;
	}

	void LateUpdate()
	{
		if( isQuad )
		{
			transf.LookAt( Camera.main.transform, Vector3.up );
		}
		else
		{
			var dot = Vector3.Dot((Camera.main.transform.position - transf.position).normalized, transf.forward.normalized);
			if (dot > 0)
				transf.RotateAround(transf.position, transf.right.normalized, Vector3.Angle(transf.up.normalized, (Camera.main.transform.position - transf.position).normalized));
			else
				transf.RotateAround(transf.position, transf.forward.normalized, Vector3.Angle(transf.up.normalized, -(transf.position - Camera.main.transform.position).normalized));
		}

	}
	/*
    public void Update()
    {

    }*/

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position,Camera.main.transform.position);
		if( !Application.isPlaying )
		{
			transform.LookAt( Camera.main.transform, Vector3.forward );
		}
    }
}

