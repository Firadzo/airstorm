using UnityEngine;
using System.Collections;

public class EditorCamera : MonoBehaviour {

	public	float	x_Offset;
	public 	float	z_offset;

	// Use this for initialization
	void Start () {
		//x_Offset =  transform.position.x - Camera.main.ScreenToWorldPoint( new Vector3( 0, Camera.main.pixelHeight, 0 ) ).x;
		//z_offset = Camera.main.ScreenToViewportPoint( new Vector3( Camera.main.pixelWidth, 0, 0 ) );
//		Debug.Log( Camera.main.pixelRect );
	}
	
	public float height = 0;
    public float xOffset = 0;

	float startY;
	float startZ;

    public void OnDrawGizmos()
	{
		//this.transform.position = new Vector3(pl.transform.position.x + xOffset, (startY + height), startZ);
		Vector3 pl = GameObject.Find("Player").transform.position;
		
		//Vector3 correct = new Vector3(
		Ray ray = new Ray(transform.position,transform.forward);	
        Vector3 spawnDirPoint = transform.position + ray.direction*Vector3.Distance(transform.position, pl)*1.3f;
		
        Gizmos.DrawLine(transform.position, spawnDirPoint);
		
		float dist = Vector3.Distance(transform.position, pl)*1.3f;
		
		Vector3 p = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(0, 0, dist));
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(p, 2F);
		
		Vector3 p2 = GetComponent<Camera>().ScreenToWorldPoint(new Vector3( 0, Camera.main.pixelHeight,dist ) );
		//Gizmos.color = Color.green;
		Gizmos.DrawSphere(p2, 2F);
		Gizmos.DrawLine(p, p2);
		
		Vector3 p3 = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, dist));
		//Gizmos.color = Color.red;
		Gizmos.DrawSphere(p3, 2F);
		Gizmos.DrawLine(p2, p3);
		
		Vector3 p4 = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, dist));
		//Gizmos.color = Color.blue;
		Gizmos.DrawSphere(p4, 2F);
		Gizmos.DrawLine(p3, p4);
		Gizmos.DrawLine(p4, p);

       // Gizmos.DrawLine(transform.position, );
		//Vector3 leftUp = new Vector3(0, 0, 0);
		
		//Vector3 left = Camera.mainCamera.ViewportToWorldPoint(leftUp);
		
		//Vector3 left = new Vector3(spawnDirPoint.x+384f, 1024f, spawnDirPoint.z);
		
		//Gizmos.DrawLine(spawnDirPoint, left);
	}
					
}
