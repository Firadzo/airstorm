﻿using UnityEngine;
using System.Collections;

public class MeshBlinker : MonoBehaviour {

	public 	float			blinkTime;
	private float			blinkTimer;

	private bool			blink = false;
	private bool			visible;
	//private MeshRenderer	meshRenderer;
	public MeshRenderer[]	meshRenderers;

	// Use this for initialization
	void Awake () {
		meshRenderers = GetComponentsInChildren<MeshRenderer>();
		//meshRenderer = GetComponent<MeshRenderer>();
	}

	private void EnableMeshRenderers( bool enabled )
	{
		for( int i = 0; i < meshRenderers.Length; i++ )
		{
			meshRenderers[ i ].enabled = enabled;
		}
	}

	public void StartBlink()
	{
		blinkTimer 	= blinkTime;
		visible 	= true;
		blink 		= true;
	}

	public void StopBlink()
	{
		blink = false;
		EnableMeshRenderers( true );
	}

	// Update is called once per frame
	void Update () {
		if( blink )
		{
			blinkTimer -= Time.deltaTime;
			if( blinkTimer <= 0 )
			{
				blinkTimer = blinkTime;
				visible = !visible;
				EnableMeshRenderers( visible );
			}
		}
	}
}
