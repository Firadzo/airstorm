﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class MedalData
{
	public GameObject 	medalPref;
	public int			precreatedCount;
}


public class MedalsManager : MonoBehaviour {

	public MedalData[]		medalsData;
	static Dictionary<string, List<BonusCoin>> medals;
	static Transform		transf;
	static MedalsManager	instance;

	void Awake()
	{
		medals = new Dictionary<string, List<BonusCoin>> ();
		transf = transform;
		instance = this;
		GameObject obj;
		foreach (MedalData medalDat in medalsData) 
		{
			if( !medals.ContainsKey( medalDat.medalPref.name ) )
			{
				medals.Add( medalDat.medalPref.name, new List<BonusCoin>() );
			}
			for( int i = 0; i < medalDat.precreatedCount; i++ )
			{
				obj = Instantiate( medalDat.medalPref ) as GameObject;
				medals[ medalDat.medalPref.name ].Add( obj.GetComponent<BonusCoin>() );
				obj.transform.parent = transf;
			}
		}
	}

	private BonusCoin CreateNew( string name )
	{
		GameObject obj;
		BonusCoin coin;
		foreach (MedalData medalDat in medalsData) 
		{
			if( medalDat.medalPref.name == name )
			{
				if( !medals.ContainsKey( medalDat.medalPref.name ) )
				{
					medals.Add( medalDat.medalPref.name, new List<BonusCoin>() );
				}
				obj = Instantiate( medalDat.medalPref ) as GameObject;
				coin = obj.GetComponent<BonusCoin>();
				medals[ medalDat.medalPref.name ].Add( coin );
				obj.transform.parent = transf;
				return coin;
			}
		}
		return null;
	}

	private static BonusCoin GetFreeCoin( string name )
	{
		foreach( BonusCoin coin in medals[ name ] )
		{
			if( coin.isActive )
			{
				return coin;
			}
		}
		return instance.CreateNew ( name );
	}

	public static void Spawn( string name, Vector3 atPos )
	{
		GetFreeCoin ( name ).Spawn( atPos );
	}
}
