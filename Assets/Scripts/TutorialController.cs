﻿using UnityEngine;
using System.Collections;

public enum ETutorialPhase{ game, select, buy, back }
public class TutorialController : MonoBehaviour {

	public ETutorialPhase	tutorialPhase;

	public Collider[]		collidersToDisable;

	public GameObject 		gameArrow;
	public GameObject[]		selectArrows;
	public GameObject		buyArrow;
	public GameObject		backArrow;
	[SerializeField]
	public bool tutorialStarted;
	public static TutorialController instance;
	const string FIRST_TIME_KEY = "FIRST_TIME";

	void Awake()
	{
		instance = this;
		if( PlayerPrefs.GetInt( FIRST_TIME_KEY, 1 ) == 0 )
		{
			this.enabled = false;
		}
	}

	private void ClickUpgrade()
	{
		for( int i = 0; i < collidersToDisable.Length; i++ )
		{
			collidersToDisable[ i ].enabled = false;
		}
		tutorialPhase = ETutorialPhase.game;
		Time.timeScale = 0;
		gameArrow.SetActive( true );
	}

	public void SelectSkill()
	{
		tutorialPhase = ETutorialPhase.select;
		gameArrow.SetActive( false );
		for( int i = 0; i < selectArrows.Length; i++ )
		{
			selectArrows[ i ].SetActive( true );
		}
	}

	public void BuyArrow()
	{
		tutorialPhase = ETutorialPhase.buy;
		buyArrow.SetActive( true );
		for( int i = 0; i < selectArrows.Length; i++ )
		{
			selectArrows[ i ].SetActive( false );
		}
	}

	public void ClickBack()
	{
		tutorialPhase = ETutorialPhase.back;
		buyArrow.SetActive( false );
		backArrow.SetActive( true );
		PlayerPrefs.SetInt( FIRST_TIME_KEY, 0 );	
	}

	public void EndTutorial()
	{
		backArrow.SetActive( false );
		tutorialStarted = false;
		for( int i = 0; i < collidersToDisable.Length; i++ )
		{
			collidersToDisable[ i ].enabled = true;
		}
		this.enabled = false;
	}

	void Update () {
		if( !tutorialStarted && SpawnPlayer.ComixDestroyed  && Time.timeScale != 0 ) 
		{
			if( PlayerScript.instance.playerMoney >= 400 )
			{
				tutorialStarted = true;
				ClickUpgrade();
			}
		}
	}
}
