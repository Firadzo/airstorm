using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class LevelMusic
{
	public AudioClip[]	levelMusic;
	public AudioClip	bossMusic;
}

public class AudioManager : MonoBehaviour {
	public LevelMusic[]	levelsMusic;
    public AudioClip 	menusMusic;
    public AudioClip 	comix;
	public AudioClip 	tires;
	public AudioClip 	pause;
	public AudioClip 	upgrades;

	private static List<AudioSource> gameSounds;
	private static List<AudioSource> pausedSounds;

	private AudioClip 	savedTrack;//параметры для проигрывания сохраненного трека после выхода из меню.
	private float		timePlayed;
	private float		upgradesTimePlayed;
	private int			curTrackIndex;
	private int			curLevelIndex;
	new AudioSource audio;

	public static AudioManager instance;
	// Use this for initialization

	public const float GLOBAL_MUSIC_LEVEL = 0.8f;

	void Awake()
	{
		instance = this;
		gameSounds = new List<AudioSource>();
		pausedSounds = new List<AudioSource>();
	}

	void Start () 
	{
		curLevelIndex = SaveLoadParams.currLevel - 1;
		curTrackIndex = 0;
		audio = GetComponent<AudioSource>();
		audio.volume = SaveLoadParams.plMusicLevel;
		playLevelMusic(); 
	}

	public static void RegisterNewSound( AudioSource clip )
	{
		gameSounds.Add( clip );
	}

	public static void PauseGameSounds()
	{
		pausedSounds.Clear();
		for( int i = 0; i < gameSounds.Count; i++ )
		{
			if( gameSounds[ i ] == null )
			{
				gameSounds.RemoveAt( i );
				i--;
			}
			else
			{
				if( gameSounds[ i ].isPlaying )
				{
					gameSounds[ i ].Pause();
					pausedSounds.Add( gameSounds[ i ] );
				}
			}
		}
	}

	public static void ResumeGameSounds()
	{
		for( int i = 0; i < pausedSounds.Count; i++ )
		{
			if( pausedSounds[ i ] != null )
			{
				pausedSounds[ i ].Play();
			}
		}
	}

	
	public static void ApplySoundsVolume( )
	{
		for( int i = 0; i < gameSounds.Count; i++ )
		{
			if( gameSounds[ i ] == null )
			{
				gameSounds.RemoveAt( i );
				i--;
			}
			else
			{
				gameSounds[ i ].volume = SaveLoadParams.plSoundLevel;
			}
		}
	}

	public static void SetVolume( float volume )
	{
		if( instance != null )
		{
			instance.audio.volume = volume * GLOBAL_MUSIC_LEVEL;
		}
	}

	private void PlaySavedTrack() 
	{
		PlayNextTrack( savedTrack );
		audio.time = timePlayed;
	}
	
	private void PlayNextTrack(AudioClip clip) 
	{
       	if( clip != upgrades && audio.clip == upgrades )
		{
			upgradesTimePlayed = audio.time;
		}
		audio.Stop();
		audio.clip = clip;
		audio.time = 0;
		audio.Play();
    }

	public bool checkTrack;
	void Update () 
	{
		if( !audio.isPlaying  || checkTrack )
		{
			checkTrack = false;
			if( Time.timeScale == 0 )
			{
				PlayNextTrack( audio.clip );
			}
			else if( audio.clip == levelsMusic[ curLevelIndex ].bossMusic )
			{
				PlayNextTrack( audio.clip );
			}
			else
			{
				curTrackIndex ++;
				if( curTrackIndex == levelsMusic[ curLevelIndex ].levelMusic.Length )
				{
					curTrackIndex = 0;
				}
				PlayNextTrack( levelsMusic[ curLevelIndex ].levelMusic[ curTrackIndex ] );
			}
		}
	}

	public void PlaySavedLevelMusic()
	{
		PlaySavedTrack();
	}

	public void playLevelMusic()
	{
		//audio.Stop();

		//int complLevels = SaveLoadParams.currLevel-1;
		//if(complLevels <=0) complLevels = 0;
        //if ( levelsMusic[ SaveLoadParams.currLevel ].Length > 0 && levelMusic[complLevels])
        {
            //audio.clip = levelMusic[complLevels];
			PlayNextTrack( levelsMusic[ curLevelIndex ].levelMusic[ curTrackIndex ] );
        }
	   // PlayNextTrack( levelMusic[complLevels] );
		//audio.Play();
	}
	
	public void playBossMusic() 
	{
		PlayNextTrack( levelsMusic[ curLevelIndex ].bossMusic );
	}
	
	public void PlayComixMusic() 
	{
	    if( comix )
		{
			PlayNextTrack( comix );
		}
	}

	public void PlayTitresMusic() 
	{
		if( tires )
		{
			PlayNextTrack( tires );
		}
	}

	public void PlayPauseMusic()
	{
		PlayNextTrack( pause );
	}

	public void PlayerUpgradesMusic()
	{
		PlayNextTrack( upgrades );
		audio.time = upgradesTimePlayed;
	}

	public void SaveInGameMusic()
	{
		savedTrack 	= audio.clip;
		timePlayed 	= audio.time;
	}

	public void PlayMenusMusicInstant() 
	{
		if (menusMusic) PlayNextTrack(menusMusic);
	}
}
