using UnityEngine;
using System.Collections;

public class ApplySoundVol : MonoBehaviour {
	//apply sound volume to effets and other
	void Start () 
	{
		AudioSource audio = this.gameObject.GetComponent<AudioSource>();
		audio.volume = SaveLoadParams.plSoundLevel;
	}
}
