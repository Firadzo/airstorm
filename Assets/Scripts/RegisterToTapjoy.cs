﻿using UnityEngine;
using System.Collections;

public class RegisterToTapjoy : MonoBehaviour {
	
	// Use this for initialization
	void Awake () {
	
		MyTapjoy.SetCoinsDialog( GetComponent<MyUIDialog>(), GetComponentInChildren<UILabel>() );
		gameObject.SetActive( false );
	}

}
