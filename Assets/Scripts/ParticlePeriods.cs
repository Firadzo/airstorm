﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ParticlePeriods : MonoBehaviour {
    public List<ParticleSystem> WeatherParticles;
    public float periodUpToMax = 2f;
    public float periodMax = 2f;
    public float periodDownToMin = 2f;
    public float periodMin = 2f;
    public float emissionCurrent = 0;

    private float[] startIntensity;
    public STATE currentState;
	private float timer;

    public enum STATE {
        UP_TO_MAX,
        MAX,
        DOWN_TO_MIN,
        MIN
    }

	void Awake()
	{
#if UNITY_ANDROID
		gameObject.SetActive( false );
#endif
	}

	void Start () {
        startIntensity = new float[WeatherParticles.Count];
        for (int i = 0; i < WeatherParticles.Count; i++) 
		{
            startIntensity[i] = WeatherParticles[i].emissionRate;
        }
        currentState = STATE.UP_TO_MAX;
	}

	int i;
	void Update () {
		timer += Time.deltaTime;
        if (currentState == STATE.UP_TO_MAX) {
            emissionCurrent += (startIntensity[0] / periodUpToMax) * Time.deltaTime;
            if (emissionCurrent >= startIntensity[0]) {
                emissionCurrent = startIntensity[0];
            }
			if( timer >= periodUpToMax )
			{	
				timer = 0;
				currentState = STATE.MAX;
			}
        }
        else if (currentState == STATE.MAX) {
            emissionCurrent = startIntensity[0];
			if( timer >= periodMax )
			{	
				timer = 0;
				currentState = STATE.DOWN_TO_MIN;
			}
        }
        else if (currentState == STATE.DOWN_TO_MIN)
        {
            emissionCurrent -= (startIntensity[0] / periodDownToMin) * Time.deltaTime;
            if (emissionCurrent <= 0) {
                emissionCurrent = 0;
            }
			if( timer >= periodDownToMin )
			{	
				timer = 0;
				currentState = STATE.MIN;
			}
        } 
        else if (currentState == STATE.MIN) {
            emissionCurrent = 0;
			if( timer >= periodMin )
			{	
				timer = 0;
				currentState = STATE.UP_TO_MAX;
			}
        }

        for ( i = 0; i < WeatherParticles.Count; i++)
		{
            WeatherParticles[i].emissionRate = emissionCurrent;
        }
	}
}
