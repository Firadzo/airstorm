﻿using UnityEngine;
using System.Collections;

public class LaserGun : MonoBehaviour {

	//public 	int				health;
	public 	float 			rotationSpeed;
	public 	GameObject[]	laserBeam;
	//public	GameObject		explosionEffect;
	public 	float			angleFrom;
	public 	float 			angleTo;
	public 	float			angleOnLaser;
	public 	float			angleOffLaser;

	private Transform 		transf;
	private bool 			shootEnable;
	private bool			rotateTo;
	private BossPart		bossPart;

	// Use this for initialization
	void Awake () {
		bossPart = GetComponent<BossPart>();
		transf = transform;
		tempV3 = transf.localEulerAngles;
		tempV3.y = angleFrom;
		transf.localEulerAngles = tempV3;
		rotateTo = true;
		for( int i = 0; i < laserBeam.Length; i++ )
		{
			laserBeam[ i ].SetActive( false );
		}
	}
	
	// Update is called once per frame
	Vector3 tempV3;
	bool	laserActive;
	int 	i;
	void Update () {
		if( bossPart.avariable )
		{
			tempV3 = Camera.main.WorldToViewportPoint( transf.position );
			if( shootEnable )
			{
				if( tempV3.y >= 0 && tempV3.y <= 1 )
				{
					laserActive = transf.localEulerAngles.y <= angleOnLaser && transf.localEulerAngles.y >= angleOffLaser;
					for( int i = 0; i < laserBeam.Length; i++ )
					{
						laserBeam[ i ].SetActive( laserActive );
					}
					tempV3 = transf.localEulerAngles;
					if( rotateTo )
					{
						tempV3.y -= rotationSpeed * Time.deltaTime;
						transf.localEulerAngles = tempV3;
						if( transf.localEulerAngles.y <= angleTo )
						{
							rotateTo = false;
						}
					}
					else
					{
						tempV3.y += rotationSpeed * Time.deltaTime;
						transf.localEulerAngles = tempV3;
						if( transf.localEulerAngles.y >= angleFrom )
						{
							rotateTo = true;
						}
					}
				}
			}
		}
	}

	/*private void TakeDamage( int damage )
	{
		health -= damage;
		if( health <= 0 )
		{
			GameObject.Destroy( Instantiate( explosionEffect, transf.position, Quaternion.identity ) as GameObject as GameObject, 5f );
			gameObject.SetActive( false );
		}
	}

	public void applyDamage(float hp) {
		TakeDamage( (int)hp );
	}*/

	public void ShootEnable() {
		shootEnable = true;
	}
	
	public void ShootDisable() {
		shootEnable = false;
	}
}
