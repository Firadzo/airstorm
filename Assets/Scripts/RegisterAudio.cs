﻿using UnityEngine;
using System.Collections;

public class RegisterAudio : MonoBehaviour {

	void Start () {
		GetComponent<AudioSource>().volume = SaveLoadParams.plSoundLevel;
		AudioManager.RegisterNewSound( GetComponent<AudioSource>() );	
	}
	
}
