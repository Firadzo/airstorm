﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour {
    public float moveSpeed = 5;
    public float additionalSpeedLimit = 20f;
    public float additionalSpeedFactor = 1f;
    public float rorateFactor = 3.5f;
    public float backDelimeter = 1.07f;

    public AnimationCurve changeSpeedCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    public AnimationCurve borderLimitSpeedCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

    public GameObject buletTemplate;
    public float fireRate = 0.1f;

    //private float nextFireTime = 0;
    //private int spawnIndex = 0;

    private Vector3 movement = new Vector3();
    private Vector3 position = new Vector3();

    private float moveX = 0;
    private float moveY = 0;

    public float maxV = 6f;
    public float maxH = 8f;

    private int rm = 40;
    private float maxSpeed = 0.015f;
    private int randomT = 40;
    private float rh;
    private float rv;


    void Update()
    {
        MoveShip();
    }

    private void MoveShip()
    {
        float moveHorizontal = 0f;
        float moveVertical = 0f;
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            if (touch.position.x > Screen.width / 2)
            {
                Vector2 direction = new Vector2(touch.deltaPosition.x, touch.deltaPosition.y);
                direction.Normalize();

                float factor = touch.deltaPosition.magnitude * 16.0f;
                factor = factor < 120.0f ? factor : 120.0f;
                moveHorizontal = direction.x * Time.deltaTime * factor;
                moveVertical = direction.y * Time.deltaTime * factor;
            }
        }
        else
        {
            moveHorizontal = Input.GetAxis("Horizontal");
            moveVertical = Input.GetAxis("Vertical");
        }

        if (moveHorizontal == 0 && moveVertical == 0)
        {
            RandomMoivement(out moveHorizontal, out moveVertical);
        }
        else
        {
            randomT = 0;
        }

        if (moveX != 0)
        {
            float curvePos = Mathf.Abs(GetComponent<Rigidbody>().position.x) / maxH;
            if (curvePos > 1) curvePos = 1;
            moveX *= 1 - borderLimitSpeedCurve.Evaluate(curvePos);
        }
        if (moveY != 0)
        {
            float curvePos = Mathf.Abs(GetComponent<Rigidbody>().position.y) / maxV;
            if (curvePos > 1) curvePos = 1;
            moveY *= 1 - borderLimitSpeedCurve.Evaluate(curvePos);
        }
        movement.x = moveHorizontal + moveX;
        movement.y = moveVertical + moveY;
        GetComponent<Rigidbody>().velocity = movement * moveSpeed;

        moveX += moveHorizontal * additionalSpeedFactor;
        moveY += moveVertical * additionalSpeedFactor;

        moveX = moveX > additionalSpeedLimit ? moveX = additionalSpeedLimit : moveX;
        moveX = moveX < -additionalSpeedLimit ? moveX = -additionalSpeedLimit : moveX;

        moveY = moveY > additionalSpeedLimit ? moveY = additionalSpeedLimit : moveY;
        moveY = moveY < -additionalSpeedLimit ? moveY = -additionalSpeedLimit : moveY;

        GetComponent<Rigidbody>().rotation = Quaternion.Euler(
            -moveY * rorateFactor * changeSpeedCurve.Evaluate(Mathf.Abs(moveY) * 0.7f),
            0f,
            -moveX * rorateFactor * changeSpeedCurve.Evaluate(Mathf.Abs(moveX) * 0.7f)
        );


        moveX /= backDelimeter;
        if (moveX < 0) moveX = moveX > 0 ? moveX = 0 : moveX;
        else if (moveX > 0) moveX = moveX < 0 ? moveX = 0 : moveX;
        
        moveY /= backDelimeter;
        if (moveY < 0) moveY = moveY > 0 ? moveY = 0 : moveY;
        else if (moveY > 0) moveY = moveY < 0 ? moveY = 0 : moveY;

        position.x = Mathf.Clamp(GetComponent<Rigidbody>().position.x, -maxH, maxH);
        position.y = Mathf.Clamp(GetComponent<Rigidbody>().position.y, -maxV, maxV);
        //position.z = Camera.main.transform.position.z + 22;
        GetComponent<Rigidbody>().position = position;
    }


    private void RandomMoivement(out float horizontal, out float vertical)
    {
        randomT++;
        if (randomT > rm)
        {
            randomT = 0;
            
            rh = Random.Range(-maxSpeed, maxSpeed);
            rv = Random.Range(-maxSpeed, maxSpeed);
        }

        horizontal = rh;
        vertical = rv;
    }

    public bool engineOn {
        set
        {
            //value ? true : false;
        }
    }
}

