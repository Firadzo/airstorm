﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class WaypointsMove : MonoBehaviour{

    public Transform WaypointsObj;
    public List<Transform> waypoints;
    public int currentWaypoint;

    public float speed = 1.0f;
    public int Lenght = 2;

    private Vector3 startPoint;
    private Vector3 endPoint;
    private float startTime;
    public Transform LookAtWaypoint;

   // private Vector3 delta;

    void Start()
    {
        startPoint = transform.position;
        startTime = Time.time;

        foreach (Transform child in WaypointsObj)
        {
            waypoints.Add(child);
        }

        currentWaypoint = 0;
        endPoint = waypoints[currentWaypoint].position;
        //LookAtWaypoint = transform.Find("lookAt");
        Destroy(gameObject, 15);
       // delta = transform.position - GameObject.Find("PlayerMove").transform.position;
    }

    void Update()
    {
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / Lenght;

        LookAtWaypoint.LookAt(endPoint);
        transform.position = Vector3.Lerp(startPoint, endPoint, fracJourney);
        transform.rotation = Quaternion.Slerp(transform.rotation, LookAtWaypoint.rotation, Time.fixedDeltaTime * 3);

        Vector3 RelativeWaypointPosition = transform.InverseTransformPoint(new Vector3(waypoints[currentWaypoint].position.x, transform.position.y, waypoints[currentWaypoint].position.z));

        // this just checks if the car's position is near enough to a waypoint to count as passing it, if it is, then change the target waypoint to the
        // next in the list.
        if (RelativeWaypointPosition.magnitude <= 1)
        {

            currentWaypoint++;
            if (currentWaypoint >= waypoints.Count)
            {
                //currentWaypoint = 0;
                Destroy(gameObject);
                return;
            }
            startTime = Time.time;
            startPoint = endPoint;
            endPoint = waypoints[currentWaypoint].position;
        }

      
    }
}