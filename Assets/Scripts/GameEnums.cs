﻿namespace AirStrike
{
	public enum TechniqueTypes
	{
		none = 0,
		LightAircrafts,
		MiddleAircrafts,
		HeavyAircrafts,
		Tanks,
		AntiAircrafts,
		Ships,
		Bosses,
		Trains,
		FAA,
		Trucks,
		Aerostats
	}
}