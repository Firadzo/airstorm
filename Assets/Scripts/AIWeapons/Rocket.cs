using UnityEngine;
using System.Collections;

public class Rocket : BulletBase {
	public GameObject explosion;
	public float timeOut= 3.0f;	
	public float Damage = 1.0f;
    public float speed = 5.0f;
	Vector3 tempPos;

	void Start()
	{
		if ( LevelConroller.instance.currentDifficultyLevel == 0)
		{
			Damage = Damage - (Damage *  LevelConroller.instance.easyLevel);
		}
		else if ( LevelConroller.instance.currentDifficultyLevel == 2)
		{
			Damage = Damage + (Damage *  LevelConroller.instance.hardLevel);
		}
	}

	public override void Launch (Vector3 launchPos, Quaternion launchRot, Vector3 direction)
	{
		base.Launch( launchPos, launchRot );
		Invoke("Kill", timeOut);

	}

	void Update()
	{
	    if (shootEnable) {
			tempPos = rigidBodyCached.position;
            rigidBodyCached.position +=  speed * transf.forward * Time.fixedDeltaTime;
	        checkOnScreen();
	    }
	}

	Vector3 posOnScreen;
	void checkOnScreen()
	{
		posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
		if(posOnScreen.y > 1) Kill();	
	}
	
	void  OnCollisionEnter(Collision collision  ){
        if ( !collision.gameObject.CompareTag( "Player" ) ) return;
		
		ContactPoint contact = collision.contacts[0];
		Quaternion rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);
	    
		EffectsManager.PlayEffect ( explosion.name, contact.point, rotation);
		PlayerScript.instance.HitPlayer( Damage );

		Kill ();    
	}

    void OnTriggerEnter(Collider coll)
    {
        if ( !coll.gameObject.CompareTag( "Shield" ) ) return;

        PlayerShield shieldHeath = coll.transform.GetComponentInChildren<PlayerShield>();
        shieldHeath.Heath -= Damage;
        Kill();
    }

	void KillSelf()
	{
		EffectsManager.PlayEffect ( explosion.name, transform.position, Quaternion.identity );
		Kill();
	}
	
	void  Kill (){
		//transform.DetachChildren();
		obj.SetActive( false );
	}

    private bool shootEnable = true;
    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}