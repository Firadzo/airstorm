using System.Collections;
using UnityEngine;

public class Bullet : BulletBase {

    public AudioClip HitSound;
    public float 	Damage = 10;
    public float 	easyLevel = 0.3f;
    public float 	hardLevel = 0.35f;
	private Vector3	speedAndDirection;
	static int		playerLayer;
	private bool 	horizontal;

	override protected void  Awake()
	{
		base.Awake ();
		playerLayer = 1 << LayerMask.NameToLayer ("Player");
	}

	void Start()
	{
		ConfigureDamage();
	}

	void ConfigureDamage()
	{
		if (SaveLoadParams.plDiff == 0)
		{
			Damage = Damage - (Damage*easyLevel);
		}
		else if (SaveLoadParams.plDiff == 2)
		{
			Damage = Damage + (Damage * hardLevel);
		}
	}

	public override void Launch (Vector3 launchPos, Quaternion launchRot, Vector3 direction )
	{
		speedAndDirection = direction;
		horizontal = direction.x == 0;
		base.Launch (launchPos, launchRot );
		if( GetComponent<AudioSource>() )
		{
			//audio.Play();
		}
	}

	public override void Launch (Vector3 launchPos, Quaternion launchRot, Vector3 direction, float damage )
	{
		Damage = damage;
		ConfigureDamage();
		speedAndDirection = direction;
		horizontal = direction.x == 0;
		base.Launch (launchPos, launchRot );
		if( GetComponent<AudioSource>() )
		{
			//audio.Play();
		}
	}

	void Update()
	{
		transf.position += speedAndDirection * Time.deltaTime;
		if (shootEnable) {            
			checkOnScreen();
			checkCollision();
		}
	}


	Vector3 posOnScreen;
    private void checkOnScreen() {
		posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
		if (horizontal) {
			if (posOnScreen.x > 1 || posOnScreen.x < 0) {
				Disable ();
			}
		} else {
			if (posOnScreen.y > 1 || posOnScreen.y < 0) {
				Disable ();
			}
		}
    }

	private void Disable()
	{
		obj.SetActive( false );
	}

    private void OnCollisionEnter(Collision collision) {
        if (HitSound) {
            GetComponent<AudioSource>().PlayOneShot(HitSound);
        }

        if (collision.collider.CompareTag( "Player" ) ) {
			Disable();
            PlayerScript.instance.HitPlayer( Damage );
        }
    }

    private bool shootEnable = true;
   // private Vector3 savedVelocity = Vector3.zero;

    public void ShootEnable() 
	{
		if( !shootEnable )
		{
	        shootEnable = true;
	       // rigidBody.isKinematic = false;
			//rigidBody.velocity = savedVelocity;
		}
    }

    public void ShootDisable()
    {
		if( shootEnable )
		{
	        shootEnable = false;
			//savedVelocity = rigidBody.velocity;
			//rigidBody.isKinematic = true;
		}
    }

    private void OnTriggerEnter(Collider coll) {
        if ( !coll.gameObject.CompareTag( "Shield" ) ) {
            return;
        }
        var shieldHeath = coll.transform.GetComponentInChildren<PlayerShield>();
        shieldHeath.Heath -= Damage;
		Disable();
    }
    
	const float hitRadius = 3f;
	#if UNITY_EDITOR
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere( transform.position, hitRadius );
	}
	#endif

     void checkCollision() {
		if (!userRaycast) {
			return;
		}
       // Vector3 posOnScreen = Camera.main.WorldToScreenPoint(transform.position);
       // Ray ray = Camera.main.ScreenPointToRay(posOnScreen);
		Vector3 fixedPos = transf.position;
		fixedPos.y = PlayerScript.getPosition.y;//РёРіРЅРѕСЂРёСЂСѓРµРј СЂР°Р·Р»РёС‡РёРµ РїРѕ РІС‹СЃРѕС‚Рµ

		var bulletRay = new Ray( fixedPos, ( PlayerScript.getPosition - fixedPos ).normalized );

		if ( Physics.Raycast( bulletRay, hitRadius, playerLayer ) ) 
		{
            //if ( hit.transform.CompareTag( "Player" ) ) {
				//Debug.DrawRay( fixedPos,  PlayerScript.getPosition - fixedPos );
//				Debug.LogError( hit.collider.gameObject.name );
				Disable();
                PlayerScript.instance.HitPlayer( Damage );
           // }
        }
    }
}