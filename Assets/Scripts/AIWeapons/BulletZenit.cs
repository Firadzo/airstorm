using UnityEngine;
using System.Collections;

public class BulletZenit : BulletBase {
	public GameObject explosion;
	public float Damage = 1.0f;
	public int explosionRadius = 5;
	public int explosionDamage = 100;
    //public float SelfDestoyTime = 5f;
    public float easyLevel = 0.3f;
    public float hardLevel = 0.35f;
    //private Vector3 dir = Vector3.zero;
	private SphereCollider	_collider;
	private Vector3			targetDir;
	public	float			detectionRadius;


#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		Gizmos.DrawWireSphere( transform.position, detectionRadius );
	}
#endif

	protected override void Awake ()
	{
		base.Awake();
		_collider = GetComponent<SphereCollider>();
	}

	void Start()
    {
		if (SaveLoadParams.plDiff == 0)
		{
			Damage = Damage - (Damage * easyLevel);
		}
		else if (SaveLoadParams.plDiff == 2)
		{
			Damage = Damage + (Damage * hardLevel);
		}
	}

	private bool explosionStart = false;

	public override void Launch (Vector3 launchPos, Quaternion launchRot, Vector3 direction )
	{
		targetDir = direction;
		base.Launch ( launchPos, launchRot );
		if( GetComponent<AudioSource>() )
		{
			GetComponent<AudioSource>().Play();
		}
		explosionStart = false;

	}


	void Update()
	{
	    if (shootEnable) {
			transf.position += targetDir * Time.deltaTime;
	        checkOnScreen();
			if ( transf.position.y >= PlayerScript.getPosition.y && !explosionStart ) 
			{
	            Explosion();
	        }
			else 
			{
				_collider.enabled = Vector3.Distance( transf.position, PlayerScript.getPosition ) <= detectionRadius;
			}


	    }
	}
	float posOnScreen;
	void checkOnScreen()
	{
		posOnScreen = Camera.main.WorldToViewportPoint( transform.position ).y;
		if(posOnScreen > 1) obj.SetActive( false );	
	}
	
	void  OnCollisionEnter ( Collision collision  )
	{
        if (!shootEnable || explosionStart ) return;
		if( !collision.gameObject.CompareTag( "Player" ) ) return;

		
		explosionStart = true;
		if (collision.contacts.Length > 0) {
			ContactPoint contact = collision.contacts [0];	
		//	Quaternion rotation = Quaternion.FromToRotation (Vector3.up, contact.normal);
			EffectsManager.PlayEffect (explosion.name, contact.point );
		} else {
			EffectsManager.PlayEffect (explosion.name, transf.position );
		}
		PlayerScript.instance.HitPlayer( Damage );
		obj.SetActive( false );
	}

	public void Explosion()	
	{	
		EffectsManager.PlayEffect ( explosion.name, transform.position, Quaternion.identity );
		obj.SetActive( false );
		
		Vector3 explosionPosition = transform.position;
		
		Collider[] colliders = Physics.OverlapSphere (explosionPosition, explosionRadius);
		
		foreach(var hit in colliders) 
		{
			if (!hit || !hit.CompareTag( "Player" ) ) continue;				
			float distance= Vector3.Distance(hit.transform.position, explosionPosition);
    	    float hitPoints = 1.0f - Mathf.Clamp01(distance / explosionRadius);
			hitPoints *= explosionDamage;
			PlayerScript.instance.HitPlayer( hitPoints );
			return;
		}
	}

    private bool shootEnable = true;
   // private Vector3 savedVelocity = Vector3.zero;
    public void ShootEnable()
    {
		if( !shootEnable )
		{
	        shootEnable = true;
	        //rigidBody.isKinematic = false;
			//rigidBody.velocity = savedVelocity;
		}

    }

    public void ShootDisable()
    {
		if( shootEnable )
		{
	        shootEnable = false;
			//savedVelocity = rigidBody.velocity;
			//rigidBody.isKinematic = true;
		}
    }
}
