using UnityEngine;
using System.Collections;

public class wave : MonoBehaviour {

	public float Speed;
	public float MaxScale = 20.0f;
	public float LifeTime;
	private float _lifeTime;
	
	//Renderer originalColour;
	//public float TimeD;	
	
	void Start ()
	{
		transform.eulerAngles = new Vector3(Random.Range(-180,180),Random.Range(180,180),Random.Range(180,180));
	//	originalColour = this.gameObject.GetComponent<Renderer>();
	}
	
	void Update ()
	{
		transform.localScale += Time.deltaTime * Speed * new Vector3(1,0,1);
		_lifeTime += Time.deltaTime;
		
		if(transform.localScale.x > MaxScale) //_lifeTime>LifeTime)
		{
			Destroy(gameObject);
		}	
	}
}
	
	
