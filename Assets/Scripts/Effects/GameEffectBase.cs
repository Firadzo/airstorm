using UnityEngine;
using System.Collections;

public class GameEffectBase : MonoBehaviour {

	protected Transform				transf;
	protected GameObject			obj;
	protected AudioSource			sound;

	public bool ready { get; protected set; }

	virtual protected void Awake()
	{
		ready = true;
		transf = transform;
		obj = gameObject;
		sound = GetComponent<AudioSource> ();
		if (sound != null) {
			sound.spatialBlend = 0;
		}
		obj.SetActive (false);
	}

	virtual public void Play( Vector3 atPos )
	{
		ready = false;
		transf.position = atPos;
		obj.SetActive (true);
		if (sound != null) {
			sound.Play ();
		}
	}
	
	virtual public void Play( Vector3 atPos, Quaternion rotation )
	{
		transf.rotation = rotation;
		Play (atPos);
	}

	virtual public void Play( Transform parent )
	{
		if (parent == null) {
			return;
		}
		transf.parent = parent;
		ready = false;
		transf.localPosition = Vector3.zero;
		obj.SetActive (true);
		if (sound != null) {
			sound.Play ();
		}
	}


	
	virtual protected void EffectEnd()
	{
		if (sound != null) {
			sound.Stop();
		}
		ready = true;
		obj.SetActive( false );
	}
}
