﻿using UnityEngine;
using System.Collections;

public class GameEffect : GameEffectBase {

	private ParticleSystem	particles;
	private bool			checkForDisable;
	public 	bool			useDuration;
	private float			effectTime;
	private float			timer;

	// Use this for initialization
	override protected void  Awake () {
		if (useDuration) {
		
			ParticleSystem[] allParticles = GetComponentsInChildren<ParticleSystem>();
			effectTime = 0;
			for( int i = 0; i < allParticles.Length; i++ )
			{
				if( allParticles[ i ].duration > effectTime )
				{
					effectTime = allParticles[ i ].duration;
				}
			}
		}
		particles = GetComponent<ParticleSystem> ();
		if (particles == null) {
			particles = GetComponentInChildren<ParticleSystem>();
			particles.Stop();
			particles.Clear();
		}
		base.Awake ();


	}

	override public void Play( Vector3 atPos )
	{
		base.Play (atPos);
		particles.Play ();
		checkForDisable = true;
		timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (checkForDisable) {

			if( useDuration )
			{
				timer += Time.deltaTime;
				if( timer >= effectTime )
				{
					//Debug.LogError("123");
					checkForDisable = false;
					EffectEnd();
				}
			}
			else
			{

			if( particles.particleCount == 0 )
				{
					checkForDisable = false;
					EffectEnd();
				}
			}
		}
	}
}
