﻿using UnityEngine;
using System.Collections;

public class GameEffectLegacy : GameEffectBase {

	private ParticleEmitter[]	emitters;
	private bool				checkForDisable;
	private int					particleCount;

	protected override void Awake ()
	{
		emitters = GetComponentsInChildren<ParticleEmitter> ();
		base.Awake ();
	}

	override public void Play( Vector3 atPos )
	{
		base.Play (atPos);
		foreach (ParticleEmitter emitter in emitters) {
			emitter.Emit();
		}
		checkForDisable = true;
	}
	
	int i;
	// Update is called once per frame
	void Update () {
		if (checkForDisable) 
		{
			particleCount = 0;
			for( i = 0; i < emitters.Length; i++ )
			{
				particleCount += emitters[ i ].particleCount;
			}
			if( particleCount == 0 )
			{
				checkForDisable = false;
				EffectEnd();
			}
		}
	}
}
