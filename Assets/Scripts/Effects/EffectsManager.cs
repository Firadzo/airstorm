﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ System.Serializable ]
public class EffectData
{
#if UNITY_EDITOR
	[HideInInspector]
	public string			name;
#endif
	public GameObject 		gameEffect;
	public int				precreatedCount = 5;
}

public class EffectsManager : MonoBehaviour {

	public EffectData[]	effectsData;

	private static Dictionary< string, List<GameEffectBase> >	effects;
	private static EffectsManager	instance;

	void Awake()
	{
		instance = this;
		effects = new Dictionary<string, List<GameEffectBase>> ();
		int k;
		string key;
		Transform transf;
		for (int i = 0; i < effectsData.Length; i++) {
		
			key = effectsData[ i ].gameEffect.name;
			if( !effects.ContainsKey( key ) )
			{
				effects.Add( key, new List<GameEffectBase>() );
			}

			for( k = 0; k < effectsData[ i ].precreatedCount; k++ )
			{
				transf = ( Instantiate( effectsData[ i ].gameEffect ) as GameObject ).transform;
				transf.parent = transform;
				effects[ key ].Add( transf.GetComponent<GameEffectBase>() );
			}
		}
	}

	public static void PlayEffect( string effect, Vector3 pos )
	{
		GetEffectByName (effect).Play ( pos );
	}

	public static void PlayEffect( string effect, Vector3 pos, Quaternion rotation )
	{
		GetEffectByName (effect).Play ( pos, rotation );
	}

	public static GameEffectBase PlayEffect( string effect, Transform parent )
	{
		GameEffectBase gameEffect = GetEffectByName (effect);
		gameEffect.Play ( parent );
		return gameEffect;
	}

	public static GameEffectBase GetEffectByName( string name )
	{
		if( effects.ContainsKey( name ) )
		{
			int i;
			for( i = 0; i < effects[ name ].Count; i++ )
			{
				if( effects[ name ][ i ].ready )
				{
					return effects[ name ][ i ];
				}
			}
			return instance.CreateEffect( name );
		}
		return null;
	}

	private GameEffectBase CreateEffect( string effecName )
	{
		for ( int i = 0; i < effectsData.Length; i++) 
		{
			if( effectsData[ i ].gameEffect.name == effecName )
			{
				Transform transf = ( Instantiate( effectsData[ i ].gameEffect ) as GameObject ).transform;
				transf.parent = transform;
				GameEffectBase effect = transf.GetComponent<GameEffectBase>();
				effects[ effecName ].Add( effect );
				return effect;
			}
		}
		return null;
	}

#if UNITY_EDITOR
	int j;
	void OnDrawGizmosSelected()
	{
		for (j = 0; j < effectsData.Length; j++) {
		
			effectsData[ j ].name = effectsData[ j ].gameEffect.name.ToString();
		}
	}
#endif
}
