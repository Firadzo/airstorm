﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class ADSInitialization : MonoBehaviour {

	public static void Initialize()
	{
		#if UNITY_IOS
		AdBuddizBinding.SetIOSPublisherKey("TEST_PUBLISHER_KEY_IOS");
		Advertisement.Initialize ("51309");
		#else
		AdBuddizBinding.SetAndroidPublisherKey("387b9326-ab15-4a81-bca4-6f2b40c9e623");
		Advertisement.Initialize ("50788");
		#endif
	}
}
