﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summmary>
/// This is a static class that references a singleton object in order to provide a simple interface for
/// interacting with the AdColony functionality.
///
/// This object is self-composing, meanining that it doesn't need to be added to the unity hierarchy list
/// in order to make calls to it. It will create the object if a call is made to it and it doesn't exist.
///
/// If desired you can still add it to the scene, however changing to another scene with an AdManager declared
/// will throw an error because two AdManagers will exist. This is because this object has DontDestroyOnLoad
/// called on it so that the scene changes will not affect its state.
///
/// To destroy this object you must manually call Destroy() on it.
///
/// This object uses a dictionary to associate arbitrary string names with video zones.
/// These arbitrary string names serve as a way to retrieve the zone information easier,
/// so that the developer does not need to remember random numbers.
///
/// The dictionary is configured by the 'ConfigureZones()' which in turn is called by the
/// 'ConfigureADCPlugin()' method on Awake()
/// </summmary>
public class ADCAdManager : MonoBehaviour {
  //---------------------------------------------------------------------------
  // The single instance of the ADCAdManager component
  private static ADCAdManager _instance;

  public static ADCAdManager Instance
  {
    get {
      if(_instance == null)
      {
				Init();
      }
      return _instance;
    }
  }

	static void Init()
	{
		_instance = FindObjectOfType( typeof(ADCAdManager) ) as ADCAdManager;
		if(_instance == null)
		{
			_instance = (new GameObject("ADCAdManager")).AddComponent<ADCAdManager>();
		}
		_instance.transform.parent = null;
		DontDestroyOnLoad( _instance.gameObject );
	}

  // Arbitrary version number
  public string version = "1.1";
  // Your application id
  private string appId = "";

	public const string VIDEO_ID = 
		#if UNITY_ANDROID
		"vz3912b980301f4523b4";
	#else
	"vz0c3685d985f84d11ad";
	#endif



  void Awake() {
		Init ();
		//Detects if running android and uses the android zone strings
		#if UNITY_ANDROID
		// App ID
		appId= "app67ba6c9728f746e69c";
		#else
		// App ID
		appId = "appcc7dded943ab443a80";
		#endif
		// This configures the AdColony SDK so that the application is targetting the correct zone for generating ads
		AdColony.Configure (version, // Arbitrary app version
		                    appId,   // ADC App ID from adcolony.com
		                    new string[ 1 ]{ VIDEO_ID });
		AdColony.OnVideoFinished += (ad_shown) => { CustomConsole.DebugText("Video finihed");};
	}
}
