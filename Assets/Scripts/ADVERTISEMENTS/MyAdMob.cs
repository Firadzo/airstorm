﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class MyAdMob : MonoBehaviour {

	const float	AD_TIME = 30f;
	private static 	bool	adVisible;
	private static 	MyAdMob instance;
	private bool	active;		
	private BannerView bannerView;

	// Use this for initialization
	void Awake () {
		if( instance == null )
		{
			instance = this;
		}
		else
		{
			Destroy( gameObject );
			return;
		}
		DontDestroyOnLoad( gameObject );
		gameObject.transform.parent = null;
		// Activate the Google Play Games platform
		/*PlayGamesPlatform.Activate();
		Social.localUser.Authenticate((bool success) => {
			// handle success or failure
		});*/

		if( SaveLoadParams.AdsEnabled )
		{
			active = true;
			//AdMobPlugin.AdLoaded += adMobPlugin.ShowBanner;
			//adMobPlugin.RequestAd();
			AdBuddizBinding.CacheAds();
			adVisible = true;
			RequestBanner();
			StartCoroutine( RefreshAD() );
		}
	}

	
	private void RequestBanner()
	{
		#if UNITY_EDITOR || UNITY_WP8
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-5253680195861242/6639010610";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-5253680195861242/4740209814";
#endif
		
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
		// Register for ad events.
		bannerView.AdLoaded += HandleAdLoaded;
		// Load a banner ad.
		bannerView.LoadAd(createAdRequest());
	}

	private AdRequest createAdRequest()
	{
		return new AdRequest.Builder().Build();
		
	}
	
	public void HandleAdLoaded( object sender, System.EventArgs args)
	{
		bannerView.Show ();
	}
	
	private IEnumerator RefreshAD()
	{
		yield return new WaitForSeconds( AD_TIME );
		bannerView.LoadAd(createAdRequest());
		StartCoroutine( RefreshAD() );
		
	}

	public static void RemoveBanner()
	{
		if (instance.active) {
			instance.active = false;
			instance.StopCoroutine ("RefreshAD");
		}
		if( adVisible )
		{
			instance.bannerView.AdLoaded -= instance.HandleAdLoaded;
		}
		if ( instance.bannerView != null) {
			instance.bannerView.Destroy ();
		}
	}

	public static void HideBunner()
	{	
		if(SaveLoadParams.AdsEnabled )
		{
			CustomConsole.DebugText("Hide" + adVisible );
#if !UNITY_EDITOR
		MyTapjoy.AddText( adVisible+"tryHide" );
		if( adVisible )
		{
			Debug.Log( "hide" );
			MyTapjoy.AddText( "hide" );
			instance.bannerView.AdLoaded -= instance.HandleAdLoaded;
			adVisible = false;
			instance.bannerView.Hide ();
		}
#endif
		}
	}

	public static void ShowBunner()
	{	if(SaveLoadParams.AdsEnabled )
		{
		CustomConsole.DebugText("Show" + adVisible );
#if !UNITY_EDITOR
		MyTapjoy.AddText( adVisible +"tryShow");
		if( !adVisible )
		{
			Debug.Log( "show" );
			MyTapjoy.AddText( "show" );
			adVisible = true;
			instance.bannerView.AdLoaded += instance.HandleAdLoaded;
			instance.bannerView.Show ();
			
		}
#endif
		}
	}
	
}
