﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TapjoyUnity;
using UnityEngine.Advertisements;

[System.Serializable]
public class MyITapjoyEvent
{
	public 	string			eventName;	
	public 	bool			screenAD;
	private  bool			showOnLoaded;
	private TJPlacement 	placement;
	private System.Action	onNoContent;
	private System.Action 	onContentDidDisappear;

	public void AddFailedEventListener( System.Action listener )
	{
		onNoContent += listener;
	}

	public void AddContentDidDisappear( System.Action listener )
	{
		onContentDidDisappear += listener;
	}

	public void CleaerFailedEventListeners()
	{
		onNoContent = null;
		onContentDidDisappear = null;
	}

	public bool Avaliable
	{
		get{ return placement.IsContentAvailable(); }
	}

	public void ShowOnLoad()
	{
		Show ( false );
	}

	//Если есть контент показываем его, если нет, то запрашиваем контект и по получении показываем его
	public bool Show( bool showOnLoad = false )
	{
		if( placement.IsContentReady() )
		{
			placement.ShowContent();
			return true;

		}
		else
		{
			if( showOnLoad )
			{
				if( onNoContent != null )
				{
					onNoContent();
					return false;
				}
				showOnLoaded = true;
				RequestPlacement();
			}
		}
		return false;
	}

	private void Init()
	{
		if (placement == null) {
			CustomConsole.DebugText( "Placement init "+ eventName );
			 placement =  TJPlacement.CreatePlacement( eventName ); 
			TJPlacement.OnContentDismiss += ContentDidDisappear;
			TJPlacement.OnContentReady += ContentIsReady;
			TJPlacement.OnRequestFailure += ( TJPlacement _placement, string error ) => { CustomConsole.DebugText( "RequestFailure "+ error +" "+eventName ); };
			TJPlacement.OnRequestSuccess += (  TJPlacement _placement  ) => { CustomConsole.DebugText( "RequestSucess"+ eventName ); };
			TJPlacement.OnContentShow += (  TJPlacement _placement  ) => {  
				if( screenAD && MyTapjoy.instance.screenAdActual )
				{ MyTapjoy.instance.waitForScreenAD = false; } 
			};
		}
	}

	public void RequestPlacement() { 
		Init ();
		//placement.( enablePreload ); 
		if (!placement.IsContentReady ()) {
			placement.RequestContent (); 
		}
	} 

	public void ContentDidDisappear( TJPlacement _placement )
	{ 
		if ( eventName == _placement.GetName()) {
			Debug.Log ("ContentDidDisappear");
			onContentDidDisappear ();
			onContentDidDisappear = null;
			RequestPlacement (); 
		}
	} 
	
	public void ContentIsReady( TJPlacement _placement ){ 
		CustomConsole.DebugText ("Content is ready for "+eventName);
		Debug.Log(" C#: ContentIsReady "); 
		if( showOnLoaded && eventName == _placement.GetName() )
		{
			showOnLoaded = false;
			if( screenAD )
			{ 
				if(  MyTapjoy.instance.screenAdActual )
				{
					placement.ShowContent();
				}
				MyTapjoy.instance.waitForScreenAD = false; 
			} 
			else
			{
				placement.ShowContent();
			}
		}
	}
}


public class MyTapjoy : MonoBehaviour  {

	public 	MyITapjoyEvent		gameOverVideo;
	public	MyITapjoyEvent		interstitial;

	private static UILabel				coinsEarnedLabel;
	private static MyUIDialog			coinsEarnedDialog;

	private delegate void OnTapjoyConnect();
	private static OnTapjoyConnect	onTapjoyConnect;

	public static MyTapjoy		instance;

	public static UILabel		_debugLbl;

	private static UILabel		storeLbl;

	const float		timeToWaitForAD = 3f;
	public float	curTimeForAD;
	public bool		waitForScreenAD;

	public bool screenAdActual
	{
		get{ return waitForScreenAD && ( curTimeForAD > Time.realtimeSinceStartup ); }
	}

	public static void SetStoreLabel( UILabel label )
	{
		storeLbl = label;
	}

	public static void SetCoinsDialog( MyUIDialog dialog, UILabel label )
	{
		coinsEarnedDialog = dialog;
		coinsEarnedLabel = label;
	}

	public static void SetText( string t )
	{
		if( _debugLbl == null )
		{
			_debugLbl = GameObject.Find("Debug").GetComponent<UILabel>();
		}
		_debugLbl.text = t;
	}

	void Awake()
	{
//		_debugLbl = GameObject.Find("Debug").GetComponent<UILabel>();
		if( instance == null )
		{
			instance = this;
			transform.parent = null;
			DontDestroyOnLoad( gameObject );
		}
		else if( instance != this )
		{
			Destroy( gameObject );
			return;
		}

		// Tapjoy Connect Events
		Tapjoy.OnConnectFailure += () => { CustomConsole.DebugText( "Tapjoy Connection failure" ); };
		Tapjoy.OnConnectSuccess += HandleTapjoyConnectSuccess;
		//TapjoyPlugin.TapjoyConnectSuccess("");
		Tapjoy.OnConnectSuccess += Tapjoy.GetCurrencyBalance;
		Tapjoy.OnViewDidClose+= ( int viewType ) => { { CheckEarning = true; Tapjoy.GetCurrencyBalance(); } };
		Tapjoy.OnViewDidOpen += ( int viewType ) => { { CheckEarning = false; } };
		Tapjoy.OnVideoComplete += Tapjoy.GetCurrencyBalance;
		AdBuddizManager.didShowAd += () => { waitForScreenAD = false; };
		ADSInitialization.Initialize ();
		//gameOverVideo.AddFailedEventListener( () => { Debug.Log( "11" ); } );
		//gameOverVideo.Execute();
	}

	private void GetCoins( int amount )
	{
		if( amount != 0 )
		{
			if( !gettingPoints )
			{
				coins = amount;
				Tapjoy.SpendCurrency( amount );
				gettingPoints = true;
			}
		}
	}

	void Start()
	{
		// Connect to the Tapjoy servers.
		Tapjoy.OnSpendCurrencyResponse += (string s, int obj) => { EarnPoints();  };
		Tapjoy.OnSpendCurrencyResponseFailure += ( errorMessage ) => {
			CustomConsole.DebugText("SpendCoinsError "+ errorMessage ); 
			gettingPoints = false;  };
		Tapjoy.OnEarnedCurrency += (currencyName, amount) => { GetCoins( amount ); };
		Tapjoy.OnGetCurrencyBalanceResponse += ( string s, int amount ) => 
		{ 
			GetCoins( amount );
			/*if( _debugLbl )
			{
				_debugLbl.text += obj.ToString()+" Get \n"; 
			}*/
		}; 
#if !UNITY_EDITOR
			if (SaveLoadParams.AdsEnabled) {
				interstitial.RequestPlacement ();
			}
			gameOverVideo.RequestPlacement ();
#endif
	}

	void AddCoins()
	{
		_debugLbl.text +="ADD \n";
		Tapjoy.AwardCurrency( 10 );
		//TapjoyPlugin.GetTapPoints();
	}

	void GetPoints()
	{
		_debugLbl.text +="GET ";
		Tapjoy.GetCurrencyBalance();
	}

	void SpendCoins()
	{
		_debugLbl.text +="SPend \n";
		Tapjoy.SpendCurrency( 1 );
	}

	public static bool ShowUnityADVideo( System.Action sucessCallback = null, System.Action failCallback = null )
	{
		if (Advertisement.IsReady ("defaultZone")) {

			ShowOptions showOptions = new ShowOptions();
			showOptions.resultCallback  =( ShowResult result )=>
				{
					switch(result)
					{
					case (ShowResult.Finished):
					if(sucessCallback != null )
					{
						sucessCallback();
					}
						break;
					case (ShowResult.Failed):
					if(failCallback != null )
					{
						failCallback();
					}
						break;
					case(ShowResult.Skipped):
					if(failCallback != null )
					{
						failCallback();
					}
						break;
					}
			};

			Advertisement.Show( "defaultZone", showOptions );
			return true;
		}
		return false;
	}
	
	public static void AddText( string Txt )
	{
		if( _debugLbl == null )
		{GameObject debug = GameObject.Find("Debug");
			if( debug )
			{
				_debugLbl = debug.GetComponent<UILabel>();
			}
		}
		if( _debugLbl )
		{
			_debugLbl.text += Txt+" ";
		}
	}

	private static void ConnectToTapjoy()
	{
#if !UNITY_EDITOR
		Tapjoy.Connect();	
#endif
	}

	private bool 	gettingPoints;
	private int 	coins;
	private void EarnPoints()
	{
		gettingPoints = false;
		SaveLoadParams.plMoney += coins;
		SaveLoadParams.realMoney += coins;
		if( PlayerScript.instance != null )
		{
			PlayerScript.instance.playerMoney = SaveLoadParams.plMoney;
		}
		if( storeLbl )
		{
			storeLbl.text = SaveLoadParams.plMoney.ToString();
		}
		SaveLoadParams.SaveMoney();
		PlayerSkills.AddStartLevelMoney( coins );
		CustomConsole.DebugText ( "CanShowDialog "+ EndLevelPopup.CanShowDialog() );
		if( coinsEarnedDialog && EndLevelPopup.CanShowDialog() )
		{
			if( Localization.instance.currentLanguage == "Russian" )
			{
				coinsEarnedLabel.text = "ВЫ ПОЛУЧИЛИ "+coins+" МОНЕТ!";
			}
			else
			{
				coinsEarnedLabel.text = "YOU HAVE RECEIVED "+coins+" COINS!";
			}
			coinsEarnedDialog.Open();
		}
		coins = 0;
	}

	public void HandleTapjoyConnectSuccess()
	{
		if( onTapjoyConnect != null )
		{
			onTapjoyConnect();
			onTapjoyConnect = null;
		}
	} 

	public static void ClearGameOverVideoListener()
	{
		instance.gameOverVideo.CleaerFailedEventListeners();
	}

	public static bool ShowAddLifeVideo()
	{
		if( Tapjoy.IsConnected )
		{
			instance.gameOverVideo.Show();
			return instance.gameOverVideo.Avaliable;
		}
		else
		{
			//onTapjoyConnect += instance.gameOverVideo.Show;
			ConnectToTapjoy();
			return false;
		}
		//#endif
	}

	public static void ShowInterstitial()
	{
#if UNITY_EDITOR
		return;
#endif
		CustomConsole.DebugText ( "ADS " + SaveLoadParams.AdsEnabled.ToString() );
		if( SaveLoadParams.AdsEnabled )
		{
			instance.waitForScreenAD = true;
			instance.curTimeForAD = Time.realtimeSinceStartup + timeToWaitForAD;
			CustomConsole.DebugText (  "Tapjoy " +Tapjoy.IsConnected );
			//_debugLbl.text += tapjoyConnected+" "+instance.interstitial.Avaliable;
			if( Tapjoy.IsConnected )
			{
				if( !instance.interstitial.Show( true ) )
				{
					AdBuddizBinding.ShowAd();
				}
			}
			else
			{	
				AdBuddizBinding.ShowAd();
				onTapjoyConnect += instance.interstitial.ShowOnLoad;
				ConnectToTapjoy();
			}
		}
	}

	private static bool checkEarning;
	public	static bool CheckEarning
	{
		set
		{
			refreshTimer = Time.realtimeSinceStartup;
			checkEarning = value; 
		}
	}
	const float	refreshTime = 1f;
	static float refreshTimer;
	void Update()
	{
		if( checkEarning )
		{
			if( Time.realtimeSinceStartup - refreshTimer >= refreshTime )
			{
				refreshTimer = Time.realtimeSinceStartup;
				Tapjoy.GetCurrencyBalance();
			}
		}
	}
}
