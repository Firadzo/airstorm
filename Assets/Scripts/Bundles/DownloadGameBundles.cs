﻿using UnityEngine;
#if UNITY_WP8
using UnityEngine.Windows;
#endif
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using com.shephertz.app42.paas.sdk.csharp;    
using com.shephertz.app42.paas.sdk.csharp.upload;    
using System.IO;

public class DownloadGameBundles : MonoBehaviour {

	class BundleData
	{
		public string version;
		public string scene;

		public BundleData( string _vers, string _scene )
		{
			version = _vers;
			scene = _scene;
		}
	}

	string userName = "bundle";  
	public UnityEngine.UI.Text		downloadProgress;
	
	const string	assetBundlesExtension = ".assetbundle";

	const string bundleVersionFileName = "bundleVersion.xml";
	private List<WWW> currentDownloads;

	private Dictionary< string, BundleData > currentBundles;
	private UploadService uploadService;
	private System.Action allBundlesDownloaded;
	public static DownloadGameBundles instance;

	void Awake()
	{

//		LoadSceneFromBundle.LoadScene ("MainMenu");
		return;
		instance = this;
		currentDownloads = new List<WWW>();
		downloadProgress.text = "Initializing";
		App42API.Initialize( "6c91b2bef171795573128938a9316f647862d2d81f276e57c46d069f95a2745a", "95209d38c8ee577212898bd35c65ae3c6109f5398c64c418d4ea945ab013a7ea" );  
		uploadService = App42API.BuildUploadService();   
		App42Log.SetDebug (true);        //Prints output in your editor console  
	//	uploadService.GetFileByUser ( fileName, userName, new UnityCallBack ( ( string s, string name )=>{ StartCoroutine( Download( s, name ) ); }, downloadProgress )) ; 

		currentBundles = new Dictionary<string, BundleData> ();
#if UNITY_WP8
		if ( UnityEngine.Windows.File.Exists (Path.Combine ( LoadSceneFromBundle.getBundlesPath, bundleVersionFileName ) ) ) 
#else
		if ( File.Exists (Path.Combine ( LoadSceneFromBundle.getBundlesPath, bundleVersionFileName ) ) ) 
#endif
		{
			currentBundles = ParseBundleVersion( System.IO.File.ReadAllText ( Path.Combine( LoadSceneFromBundle.getBundlesPath, bundleVersionFileName ) ) );
		}
		uploadService.GetFileByUser ( bundleVersionFileName, userName, 
		                             new UnityCallBack ( ( Upload.File file )=>{ DownloadBundleVersion( file ); }, downloadProgress )) ; 
	}

	void DownloadBundleVersion( Upload.File file ) {

		StartCoroutine ( Download ( file.url, CompareBundlesFiles ));
		
	}

	IEnumerator Download( string url, System.Action<WWW> onDownloadComplete )
	{
		using (WWW www = new WWW(url)) {
			currentDownloads.Add (www);
			yield return www;
			currentDownloads.Remove (www);
			onDownloadComplete( www );
		}
	}

	private Dictionary< string, BundleData > ParseBundleVersion( string text )
	{
		Debug.Log ( text );
		XmlDocument doc = new XmlDocument();
		#if UNITY_WP8
		doc.LoadXml( UTF8ByteArrayToString( UnityEngine.Windows.File.ReadAllBytes ( text ) ) );
		#else
		text.TrimStart();
		doc.LoadXml( text );
		#endif
		XmlElement root = doc.DocumentElement;
		string currentPlatform = "";
		switch ( Application.platform )
		{
		case RuntimePlatform.WindowsEditor:
			currentPlatform = "Editor";
			break;
		case RuntimePlatform.Android:
			currentPlatform = "Android";
			break;
		case RuntimePlatform.IPhonePlayer:
			currentPlatform = "IOS";
			break;
		}
		XmlNodeList platforms = root.GetElementsByTagName( "Platform" );
		XmlElement currentPlatformElement = null;
		Dictionary< string, BundleData > loadedBundleData = new Dictionary<string, BundleData>();
		foreach( XmlElement platform in platforms )
		{
			if( currentPlatform == platform.GetAttribute("Name") )
			{
				currentPlatformElement = platform;
				break;
			}
		}
		if( currentPlatformElement != null )
		{
			XmlNodeList bundlesData = currentPlatformElement.GetElementsByTagName("BundleData");
			foreach( XmlElement bundleData in bundlesData )
			{
				loadedBundleData.Add( bundleData.GetAttribute("Name"), new BundleData( bundleData.GetAttribute("Version"), bundleData.GetAttribute("Scene") ) );
			}
		}
		else
		{
			Debug.LogWarning("No bundle data for current platform");
		}
		return loadedBundleData;
	}

	int toDownloadCount;
	private void CompareBundlesFiles( WWW loadedBundleWWW )
	{
		byte[] bytes = loadedBundleWWW.bytes;

#if UNITY_WP8
		allBundlesDownloaded = () => { UnityEngine.Windows.File.WriteAllBytes ( Path.Combine ( LoadSceneFromBundle.getBundlesPath, bundleVersionFileName ), bytes );};
#else
		allBundlesDownloaded = () => { File.WriteAllBytes ( Path.Combine ( LoadSceneFromBundle.getBundlesPath, bundleVersionFileName ), bytes );};
#endif
		Dictionary< string, BundleData > loadedBundleData = new Dictionary<string, BundleData>();
		loadedBundleData = ParseBundleVersion (loadedBundleWWW.text);
		Dictionary< string, BundleData > bundlesToDownload = new Dictionary<string, BundleData>();
		string path = "";
		foreach (KeyValuePair< string, BundleData > loaded in loadedBundleData) {
			if( currentBundles.ContainsKey( loaded.Key ) )
			{
				if( loaded.Value.version == currentBundles[ loaded.Key ].version )
				{
					path = Path.Combine( LoadSceneFromBundle.getBundlesPath, loaded.Key );
					#if UNITY_WP8
					if( !UnityEngine.Windows.File.Exists ( path ) )
#else
					if( !File.Exists ( path ) )
#endif
					{
						bundlesToDownload.Add( loaded.Key, loaded.Value );
					}
					else
					{
						LoadSceneFromBundle.AddSceneToBundlePath( loaded.Value.scene, path );
					}
				}
				else
				{
					bundlesToDownload.Add( loaded.Key, loaded.Value );
				}
			}
			else
			{
				bundlesToDownload.Add( loaded.Key, loaded.Value );
			}
		}

		toDownloadCount = bundlesToDownload.Values.Count; 
		downloadProgress.text += "To download "+toDownloadCount + "\n";
		if ( toDownloadCount == 0 ) {
			downloadProgress.text += "Loading level";
			LoadSceneFromBundle.LoadScene ("MainMenu");
		} else {
			foreach ( KeyValuePair< string, BundleData > loaded in bundlesToDownload ) 
			{
				string sceneName = loaded.Value.scene;
				uploadService.GetFileByUser ( loaded.Key, userName, 
				                             new UnityCallBack ( (  Upload.File file )=>{ DownloadBundle( file, sceneName ); }, downloadProgress )) ; 
				Debug.Log( loaded.Key );
			}
		}
	}


	private void DownloadBundle( Upload.File file, string scene )
	{
		if (file == null) {
			Debug.Log ("File for scene " + scene + " is null");
			downloadProgress.text += "File for scene " + scene + " is null\n";
		} 
		else {
			StartCoroutine( Download( file.url, ( WWW www ) =>{ BundleDownloaded( www, file.name, scene ); } ) );
		} 
	}

	private int bundlesDownloaded = 0;
	private void BundleDownloaded(  WWW www, string fileName, string scene )
	{
		downloadProgress.text += "Downloaded call " + scene + " / " + fileName +"\n";
		bundlesDownloaded++;
		if( string.IsNullOrEmpty( www.error ) )
		{
			string path = Path.Combine ( LoadSceneFromBundle.getBundlesPath, fileName );
#if UNITY_WP8
			UnityEngine.Windows.File.WriteAllBytes ( path, www.bytes );
#else
			File.WriteAllBytes ( path, www.bytes );
#endif
			LoadSceneFromBundle.AddSceneToBundlePath ( scene, path );
		}
		else
		{
			Debug.Log( www.error );
			test += www.error + "\n";
		}
		downloadProgress.text += "Checking "+ bundlesDownloaded +" / " + toDownloadCount + "\n";
		if ( bundlesDownloaded == toDownloadCount ) {
			Debug.Log( bundlesDownloaded +" "+ toDownloadCount );
			downloadProgress.text = bundlesDownloaded + " / " + toDownloadCount +"\n";
			allBundlesDownloaded ();
			downloadProgress.text += "Loading level";
			LoadSceneFromBundle.LoadScene ("MainMenu");
		}
	}
	private bool b = false;
	string  test = "";
	void Update()
	{
		if (currentDownloads.Count > 0) {
			string text = "";
			foreach (WWW download in currentDownloads) {
				if (download != null) {
					if (!download.isDone) {
						text += Mathf.RoundToInt (download.progress * 100f) + "%\n";
					}
					if( !string.IsNullOrEmpty( download.error ) )
					{
						text += download.error + "\n";
					}
				}
			}
			text += bundlesDownloaded + " / " + toDownloadCount;
			downloadProgress.text = text;
		} else {
			if( b )
			{
				b  = false;
				Debug.Log( bundlesDownloaded +" "+ toDownloadCount );
				allBundlesDownloaded ();
				downloadProgress.text += "Loading level";
				LoadSceneFromBundle.LoadScene ("MainMenu");
			}
		}
	}
	
	public class UnityCallBack : App42CallBack  
	{  
		public System.Action<Upload.File> callBack;
		private UnityEngine.UI.Text	statusLbl;
		
		
		public UnityCallBack( System.Action<Upload.File> _callBack, UnityEngine.UI.Text label)
		{
			statusLbl = label;
			callBack = _callBack;
		}
		
		public void OnSuccess(object response)  
		{  
			statusLbl.text = "Sucess";
			Upload upload = (Upload) response;    
			IList<Upload.File>  fileList = upload.GetFileList();  
			for(int i=0; i < fileList.Count; i++)  
			{  
//				App42Log.Console("fileName is " + fileList[i].GetName());  
//				App42Log.Console("fileType is " + fileList[i].GetType());  
//				App42Log.Console("fileUrl is " + fileList[i].GetUrl());  
//				App42Log.Console("TinyUrl Is  : " + fileList[i].GetTinyUrl());  
//				App42Log.Console("fileDescription is " + fileList[i].GetDescription());  
				if( fileList[i].GetUrl() != "" )
				{
					callBack( fileList[i] );
				}
			}  
		}  
		public void OnException( System.Exception e)  
		{  
			App42Log.Console("Exception : " + e);  
			statusLbl.text = e.Message;
			callBack( null );
		}  
		
		
	} 

	string UTF8ByteArrayToString(byte[] characters) 
	{      
		System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding(); 
		string constructedString = encoding.GetString( characters, 0, characters.Length ); 
		return (constructedString); 
	} 
}
