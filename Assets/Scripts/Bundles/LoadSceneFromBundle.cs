﻿#define TEST_SCENE

using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;


public class LoadSceneFromBundle : MonoBehaviour {

	public static string 	currentScene ="";
	public string[]			names;
	public GameObject[]		lvls;

	private static string 	prevScene;

	private static string	bundlesPath;

	static Dictionary< string, string > sceneNameToBundlePath = new Dictionary<string, string>();
	static Dictionary< string, AssetBundle > sceneLoadedBundles = new Dictionary<string, AssetBundle>();

	private static LoadSceneFromBundle instance;

	private static LoadSceneFromBundle getInstance
	{
		get{
			if( instance == null )
			{
				GameObject obj = new GameObject("LoadSceneFromBundle");
				instance = obj.AddComponent<LoadSceneFromBundle>();
				DontDestroyOnLoad( obj );
			}
			return instance;
		}
	}

	const string bundleLevel = "test";

#if TEST_SCENE	
	void Awake()
	{
		DontDestroyOnLoad( gameObject );
		LoadSceneFromBundle.LoadScene ("MainMenu");
	}
#endif

	public static void AddSceneToBundlePath( string scene, string bundlePath )
	{
		sceneNameToBundlePath.Add (scene, bundlePath);
	}

	public static void AddBundle( string scene, AssetBundle bundle )
	{
		if ( !sceneLoadedBundles.ContainsKey (scene) )
		{
			sceneLoadedBundles.Add (scene, bundle);
		}
	}


	public static string getBundlesPath
	{
		get{
			if( string.IsNullOrEmpty( bundlesPath ) )
			{
#if UNITY_EDITOR
				bundlesPath = Path.Combine( Application.dataPath, "bundles" );
#else
				bundlesPath = Path.Combine( Application.persistentDataPath, "bundles" );
#endif

				if( !Directory.Exists( bundlesPath ) )
				{
					Directory.CreateDirectory( bundlesPath );
				}
			}
			return bundlesPath; }
	}


	public static void LoadScene( string name )
	{
#if TEST_SCENE
		currentScene = name;
		Debug.Log( currentScene );
		Application.LoadLevelAsync(bundleLevel);
		return;
#endif

		Debug.Log ("LOAD_SCENE");
		prevScene = currentScene;
		currentScene = name;
		if (!sceneLoadedBundles.ContainsKey (currentScene)) {
			DownloadGameBundles.instance.downloadProgress.text += "Not Loaded \n";
			if (sceneNameToBundlePath.ContainsKey (currentScene)) 
			{
				DownloadGameBundles.instance.downloadProgress.text += "Path Exists \n";
				string path = Path.Combine (getBundlesPath, sceneNameToBundlePath [currentScene]);
				if (File.Exists (path)) {
					DownloadGameBundles.instance.downloadProgress.text += "File Exists \n";
					getInstance.StartCoroutine (PreloadBundle (new WWW ("file://" + path), ()=>{ Application.LoadLevel(bundleLevel); } ));	
				} else {
					DownloadGameBundles.instance.downloadProgress.text += "None file \n";
					Debug.Log ("Bundles file with name (" + sceneNameToBundlePath [currentScene] + ") does not exists");
				}
			}
		} else {
			DownloadGameBundles.instance.downloadProgress.text += "BundleLoaded";
			Application.LoadLevel(bundleLevel);
		}

	}


	IEnumerator CreateLevel( WWW assetBundle )
	{
		while (!assetBundle.isDone )
			yield return null;
		sceneLoadedBundles.Add (currentScene, assetBundle.assetBundle);
		Instantiate( assetBundle.assetBundle.mainAsset );
		Resources.UnloadUnusedAssets ();
	}

	static IEnumerator PreloadBundle( WWW assetBundle, System.Action onLoaded )
	{
		while (!assetBundle.isDone )
			yield return null;
		sceneLoadedBundles.Add (currentScene, assetBundle.assetBundle);
		DownloadGameBundles.instance.downloadProgress.text += "Preloaded \n";
		if( onLoaded != null )
		{
			DownloadGameBundles.instance.downloadProgress.text += "ON_LOAD_CALL \n";
			try{
				Application.LoadLevel(bundleLevel);
			}
			catch(System.Exception e ){
				DownloadGameBundles.instance.downloadProgress.text += e.Message +"\n";
			}
		}

	}
	

	void OnLevelWasLoaded(int level) {
		#if TEST_SCENE
		Resources.UnloadUnusedAssets();
		for( int i = 0; i < names.Length; i++ )
		{
			if( names[ i ] == currentScene )
			{
				GameObject currentLevel = Resources.Load<GameObject>("Levels/"+names[ i ]) as GameObject;
				if( currentLevel != null )
				{
					Instantiate( currentLevel );
				}
				break;
			}
		}
		return;
		#endif

		if (sceneLoadedBundles.ContainsKey ( prevScene ) ) {
		
			//sceneLoadedBundles[ prevScene ].Unload( true );
			sceneLoadedBundles.Remove( prevScene );
		}
		if (string.IsNullOrEmpty (currentScene)) {
			Debug.Log ("CurrentScene is null or empty");
		} 
		else 
		{
			if( sceneLoadedBundles.ContainsKey( currentScene ) )
			{
				Instantiate( sceneLoadedBundles[ currentScene ].mainAsset );
				Resources.UnloadUnusedAssets();
				//sceneLoadedBundles[ currentScene ].Unload( false );
			}
			else if( sceneNameToBundlePath.ContainsKey( currentScene ) )
			{
				string path = Path.Combine( getBundlesPath, sceneNameToBundlePath[ currentScene ] );
				if( File.Exists( path ) )
				{
					StartCoroutine( CreateLevel( new WWW( "file://" + path ) ) );	
				}
				else
				{
					Debug.Log("Bundles file with name ("+sceneNameToBundlePath[ currentScene ]+") does not exists");
				}
			}
		}
		//currentScene = "";
		
	}
}

