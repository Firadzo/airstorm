﻿using UnityEngine;
using System.Collections;

public class AtlasConrollerMainMenu : MonoBehaviour {
    public int targerFPS = 30;

    public bool resolution800x480 = false;//Android telephones
    public bool resolution1024x768 = false;//Ipad 1-4, ipad mini 
    public bool resolution1136x640 = false;//Iphone 4, 5, HD Android telephones

    public int resulutionAndroid = 800;
    public int resulutionIpad = 1024;
    public int resoltionHD = 1136;

    //gui prefab (3 different)
    public GameObject androidGUI;
    public GameObject ipadGUI;
    public GameObject hdGUI;

    public UILabel screenWidthLabel;
    public UILabel screenHeightLabel;


    private GameObject pl;

    private void Awake() {

        int ScreenHeight = Screen.height;
        int ScreenWidth = Screen.width;

        if (screenHeightLabel) {
            screenHeightLabel.text = ScreenHeight.ToString();
        }
        if (screenWidthLabel) {
            screenWidthLabel.text = ScreenWidth.ToString();
        }

        
        //GameObject obj;

        if (resolution800x480)
        {
			Instantiate(androidGUI, transform.position, Quaternion.identity);// as GameObject;
            PlayerPrefs.SetInt("guiType", 1);
        }
        else if (resolution1024x768)
        {
			Instantiate(ipadGUI, transform.position, Quaternion.identity);// as GameObject;
            PlayerPrefs.SetInt("guiType", 2);
        }
        else if (resolution1136x640)
        {
			Instantiate(hdGUI, transform.position, Quaternion.identity);// as GameObject;
            PlayerPrefs.SetInt("guiType", 3);
        }
    }

    void checkDevice() {
        
    }
}
