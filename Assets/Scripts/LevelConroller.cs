﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;



public class LevelConroller : MonoBehaviour
{
	[System.Serializable]
	public class UnitLoadStats
	{
		public AirStrike.TechniqueTypes type;
		public string name;
		public float damage;
		public float health;
	}
	
	private List<UnitLoadStats> _enemyUnits;
	private UnitLoadStats _playerUnitStats;
	private AirStrike.TechniqueTypesContainer _techniquesContainer;

	private const string PATH_TO_LEVEL_DATA = "Enemies_stats";

	public string levelName = "";
	public string playerName = "";
    public string currentLanguage = "Russian";
    //public int startMoney = 20000;
    public 	float 	SpawnInterval = 6.0f;
	private float	spawnTimer = 0;
    public int CurrentWave = 0;
    public float SpawnHeightFix = 10f;
    public float DestroyTraectoryTIMER = 15;

    public List<MainTraectory> LevelEnemies = new List<MainTraectory>();

    public int smallCoin = 10;
    public int bigCoin = 50;

    public int planesChancePer = 50;
    public int veniclesChancePer = 50;
    public int housesChancePer = 70;

    public float easyLevel = 0.3f;
    public float hardLevel = 0.35f;

    //public float levelHPFactor = 1f;

    public int currentDifficultyLevel = 0;

    public GameObject DebugPlane;

    private Localization 			_localization;
	private List<SpawnQueueObject>	spawnQueue;
	public static bool				spawnDisabled;
	//private float _levelHPFactor = 1f;
	//private float _levelDamageFactor = 1f;


	private Dictionary< string, List<AIPlane> >	planes;
	private SpawnPlayer playerSpawner;

    [System.Serializable]
    public class MainTraectory {
		[HideInInspector]
		public	string				name;
        public 	bool 				reCalcSubTraectories = false;
        public 	GameObject 			TraectoryPrefab;
        public 	List<SubTraectory> 	SubTraect = new List<SubTraectory>();
        public 	bool 				spawned = false;
        public 	float 				speed = 0.1f;
    }

    [System.Serializable]
    public class SubTraectory {
        public Transform Traectory;
        public GameObject EnemyPrefab;

        public AIPlane.gunLaunch launchType;
        public Transform BulletObject;
        public int 		BulletSpeed;
        public float 	ShotCoolDown;
		public float 	AttackInterval;
        public int 		ShotsPerAttack;

        public int count = 1;
        public float timeoutSpawn = 0;
        public bool spawned = false;
    }

	[System.Serializable]
	public class SpawnQueueObject
	{
		private float 			timer;
		public float			waitTime;
		public SubTraectory		enemy;
		public float			speed;
		public Transform		subEnemyTraject;
		public bool				active;
		
		public SpawnQueueObject( float _waitTime, SubTraectory _enemy, float _speed, Transform _subEnemyTraject )
		{
			Init( _waitTime, _enemy, _speed, _subEnemyTraject );
		}

		public void Init( float _waitTime, SubTraectory _enemy, float _speed, Transform _subEnemyTraject )
		{
			waitTime = _waitTime;
			enemy = _enemy;
			speed = _speed;
			subEnemyTraject = _subEnemyTraject;
			timer = 0;
			active = true;
		}

		public bool Update()
		{
			timer += Time.deltaTime;
			return (timer >= waitTime);
		}
	}

 #if UNITY_EDITOR
//	public bool fixValue;
	void OnDrawGizmosSelected()
	{
		/*if( fixValue )
		{
			fixValue = false;
			for( int i = 0; i < LevelEnemies.Count; i++ )
			{
				for( int j = 0; j < LevelEnemies[ i ].SubTraect.Count; j++ )
				{
					LevelEnemies[ i ].SubTraect[ j ].AttackInterval *= 1.5f;
					LevelEnemies[ i ].SubTraect[ j ].ShotCoolDown *= 1.5f;
				}
			}

		}*/
		for( int i = 0; i < LevelEnemies.Count; i++ )
		{
			if( LevelEnemies[ i ].SubTraect != null && LevelEnemies[ i ].SubTraect.Count > 0 )
			{
				if( LevelEnemies[ i ].SubTraect[ 0 ].EnemyPrefab != null )
				{
					LevelEnemies[ i ].name = LevelEnemies[ i ].SubTraect[ 0 ].EnemyPrefab.name+" "+LevelEnemies[ i ].TraectoryPrefab.name;
				}
				for( int j = 0; j < LevelEnemies[ i ].SubTraect.Count; j++ )
				{
					if( LevelEnemies[ i ].SubTraect[ j ].Traectory.GetComponent<SplineDataCopy>() != null )
					{
						LevelEnemies[ i ].name +="C";
					}

					if( LevelEnemies[ i ].SubTraect[ j ].BulletObject != null ||  LevelEnemies[ i ].SubTraect[ j ].BulletSpeed != 0 ||
					   LevelEnemies[ i ].SubTraect[ j ].AttackInterval != 0 || LevelEnemies[ i ].SubTraect[ j ].ShotCoolDown != 0 
					   || LevelEnemies[ i ].SubTraect[ j ].ShotsPerAttack != 0 )
					{
						LevelEnemies[ i ].name += " ["+j+"]";
					}
				}
			}
		}
	}
 #endif

    private void OnDrawGizmos() {
        for (int d = 0; d < LevelEnemies.Count; d++) {
            if (LevelEnemies[d].reCalcSubTraectories) {
                MainTraectory enemy = LevelEnemies[d];
                enemy.SubTraect = new List<SubTraectory>();

                for (int c = 0; c < enemy.TraectoryPrefab.transform.childCount; c++) {
                    SubTraectory currentTraect = new SubTraectory();
                    currentTraect.Traectory = enemy.TraectoryPrefab.transform.GetChild(c);
                    enemy.SubTraect.Add(currentTraect);
                    if (DebugPlane) {
                        currentTraect.EnemyPrefab = DebugPlane;
                    }
                }
                LevelEnemies[d].reCalcSubTraectories = false;
            }
        }
    }

	public static LevelConroller instance;

    private void Awake() {
		SaveLoadParams.LoadParams();
		instance = this;
		levelName = LoadSceneFromBundle.currentScene;
        //Application.targetFrameRate = 10;
        QualitySettings.vSyncCount = 0;
		spawnQueue = new List<SpawnQueueObject> ();
		playerSpawner = gameObject.GetComponent<SpawnPlayer>();
		playerSpawner.typeSpawn = SaveLoadParams.plPlane;

		LoadPlayerTypes( Resources.Load<TextAsset>("Player_stats").text );
		LoadTechniquesTypes( Resources.Load<TextAsset>("Technique_types").text );
		LoadLevelParameters( Resources.Load<TextAsset>("Enemies_stats").text );

        //Screen.sleepTimeout = SleepTimeout.NeverSleep;
        CurrentWave = 0;
        Application.targetFrameRate = 60;

        PlayerPrefs.SetInt("bigCoin", bigCoin);
        PlayerPrefs.SetInt("smallCoin", smallCoin);

        currentDifficultyLevel = SaveLoadParams.plDiff;

        _localization = FindObjectOfType<Localization>();

        if (_localization != null && currentLanguage.Length > 0) {
            //_localization.currentLanguage = currentLanguage;
        }
		planes = new Dictionary<string, List<AIPlane>> ();
		Transform transf = transform;
		int maxCount = 4;
		int k;
		for( int i = 0; i < LevelEnemies.Count; i++ )
		{
			if( LevelEnemies[ i ].SubTraect != null )
			{
				for( int j = 0; j < LevelEnemies[ i ].SubTraect.Count; j++ )
				{
					if( LevelEnemies[ i ].SubTraect[ j ].EnemyPrefab != null )
					{
						for( k = 0; k < LevelEnemies[ i ].SubTraect[ j ].count; k++ )
						{
							if( planes.ContainsKey( LevelEnemies[ i ].SubTraect[ j ].EnemyPrefab.name ) )
							{
								if( planes[ LevelEnemies[ i ].SubTraect[ j ].EnemyPrefab.name ].Count == maxCount )
								{
									continue;
								}
							}
							//CreatePlane( LevelEnemies[ i ].SubTraect[ j ].EnemyPrefab, transf );
						}
					}
				}
			}
		}
    }

	private void CreatePlane( GameObject prefab, Transform parent )
	{
		if( !planes.ContainsKey( prefab.name ) )
		{
			planes.Add( prefab.name, new List<AIPlane>() );
		}
		GameObject enemy = Instantiate( prefab ) as GameObject;
		enemy.name = enemy.name.Replace ("(Clone)", "");
		enemy.transform.parent = parent;
		planes[ prefab.name ].Add( enemy.GetComponent<AIPlane>() );
	}

	void Start()
	{
		if (PlayerPrefs.GetInt("replay", -1) <= 0)
		{
			playerSpawner.spawnPlayer();//_playerUnitStats);
		}
	}

	private AIPlane GetPlane( GameObject prefab )
	{
		if ( planes.ContainsKey ( prefab.name ) ) 
		{
			foreach( AIPlane plane in planes[ prefab.name ] )
			{
				if( plane.ready )
				{
					return plane;
				}
			}
		}
		CreatePlane ( prefab, transform );
		return planes[ prefab.name ][ planes[ prefab.name ].Count - 1 ];
	}

    private void SpawnWave() 
	{
        if ( spawnDisabled ) 
		{
			return;
        }

            if ( CurrentWave < LevelEnemies.Count ) 
			{
				SpawnEnemies( CurrentWave );
                CurrentWave++;
            } 
			else 
			{
                Debug.Log("All Enemies Spawned");
            }
    }

    /*
	 * 
	 * for(int i = 0; i<LevelEnemies.Count; i++)
		{
			TriggerEnter coll = LevelEnemies[i].Trigger.GetComponent<TriggerEnter>();
			if(coll.collision && !coll.used) 
			{
                coll.collision = true;
                coll.used = true;

				if(i>current_trigger)
				{
					//Clear OLD traectories
					Transform Trigger = LevelEnemies[i-1].Trigger.transform;
					for(int d = 0; d < Trigger.GetChildCount(); d++)
					{	
						Destroy(Trigger.GetChild(d).gameObject);
					}
				}
				current_trigger = i;
				Debug.Log("Spawn Trigger id "+i);
				spawnEnemies(i);
				
				//LevelEnemies[i].Trigger.SetActive(false);
				//break;
			}
		}
	 * 
	 * */

	private void AddToSpawn( float _waitTime, SubTraectory _enemy, float _speed, Transform _subEnemyTraject )
	{
		for ( int i = 0; i < spawnQueue.Count; i++ ) 
		{
			if( !spawnQueue[ i ].active )
			{
				spawnQueue[ i ].Init( _waitTime, _enemy, _speed, _subEnemyTraject );
				return;
			}
		}
		spawnQueue.Add( new SpawnQueueObject( _waitTime, _enemy, _speed, _subEnemyTraject ) );
	}


    private Vector3 spawnDirPoint = Vector3.zero;

	int k;
    private void Update() {
		if ( !spawnDisabled ) 
		{
			spawnTimer += Time.deltaTime;
			if( spawnTimer >= SpawnInterval )
			{
				SpawnWave();
				spawnTimer = 0;
			}

			for( k = 0; k < spawnQueue.Count; k++ )
			{
				if( spawnQueue[ k ].active )
				{
					if( spawnQueue[ k ].Update() )
					{
						spawnQueue[ k ].active = false;
						SpawnEnemy( spawnQueue[ k ] );
					}
				}
			}
		}

        //Ray ray = new Ray(Camera.mainCamera.transform.position, Camera.mainCamera.transform.forward);
        // spawnDirPoint = Camera.mainCamera.transform.position + ray.direction*65f;
    }


    private void SpawnEnemies(int id) {
        //Список противников
        if ( !LevelEnemies[id].spawned ) 
		{
            GameObject traectory = Instantiate(LevelEnemies[id].TraectoryPrefab, Vector3.zero, Quaternion.identity) as GameObject; //current traectory	
            traectory.transform.parent = this.transform;

			spawnDirPoint = PlayerMoveController.instance.transform.position;

            traectory.transform.position = new Vector3(spawnDirPoint.x, spawnDirPoint.y - SpawnHeightFix, spawnDirPoint.z);

            Spline[] spline = traectory.GetComponentsInChildren<Spline>();

            for (int i = 0; i < spline.Length; i++) 
			{
                spline[i].updateMode = Spline.UpdateMode.DontUpdate;
                spline[i].interpolationAccuracy = 1;
            }

            //Destroy( traectory, DestroyTraectoryTIMER );
			SubTraectory enemy;
			float waitTime;
			Transform subEnemyTraect;
            for ( int c = 0; c < LevelEnemies[id].SubTraect.Count; c++ )
			{
                enemy = LevelEnemies[id].SubTraect[c];
                if ( !enemy.spawned ) 
				{
                    waitTime = enemy.timeoutSpawn;
                    if ( traectory.transform ) 
					{
                        subEnemyTraect = traectory.transform.GetChild( c );

                        //Список кол-ва противников данного типа и траектории
                        for (int d = 0; d < enemy.count; d++) 
						{
							AddToSpawn( waitTime * (d + 1), enemy, LevelEnemies[id].speed, subEnemyTraect );
                        }
                        enemy.spawned = true;
                    }
                }
            }
        }
    }


	private void SpawnEnemy( SpawnQueueObject enemyToSpawn ) 
	{
#if UNITY_EDITOR
		if (!enemyToSpawn.enemy.EnemyPrefab ) 
		{
			Debug.Log("No enemy prefab assigned!");
		}
		
		if ( !enemyToSpawn.subEnemyTraject ) 
		{
			Debug.Log("No traectory assigned!");
		}
#endif
		
		if ( !enemyToSpawn.enemy.EnemyPrefab || !enemyToSpawn.subEnemyTraject ) 
		{
			return;
		}
		AIPlane plane = GetPlane ( enemyToSpawn.enemy.EnemyPrefab );

		if (plane == null) {
			Debug.LogError( enemyToSpawn.enemy.EnemyPrefab.name  );
		}
		plane.monetChance = planesChancePer;
		//plane.Health *= levelHPFactor;
		UnitLoadStats unitStats = GetUnitLoadStats(plane.techniqueType);
		if( unitStats == null)
		{
			Debug.LogError("Cant find unit load stats!!! - " + plane.name);
			//GameObject.DestroyImmediate( plane.gameObject );
			return;
		}
		
		plane.Health = unitStats.health;
		plane.bulletDamage = unitStats.damage;
		if (currentDifficultyLevel == 0) {
			plane.Health = plane.Health - (plane.Health * easyLevel);
		} else if (currentDifficultyLevel == 2) {
			plane.Health = plane.Health + (plane.Health * hardLevel);
		}
		plane.launchType = enemyToSpawn.enemy.launchType;
		if ( enemyToSpawn.enemy.BulletObject ) 
		{
			plane.BulletObject = enemyToSpawn.enemy.BulletObject;
		}
		if ( enemyToSpawn.enemy.BulletSpeed != 0 ) 
		{
			plane.BulletSpeed = enemyToSpawn.enemy.BulletSpeed;
		}
		
		//Debug.Log( ( AttackInterval / 60f ) +"  "+( ShotCoolDown / 60f ) +"  "+ShotsPerAttack );
		plane.InitPlane( enemyToSpawn.enemy.AttackInterval, enemyToSpawn.enemy.ShotCoolDown, enemyToSpawn.enemy.ShotsPerAttack, enemyToSpawn.subEnemyTraject, enemyToSpawn.speed );
	}

#region ParamsLoad
	private void LoadTechniquesTypes(string techniqueTypesXMLText)
	{
		if( techniqueTypesXMLText.Trim().Equals("") )
		{
			Debug.LogError("Cant find any game units stats!!!");
			return;
		}

		XmlDocument doc = new XmlDocument();
		doc.LoadXml( techniqueTypesXMLText );
		XmlElement techniqueTypesNode = doc.DocumentElement;
		_techniquesContainer = new AirStrike.TechniqueTypesContainer ();
		if( !_techniquesContainer.ParseTechniquesTypes(techniqueTypesNode) )
		{
			Debug.LogError("Cant parse technique by types!!!");
		}
	}

	private void LoadPlayerTypes(string playerTypesXMLText)
	{
		if( playerTypesXMLText.Trim().Equals("") )
		{
			Debug.LogError("Cant find any player units stats!!!");
			return;
		}
		
		XmlDocument doc = new XmlDocument();
		doc.LoadXml( playerTypesXMLText );
		XmlElement playerUnitsNode = doc.DocumentElement;
		string tempName;
		_playerUnitStats = null;
		foreach( XmlElement unitIn in playerUnitsNode.ChildNodes )
		{
			tempName = unitIn.GetAttribute("Name");
			if( !playerSpawner.PlayerPlanes[playerSpawner.typeSpawn].name.Equals(tempName) )
			{
				continue;
			}
			
			_playerUnitStats = new UnitLoadStats();
			_playerUnitStats.name = tempName;
			_playerUnitStats.damage = System.Convert.ToSingle(unitIn.GetAttribute("GunDamage"));
			_playerUnitStats.health = System.Convert.ToSingle(unitIn.GetAttribute("Health"));
			break;
		}

		if( _playerUnitStats == null )
		{
			Debug.LogError("Cant find current player unit stats!!!");
			return;
		}
	}

	private UnitLoadStats GetUnitLoadStats(AirStrike.TechniqueTypes unitType)
	{
		foreach(UnitLoadStats unitStats in _enemyUnits)
		{
			if(unitStats.type == unitType)
			{
				return unitStats;
			}
		}

		return null;
	}

	private void LoadLevelParameters(string gameUnitsStatsXMLText)
	{
		if( gameUnitsStatsXMLText.Trim().Equals("") )
		{
			Debug.LogError("Cant find any game units stats!!!");
			return;
		}
		
		XmlDocument doc = new XmlDocument();
		doc.LoadXml( gameUnitsStatsXMLText );
		XmlElement gameUnitsStatsNode = doc.DocumentElement;
		
		XmlElement levelsNode = (XmlElement)gameUnitsStatsNode.GetElementsByTagName("Levels")[0];
		XmlElement enemiesNode;
		UnitLoadStats enemyStats;
		string tempName;
		foreach( XmlElement levelIn in levelsNode.ChildNodes )
		{
			tempName = levelIn.GetAttribute("Name");
			if( !tempName.Equals( levelName ) )
			{
				continue;
			}

			_enemyUnits = new List<UnitLoadStats>();
			enemiesNode = (XmlElement)levelIn.GetElementsByTagName("EnemyTypes")[0];
			foreach( XmlElement unitIn in enemiesNode.ChildNodes )
			{
				enemyStats = new UnitLoadStats();
				//enemyStats.name = unitIn.GetAttribute("Name");
				enemyStats.type = AirStrike.TechniqueByType.GetTechniqueTypeByName(unitIn.GetAttribute("Name"));
				enemyStats.damage = System.Convert.ToSingle(unitIn.GetAttribute("Damage"));
				enemyStats.health = System.Convert.ToSingle(unitIn.GetAttribute("Health"));
				_enemyUnits.Add(enemyStats);
			}
			break;
		}

		if(_enemyUnits == null)
		{
			Debug.LogError("Cant find any enemy units stats!!!");
			return;
		}
	}
	#endregion
}
