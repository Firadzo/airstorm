﻿using UnityEngine;
using System.Collections;

public class BossZepelinRocket : MonoBehaviour {
	public Transform LookTarget;
	public Transform Turret;
	public Transform tiltableGun;
    public Transform[] FirePos;
	
    public Transform BulletObject;
    public int ShotCoolDown = 20; 
	
    private Transform BulletObjectCopy;
    private int ShotCoolDownCount;
    private int AttackIntervalCount;
    private int ShotsPerAttackCount;

    private GameObject Player;
	public bool shootEnable = true;
	
    //Constructor
    public BossZepelinRocket()
    {
        ShotCoolDownCount = ShotCoolDown;
    }

    void Start()
    {
        Player = GameObject.Find("Player"); //Set player object
		shootEnable = Player.GetComponentInChildren<PlayerScript>().shootEnable;
        ShotCoolDownCount = 0;
    }
	
	private bool avariable = false;
	
    void Update()
    {
		if(!avariable)
		{
			avariable = GetComponent<BossPart>().avariable;
			return;
		}
		//shootEnable = Player.GetComponentInChildren<PlayerScript>().shootEnable;
		
		if(shootEnable)
		{
			if(Turret)
			{
				//rotate turret
				Turret.transform.localEulerAngles = new Vector3(
					Turret.localEulerAngles.x, 
					LookTarget.localEulerAngles.y + 45,
					Turret.localEulerAngles.z);
			}
			
			if(tiltableGun)
			{
				//tilt gun
				tiltableGun.localEulerAngles = new Vector3(
				tiltableGun.localEulerAngles.x,
				tiltableGun.localEulerAngles.y,
				LookTarget.localEulerAngles.z + 125);
			
			}
		
			Vector3 posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
			
			if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) 
            {
	            if (ShotCoolDownCount > 0) {
	                ShotCoolDownCount--;
	            } else {

                    for (int i = 0; i < FirePos.Length; i++)
                    {
                        Shoot(FirePos[i]);
                    }
	            }
			}
		}
 
    }
	

    //Shoot
    private void Shoot(Transform FirePos)
    {
        if (BulletObject)
        {
            BulletObjectCopy = Instantiate(BulletObject, FirePos.position, Quaternion.identity) as Transform;
			BulletObjectCopy.transform.Translate(FirePos.transform.forward*2, Space.World);	
            ShotCoolDownCount = ShotCoolDown;
        }
    }

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
