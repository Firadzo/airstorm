﻿using UnityEngine;
using System.Collections;

public class BossZepelinFreezeGun : MonoBehaviour {
public Transform LookTarget;
	public Transform Turret;
	public Transform tiltableGun;
    public Transform FirePos;

    public Transform BulletObject;
    public int ShotCoolDown = 20; //How long to wait between each new bullet shot
	public int BulletSpeed = 50; //bullet's speed when shot
	
	public bool shootEnable = true;
	public bool avariable = false;
	
    private Transform BulletObjectCopy; //Holds a copy of the created bullet object so we can give it some velocity later
    private int ShotCoolDownCount; //Used to hold the original cooldown value set by us

    private GameObject Player;
	private bool isHide = true;
	private bool enableFire = false;
	
    //Constructor
    public BossZepelinFreezeGun()
    {
        ShotCoolDownCount = ShotCoolDown;
    }
	
    void Start()
    {
        Player = GameObject.Find("Player"); //Set player object
		shootEnable = Player.GetComponentInChildren<PlayerScript>().shootEnable;
        ShotCoolDownCount = 0;
		
		ParticleEmitter emitter= GetComponentInChildren<ParticleEmitter>();
		if (emitter) emitter.emit = false;
    }

    void Update()
    {
     	if(!avariable)return;
		
		if(isHide && !enableFire)
		{
			GetComponent<Animation>().Play("ZepelinOpenTurel");
			return;
		}
		
		if(shootEnable)
		{
			GetComponent<Animation>().Play("MausUpTurretRot");
			
			if(tiltableGun)
			{
				tiltableGun.localEulerAngles = new Vector3(
                    tiltableGun.localEulerAngles.x,
                    LookTarget.localEulerAngles.y - 45,
                    tiltableGun.localEulerAngles.z);
			
			}
		
			Vector3 posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
			
			if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) 
            {
				 if (ShotCoolDownCount > 0) {
	                ShotCoolDownCount--;
	            } else {
                     Shoot(FirePos);
	            }
			}
		}
 
    }
	
    //Shoot
    private void Shoot(Transform FirePos)
    {
        BulletObjectCopy = Instantiate(BulletObject, FirePos.position, Quaternion.identity) as Transform;

        //Vector3 dir = transform.position - Player.transform.position;

        //Give the bullet some forward force
        if (BulletObjectCopy.GetComponent<Rigidbody>())
        {
            BulletObjectCopy.GetComponent<Rigidbody>().AddForce(tiltableGun.forward * BulletSpeed*10);
        }
        //Reset the cooldown timer for the bullet
        ShotCoolDownCount = ShotCoolDown;
  
    }	
	
	public void enableShootTurel()
	{
		enableFire = true;
	}
	
	public void EnableGun()
	{
		avariable = true;
	}

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
