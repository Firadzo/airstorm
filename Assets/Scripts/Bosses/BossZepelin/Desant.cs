﻿using UnityEngine;
using System.Collections;

public class Desant : MonoBehaviour {

    private bool enableDestroy = false;

	void Start () {
	    Destroy(gameObject, 5);
	    StartCoroutine(enabeAutoDestoy());
	}

    private IEnumerator enabeAutoDestoy() {
        yield return new WaitForSeconds(0.3f);
        enableDestroy = true;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name != "Aim" && enableDestroy)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.name != "Aim" && enableDestroy)
        {
            Destroy(gameObject);
        }
    }
}
