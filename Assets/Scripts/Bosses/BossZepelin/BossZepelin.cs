﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossZepelin : MonoBehaviour {

    public Vector3 moveDir = Vector3.back;
	public Vector3 RotationFIX = Vector3.zero;
    public float ANGLE_FIX = 90f;
	
	public List<BossParts> bossParts = new List<BossParts>();
    public Spline inTraectory;
    public bool nowInTraectory = false;
    public GameObject ExplosionEffect;
	public float speed = 0f;
	
	public string moveAnimName;
	
    private Spline spline;
    private float passedTime;
    public WrapMode wrapMode = WrapMode.Clamp;

    public int currentDestroyPhase = 0;
	public BossZepelinFreezeGun freezeGun;
	
	public EndLevelPopup endLevel;
	
	public GameObject Plane;
    public Transform SpawnPos;
    public Transform WaypointsObject;
	public int respawnPlanes = 4;

    public Transform kamikadzePrefab;
    public Spline SplineKamikadze1;
    public Spline SplineKamikadze2;

    public int desantCount = 0;
    public int desantWinCount = 4;
    public GameObject[] BossAim;

	[System.Serializable]
	public class BossParts
	{
		public GameObject rootPart;
		public int HeathPart;
	}

    public GameObject logo;

    public Transform monetObjSmall;
    public int monetCountSmall = 10;
    public Transform monetObjBig;
    public int monetCountBig = 4;

    private bool moveEnable = true;
    private GameObject Player;
    private float speedMove = 0f;
	private float speedIn = 0.1f;
	private PlayerMoveController _moveController;

	void Start () 
    {
	    spline = inTraectory;
        nowInTraectory = true;
        Player = GameObject.Find("Player");
		if (Player) {
			moveEnable = Player.GetComponentInChildren<PlayerScript>().moveEnable;
		}
		_moveController = FindObjectOfType<PlayerMoveController>();
		if (_moveController) speedMove = _moveController.Speed;

		speedIn = speedMove * 0.022f;
        currentDestroyPhase = 0;

        activePart();

        for (int j = 0; j < BossAim.Length; j++)
        {
           BossAim[j].SetActive(false);
        }

	    GameObject.Find("WaterBlue").GetComponent<MoveSyncPlayer>().enabled = true;

        endLevel = GameObject.Find("EndLevel").GetComponent<EndLevelPopup>();
	}

    public bool desantEnable = false;
	public bool moveToBow = false;
	private bool startSpawnPlanes = false;
	
	void Update () 
    {
		if(!spline)return;

		if (moveEnable && nowInTraectory) {
			passedTime += Time.fixedDeltaTime * speedIn;
			
			float posOnSplne = WrapValue(passedTime, 0f, 1f, wrapMode);
			
			if (nowInTraectory && posOnSplne > 0.55f) {
				nowInTraectory = false;
				return;
	        }
			else if(moveEnable)
			{
				
				if (moveAnimName.Length > 0) {
					transform.GetChild(0).GetComponent<Animation>().Play(moveAnimName);
				}
				Debug.Log("BossZepelin speedMove =" + speedMove);
				transform.Translate(Vector3.forward * speedMove *Time.deltaTime);
			}

            transform.position = spline.GetPositionOnSpline(posOnSplne);
	
	        Quaternion rot = spline.GetOrientationOnSpline(WrapValue(passedTime, 0f, 1f, wrapMode));
	        Quaternion addHorizontalRot = Quaternion.AngleAxis(ANGLE_FIX, RotationFIX);
	
	        transform.rotation = Quaternion.Slerp(transform.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f);
	    }
		else if(!startSpawnPlanes)
		{
			startSpawnPlanes = true;
			StartCoroutine(spawnEnemy());
		}

        //move after traectory
        if (moveEnable && !nowInTraectory)
        {
            transform.Translate(moveDir * speedMove / 100);
        }

		if(moveToBow)
		{
			passedTime -= Time.deltaTime * speed;

	        float posOnSplne = WrapValue(passedTime, 0f, 1f, wrapMode);

	        if (posOnSplne <= 0.7f) {
	            moveToBow = false;
                return;
	        }
			
            transform.position =  spline.GetPositionOnSpline(posOnSplne);;
	
	        Quaternion rot = spline.GetOrientationOnSpline(WrapValue(passedTime, 0f, 1f, wrapMode));
	        Quaternion addHorizontalRot = Quaternion.AngleAxis(ANGLE_FIX, RotationFIX);
	
	        transform.rotation = Quaternion.Slerp(transform.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f);
		}

        if(!desantEnable) checkParts();
	}

    private void checkParts() 
	{
        if (bossParts.Count > 0) 
        {
			if(!bossParts[currentDestroyPhase].rootPart) currentDestroyPhase += 1;
			
            BossPart[] parts = bossParts[currentDestroyPhase].rootPart.GetComponentsInChildren<BossPart>();
				
            if (parts.Length <= 0) 
            {
                currentDestroyPhase += 1;

                if ((currentDestroyPhase == 2) && (desantCount <= 0)) //enable desant only ones!
                {  
                    Debug.Log("Desant Enable!");
                    EnableDesant();
                    return;
                }
				
                if (currentDestroyPhase >= bossParts.Count) {

                    showLogo();

                    StopCoroutine("spawnEnemy");

					StartCoroutine(playerWin());

                    spline = null;

                    addCoin();

                    return;
                }

                activePart();
            }
        } else {
            Debug.Log("Boss not have parts");
        }
    }
	
    private void activePart()
    {
        GameObject levConroller = GameObject.Find("LevelConroller");
        LevelConroller comp = levConroller.GetComponent<LevelConroller>();

        BossPart[] partsNext = bossParts[currentDestroyPhase].rootPart.GetComponentsInChildren<BossPart>();

        for (int i = 0; i < partsNext.Length; i++)
        {
			partsNext[i].Activate();

            float heath = bossParts[currentDestroyPhase].HeathPart;

            if (comp.currentDifficultyLevel == 0)
            {
                partsNext[i].Heath = heath + (heath * comp.easyLevel);
            }
            else if (comp.currentDifficultyLevel == 2)
            {
                partsNext[i].Heath = heath + (heath * comp.hardLevel);
            }
            else
            {
                partsNext[i].Heath = heath;
            }
            if (partsNext[i].GetComponent<BossTurel>()) partsNext[i].GetComponent<BossTurel>().shootEnable = true;
        }
    }

	IEnumerator playerWin()
	{
		yield return new WaitForSeconds(4);
		endLevel.endLevel();
	}
	
    private float WrapValue(float v, float start, float end, WrapMode wMode)
    {
        switch (wMode)
        {
            case WrapMode.Clamp:
            case WrapMode.ClampForever:
                return Mathf.Clamp(v, start, end);
            case WrapMode.Default:
            case WrapMode.Loop:
                return Mathf.Repeat(v, end - start) + start;
            case WrapMode.PingPong:
                return Mathf.PingPong(v, end - start) + start;
            default:
                return v;
        }
    }

    public void MoveEnableNow()
    {
        moveEnable = true;
    }

    public void MoveDisableNow()
    {
        moveEnable = false;
    }
	
	public void enableFreezeGun()
	{
		if(freezeGun)
		{
			freezeGun.EnableGun();
		}

        activePart();

        WaypointsMove[] planes = gameObject.GetComponentsInChildren<WaypointsMove>();
        for (int i = 0; i < planes.Length; i++) {
            Destroy(planes[i].gameObject);
        }
        moveToBow = true;
	}

	IEnumerator spawnEnemy()
    {
        yield return new WaitForSeconds(respawnPlanes);
        GameObject enemy = Instantiate(Plane, SpawnPos.position, Quaternion.FromToRotation(Vector3.zero, new Vector3(270, 0, 0))) as GameObject;
	    enemy.GetComponent<WaypointsMove>().WaypointsObj = WaypointsObject;
	    enemy.GetComponent<Transform>().parent = gameObject.transform;

       // GameObject enemy2 = Instantiate(kamikadzePrefab, Vector3.zero, Quaternion.FromToRotation(Vector3.zero, new Vector3(270, 0, 0))) as GameObject;
        SplineAnimator splineEnemy = kamikadzePrefab.GetComponent<SplineAnimator>();
        splineEnemy.spline = SplineKamikadze1;

       // GameObject enemy3 = Instantiate(kamikadzePrefab, Vector3.zero, Quaternion.FromToRotation(Vector3.zero, new Vector3(270, 0, 0))) as GameObject;
        SplineAnimator splineEnemy2 = kamikadzePrefab.GetComponent<SplineAnimator>();
        splineEnemy2.spline = SplineKamikadze2;

	    StartCoroutine(spawnEnemy());
    }

   public void EnableDesant()
   {
       desantEnable = true;
       BossAim[0].SetActive(true);
       Player.GetComponentInChildren<PlayerScript>().DesantEnable();
   }

    public void increaseDesant()
    {
        if (!desantEnable) return;

        desantCount++;

        if (desantCount >= desantWinCount)
        {
            Debug.Log("Desant disable");
            enableFreezeGun();
            desantEnable = false;
            Player.GetComponentInChildren<PlayerScript>().DesantDisable();
            //play flag animation
            return;
        }
   
        BossAim[desantCount].SetActive(true);
    }

    public void showLogo() {
        if (logo && logo.GetComponent<Animation>()) logo.GetComponent<Animation>().Play();
    }

    private void addCoin() {
        if (monetObjSmall) {
            for (int i = 0; i < monetCountSmall; i++) {
                Vector3 pos = new Vector3(Random.Range(-100, 100), transform.localPosition.y, Random.Range(-40, 40));
                Instantiate(monetObjSmall, transform.TransformPoint(pos), Quaternion.identity);
            }
        }
        if (monetObjBig) {
            for (int i = 0; i < monetCountBig; i++) {
                Vector3 pos = new Vector3(Random.Range(-100, 100), transform.localPosition.y, Random.Range(-40, 40));
                Instantiate(monetObjBig, transform.TransformPoint(pos), Quaternion.identity);
            }
        }
    }
}
