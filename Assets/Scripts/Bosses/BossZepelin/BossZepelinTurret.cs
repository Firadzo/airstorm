﻿using UnityEngine;
using System.Collections;

public class BossZepelinTurret : MonoBehaviour {
    public Transform LookTarget;
	public Transform tiltableGun;
    public Transform[] FirePos;

    //Destroy Section
    //public Transform Debris; //The debri object that flies from the crashed plane
    public Transform BulletObject; //The bullet shot from the plane
  
    public int BulletSpeed = 50; //bullet's speed when shot
    public int ShotCoolDown = 10; //How long to wait between each new bullet shot
    public int AttackInterval = 70; //How long to wait between each new attack
    public int ShotsPerAttack = 3; //How many bullets to shoot each attack
	
	public bool shootEnable = true;
	public bool avariable = false;
	
    private Transform BulletObjectCopy; //Holds a copy of the created bullet object so we can give it some velocity later
    private int ShotCoolDownCount; //Used to hold the original cooldown value set by us
    private int AttackIntervalCount; //Used to hold the original Attack Interval set by us
    private int ShotsPerAttackCount; //Used to hold the original Shots Per Attack set by us

    private GameObject Player;
    private EndLevelPopup popup;

    //Constructor
    public BossZepelinTurret()
    {
        ShotsPerAttackCount = ShotsPerAttack;
        AttackIntervalCount = AttackInterval;
        ShotCoolDownCount = ShotCoolDown;
    }
	
    void Start()
    {
        Player = GameObject.Find("Player"); //Set player object
		shootEnable = Player.GetComponentInChildren<PlayerScript>().shootEnable;
        AttackIntervalCount = 0;
        ShotCoolDownCount = 0;
		
    }
	
	
    void Update() {
        
		//shootEnable = Player.GetComponentInChildren<PlayerScript>().shootEnable;	
		if(!avariable) avariable = GetComponent<BossPart>().avariable;
		
		if(!avariable) return;
		
		if(shootEnable) {
			GetComponent<Animation>().Play("ZepelinTurretRot");
			
			if(tiltableGun)
			{
				tiltableGun.localEulerAngles = new Vector3(
				tiltableGun.localEulerAngles.x,
				-20,
				tiltableGun.localEulerAngles.z);
			}
		
			Vector3 posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
			
			if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) 
            {
		        //First, we wait for a set amount of frame updates, and then start shooting a burst
		        if (AttackIntervalCount > 0) {
		            AttackIntervalCount--;
		        } else {
		            //The rate of fire is set by the value of ShotCooldown, and is set in frame updates
		            if (ShotCoolDownCount > 0) {
		                //Decrease the cooldown counter
		                ShotCoolDownCount--;
		            } else {
						
                        for (int i = 0; i < FirePos.Length; i++)
                        {
                            Shoot(FirePos[i]);
                        }
			            //The number of shots per burst is set by ShotsPerAttack
		                if (ShotsPerAttackCount > 0) {
		                    ShotsPerAttackCount--;
		                } else {
		                    ShotsPerAttackCount = ShotsPerAttack;
		                    AttackIntervalCount = AttackInterval;
		                }
		            }
		        }
			}
		}
    }
	
    private void Shoot(Transform FirePos)
    {
        if (BulletObject)
        {
            BulletObjectCopy = Instantiate(BulletObject, FirePos.position, Quaternion.identity) as Transform;

            //Vector3 dir = transform.position - Player.transform.position;

            //Give the bullet some forward force
            if (BulletObjectCopy.GetComponent<Rigidbody>())
            {
                BulletObjectCopy.GetComponent<Rigidbody>().AddForce(-FirePos.forward * BulletSpeed*10);
            }
            //Reset the cooldown timer for the bullet
            ShotCoolDownCount = ShotCoolDown;
        }
    }

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
