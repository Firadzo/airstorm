﻿using UnityEngine;
using System.Collections;

public class BossZepelinAim : MonoBehaviour {

    new public string name = "Desant";

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.name == name)
        {
            BossZepelin boss = gameObject.transform.root.GetComponent<BossZepelin>();
            boss.increaseDesant();
            Destroy(coll.gameObject);
            Destroy(gameObject);

        }
    }

}
