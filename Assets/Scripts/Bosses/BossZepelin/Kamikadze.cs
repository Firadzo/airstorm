﻿using UnityEngine;
using System.Collections;

public class Kamikadze : MonoBehaviour {

    public GameObject explosion;
   // public int timeOut = 3;
    public int speed = 5;
    public float Damage = 1;

    Transform Player;

    public bool moveEnable = true;
    
    private bool kamikadzeModeOff = false;
    //private Vector3 defaultDir = Vector3.zero;

    void Start()
    {
        Player = GameObject.Find("Player").transform;
        moveEnable = PlayerScript.instance.moveEnable;
       // defaultDir = transform.forward;
    }

    void Update()
    {
        if (moveEnable)
        {

            if (Vector3.Distance(transform.position, Player.position) < 30 && (transform.position.x - Player.transform.position.x < 0))
            {
                kamikadzeModeOff = true;
            }

            if (kamikadzeModeOff) {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, 270, 0)), Time.fixedDeltaTime * 4);
                //transform.LookAt(Vector3.forward);
                transform.Translate(speed * Vector3.forward * Time.fixedDeltaTime);
                return;
            }

            transform.LookAt(Player.position);
            transform.Translate(speed * Vector3.forward * Time.fixedDeltaTime);

        }
        else
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Player") return;

        ContactPoint contact = collision.contacts[0];
        Quaternion rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Instantiate(explosion, contact.point, rotation);

		PlayerScript.instance.HitPlayer( Damage );
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    public void MoveEnableNow()
    {
        moveEnable = true;
    }

    public void MoveDisableNow()
    {
        moveEnable = false;
    }
}
