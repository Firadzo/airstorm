﻿using UnityEngine;
using System.Collections;

public class MausBackTurel : MonoBehaviour {
	//GUN
   // public Transform LookTarget;
    public Transform FirePos;

    //Destroy Section
    //public Transform ExplosionEffect; //The effect created when the plane explodes
    public Transform BulletObject; //The bullet shot from the plane
  
    public int 		BulletSpeed = 50; //bullet's speed when shot
	public float 	ShotCoolDown = 10; //How long to wait between each new bullet shot
	public float 	AttackInterval = 70; //How long to wait between each new attack
    public int 		ShotsPerAttack = 3; //How many bullets to shoot each attack
	
	//public bool rightGun = false;
	
    private Transform 	BulletObjectCopy; //Holds a copy of the created bullet object so we can give it some velocity later
    private float 		ShotCoolDownCount; //Used to hold the original cooldown value set by us
	private float 		AttackIntervalCount; //Used to hold the original Attack Interval set by us
	private float 		ShotsPerAttackCount; //Used to hold the original Shots Per Attack set by us
	private Vector3 	posOnScreen;
	private BossPart	bossPart;

	const float rotationSpeed = 40f;


    //Constructor
    public MausBackTurel()
    {
        ShotsPerAttackCount = ShotsPerAttack;
        AttackIntervalCount = AttackInterval;
        ShotCoolDownCount = ShotCoolDown;
    }
	
	private bool shootEnable = true;
    public Vector3 turelRotateVector = Vector3.zero;
    public bool freezeX = false;
    public bool freezeY = false;
    public bool freezeZ = false;

    void Start()
    {
		bossPart = GetComponent<BossPart>();
		shootEnable = PlayerScript.instance.shootEnable;
		AttackIntervalCount = AttackInterval;
		ShotsPerAttackCount = 0;
		ShotCoolDownCount = 0;
    }


    void Update()
    {       
		if( !bossPart.avariable ) return;
		posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
		if(shootEnable && posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) {

            Vector3 dir = PlayerScript.getPosition - transform.position;
            Quaternion rotation = Quaternion.LookRotation( -dir );
			Vector3 fixedAngles = rotation.eulerAngles;
			if(freezeY)fixedAngles.y = 0;
			if(freezeX)fixedAngles.x = -65f;
			if(freezeZ)fixedAngles.z = -90;
			rotation.eulerAngles = fixedAngles;
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation , rotationSpeed * Time.deltaTime);

			
			//transform.localEulerAngles = new Vector3(LookTarget.localEulerAngles.x + (((rightGun) ? 1 : -1) * 25), transform.localEulerAngles.y, transform.localEulerAngles.z);
		
			//The rate of fire is set by the value of ShotCooldown, and is set in frame updates * Quaternion.Euler(turelRotateVector)
            if( AttackIntervalCount > 0 )
			{
				AttackIntervalCount -= Time.deltaTime;
			}
			else
			{
				if (ShotCoolDownCount > 0) 
				{
	                //Decrease the cooldown counter
	                ShotCoolDownCount -= Time.deltaTime;
	            } else 
				{
	                Shoot( FirePos );
					ShotCoolDownCount = ShotCoolDown;
					ShotsPerAttackCount++;
					if( ShotsPerAttackCount == ShotsPerAttack )
					{
						AttackIntervalCount = AttackInterval;
						ShotsPerAttackCount = 0;
						ShotCoolDownCount = 0;
					}

	            }	
			}
		}
    }
	
    private void Shoot(Transform FirePos)
    {
        if (BulletObject)
        {
			BulletBase bullet = BulletsManager.GetFreeBullet( BulletObject.name );
			if( bullet != null )
			{
				Vector3 dir = ( PlayerScript.getPosition - transform.position ).normalized * BulletSpeed;
				bullet.Launch( FirePos.position, Quaternion.identity, dir );
			}
        }
    }

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
