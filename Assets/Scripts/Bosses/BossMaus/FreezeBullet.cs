﻿using UnityEngine;
using System.Collections;

public class FreezeBullet : MonoBehaviour {

	public GameObject explosion;
	public int explosionRadius = 5;
	
//	private GameObject Player;
	
	void Start()
    {
 //     	Player = GameObject.Find("Player");
		Invoke("Explosion", 1);
	}
	
	//private bool explosionStart = false;
	
	void Update()
	{
		checkOnScreen();
	}
	
	void checkOnScreen()
	{
		Vector3 posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
		if(posOnScreen.y > 1) Destroy(gameObject);	
	}
	
	void  OnCollisionEnter ( Collision collision  )
	{

		if( collision.gameObject.tag != "Player") return;
		
		ContactPoint contact = collision.contacts[0];	
		Quaternion rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);
		
	    Instantiate (explosion, contact.point, rotation);
		
        
		Destroy(gameObject);
	}

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag != "Shield") return;
        
        Destroy(gameObject);
    }

	public void Explosion()	
	{
		
	    Instantiate(explosion, transform.position, Quaternion.identity);
		
		Vector3 explosionPosition = transform.position;
		
		Collider[] colliders = Physics.OverlapSphere(explosionPosition, explosionRadius);
		
		foreach(var hit in colliders) 
		{
			if (!hit || hit.tag != "Player") continue;
			hit.gameObject.SendMessage("freezeMoveStart", 0, SendMessageOptions.DontRequireReceiver);	
		}
		// destroy the explosion
		Destroy (gameObject, 1);
		//Debug.DrawLine(explosionPosition, explosionPosition*2);
	}
}
