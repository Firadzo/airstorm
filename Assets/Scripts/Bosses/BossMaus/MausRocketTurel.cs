﻿using UnityEngine;
using System.Collections;

public class MausRocketTurel : MonoBehaviour {
    public Transform[] FirePos;
    public Transform ExplosionEffect; //The effect created when the plane explodes
    public Transform BulletObject; //The bullet shot from the plane
  
    public int BulletSpeed = 50; //bullet's speed when shot
    public float ShotCoolDown = 10; //How long to wait between each new bullet shot
	private float ShotCoolDownCount;

    private Transform BulletObjectCopy; //Holds a copy of the created bullet object so we can give it some velocity later
     //Used to hold the original cooldown value set by us
	private BossPart	bossPart;

    public MausRocketTurel()
    {
        ShotCoolDownCount = ShotCoolDown;
    }
	
	private bool shootEnable = true;
	
    void Start()
    {
		bossPart = GetComponent<BossPart>();
		shootEnable = PlayerScript.instance.shootEnable;
        ShotCoolDownCount = 0;
    }
	
	private bool turel_enable = false;

	Vector3 posOnScreen;
    void Update()
    {      
		//shootEnable = Player.GetComponentInChildren<PlayerScript>().shootEnable;
		
		if( !bossPart.avariable ) return;
		
		if(!turel_enable)
		{
			GetComponent<Animation>().Play();
			return;
		}
		
		if(shootEnable) {
			posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
			
			if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) 
            {
	            //The rate of fire is set by the value of ShotCooldown, and is set in frame updates
	            if (ShotCoolDownCount > 0) {
	                //Decrease the cooldown counter
	                ShotCoolDownCount -= Time.deltaTime;
	            } else {
                    for(int i=0; i<FirePos.Length; i++)
					{
						Shoot(FirePos[i]);
					}
	            }
			}
		}
    }
	
    private void Shoot(Transform FirePos)
    {
        if (BulletObject)
        {
            BulletObjectCopy = Instantiate( BulletObject, FirePos.position, Quaternion.identity ) as Transform;
			BulletObjectCopy.transform.Translate( FirePos.transform.forward * 2 );
			BulletObjectCopy.transform.localEulerAngles = new Vector3( 0, -90f, 0 );
            //Reset the cooldown timer for the bullet
            ShotCoolDownCount = ShotCoolDown;
        }
    }
	
	public void enableRocketTurel()
	{
		turel_enable = true;
	}

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}

