﻿using UnityEngine;
using System.Collections;

public class MausMainGun : MonoBehaviour {
    public Transform LookTarget;
	public Transform Turret;
	public Transform tiltableGun;
    public Transform FirePos;

    public Transform FireObj; //The bullet shot from the plane
    public int ShotCoolDown = 20; //How long to wait between each new bullet shot

    private Transform BulletObjectCopy; //Holds a copy of the created bullet object so we can give it some velocity later
    private int ShotCoolDownCount; //Used to hold the original cooldown value set by us


    private GameObject Player;
    private EndLevelPopup popup;

    //Constructor
    public MausMainGun()
    {
        ShotCoolDownCount = ShotCoolDown;
    }
	
	public bool shootEnable = true;
	public bool avariable = false;
	
    void Start()
    {
        GameObject EndLevel = GameObject.Find("EndLevel");
        popup = EndLevel.GetComponent<EndLevelPopup>();
        popup.allEnemyCnt++;

        Player = GameObject.Find("Player"); //Set player object
		shootEnable = Player.GetComponentInChildren<PlayerScript>().shootEnable;
        ShotCoolDownCount = 0;
		
		ParticleEmitter emitter= GetComponentInChildren<ParticleEmitter>();
		if (emitter) emitter.emit = false;
    }

    public Vector3 gunRotateVector = Vector3.zero;
    public bool freezeY = false;

    void Update()
    {
     	if(!avariable)return;
		
		//shootEnable = Player.GetComponentInChildren<PlayerScript>().shootEnable;
		
		if(shootEnable)
		{
			if(Turret)
			{
				//rotate turret
				Turret.transform.localEulerAngles = new Vector3(
					Turret.localEulerAngles.x, 
					LookTarget.localEulerAngles.y - 90,
					Turret.localEulerAngles.z);
               
			}
			
			if(tiltableGun)
			{
                /*Vector3 dir = Player.transform.position - transform.position;
			    dir.x = 0;
			    dir.z = 0;
                Quaternion rotation = Quaternion.LookRotation(dir);
                tiltableGun.transform.rotation = Quaternion.Slerp(transform.rotation, rotation * Quaternion.Euler(gunRotateVector), 40 * Time.deltaTime);*/

				//tilt gun
				tiltableGun.localEulerAngles = new Vector3(
                    tiltableGun.localEulerAngles.x,
                    LookTarget.localEulerAngles.y - 25f,
                    tiltableGun.localEulerAngles.z);
			
			}
		
			Vector3 posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
			
			if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) 
            {
				ParticleEmitter emitter= GetComponentInChildren<ParticleEmitter>();
				if (emitter) emitter.emit = true;
			}
		}
    }
	
	public void EnableGun()
	{
		avariable = true;
	}

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
