﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {
	public float Damage = 10;
    public float FireSpeed = 10;
    public CapsuleCollider coll;
    public float maxX;
    public float minX;
    public bool dirBack = false;

    void Awake() {
        coll = GetComponent<CapsuleCollider>();
    }

	void Update() {
        if (dirBack || !coll) return;

        if (coll.center.x < maxX && !dirBack)
        {
            coll.center = new Vector3(coll.center.x + Time.deltaTime * FireSpeed, coll.center.y, coll.center.z);
	    } 
        else 
        {
            dirBack = true;
            StartCoroutine(cycleFire());
        }
	}

    IEnumerator cycleFire() {
        yield return new WaitForSeconds(0.1f);
        coll.center = new Vector3(minX, coll.center.y, coll.center.z);
        dirBack = false;
    }

    public void OnTriggerEnter(Collider collision)
    {
        Debug.Log(collision.name);
        if (collision.tag == "Player")
        {
				PlayerScript.instance.HitPlayer( Damage );
        }
    }

    void OnParticleCollision(GameObject other)
    {
        Debug.Log(other.name);
    }
}
