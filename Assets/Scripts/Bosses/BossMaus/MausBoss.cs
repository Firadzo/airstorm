﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MausBoss : MonoBehaviour {	
	//public Vector3 RotationFIX = Vector3.zero;
    //public float ANGLE_FIX = 90f;
	
	public List<BossParts> bossParts = new List<BossParts>();
    public Spline 		inTraectory;
    public GameObject 	ExplosionEffect;
    private float 		speedMove = 0f;
	public float 		speed = 0f;
	
	public string 		moveAnimName;
	
    private Spline 		spline;
    private float 		passedTime;
    public WrapMode 	wrapMode = WrapMode.Clamp;

	public MausMainGun mainGun;
	
	[System.Serializable]
	public class BossParts
	{
		public GameObject rootPart;
		public int HeathPart;
	}

    public Transform 		monetObjSmall;
    public int 				monetCountSmall = 10;
    public Transform 		monetObjBig;
    public int 				monetCountBig = 4;

    private bool 				moveEnable = true;
	private PlayerMoveController _moveController;
	private float 				speedIn = 0.1f;
	private bool 				nowInTraectory = false;
	private int 				currentDestroyPhase = 0;

	void Start () 
    {
	    spline = inTraectory;
        nowInTraectory = true;

         moveEnable = PlayerScript.instance.moveEnable;
        _moveController = FindObjectOfType<PlayerMoveController>();
        if (_moveController) speedMove = _moveController.Speed;
		
		speedIn = speedMove * 0.022f;
        currentDestroyPhase = 0;

        BossPart[] parts = bossParts[currentDestroyPhase].rootPart.GetComponentsInChildren<BossPart>();

        for ( int i = 0; i < parts.Length; i++ )	
        {
			ActivateBossPart( parts[ i ] );
        }
	}

	private void ActivateBossPart( BossPart bossPart )
	{
		bossPart.Activate();
		
		float heath = bossParts[currentDestroyPhase].HeathPart;
		
		if ( LevelConroller.instance.currentDifficultyLevel == 0 )
		{
			bossPart.Heath = heath + (heath * LevelConroller.instance.easyLevel);
		}
		else if ( LevelConroller.instance.currentDifficultyLevel == 2 )
		{
			bossPart.Heath = heath + (heath * LevelConroller.instance.hardLevel);
		}
		else
		{
			bossPart.Heath = heath;
		}
	}

	void Update () 
    {
		//Передавать урон от какой-либо части вверх до этого компонента, здесь решается какая фаза и если здоровье части > 0 - она мигает или разваливается, идет проверка
		//переключателя фаз
		//Сделать переключатель фаз разрушения
		if(!spline)return;

        if (moveEnable && nowInTraectory) {
                passedTime += Time.deltaTime * speedIn;

                float posOnSplne = WrapValue(passedTime, 0f, 1f, wrapMode);

                if (nowInTraectory && posOnSplne > 0.55f) {
                    nowInTraectory = false;
                    return;
	        }
			
            transform.position =  spline.GetPositionOnSpline(posOnSplne);;
	
	        Quaternion rot = spline.GetOrientationOnSpline(WrapValue(passedTime, 0f, 1f, wrapMode));
	       // Quaternion addHorizontalRot = Quaternion.AngleAxis(ANGLE_FIX, RotationFIX);

            transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 3f);
		   // if (transform.FindChild("PlayerPosition")) {
           // PlayerScript.instance.transform.position = transform.FindChild("PlayerPosition").position;}
	    } 
        else if(moveEnable)
	    {
			if (moveAnimName.Length > 0) {
                    transform.GetChild(0).GetComponent<Animation>().Play(moveAnimName);
                }
               // Debug.Log("BossPlane speedMove =" + speedMove);
                transform.Translate( Vector3.right * speedMove * Time.deltaTime );
        }

	    CheckParts();

	}

    private void CheckParts() 
	{
        if (bossParts.Count > 0) 
        {
			if( !bossParts[currentDestroyPhase].rootPart ) currentDestroyPhase += 1;
			
            BossPart[] parts = bossParts[currentDestroyPhase].rootPart.GetComponentsInChildren<BossPart>();
				
            if (parts.Length <= 0) 
            {
                currentDestroyPhase += 1;
				
				if( currentDestroyPhase == 2 ) enableMainGun();
				
                if (currentDestroyPhase >= bossParts.Count) {

                    Transform[] root = GetComponentsInChildren<Transform>();

                    for (int d = 0; d < root.Length; d++)
                    {
                        GameObject obj = root[d].gameObject;
                        obj.transform.parent = null;
                        /*obj.AddComponent<Rigidbody>();
                        obj.AddComponent<BoxCollider>();
                        obj.rigidbody.mass = 150f;
                        obj.rigidbody.useGravity = true;*/
                        //obj.rigidbody.AddForce(new Vector3(Random.value, Random.value, Random.value) * 2000f, ForceMode.Impulse);
                        Destroy(obj, 8);
						
                    }	
					
					if (ExplosionEffect) {
                    	GameObject eff = Instantiate(ExplosionEffect, transform.position, transform.rotation) as GameObject;
						Destroy(eff,3);
            		}
                   
					PlayerScript.instance.BossDefeted();

                    spline = null;

                    addCoin();

                    return;
                }

                BossPart[] partsNext = bossParts[currentDestroyPhase].rootPart.GetComponentsInChildren<BossPart>();

                for (int i = 0; i < partsNext.Length; i++)
                {
					ActivateBossPart( partsNext[ i ] );
                }
            }
        } else {
            Debug.Log("Boss not have parts");
        }
    }
	
    private float WrapValue(float v, float start, float end, WrapMode wMode)
    {
        switch (wMode)
        {
            case WrapMode.Clamp:
            case WrapMode.ClampForever:
                return Mathf.Clamp(v, start, end);
            case WrapMode.Default:
            case WrapMode.Loop:
                return Mathf.Repeat(v, end - start) + start;
            case WrapMode.PingPong:
                return Mathf.PingPong(v, end - start) + start;
            default:
                return v;
        }
    }

    public void MoveEnableNow()
    {
        moveEnable = true;
    }

    public void MoveDisableNow()
    {
        moveEnable = false;
    }
	
	public void enableMainGun()
	{
		if(mainGun)
		{
			mainGun.EnableGun();
		}
	}

    private void addCoin()
    {
        if (monetObjSmall)
        {
            for (int i = 0; i < monetCountSmall; i++)
            {
                Vector3 pos = new Vector3(Random.Range(-100, 100), transform.localPosition.y, Random.Range(-40, 40));
                Instantiate(monetObjSmall, transform.TransformPoint(pos), Quaternion.identity);
            }
        }
        if (monetObjBig)
        {
            for (int i = 0; i < monetCountBig; i++)
            {
                Vector3 pos = new Vector3(Random.Range(-100, 100), transform.localPosition.y, Random.Range(-40, 40));
                Instantiate(monetObjBig, transform.TransformPoint(pos), Quaternion.identity);
            }
        }
    }
}
