﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossPlane : MonoBehaviour {
    private bool 				freezeMove = false;

    public bool 				itIsFirstPlane = false;
    public int 					cameraHorHeight = 5;
	public	Animation			moveAnimation;
    public List<BossParts> 		bossParts = new List<BossParts>();
	public List<ParticleEmitter> emitters = new List<ParticleEmitter>();
    public bool 				nowInTraectory = false;
    public GameObject 			ExplosionEffect;

    public Transform[] 			RocketSpawnPos;
	public	int 				phaseForRocketStart;
    public 	int 				AIRocketsRespawn = 3;
    public GameObject 			AIRocket;

    public WrapMode 			wrapMode = WrapMode.Clamp;

    public int 					currentDestroyPhase = 0;
    public string 				moveAnimName;

    public EndLevelPopup 		endLevel;

    //public BossPlane BossPlanePrefab;
    private Spline spline;
    private float passedTime;
    private float speedMove = 0f;
    private float speedIn = 0.1f;
    private bool moveEnable = true;

    private bool enableGun = false;

    public int respawnPlanesTime = 5;

    //public int TimeOutToCreateBoss2 = 5;

    public Transform planePrefab;
    public Spline Spline1;
    public Spline Spline2;

    [System.Serializable]
    public class BossParts {
        public GameObject rootPart;
        public int HeathPart;
    }

    public Transform monetObjSmall;
    public int monetCountSmall = 10;
    public Transform monetObjBig;
    public int monetCountBig = 4;

    //private Turel currTurel;
    private SmoothLookAt camLook;
    private PlayerCamera cam;
    private float defaultSpeed;

    public bool 			isFiveLevel = false;
    public bool 			enableTurelsFiveLevel = false;
    public bool 			UseDissolveEffect = false;
	public Material 		dissolveMaterial;
	public MeshRenderer[]	dissolveObjects;
    public float 			dissolveTime = 3f;
	private float			dissolveTimer;
    private bool 			dissolveEnabled = false;

    private GameObject levConroller;
    private LevelConroller comp;

    private PlayerMoveController _moveController;

	private BossTrigger 		bossTrigger;
	private bool 				destroyed;

	public void InitBoss( BossTrigger boss_Trigger, Spline trajectory )
	{
		spline 		= trajectory;
		bossTrigger = boss_Trigger; 
	}


	public static BossPlane	instance;

	void Awake()
	{
		instance = this;
	}

    private void Start() {
		dissolveTimer = dissolveTime;
		if( dissolveMaterial != null )
		{
			dissolveMaterial.SetFloat("_Range", dissolveTime);
		}
        nowInTraectory = true;
        moveEnable = PlayerScript.instance.moveEnable;
        _moveController = FindObjectOfType<PlayerMoveController>();
        if (_moveController) {
            speedMove = _moveController.Speed;
        }

        speedIn = speedMove * 0.018f;
        currentDestroyPhase = 0;

        levConroller = GameObject.Find("LevelConroller");
        comp = levConroller.GetComponent<LevelConroller>();

        if (!isFiveLevel) {
            BossPart[] parts = bossParts[currentDestroyPhase].rootPart.GetComponentsInChildren<BossPart>();
            for (int i = 0; i < parts.Length; i++) {
                parts[i].Activate();
                float health = bossParts[currentDestroyPhase].HeathPart;

                if (comp.currentDifficultyLevel == 0) {
                    parts[i].Heath = health + (health * comp.easyLevel);
                } else if (comp.currentDifficultyLevel == 2) {
                    parts[i].Heath = health + (health * comp.hardLevel);
                } else {
                    parts[i].Heath = health;
                }
            }
        }

        endLevel = GameObject.Find("EndLevel").GetComponent<EndLevelPopup>();
        enableGun = false;

        SwitchWeaponEnable(false, true);

        //currTurel = Player.GetComponentInChildren<Turel>();
        camLook = Camera.main.GetComponent<SmoothLookAt>();
        cam = Camera.main.GetComponent<PlayerCamera>();

        /* if (itIsFirstPlane) {
            Debug.Log("Boss with horizantal view");
 
            camLook.enabled = true;
            camLook.target = transform;
            cam.setHeight(cameraHorHeight);

            Player.GetComponent<PlayerConrrol>().inverseControl = true;

            currTurel.setBossBullet(playerBullet);
        
        } else {*/
        Debug.Log("Set default player bullet, camera");

        //camLook.enabled = false;
        // cam.setToDefault();

        // Player.GetComponent<PlayerConrrol>().inverseControl = false;

        //currTurel.setDefault();

        //fix correct height
        //Player.transform.position = GameObject.Find("PlayerMove").transform.position;
        //  }
    }

	public void AddEmitter( ParticleEmitter emitter )
	{
		emitters.Add( emitter );
	}

	public bool destroy;
    private void Update() {

        if (UseDissolveEffect && dissolveEnabled) {
            if ( dissolveTimer >= 0f ) {
				dissolveTimer -= Time.deltaTime;
                dissolveMaterial.SetFloat("_Range", dissolveTimer / dissolveTime );
                //Debug.Log(maxDissolve);
            } else {
				for( int i = 0; i < emitters.Count; i++ )
				{
					if( emitters[ i ] != null )
					{
						emitters[ i ].transform.parent = null;
					}
				}
                Destroy(gameObject);
            }
            return;
        }

		if( destroyed ){ return; }

        if (!isFiveLevel) {
            if (!spline) {
                return;
            }

            if (moveEnable && nowInTraectory) 
			{
                passedTime += Time.deltaTime * speedIn;

                float posOnSplne = WrapValue(passedTime, 0f, 1f, wrapMode);

                if (nowInTraectory && posOnSplne > 0.98f) {
                    nowInTraectory = false;
                    return;
                }

                Vector3 pos = spline.GetPositionOnSpline(posOnSplne);

                //base rotation
                Quaternion rot = spline.GetOrientationOnSpline(WrapValue(passedTime, 0f, 1f, wrapMode));
                Vector3 rotationDir = pos - transform.position;

                float angle = Vector3.Angle( pos - transform.position, transform.forward );
                //left rotation fix
                if (rotationDir.normalized.z < 0) {
                    angle = angle * -1 * 6f;
                } else {
                    angle = angle * 6f;
                }
                Quaternion addHorizontalRot = Quaternion.AngleAxis((angle), Vector3.forward);
                transform.rotation = Quaternion.Slerp(transform.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f);
                transform.position = pos;
            } 
			else if (moveEnable) 
			{
                if (moveAnimName.Length > 0) {
					if( moveAnimation != null ) moveAnimation.Play(moveAnimName);
                }
               // Debug.Log("BossPlane speedMove =" + speedMove);
                transform.Translate(Vector3.forward * speedMove * Time.deltaTime);


                if (!enableGun) {
                    enableGun = true;
                    SwitchWeaponEnable( true, false );
                   // StartCoroutine( spawnRockets() );
                    //SET correct player Position
                    if (transform.FindChild("PlayerPosition")) {
                        PlayerScript.instance.transform.position = transform.FindChild("PlayerPosition").position;
                    }
                }
            }
        }

        if ((enableTurelsFiveLevel && isFiveLevel) || (!enableTurelsFiveLevel && !isFiveLevel)) {
            CheckParts();
        }
    }

    private void CheckParts() {
        if ( bossParts.Count > 0 ) 
		{
            BossPart[] parts = bossParts[ currentDestroyPhase ].rootPart.GetComponentsInChildren<BossPart>();

            if ( parts.Length <= 0 || destroy ) 
			{
                currentDestroyPhase += 1;

                if (currentDestroyPhase >= bossParts.Count || destroy ) 
				{//победили босса
                    
					destroyed = true;
					for( int i = 0; i < emitters.Count; i++ )
					{
						emitters[ i ].emit = false;
					}

					Transform[] root = GetComponentsInChildren<Transform>();
                    for (int d = 0; d < root.Length; d++) {
                        if (!UseDissolveEffect) {
                            GameObject obj = root[d].gameObject;
                            obj.transform.parent = null;
                            obj.AddComponent<Rigidbody>();
                            obj.AddComponent<BoxCollider>();
                            obj.GetComponent<Rigidbody>().useGravity = true;
                            //obj.rigidbody.AddForce(new Vector3(Random.value, Random.value, Random.value) * 2000f, ForceMode.Impulse);
                            obj.GetComponent<Rigidbody>().mass = 150f;
                            Destroy(obj, 6);
                        } 
						else {
	                            spline = null;
	                            dissolveEnabled = true;
	                            for (int i = 0; i< dissolveObjects.Length; i++)
								{
									dissolveObjects[ i ].sharedMaterial = dissolveMaterial;
	                            }
                            	//return;
                        }
                    }

                    //StopCoroutine("spawnEnemy");
                    StopCoroutine("spawnRockets");

                    if (ExplosionEffect) {
                        GameObject eff = Instantiate(ExplosionEffect, transform.position, transform.rotation) as GameObject;
                        Destroy(eff, 3);
                    }
					if( itIsFirstPlane )
					{
						bossTrigger.SpawnSecondBoss();
					}
					else
					{
						PlayerScript.instance.BossDefeted();
						LevelConroller.spawnDisabled = false;
					}

                    	if (freezeMove) {
                        	PlayerMoveController.instance.Speed = defaultSpeed;
                    	}


                    spline = null; //disable update, but no remove obj, but we start playerwin coroutine or create new collider

                    if (camLook) {
                        camLook.enabled = false;
                    }
                    if (cam) {
                        cam.setToDefault();
                    }

                    /*if (currTurel) {
                        currTurel.setDefault();
                    }*/

                    //fix correct height
                    //PlayerScript.instance.transform.position = GameObject.Find("PlayerMove").transform.position;

                    addCoin();


                    return;
                }

				if( currentDestroyPhase == phaseForRocketStart )
				{
					StartCoroutine("spawnRockets");
				}

                SwitchWeaponEnable( true, false );

                BossPart[] partsNext = bossParts[currentDestroyPhase].rootPart.GetComponentsInChildren<BossPart>();
				//активация следующей части босса
                for (int i = 0; i < partsNext.Length; i++) {
                    partsNext[i].Activate();

                    float heath = bossParts[currentDestroyPhase].HeathPart;

                    if (comp.currentDifficultyLevel == 0) {
                        partsNext[i].Heath = heath + (heath * comp.easyLevel);
                    } else if (comp.currentDifficultyLevel == 2) {
                        partsNext[i].Heath = heath + (heath * comp.hardLevel);
                    } else {
                        partsNext[i].Heath = heath;
                    }
                    if (partsNext[i].GetComponent<BossTurel>()) {
                        partsNext[i].GetComponent<BossTurel>().shootEnable = true;
                    }
                }
            }
        } else {
            Debug.Log("Boss not have parts");
        }
    }



    private float WrapValue(float v, float start, float end, WrapMode wMode) {
        switch (wMode) {
            case WrapMode.Clamp:
            case WrapMode.ClampForever:
                return Mathf.Clamp(v, start, end);
            case WrapMode.Default:
            case WrapMode.Loop:
                return Mathf.Repeat(v, end - start) + start;
            case WrapMode.PingPong:
                return Mathf.PingPong(v, end - start) + start;
            default:
                return v;
        }
    }

    public void MoveEnableNow() {
        moveEnable = true;
    }

    public void MoveDisableNow() {
        moveEnable = false;
    }

    public void enableTurels() {
        enableTurelsFiveLevel = true;
        BossPart[] parts = bossParts[currentDestroyPhase].rootPart.GetComponentsInChildren<BossPart>();
        for (int i = 0; i < parts.Length; i++) {
            parts[i].Activate();

            float heath = bossParts[currentDestroyPhase].HeathPart;

            if (comp.currentDifficultyLevel == 0) {
                parts[i].Heath = heath + (heath * comp.easyLevel);
            } else if (comp.currentDifficultyLevel == 2) {
                parts[i].Heath = heath + (heath * comp.hardLevel);
            } else {
                parts[i].Heath = heath;
            }
            if (parts[i].GetComponent<BossTurel>()) {
                parts[i].GetComponent<BossTurel>().shootEnable = true;
            }
        }
    }

    private void SwitchWeaponEnable(bool enable, bool allTurels) {
        if (allTurels) {
            for (int i = 0; i < bossParts.Count; i++) {
                BossTurel[] turels = bossParts[i].rootPart.GetComponentsInChildren<BossTurel>();

                for (int j = 0; j < turels.Length; j++) {
                    //turels[j].shootEnable = enable;
                    sendMessageToTurel(enable, turels[j].gameObject);
                }
            }
        } else {
            BossTurel[] turels = bossParts[currentDestroyPhase].rootPart.GetComponentsInChildren<BossTurel>();

            for (int j = 0; j < turels.Length; j++) {
                //turels[j].shootEnable = enable;
                sendMessageToTurel(enable, turels[j].gameObject);
            }
        }

       /* if (PlayerPrefs.GetInt("tmpSpawnDisable", -1) != 1) {
            if (enable && currentDestroyPhase == 0) {
                StartCoroutine(spawnEnemy());
            }
        }*/
    }

    private void sendMessageToTurel(bool enable, GameObject obj) {
        if (enable) {
            obj.SendMessage("ShootEnable", SendMessageOptions.RequireReceiver);
        } else {
            obj.SendMessage("ShootDisable", SendMessageOptions.RequireReceiver);
        }
    }


    private IEnumerator spawnEnemy() {
        yield return new WaitForSeconds(respawnPlanesTime);
        if (!moveEnable) {
            yield return new WaitForSeconds(respawnPlanesTime);
        } else {
           // GameObject enemy2 = Instantiate(planePrefab, Vector3.zero, Quaternion.FromToRotation(Vector3.zero, new Vector3(270, 0, 0))) as GameObject;
            SplineAnimator splineEnemy = planePrefab.GetComponent<SplineAnimator>();
            splineEnemy.spline = Spline1;

           // GameObject enemy3 = Instantiate(planePrefab, Vector3.zero, Quaternion.FromToRotation(Vector3.zero, new Vector3(270, 0, 0))) as GameObject;
            SplineAnimator splineEnemy2 = planePrefab.GetComponent<SplineAnimator>();
            splineEnemy2.spline = Spline2;

            StartCoroutine(spawnEnemy());
        }
    }

    private IEnumerator spawnRockets() {
        yield return new WaitForSeconds(AIRocketsRespawn);
        if (!moveEnable) {
            yield return new WaitForSeconds(AIRocketsRespawn);
        } else {
            for (int i = 0; i < RocketSpawnPos.Length; i++) {
                GameObject rocket = Instantiate(AIRocket, RocketSpawnPos[i].position, Quaternion.identity) as GameObject;
				rocket.transform.eulerAngles = new Vector3(0, 90, 0);
				AIRocket r = rocket.GetComponent<AIRocket>();
                r.AILaunchPause = true;
            }

            StartCoroutine(spawnRockets());
        }
    }

    private void addCoin() {
        if (monetObjSmall) {
            for (int i = 0; i < monetCountSmall; i++) {
                Vector3 pos = new Vector3(Random.Range(-100, 100), transform.localPosition.y, Random.Range(-40, 40));
                Instantiate(monetObjSmall, transform.TransformPoint(pos), Quaternion.identity);
            }
        }
        if (monetObjBig) {
            for (int i = 0; i < monetCountBig; i++) {
                Vector3 pos = new Vector3(Random.Range(-100, 100), transform.localPosition.y, Random.Range(-40, 40));
                Instantiate(monetObjBig, transform.TransformPoint(pos), Quaternion.identity);
            }
        }
    }

    public void PlayerFreeze() {
        defaultSpeed = PlayerMoveController.instance.Speed;
		PlayerMoveController.instance.Speed = 0;
        freezeMove = true;
    }
}
