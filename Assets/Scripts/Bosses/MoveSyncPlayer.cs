﻿using UnityEngine;
using System.Collections;

public class MoveSyncPlayer : MonoBehaviour
{
    public Vector3 moveDir = Vector3.back;
    public bool itIsWater = false;

    private float speedMove;
    public bool moveEnable = true;

    private PlayerMoveController _moveController;

	// Use this for initialization
	void Start ()
	{
	    _moveController = FindObjectOfType<PlayerMoveController>();
        if (_moveController) speedMove = _moveController.Speed;
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (moveEnable) {
            if (itIsWater) {
                transform.Translate(moveDir / 100f );
                return;
            }
            transform.Translate( moveDir * speedMove * Time.deltaTime );
        }
	}
}
