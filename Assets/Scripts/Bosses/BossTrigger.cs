﻿using UnityEngine;
using System.Collections;

public class BossTrigger : MonoBehaviour {

    public GameObject 	CameraBoss;
    public int 			cameraHeight = -57;
    public GameObject 	Boss;
	public GameObject 	Boss2;
    public GameObject 	BossIn;

    public bool 		used = false;
    public bool 		disableSpawn = false;
    public bool 		disableFire = false;
    public bool 		isFiveLevelTrigger = false;
    public bool 		freezeMove = false;

    public Vector3 		SpawnRotateFix;
    public float 		SpawnHeightFix = 10f;

    private Vector3 	spawnDirPoint;
    private EndLevelPopup EndLevel;
	private Transform	cameraTransf;
	private Transform	transf;
  
    void Start() {
		cameraTransf 	= Camera.main.transform;
		transf 			= transform;
        if (GameObject.Find("EndLevel")) {
            EndLevel = GameObject.Find("EndLevel").GetComponent<EndLevelPopup>();
        }

        if (isFiveLevelTrigger) {
            Boss.GetComponent<BossPlane>().isFiveLevel = true;
        }
    }

	void Update()
	{
		if( transf.position.x <= cameraTransf.position.x )
		{
			StartBoss();
		}
	}

    //private void OnCollisionEnter(Collision coll)
    //{
    //    Debug.Log("BossTrigger OnCollisionEnter--------" + coll.gameObject);

    //    if (coll.gameObject == FindObjectOfType<PlayerScript>().gameObject)
    //    {
    //        StartBoss();
    //    }
    //}

	public void SpawnSecondBoss()
	{
		SpawnBoss( Boss2 );
	}

	private void SpawnBoss( GameObject _boss )
	{
		spawnDirPoint = PlayerMoveController.instance.transform.position;
		spawnDirPoint.z = PlayerMoveController.instance.GetScreenCenter();
		
		GameObject inTract =
			Instantiate(BossIn, new Vector3(spawnDirPoint.x, spawnDirPoint.y - SpawnHeightFix, spawnDirPoint.z),
			            Quaternion.Euler(SpawnRotateFix)) as GameObject;
		Spline inTractorySpline = inTract.GetComponent<Spline>();
		
		GameObject bossModel =
			Instantiate( _boss, new Vector3(spawnDirPoint.x, spawnDirPoint.y - SpawnHeightFix, spawnDirPoint.z),
			            Quaternion.Euler(SpawnRotateFix)) as GameObject;
		//	Debug.LogError("");
		if (bossModel.GetComponent<BossPlane>())
		{
			BossPlane boss = bossModel.GetComponent<BossPlane>();
			boss.InitBoss( this, inTractorySpline );
		}
		else if (bossModel.GetComponent<MausBoss>())
		{
			MausBoss bossTank = bossModel.GetComponent<MausBoss>();
			bossTank.inTraectory = inTractorySpline;
		}
		else if (bossModel.GetComponent<BossZepelin>())
		{
			BossZepelin bossZepelin = bossModel.GetComponent<BossZepelin>();
			bossZepelin.inTraectory = inTractorySpline;
			bossZepelin.endLevel = EndLevel;
			//inTract.transform.parent = bossModel.transform;
		}
	}

    private void StartBoss()
    {
        if (!used)
        {
            used = true;
        }
        else
        {
            return;
        }

        //if (disableSpawn)
        {
			Debug.Log("spawnDis");
			LevelConroller.spawnDisabled = true;
        }

        if (disableFire)
        {
            Boss.GetComponent<BossPlane>().enableTurels();
            Debug.Log("Enable fire trigger");
        }

		GameObject snd = GameObject.Find("AudioManager");
		snd.SendMessage("playBossMusic", SendMessageOptions.DontRequireReceiver);

		if (freezeMove)
        {
            if (Boss.GetComponent<BossPlane>())
            {
                Boss.GetComponent<BossPlane>().PlayerFreeze();
            }
            if (isFiveLevelTrigger) Destroy(gameObject);
            return;
        }
		SpawnBoss( Boss );

        if (CameraBoss)
        {
            PlayerCamera cam = CameraBoss.GetComponent<PlayerCamera>();
            cam.setHeight(cameraHeight);
        }

		gameObject.SetActive( false );
    }
}