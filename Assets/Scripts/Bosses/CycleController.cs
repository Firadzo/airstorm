﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class CycleController : MonoBehaviour
{
    public GameObject Tile;
    public GameObject latestTerrainPart;
    public int firstTileOffset = 1200;//first tile
    public int allNextTilesOffset = 1200;//all other tiles
    public float startXPos = 0;
    public Vector3 tilePos;
    public bool used = false;

    public float currDelta = 0;

    private GameObject PlayerMove;

    public List<GameObject> spawnTiles;
    public bool cleanTerrain = false;

    public bool revertMove = false;
    private GameObject latestTile;

	// Use this for initialization
	void Start ()
	{
        PlayerMove = GameObject.Find("PlayerMove");
        if (!latestTerrainPart){ Debug.Log("No latest terraint part!");}
	}
	
    public void startCycle()
    {
        if (!latestTerrainPart) return;
        
        Vector3 pos;
        if (spawnTiles.Count > 0)
        {
            pos = new Vector3(allNextTilesOffset + latestTerrainPart.transform.position.x, tilePos.y, tilePos.z);
        }
        else
        {
            pos = new Vector3(firstTileOffset + latestTerrainPart.transform.position.x, tilePos.y, tilePos.z);
        }
        GameObject tile = Instantiate(Tile, pos, Quaternion.identity) as GameObject;
        spawnTiles.Add(tile);
        latestTerrainPart = tile;
        startXPos = PlayerMove.transform.position.x;// latestTerrainPart.transform.position.x;//

        if (spawnTiles.Count > 3 && cleanTerrain)
        {
            Destroy(spawnTiles[0]);
            spawnTiles.RemoveAt(0);
        }
    }

    
    void FixedUpdate()
    {
		this.transform.position = new Vector3(this.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);

        if (used)
        {
            currDelta = PlayerMove.transform.position.x - firstTileOffset;//latestTerrainPart.transform.position.x
            /*if (revertMove) 
            {
                 if (currDelta <= startXPos / 2) {
                     startCycle();
                     return;
                 }
            }*/

            if (currDelta >= startXPos)
            {
                startCycle();
            }
 
        }
    }


    void OnTriggerEnter(Collider coll)
    {
        if (!used)
        {
            used = true;
        }
        else
        {
            return;
        }
        startCycle();
    }
}
