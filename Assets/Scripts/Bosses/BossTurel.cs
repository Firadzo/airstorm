using UnityEngine;
using System.Collections;

public class BossTurel : MonoBehaviour {
	//GUN
    public Transform LookTarget;
	public Transform Turret;
	
	public Transform tiltableGun;
    public Transform[] FirePos;

    //Destroy Section
    public Transform BulletObject; //The bullet shot from the plane
  
    public int BulletSpeed = 50; //bullet's speed when shot
    public int ShotCoolDown = 10; //How long to wait between each new bullet shot
    public int AttackInterval = 70; //How long to wait between each new attack
    public int ShotsPerAttack = 3; //How many bullets to shoot each attack
	
    private Transform BulletObjectCopy; //Holds a copy of the created bullet object so we can give it some velocity later
    private int ShotCoolDownCount; //Used to hold the original cooldown value set by us
    private int AttackIntervalCount; //Used to hold the original Attack Interval set by us
    private int ShotsPerAttackCount; //Used to hold the original Shots Per Attack set by us

    private EndLevelPopup popup;

    public Vector3 turelRotateVector = Vector3.zero;

    public BossTurel()
    {
        ShotsPerAttackCount = ShotsPerAttack;
        AttackIntervalCount = AttackInterval;
        ShotCoolDownCount = ShotCoolDown;
    }
	
	public bool shootEnable = true;

    void Start()
    {
        AttackIntervalCount = 0;
        ShotCoolDownCount = 0;
    }

    void Update()
    {      
		if(shootEnable){
			if(Turret) {
                //rotate turret
               Vector3 dir = PlayerScript.getPosition - transform.position;
               Quaternion rotation = Quaternion.LookRotation(dir);
               rotation.x = 0;
               rotation.z = 0;
               transform.rotation= Quaternion.Slerp(transform.rotation, rotation * Quaternion.Euler(turelRotateVector), 40 * Time.deltaTime);
/*
			    Turret.transform.localEulerAngles = turelRotateVector;
				Turret.transform.localEulerAngles = new Vector3(
					Turret.localEulerAngles.x, 
					LookTarget.localEulerAngles.y,
					Turret.localEulerAngles.z);*/
			}
			
			if(tiltableGun) {
				//tilt gun
				tiltableGun.localEulerAngles = new Vector3(
                    LookTarget.localEulerAngles.x,
                    LookTarget.localEulerAngles.y,
                    tiltableGun.localEulerAngles.z);
			
			}
		
			Vector3 posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
			
			if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) 
            {
		        //First, we wait for a set amount of frame updates, and then start shooting a burst
		        if (AttackIntervalCount > 0) {
		            AttackIntervalCount--;
		        } else {
		            //The rate of fire is set by the value of ShotCooldown, and is set in frame updates
		            if (ShotCoolDownCount > 0) {
		                //Decrease the cooldown counter
		                ShotCoolDownCount--;
		            } else {

                        for (int i = 0; i < FirePos.Length; i++)
                        {
                            Shoot(FirePos[i]);
                        }

			            //The number of shots per burst is set by ShotsPerAttack
		                if (ShotsPerAttackCount > 0) {
		                    ShotsPerAttackCount--;
		                } else {
		                    ShotsPerAttackCount = ShotsPerAttack;
		                    AttackIntervalCount = AttackInterval;
		                }
		            }
		        }
			}
		}
    }
	
    private void Shoot(Transform FirePos)
    {
        if (BulletObject)
        {
			BulletBase bullet = BulletsManager.GetFreeBullet( BulletObject.name );
			Vector3 dir = PlayerScript.getPosition - Turret.transform.position;
			if( bullet != null )
			{
				bullet.Launch( FirePos.position, Quaternion.identity, dir.normalized * BulletSpeed );
			}
            //Reset the cooldown timer for the bullet
            ShotCoolDownCount = ShotCoolDown;
        }
    }

    public void ShootEnable() {
        if (!gameObject.GetComponent<BossPart>().avariable)
        {
            shootEnable = false;
            return;
        }
        shootEnable = true;
    }

    public void ShootDisable() {
        shootEnable = false;
    }
}
