﻿using UnityEngine;
using System.Collections;

public class MainBossTurel : MonoBehaviour {
	public Transform tiltableGun;
	public Transform[] FirePos;
	
	//Destroy Section
	//public Transform Debris; //The debri object that flies from the crashed plane
	public Transform ExplosionEffect; //The effect created when the plane explodes
	public Transform BulletObject; //The bullet shot from the plane
	
	public int 		BulletSpeed = 50; //bullet's speed when shot
	public float 	ShotCoolDown = 10; //How long to wait between each new bullet shot
	public float 	AttackInterval = 70; //How long to wait between each new attack
	public int 		ShotsPerAttack = 3; //How many bullets to shoot each attack
	public string	animStateName;
	private bool 	shootEnable = true;

	private float 		ShotCoolDownCount; //Used to hold the original cooldown value set by us
	private float 		AttackIntervalCount; //Used to hold the original Attack Interval set by us
	private int 		ShotsPerAttackCount; //Used to hold the original Shots Per Attack set by us
	private BossPart	bossPart;

	
	void Start()
	{
		bossPart = GetComponent<BossPart>();
		shootEnable = PlayerScript.instance.shootEnable;
		AttackIntervalCount = AttackInterval;
		ShotCoolDownCount = 0;
		ShotsPerAttackCount = ShotsPerAttack;
		
	}

	private bool animStarted;
	private void StartAnim()
	{
		Animator animator = GetComponent<Animator>();
		if( animator )
		{
			animator.Play( animStateName );
		}
	}

	public Transform		lookToPlayer;
	Vector3 posOnScreen;
	void Update()
	{
		if( !bossPart.avariable ) return;
		
		if(shootEnable) {
			if( !animStarted )
			{
				animStarted = true;
				StartAnim();
			}
			
			if(tiltableGun)
			{
				Debug.DrawRay( tiltableGun.position,PlayerScript.getPosition - tiltableGun.position );
				//tiltableGun.localEulerAngles = Quaternion.LookRotation( ( PlayerScript.getPosition - tiltableGun.position ).normalized ).eulerAngles; 
				tiltableGun.localEulerAngles =	new Vector3(
					tiltableGun.localEulerAngles.x,
					-Quaternion.LookRotation( ( PlayerScript.getPosition - tiltableGun.position ).normalized ).eulerAngles.y,
					tiltableGun.localEulerAngles.z );
			}
			
			posOnScreen = Camera.main.WorldToViewportPoint(transform.position);
			
			if (posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0) 
			{
				//First, we wait for a set amount of frame updates, and then start shooting a burst
				if (AttackIntervalCount > 0) {
					AttackIntervalCount -= Time.deltaTime;
				} else {
					//The rate of fire is set by the value of ShotCooldown, and is set in frame updates
					if (ShotCoolDownCount > 0) {
						//Decrease the cooldown counter
						ShotCoolDownCount -= Time.deltaTime;
					} else {
						
						for (int i = 0; i < FirePos.Length; i++)
						{
							Shoot(FirePos[i]);
						}
						ShotsPerAttackCount --;
						//The number of shots per burst is set by ShotsPerAttack
						if (ShotsPerAttackCount == 0 ) 
						{
							ShotsPerAttackCount = ShotsPerAttack;
							AttackIntervalCount = AttackInterval;
						}
					}
				}
			}
		}
	}
	
	private void Shoot(Transform FirePos)
	{
		if (BulletObject)
		{
			BulletBase bullet = BulletsManager.GetFreeBullet( BulletObject.name );
			
			if( bullet != null )
			{
				bullet.Launch( FirePos.position, Quaternion.identity, FirePos.forward * BulletSpeed );
			}
			ShotCoolDownCount = ShotCoolDown;
		}
	}
	
	public void ShootEnable()
	{
		shootEnable = true;
	}
	
	public void ShootDisable()
	{
		shootEnable = false;
	}
}
