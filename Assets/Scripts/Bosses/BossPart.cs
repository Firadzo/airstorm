﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossPart : MonoBehaviour
{
    public bool 		avariable = false;
    public GameObject 	ExplosionEffect;
    public float 		Heath;
    private Color 		defaultColor;
    public GameObject 	SmokePref;
	private BossPlane 	bossPlane;
	private BoxCollider	boxCollider;
    // Use this for initialization
	private MeshRenderer[]	partRenderers;

	void Awake()
	{
		boxCollider = GetComponent<BoxCollider>();
		boxCollider.enabled = avariable;
	}

	public void Activate()
	{
		avariable = true;
		boxCollider.enabled = true;
	}

    void Start()
    {
		List<MeshRenderer> renderers = new List<MeshRenderer>();
        Transform[] root = GetComponentsInChildren<Transform>();
        for (int d = 0; d < root.Length; d++)
        {
			MeshRenderer temp = root[d].GetComponent<MeshRenderer>();
			if ( temp != null && temp.material.HasProperty( "_Color" ) )
            {
				renderers.Add( temp );
				defaultColor = temp.material.color;

            }
        }
		partRenderers = renderers.ToArray();
    }



    private Transform _smokeEffect;

    //flashing after damage
    public void SetDamageColor()
    {
        if (!avariable) return;

        if (Heath > 0)
        {

			for (int d = 0; d < partRenderers.Length; d++)
            {
				if ( partRenderers[ d ] != null ) 
				{
					partRenderers[ d ].material.SetColor("_Color", new Color(255f, 255f, 255f, 255f));
				}
            }

            StartCoroutine(setDefaultTextures());

        }
    }

    IEnumerator setDefaultTextures()
    {
        yield return new WaitForSeconds(0.1f);

		for (int d = 0; d < partRenderers.Length; d++)
		{
			if ( partRenderers[ d ] != null ) 
			{
				partRenderers[ d ].material.SetColor("_Color", defaultColor );
			}
		}
    }

    public void applyDamage(float hp)
    {
        if (!avariable) return;
        Heath -= hp;
		CheckHealth ();
    }

	private void TakeDamage( int damage )
	{
		if( avariable )
		{
			Heath -= damage;
			SetDamageColor();
			CheckHealth();
		}
	}
	private void CheckHealth()
	{
		if (Heath <= 0)
		{
			if (ExplosionEffect)
			{
				Transform eff = Instantiate(ExplosionEffect, transform.position + (new Vector3(0, 1.6f, 0)), transform.rotation) as Transform;
				if (eff) Destroy(eff.gameObject, 3);
			}
			
			if (SmokePref && !_smokeEffect)
			{
				_smokeEffect = ((GameObject)Instantiate(SmokePref)).transform;
				_smokeEffect.parent = transform;
				_smokeEffect.localPosition = Vector3.zero;
				_smokeEffect.localScale = Vector3.one;
				_smokeEffect.localRotation = Quaternion.identity;
				_smokeEffect.parent = transform.parent;
				if( BossPlane.instance != null )
				{
					BossPlane.instance.AddEmitter( _smokeEffect.GetComponent<ParticleEmitter>() );
				}
			}
			
			Destroy(gameObject);
		}
	}
}
