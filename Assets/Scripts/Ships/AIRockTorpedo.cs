﻿using UnityEngine;
using System.Collections;

public class AIRockTorpedo : MonoBehaviour {
    public int 			attackRadius = 100;
    public GameObject 	explosion;
    public int 			timeOut = 3;
    public int 			speed = 5;
    public int 			rotSpeed = 5;
    public float 		Damage = 1;
    public int 			explosionRadius = 10;
    public bool 		shootEnable = true;

    public ParticleEmitter smokeTorpedo;
    public ParticleEmitter smokeRocket;

    private Transform 	Player;
    private int 		defSpeed;
    private bool 		launch = false;
    private bool 		explosionStart = false;

    public bool DRAWRadius = false;

    public bool waterPhase = false;
    private bool launchPhase = false;
    private bool enableAIPhase = false;

    void OnDrawGizmos()
    {
        if (DRAWRadius)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, attackRadius);
        }
    }
 
    void Start()
    {
		Player = PlayerScript.instance.transform;
        shootEnable = PlayerScript.instance.shootEnable;
        defSpeed 	= speed;
        speed 		= speed / 3;
        smokeTorpedo.enabled = false;
		smokeRocket.enabled = false;
    }

    void Update()
    {
        if (explosionStart) return;

        if (!launch && Vector3.Distance(transform.position, Player.position ) < attackRadius) {
            launch = true;
           // Invoke("Explosion",timeOut);
        }

        if (shootEnable && launch)
        {
            if (!enableAIPhase && !waterPhase && !launchPhase)
            {
                GetComponent<Animation>().Play();
                return;
            }

            if (enableAIPhase)
            {
                //transform.LookAt(Player);
                Vector3 dir = Player.position - transform.position;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.FromToRotation(transform.position.normalized, dir.normalized), Time.deltaTime * rotSpeed);
                speed = defSpeed;
            }

            if (waterPhase) 
            {
                transform.Translate(speed * Vector3.forward * Time.deltaTime);
                if (smokeTorpedo) smokeTorpedo.enabled = true;
            }
            else if (launchPhase) 
            { 
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.FromToRotation(transform.position.normalized, Vector3.up), Time.deltaTime * rotSpeed);
                transform.Translate(speed * Vector3.forward * Time.deltaTime);
            } 
            else 
            {
                transform.Translate(speed * Vector3.forward * Time.deltaTime);
            }
        }

        if (!explosionStart && Vector3.Distance(transform.position, Player.position) < 20)
        {
            Explosion();
            explosionStart = true;
        }
    }

    void EnableWaterState() {
        waterPhase = true;
        StartCoroutine(waterStatePause());
    }

    IEnumerator waterStatePause()
    {
        yield return new WaitForSeconds(2);
        waterPhase = false;
        launchPhase = true;
        smokeTorpedo.enabled = false;
        smokeRocket.enabled = true;
        StartCoroutine(launchPhaseState());
    }

    IEnumerator launchPhaseState()
    {
        yield return new WaitForSeconds(2);
        launchPhase = false;
        enableAIPhase = true;
      
    }

    void Explosion()
    {
        Vector3 explosionPosition = transform.position;
        Collider[] colliders = Physics.OverlapSphere( explosionPosition, explosionRadius );

        foreach (var hit in colliders)
        {
            if (!hit) continue;

			if ( hit.CompareTag ( "Player" ) )
            {
                float distance = Vector3.Distance(hit.transform.position, explosionPosition);

                float hitPoints = 1.0f - Mathf.Clamp01(distance / explosionRadius);
                hitPoints *= Damage;
				PlayerScript.instance.HitPlayer( hitPoints );
            }
        }
		EffectsManager.PlayEffect ( explosion.name, transform.position, Quaternion.identity );

      //  ParticleEmitter emitter = GetComponentInChildren<ParticleEmitter>();
     //   if (emitter) emitter.emit = false;
        Destroy(gameObject);
    }

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
