﻿using UnityEngine;
using System.Collections;

public class SubmarineTurel : MonoBehaviour
{
	public	int			health = 200;
	public Transform 	ExplosionEffect; 
    public Transform[] 	FirePos;
    public int 			maxTilt = 90;
    public Transform 	Turret;
    public Transform 	tiltableGun;
    public float 		attackRadius = 50;
    public Transform 	BulletObject;
    public int 			BulletSpeed = 5;
	public float 		ShotCoolDown = 20;
    public bool 		shootEnable = true;
    public bool 		DRAWRadius = false;

    private float ShotCoolDownCount;
	private EndLevelPopup	popup;

    void OnDrawGizmos()
    {
        if (DRAWRadius)
        {
            Gizmos.color = Color.grey;
            Gizmos.DrawWireSphere(transform.position, attackRadius);
        }
    }

    public SubmarineTurel()
    {
        ShotCoolDownCount = ShotCoolDown;
    } 

    void Start()
    {
        shootEnable = PlayerScript.instance.shootEnable;
        ShotCoolDownCount = 0;
		popup = EndLevelPopup.instance;
		popup.allEnemyCnt++;
		if (GetComponent<Rigidbody> () == null) {
			
			Rigidbody r = gameObject.AddComponent<Rigidbody>();
			r.isKinematic = true;
		}
    }

	private void TakeDamage( int damage )
	{
		health -= damage;
		CheckHealth();
	}

	public void applyDamage( float hp )
	{
		health -= (int)hp;	
		CheckHealth();
	}

	private void CheckHealth()
	{
		if( health <= 0 )
		{
			popup.allEnemyCnt--;
			if (ExplosionEffect) {
				Instantiate(ExplosionEffect, transform.position + (new Vector3(0, 1.6f, 0)), transform.rotation);
			}
			Destroy(gameObject);
		}
	}

	Vector3 posOnScreen;
    void Update()
    {
        if ( shootEnable )
        {
            //tilt gun
           /* float angle = -LookTarget.localEulerAngles.y;
            if (Mathf.Abs(angle) > maxTilt)
            {
                angle *= -1;
                angle -= 180;
            }  

            if ((transform.position.z - Player.transform.position.z) > 0) {
                angle += 180;
            }*/

			posOnScreen = Camera.main.WorldToViewportPoint(transform.position);

            if ( posOnScreen.y < 1 && posOnScreen.x < 1 && posOnScreen.y > 0 && posOnScreen.x > 0 )
            {
				Turret.eulerAngles = new Vector3( Turret.eulerAngles.x,
				                                           Quaternion.LookRotation( PlayerScript.getPosition - Turret.position ).eulerAngles.y
				                                            , Turret.eulerAngles.z );
				tiltableGun.localEulerAngles = new Vector3( tiltableGun.localEulerAngles.x, -35, tiltableGun.localEulerAngles.z );
				
				if (ShotCoolDownCount > 0)
                {
                    ShotCoolDownCount -= Time.deltaTime;
                }
                else
                {
					
					if( (Vector3.Distance(transform.position, PlayerScript.getPosition ) < attackRadius) )
					{
	                    for (int i = 0; i < FirePos.Length; i++)
	                    {
	                        Shoot(FirePos[i]);
	                    }      
					}
                }  
            }
        }
    }

    //Shoot
    private void Shoot(Transform FirePos)
    {
        if (BulletObject)
        {
			BulletBase bullet = BulletsManager.GetFreeBullet( BulletObject.name );
			if( bullet != null )
			{
				bullet.Launch( FirePos.position, Quaternion.identity, ( PlayerScript.getPosition - FirePos.position ).normalized * BulletSpeed  );
			}
            ShotCoolDownCount = ShotCoolDown;
        }
    }

    public void ShootEnable()
    {
        shootEnable = true;
    }

    public void ShootDisable()
    {
        shootEnable = false;
    }
}
