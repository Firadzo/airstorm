﻿using System;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

namespace AirStrike
{
	[System.Serializable]
	public class TechniqueTypesContainer
	{
		private List<TechniqueByType>[] _techniques;

		public TechniqueTypesContainer()
		{
			_techniques = new List<TechniqueByType>[ Enum.GetNames( typeof(TechniqueTypes) ).Length ];
			for( int i = 0; i < _techniques.Length; ++i )
			{
				_techniques[ i ] = new List<TechniqueByType>();
			}
		}

		public bool ParseTechniquesTypes( XmlElement techniqueTypesNode )
		{
			TechniqueByType technique;
			TechniqueTypes techniqueType;
			int colParse = 0;
			foreach( XmlElement typeNode in techniqueTypesNode.ChildNodes )
			{
				techniqueType = TechniqueByType.GetTechniqueTypeByName(typeNode.Name);
				if(techniqueType != TechniqueTypes.none)
				{
					foreach( XmlElement unitNode in typeNode.ChildNodes )
					{
						colParse++;
						technique = new TechniqueByType(unitNode.GetAttribute("Name"), techniqueType);
						_techniques[(int)techniqueType].Add(technique);
					}
				}
			}

			if(colParse > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public List<TechniqueByType> GetTechniqueByType(TechniqueTypes type)
		{
			return _techniques[ (int)type ];
		}
	}

	[System.Serializable]
	public class TechniqueByType
	{
		private string _name;
		private TechniqueTypes _type;

		public string name
		{
			get
			{
				return _name;
			}
		}

		public TechniqueTypes type
		{
			get
			{
				return _type;
			}
		}

		public TechniqueByType(string name, TechniqueTypes type)
		{
			_name = name;
			_type = type;
		}

		static public TechniqueTypes GetTechniqueTypeByName( string typeName )
		{
			string[] techniqueTypeNames = Enum.GetNames( typeof(TechniqueTypes) );
			for( int i = 0; i < techniqueTypeNames.Length; ++i )
			{
				if( typeName.Equals(techniqueTypeNames[i]) )
				{
					return (TechniqueTypes)i;
				}
			}

			return TechniqueTypes.none;
		}
	}
}