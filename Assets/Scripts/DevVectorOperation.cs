﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class DevVectorOperation : MonoBehaviour {

	// Use this for initialization
    public Transform Obj1;
    public Transform Obj2;

	void Start ()
	{

	    
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void OnDrawGizmos()
    {
        if (Obj2 && Obj1)
        {
            var heading = Obj1.position - Obj2.position;
            //var heading2 = Obj2.forward - Obj1.forward;

            Gizmos.color = Color.blue;
            Gizmos.DrawLine(Obj1.position, heading);

            //Gizmos.color = Color.green;
            //Gizmos.DrawLine(Obj1.position, heading2);

            var distance = heading.magnitude;
            var direction = heading / distance;
            Gizmos.color = Color.red;
            Gizmos.DrawLine(Obj1.position, direction);

            //Debug.Log("ang = " + Vector3.Angle(Obj1.forward, direction) + "dot = " + Vector3.Dot(Obj1.forward,Obj2.forward));

           // var v1 = Obj2.rotation*Vector3.forward;

            Debug.Log("heading = " + heading + " Obj2.forward =" + Obj2.forward + " dot = " + Vector3.Dot(heading, Obj2.forward));

        }

        
    }
}
