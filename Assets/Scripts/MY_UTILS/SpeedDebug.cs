﻿using UnityEngine;
using System.Collections;

public class SpeedDebug : MonoBehaviour {

	private UIScrollBar bar;
	// Use this for initialization
	void Start () {
		bar = GetComponent<UIScrollBar> ();
		bar.onChange.Add ( new EventDelegate( OnChange ) );
		bar.value = 25f / 400f;
	}

	void OnChange()
	{
		PlayerMoveController.instance.SetSpeed ( bar.value );
	}
}
