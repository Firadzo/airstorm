﻿using UnityEngine;
using System.Collections;

public class FixPosAfterScale : MonoBehaviour {

	public Vector3		scale;
	public Transform[]	transforms;
	public bool fix;
	public bool back;
	public bool fixChild;
	private Vector3[] scales;

#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		if (fixChild) 
		{
			fixChild  = false;
			transforms = new Transform[ transform.childCount ];
			scales = new Vector3[ transform.childCount ];
			for( int i = 0; i < transform.childCount; i++ )
			{
				transforms[ i ] = transform.GetChild( i );
				scales[ i ] = transform.GetChild( i ).parent.localScale;
			}

			//transform.localScale = Vector3.one;
			for( int i = 0; i < transforms.Length; i++ )
			{
				transforms[ i ].localScale = Vector3.one;
			}

			Vector3 pos;
			Vector3 scale = transform.localScale;
			transform.localScale = Vector3.one;
			for( int i = 0; i < transforms.Length; i++ )
			{
				pos = transforms [i].localPosition;
				pos.x *= scale.x;
				pos.y *= scale.y;
				pos.z *= scale.z;
				transforms [i].localPosition = pos;
			}

		}

		if (fix) {
		
			fix = false;
			Vector3 pos;
			for (int i = 0; i < transforms.Length; i++) {
				pos = transforms [i].localPosition;
				pos.x *= scale.x;
				pos.y *= scale.y;
				pos.z *= scale.z;
				transforms [i].localPosition = pos;
			}
		} else if (back) {
			back = false;
			Vector3 pos;
			for (int i = 0; i < transforms.Length; i++) {
				pos = transforms [i].localPosition;
				pos.x /= scale.x;
				pos.y /= scale.y;
				pos.z /= scale.z;
				transforms [i].localPosition = pos;
			}
		}
	}
#endif
}
