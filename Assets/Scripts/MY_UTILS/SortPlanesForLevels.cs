﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Test
{
	public List< GameObject > planes;

	public bool Contains( GameObject obj )
	{
		foreach( GameObject objIn in planes )
		{
			if( objIn == obj )
			{
				return true;
			}
		}
		return false;
	}

	public void Add( GameObject obj )
	{
		if ( !Contains (obj)) {
			planes.Add (obj);
		}
	}

	public Test()
	{
		planes = new List<GameObject> ();
	}
}

[System.Serializable]
public class Group
{
	public string name;
	public List< GameObject > planes;
	
	public Group( string _name, List< GameObject > _planes )
	{
		name = _name;
		planes = _planes;
	}
}

public class SortPlanesForLevels : MonoBehaviour {

	public GameObject		planeToCheck;
	public LevelConroller[] levelControllers;

	public Dictionary< string , List< GameObject >> groups;
	public Test[] planes;
	public List<Group> sortedGroups;

	public bool sort;
	public bool	checkPlane;


	void OnDrawGizmosSelected()
	{
		if (checkPlane) {
		
			checkPlane = false;
			for( int i = 0; i < planes.Length; i++ )
			{
				foreach( GameObject plane in planes[ i ].planes )
				{
					if( plane == checkPlane )
					{
						Debug.Log( "true "+i);
						return;
					}
				}
			}
			Debug.Log( "False");
		}

		if (sort) {
			sort = false;
			groups = new Dictionary< string , List< GameObject >> ();
			planes = new Test[ levelControllers.Length ];
			sortedGroups = new List<Group>();
			
			for( int i = 0; i < levelControllers.Length; i++ )
			{
				planes[ i ] = new Test();
				for( int j = 0; j < levelControllers[ i ].LevelEnemies.Count; j++ )
				{
					for( int k = 0; k < levelControllers[ i ].LevelEnemies[ j ].SubTraect.Count; k++ )
					{
						planes[ i ].Add( levelControllers[ i ].LevelEnemies[ j ].SubTraect[ k ].EnemyPrefab );
					}
				}
			}

			for( int i = 0; i < planes.Length; i++ )
			{
				foreach( GameObject plane in planes[ i ].planes )
				{
					string lvls = "";
					bool cont = false;
					foreach( KeyValuePair< string, List<GameObject>> _group in groups )
					{
						foreach( GameObject used in _group.Value )
						{
							if( used == plane )
							{
								cont = true;
								break;
							}
						}
						if( cont ){ break; }
					}
					if( cont ){ continue; }
					for( int j = i; j < planes.Length; j++ )
					{
						foreach( GameObject planeIn in planes[ j ].planes )
						{
							if( plane == planeIn )
							{
								lvls += "_"+j;
							}
						}
					}
					if( !groups.ContainsKey( lvls ) )
					{
						groups.Add( lvls, new List<GameObject>() );
					}
					groups[lvls].Add( plane );
				}
			}

			foreach( KeyValuePair< string, List<GameObject>> _group in groups )
			{
				sortedGroups.Add( new Group( _group.Key, _group.Value ) );
			}



		}
	}
}
