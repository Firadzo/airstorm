﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SplineDataCopy : MonoBehaviour {

	public Spline		spline;

	public Vector3[] 	points;
	public Quaternion[]	rotations;
	public int			pointsNumber = 10;
	public bool			local = true;

	public Vector3 			calculatedPosition { get; private set; }
	public Quaternion		calculatedRotation { get; private set; }
	


	public void CalculatePosAndRotation( float ratio, float prevRatio )
	{
		calculatedPosition = GetPosition ( ratio );
		Quaternion prevRotation = GetRotation (prevRatio);
		Quaternion currRotation = GetRotation (ratio);
		//calculatedRotation = Quaternion.Lerp ( prevRatio, currRotation, );

	}

	public void CalculatePosAndRotation( float ratio )
	{
		float currIndex = ( points.Length - 1f ) * ratio;
		int prevIndex = Mathf.FloorToInt ( currIndex );
		float part = currIndex - prevIndex;
		if ( part == 0 ) 
		{
			if( local )
			{ 
				calculatedPosition = transform.position + points [ prevIndex ]; 
			}
			else
			{
				calculatedPosition =  points [ prevIndex ];
			}
			calculatedRotation =  rotations [ prevIndex ];
		} 
		else 
		{
			int nextIndex = prevIndex + 1;
			if( local )
			{ 
				calculatedPosition = transform.position + Vector3.Lerp( points[ prevIndex ], points[ nextIndex ], part ); 
			}
			else
			{
				calculatedPosition =  Vector3.Lerp( points[ prevIndex ], points[ nextIndex ], part );
			}
			calculatedRotation = Quaternion.Lerp( rotations[ prevIndex ], rotations[ nextIndex ], part );
		}
	}

	public Vector3 GetPosition( float ratio )
	{
		float currIndex = ( points.Length - 1f ) * ratio;
		int prevIndex = Mathf.FloorToInt ( currIndex );
		float part = currIndex - prevIndex;
		if ( part == 0 ) 
		{
			if( local )
			{
				return transform.position + points [ prevIndex ];
			}
			return points [ prevIndex ];
		} 
		else 
		{
			int nextIndex = prevIndex + 1;
			if( local )
			{
				return transform.position + Vector3.Lerp( points[ prevIndex ], points[ nextIndex ], part );
			}
			return Vector3.Lerp( points[ prevIndex ], points[ nextIndex ], part );
		}
	}

	public Quaternion GetRotation( float ratio )
	{
		float currIndex = ( rotations.Length - 1f ) * ratio;
		int prevIndex = Mathf.FloorToInt ( currIndex );
		float part = currIndex - prevIndex;
		if ( part == 0 ) 
		{
			return rotations [ prevIndex ];
		} 
		else 
		{
			int nextIndex = prevIndex + 1;
			return Quaternion.Lerp( rotations[ prevIndex ], rotations[ nextIndex ], part );
		}
	}

#if UNITY_EDITOR
	public bool readSpline;
	public bool	drawSpline = true;
	public float debugRatio;
	public Transform test;

	public Vector3 RotationFIX = Vector3.zero;
	public float ANGLE_FIX = 90f;
	public bool	fixRotation;

	public Vector3	fixVector;
	public bool fix;
	public bool reverse;
	public bool	testFix;

	private void FixRotation()
	{
		for( int i = 0; i < points.Length; i++ )
		{
			points[ i ] = ( transform.rotation * points[ i ] );
			points[ i ].x *= transform.lossyScale.x;
			points[ i ].y *= transform.lossyScale.y;
			points[ i ].z *= transform.lossyScale.z;
		}
	}

	void OnDrawGizmosSelected()
	{
		if (testFix) {
			testFix = false;
			FixRotation();
		}

		if (reverse) {
		
			reverse = false;

			List<Vector3> p = new List< Vector3 >( points );
			p.Reverse();
			points = p.ToArray();

			List<Quaternion> rot = new List< Quaternion >( rotations );
			rot.Reverse();
			rotations = rot.ToArray();
		}

		if (fixRotation) {
			fixRotation = false;
			for( int i = 0; i < rotations.Length; i++ )
			{
				rotations[ i ] *= Quaternion.AngleAxis( ANGLE_FIX, RotationFIX );
			}
		}

		if (fix) {
			fix = false;
			for( int i = 0; i < points.Length; i++ )
			{
				points[ i ].x *= fixVector.x;
				points[ i ].y *= fixVector.y;
				points[ i ].z *= fixVector.z;
			}
		}

		if (readSpline) {
		
			readSpline = false;
			if( spline == null ) spline = GetComponent<Spline>();

			points = new Vector3[ pointsNumber + 1 ];
			rotations = new Quaternion[ pointsNumber + 1 ];
			for( int i = 0; i <= pointsNumber; i++ )
			{
				points[ i ] = ( spline.GetPositionOnSpline( i / (float)pointsNumber ) );
				if( local )
				{
					points[ i ] = transform.InverseTransformPoint( points[ i ] );
				}
				rotations[ i ] = spline.GetOrientationOnSpline( i / (float)pointsNumber );
			}
			if( local ){
				FixRotation();
			}
		}
		if (drawSpline) {
			Gizmos.color = Color.green;
			if( points == null ){ return; }
			for( int i = 0; i < points.Length - 1; i++ )
			{
				if( local )
				{
					Gizmos.DrawLine( transform.position + points[ i ], transform.position + points[ i + 1 ] );
				}
				else
				{
					Gizmos.DrawLine( points[ i ], points[ i + 1 ] );
				}
			}
			debugRatio = Mathf.Clamp( debugRatio, 0, 1f );
			Gizmos.DrawSphere( GetPosition( debugRatio ), 5f );

		}
		if (test != null) {
			debugRatio = Mathf.Clamp( debugRatio, 0, 1f );
			test.transform.position  = GetPosition( debugRatio );
			test.rotation = GetRotation( debugRatio );
		}
	}

	public void Setup()
	{
		pointsNumber = 15;
		local = true;
		if( spline == null ) spline = GetComponent<Spline>();
		points = new Vector3[ pointsNumber + 1 ];
		rotations = new Quaternion[ pointsNumber + 1 ];
		for( int i = 0; i <= pointsNumber; i++ )
		{
			points[ i ] = ( spline.GetPositionOnSpline( i / (float)pointsNumber ) );
			if( local )
			{
				points[ i ] = transform.InverseTransformPoint( points[ i ] );
			}
			rotations[ i ] = spline.GetOrientationOnSpline( i / (float)pointsNumber );
		}
		if( local ){
			FixRotation();
		}
		drawSpline = true;
	}
#endif
}
