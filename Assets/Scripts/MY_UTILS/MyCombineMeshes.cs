﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MyCombineMeshes : MonoBehaviour {

	public MeshFilter				meshToSave;
	public bool						copy;

	public MeshFilter[]		meshFilters;
	public bool 				readMeshes;
	public bool 				combine;
	public Mesh					combined;
	public bool					save;
	public string				combinedName;

	#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		if (readMeshes) 
		{
			readMeshes = false;
			meshFilters = GetComponentsInChildren<MeshFilter>();
		}
		if (combine) 
		{
			combine = false;
			CombineInstance[] _combine = new CombineInstance[ meshFilters.Length ];
			int i = 0;
			while (i < meshFilters.Length) {
				_combine[i].mesh = meshFilters[i].sharedMesh;
				_combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
				i++;
			}
			combined = new Mesh();
			combined.CombineMeshes( _combine, true );
		}
		if( save )
		{
			save = false;
			if( combined != null )
			{
				combined.name = combinedName;
				AssetDatabase.CreateAsset( combined, "Assets/TEST/"+name+".asset");
				AssetDatabase.SaveAssets();
			}
		}

		if (copy) {
			copy = false;
			Mesh mesh = meshToSave.mesh;
			mesh.name = combinedName;
			AssetDatabase.CreateAsset( mesh, "Assets/TEST/"+name+".asset");
			AssetDatabase.SaveAssets();
		}
	}
	#endif
}
