﻿using UnityEngine;
using System.Collections;

public class EditorReplace : MonoBehaviour {

#if UNITY_EDITOR
	public Mesh old;
	public Mesh newMesh;
	public Material mat;

	public bool replace;

	public bool debugChildVertexes;
	public bool debugAllInScene;
	public bool replaceMaterialOnButched;
	public bool replaceMissedMats;
	//public bool replaceByName;

	void OnDrawGizmosSelected()
	{
		if (replaceMissedMats) 
		{
			replaceMissedMats = false;
			MeshRenderer[]	renderers = GetComponentsInChildren<MeshRenderer>();
			foreach( MeshRenderer render in renderers )
			{
				if( render.sharedMaterials[0] == null )
				{
					Debug.Log( render.gameObject.name );
				}
			}
		}
		/*if (replaceByName) {
			replaceByName = false;
			MeshRenderer[]	renderers = GetComponentsInChildren<MeshRenderer>();
			MeshFilter[] filters = (MeshFilter[])FindObjectsOfType<MeshFilter>( );
			
			foreach( MeshRenderer render in renderers )
			{
				Mesh sharedMesh = render.GetComponent<MeshFilter>().sharedMesh;
				string name = render.gameObject.name;
				foreach( MeshFilter filter in filters )
				{
					if( filter.name == )
					if( filter.sharedMesh == sharedMesh )
					{
						filter.GetComponent<MeshRenderer>().sharedMaterial = render.sharedMaterial;
					}
				}
			}
		}*/

		if (replace) {
		
			replace = false;
			MeshFilter[] filters = (MeshFilter[])FindObjectsOfType<MeshFilter>(  );
			for( int i =0; i < filters.Length; i++ )
			{
				if( filters[ i ].sharedMesh == old )
				{
					filters[ i ].sharedMesh = newMesh;
					filters[ i ].GetComponent<MeshRenderer>().sharedMaterial = mat;
				}
			}
		}
		if (debugChildVertexes) {
		
			debugChildVertexes = false;
			MeshFilter[] meshes = GetComponentsInChildren<MeshFilter>();
			foreach( MeshFilter meshF in meshes )
			{
				Debug.Log( meshF.sharedMesh.vertexCount +"  "+meshF.gameObject.name );
			}
		}
		if (debugAllInScene) {
			debugAllInScene = false;
			MeshFilter[] filters = (MeshFilter[])FindObjectsOfType<MeshFilter>(  );
			for( int i =0; i < filters.Length; i++ )
			{
				if(filters[ i ].sharedMesh != null )
				Debug.Log( filters[ i ].sharedMesh.vertices.Length +"  "+ filters[ i ].gameObject.name );
			}
		}

		if (replaceMaterialOnButched) {
			replaceMaterialOnButched = false;
			MeshRenderer[]	renderers = GetComponentsInChildren<MeshRenderer>();
			MeshFilter[] filters = (MeshFilter[])FindObjectsOfType<MeshFilter>( );

			foreach( MeshRenderer render in renderers )
			{
				Mesh sharedMesh = render.GetComponent<MeshFilter>().sharedMesh;
				foreach( MeshFilter filter in filters )
				{
					if( filter.sharedMesh == sharedMesh )
					{
						filter.GetComponent<MeshRenderer>().sharedMaterial = render.sharedMaterial;
					}
				}
			}
		}


	}
#endif
}
