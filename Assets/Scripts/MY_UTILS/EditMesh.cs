﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EditMesh : MonoBehaviour {
#if UNITY_EDITOR
	public Mesh source;

	const string editedName = "=>ODCObj";
	private Dictionary< MeshFilter, MeshFilter > result;

	public bool edit;
	public bool editChild;
	public bool deleteSource = true;
	public bool deleteSourceNow;
	public bool	saveNew = false;
	public bool enableAllRenderers;
	public bool	checkSameName;
	void OnDrawGizmosSelected()
	{
		if (enableAllRenderers) {
			enableAllRenderers = false;
			MeshRenderer[]	meshFilters = GetComponentsInChildren<MeshRenderer>();
			foreach( MeshRenderer meshR in meshFilters )
			{
				meshR.enabled = true;
			}
		}

		if (deleteSourceNow) {
			deleteSourceNow = false;
			Transform[] obj = GetComponentsInChildren<Transform>();

			foreach( Transform transf in obj )
			{
				if( transf.name.Contains( editedName ) )
				{
					DestroyImmediate( transf.gameObject );
				}
			}
		}

		if (edit) {
		
			edit = false;
			Mesh edited = GetComponent<MeshFilter>().sharedMesh;
			if( edited != null )
			{
				source.uv = edited.uv;
			}
		}
		if (editChild) {
			editChild = false;
			MeshFilter[]	meshFilters = GetComponentsInChildren<MeshFilter>();
			result = new Dictionary<MeshFilter, MeshFilter>();
			foreach( MeshFilter meshF in meshFilters )
			{
				if( !meshF.name.Contains(editedName ) )
				{
					bool contains = false;
					foreach( KeyValuePair< MeshFilter,MeshFilter> mf in result  )
					{
						if( mf.Key.name == meshF.name )
						{
							Debug.Log( mf.Key.name );
							contains = true;
							break;
						}
					}
					if( contains ){ continue; }
						foreach( MeshFilter meshF2 in meshFilters  )
						{
							if( meshF2.name == meshF.name + editedName )
							{
								result.Add( meshF, meshF2 );
								break;
							}
						}


				}
			}

			foreach( KeyValuePair< MeshFilter,MeshFilter> meshF in result  )
			{
			//	Debug.Log( meshF.Key+" = "+meshF.Value );
				if( saveNew )
				{
				//	meshF.Key.sharedMesh = meshF.Value.sharedMesh;
					meshF.Value.transform.name = meshF.Value.transform.name.Replace( editedName,"" );
					MeshesToFile.Save( meshF.Value );
				}

				//meshF.Key.GetComponent<MeshRenderer>().enabled = true;
				//meshF.Key.GetComponent<MeshRenderer>().sharedMaterial = meshF.Value.GetComponent<MeshRenderer>().sharedMaterial;

				if( deleteSource )
				{
					//DestroyImmediate( meshF.Value.gameObject );
				}
	

			}

		}

		if (checkSameName) {
		
			checkSameName = false;
			MeshFilter[]	meshFilters = GetComponentsInChildren<MeshFilter>();
			result = new Dictionary<MeshFilter, MeshFilter>();
			foreach( MeshFilter meshF in meshFilters )
			{
				if( !meshF.name.Contains(editedName ) )
				{
					bool contains = false;
					foreach( KeyValuePair< MeshFilter,MeshFilter> mf in result  )
					{
						if( mf.Key.name == meshF.name )
						{
							Debug.Log( mf.Key.name );
							contains = true;
							break;
						}
					}
					if( contains ){ continue; }
					foreach( MeshFilter meshF2 in meshFilters  )
					{
						if( meshF2.name == meshF.name + editedName )
						{
							result.Add( meshF, meshF2 );
							break;
						}
					}
					
					
				}
			}
		}


	}
#endif

}
