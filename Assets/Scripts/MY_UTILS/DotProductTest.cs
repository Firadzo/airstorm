﻿using UnityEngine;
using System.Collections;

public class DotProductTest : MonoBehaviour {

	public Vector3	point1;
	//public Vector3  point2;

	public bool debug;
	void OnDrawGizmosSelected()
	{
		Gizmos.DrawSphere( point1, 15f );
		if( debug )
		{
			debug = false;
			Debug.Log(Vector3.forward);
			Debug.Log( Vector3.Dot( point1 - transform.position, Vector3.forward ) );
		}
	}
}
