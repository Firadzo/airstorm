﻿using UnityEngine;
using System.Collections;

public class CheckScrpit : MonoBehaviour {

	public bool moveByPhysics;
	public bool moveByCode;
	public bool calculateDistance;

	public int copiesCount;

	static bool created = false;
	private Transform transf;
//	static 	Transform	mainCam;
	private Rigidbody	rb;
	public 	float 		speed = 100f;
	// Use this for initialization
	void Awake () {
		//mainCam = Camera.main.transform;
		rb = GetComponent<Rigidbody>();
		transf = transform;
		if( !created )
		{
			created = true;
			for( int i = 0; i < copiesCount; i++ )
			{
				Instantiate( gameObject );
			}
		}
	}
	
	// Update is called once per frame
//	float dist;
	void Update () {
	if( moveByPhysics )
		{
			moveByPhysics = false;
			rb.AddForce( Vector3.forward * speed );
		}
		if( moveByCode )
		{
			transf.position += Vector3.forward * speed * Time.deltaTime;
		}
		if( calculateDistance )
		{
			//dist = Vector3.Distance( transf.position, mainCam.position );
		}
	}
}
