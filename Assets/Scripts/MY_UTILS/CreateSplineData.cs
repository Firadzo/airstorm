﻿using UnityEngine;
using System.Collections;

public class CreateSplineData : MonoBehaviour {
#if UNITY_EDITOR
	public Transform[]	tranjectories;
	public bool run;
	public bool	runForChild;


	void OnDrawGizmosSelected()
	{
		if (run) {
			run = false;
			for (int i=0; i < tranjectories.Length; i++) {
		
				for (int j = 0; j < tranjectories[ i ].childCount; j++) {
					if (tranjectories [i].GetChild (j).GetComponent<SplineDataCopy> () == null) {
						tranjectories [i].GetChild (j).gameObject.AddComponent<SplineDataCopy> ().Setup ();
					}
				}
			}
		}
		if (runForChild) {
		
			runForChild = false;
				for (int j = 0; j < transform.childCount; j++) {
				if (transform.GetChild (j).GetComponent<SplineDataCopy> () == null) {
					transform.GetChild (j).gameObject.AddComponent<SplineDataCopy> ().Setup ();
					}
			}
		}
	}
#endif
}
