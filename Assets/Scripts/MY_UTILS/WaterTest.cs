﻿using UnityEngine;
using System.Collections;

public class WaterTest : MonoBehaviour {
	private UILabel		label;
	public string   	waterObjName;
	private GameObject	waterObj;
	private bool 		waterOn = true;

	void Awake()
	{
		label = GetComponent<UILabel>();
		waterObj = GameObject.Find ( waterObjName );
	}

	// Use this for initialization
	void Start () {
	
	}

	public void OnOffWater()
	{
		waterOn = !waterOn;
		label.text = "Water "+ ( waterOn ? "ON":"OFF" );
		waterObj.SetActive (waterOn);
	}
}
