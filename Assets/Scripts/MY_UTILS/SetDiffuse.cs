﻿using UnityEngine;
using System.Collections;

public class SetDiffuse : MonoBehaviour {
#if UNITY_EDITOR
	public bool set;

	void OnDrawGizmosSelected()
	{
		if (set) {
			set = false;
			MeshRenderer render;
			for( int i = 0; i < transform.childCount; i ++ )
			{
				render = transform.GetChild( i ).GetComponent<MeshRenderer>();
				if( render != null )
				{
					render.sharedMaterial.shader = Shader.Find( "Diffuse" );
				}
			}
		}
	}
#endif
}
