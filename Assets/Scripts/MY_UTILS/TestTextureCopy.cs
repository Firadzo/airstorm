﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TestTextureCopy : MonoBehaviour {
	#if UNITY_EDITOR
	public Texture2D 	source;
	public Texture2D	result;



	public bool create;
	void OnDrawGizmosSelected()
	{
		if (create) {
		
			create = false;
			if( source != null )
			{
				result = new Texture2D( 1024, 1024, TextureFormat.ARGB32, false );
				result.SetPixels32( result.GetPixels32() );
				result.Apply();
			}
		}
	}
# endif
}
