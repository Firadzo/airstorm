﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class MeshesToFile : MonoBehaviour {
	#if UNITY_EDITOR
	public 	bool 	save;
	public 	bool 	useScale;
	public	bool 	toObj;
	public 	bool	clearTangentsAndNormals;
	public 	bool	debug;
	public Vector3	scale;
	public MeshFilter source;
	public MeshFilter[]	filters;
	public bool share;


	public static void Save( MeshFilter source, bool _clearTangentsAndNormals = true )
	{
		if( source != null )
		{
			Mesh mesh = source.mesh;
			if( _clearTangentsAndNormals )
			{
				mesh.tangents = null;
				mesh.normals = null;
			}
			
			mesh.name = source.transform.name;
			Debug.Log("Vert "+mesh.vertices.Length );
			Debug.Log("Norm "+mesh.normals.Length );
			Debug.Log("Tang "+mesh.tangents.Length );
			/*if( useScale )//&& source.transform.localScale != Vector3.one )
			{

				for( int j = 0; j < vertices.Length; j++ )
				{
					vertices[ j ].x *= scale.x;
					vertices[ j ].y *= scale.y;
					vertices[ j ].z *= scale.z;
				}

			}*/
			
			AssetDatabase.CreateAsset( mesh, "Assets/TEST/" + mesh.name + ".asset" );
			AssetDatabase.SaveAssets();
		}
	}

	void OnDrawGizmosSelected()
	{
		if (share) {
			share = false;
			for( int i = 0; i < filters.Length; i++ )
			{
				filters[ i ].sharedMesh = source.sharedMesh;
			}
		}

		if (debug) {
		
			debug = false;
			if( source != null )
			{
				Debug.Log( source.sharedMesh.tangents.Length +"  "+ source.sharedMesh.normals.Length );
				Debug.Log( source.sharedMesh.uv.Length +"  "+ source.sharedMesh.uv2.Length +"  "+source.sharedMesh.uv2.Length );
				Debug.Log( source.sharedMesh.colors.Length+"  "+source.sharedMesh.colors32.Length);
			}
		}

		if (save) 
		{
			save  = false;
			Transform tr;
			MeshFilter mf;
			Mesh mesh;

			if( source != null )
			{
				mesh = source.mesh;
				if( clearTangentsAndNormals )
				{
					mesh.tangents = null;
					mesh.normals = null;
				}

				mesh.name = source.transform.name;
				Debug.Log("Vert "+mesh.vertices.Length );
				Debug.Log("Norm "+mesh.normals.Length );
				Debug.Log("Tang "+mesh.tangents.Length );
				if( useScale )//&& source.transform.localScale != Vector3.one )
				{
					Vector3[] 	vertices = mesh.vertices;
					for( int j = 0; j < vertices.Length; j++ )
					{
						vertices[ j ].x *= scale.x;
						vertices[ j ].y *= scale.y;
						vertices[ j ].z *= scale.z;
					}
					mesh.vertices = vertices;
				}


				AssetDatabase.CreateAsset( mesh, "Assets/TEST/" + mesh.name + ( toObj ? ".obj":".asset" ) );
				AssetDatabase.SaveAssets();
			}
			else
			{
				for( int i = 0; i < transform.childCount; i++ )
				{
					tr = transform.GetChild( i );
					if( tr && tr.gameObject.activeSelf )
					{
						mf = tr.GetComponent<MeshFilter>();
						if( mf )
						{
							mesh = mf.mesh;

							if( clearTangentsAndNormals )
							{
								mesh.tangents = null;
								mesh.normals = null;
							}

							mesh.name = tr.name;
							Debug.Log("Vert "+mesh.vertices.Length );
							Debug.Log("Norm "+mesh.normals.Length );
							Debug.Log("Tang "+mesh.tangents.Length );
							if( useScale && tr.localScale != Vector3.one )
							{
								Vector3[] 	vertices = mesh.vertices;
								for( int j = 0; j < vertices.Length; j++ )
								{
									vertices[ j ].x *= tr.localScale.x;
									vertices[ j ].y *= tr.localScale.y;
									vertices[ j ].z *= tr.localScale.z;
								}
								mesh.vertices = vertices;
							}
							AssetDatabase.CreateAsset( mesh, "Assets/TEST/" + mesh.name + ( toObj ? ".fbx":".asset" ) );
							AssetDatabase.SaveAssets();
						}
					}
				}
			}
		}
	}
	#endif
}
