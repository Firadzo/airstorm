﻿using UnityEngine;
using System.Collections;

public class UVEditor : MonoBehaviour {
#if UNITY_EDITOR
	public bool debugUV;
	public bool fixUV;
	public bool back;
	public Vector2[]	defaultUV;

	public Vector2 	scaleFactor;
	public Vector2	offset;

	void OnDrawGizmosSelected()
	{

		if (debugUV) {
			debugUV = false;
			MeshFilter mf = GetComponent<MeshFilter> ();
			foreach (Vector2 uv in mf.sharedMesh.uv) {
				Debug.Log (uv);
			}
		} 
		if (fixUV) {
			fixUV = false;
			MeshFilter mf = GetComponent<MeshFilter>();
			defaultUV = mf.sharedMesh.uv;
			Vector2[] newUVS = (Vector2[])mf.sharedMesh.uv.Clone();
			for( int i = 0; i < newUVS.Length; i++ )
			{
				newUVS[ i ].x *= scaleFactor.x;
				newUVS[ i ].y *= scaleFactor.y;
				newUVS[ i ] += offset;
			}
			mf.sharedMesh.uv = newUVS;
			//mf.mesh.

		}
		if (back) {
			back = false;
			MeshFilter mf = GetComponent<MeshFilter>();
			Vector2[] newUVS = (Vector2[])mf.sharedMesh.uv.Clone();
			for( int i = 0; i < newUVS.Length; i++ )
			{
				newUVS[ i ] -= offset;
				newUVS[ i ].x /= scaleFactor.x;
				newUVS[ i ].y /= scaleFactor.y;
			}
			mf.sharedMesh.uv = newUVS;
		}
	}
#endif
}
