﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CustomConsole : MonoBehaviour {

	private static CustomConsole 	instance;
	private static string 			debugString;
	
	public delegate bool CommandAction( string command );
	private static List< CommandAction > commands = new List< CommandAction >();

	public static UILabel	consoleLbl;
	

	public static void AddCommand( CommandAction commandAction )
	{
		commands.Add( commandAction );
	}

	public static void CheckCommand( string command )
	{
		command = command.ToLower();
		foreach( CommandAction commandAction in commands )
		{
			if( commandAction( command ) )
			{
				return;
			}
		}
		DebugText( command + " command does not exists" );
	}
	
	public static CustomConsole GetInstanse()
	{
		if( instance == null )
		{
//			debugString = "";
//			GameObject guiDebug = new GameObject( "OnGUIDebug" );
//			instance = guiDebug.AddComponent<CustomConsole>();
//			DontDestroyOnLoad (guiDebug);
			
		}
		return instance;
	}

	void OnLevelWasLoaded(int level) {
	
		if( !string.IsNullOrEmpty( debugString ) )
		{
			//GetInstanse();
		}
		
	}

	public static void DebugText( string text )
	{
		//GetInstanse();
		debugString = debugString +"\n"+text;
		if (consoleLbl != null) {
			consoleLbl.text = debugString;
		}
	} 

	public static void ClearText( )
	{
		//GetInstanse();
		debugString = "";
		consoleLbl.text = debugString;
	} 
}
