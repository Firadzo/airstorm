﻿using UnityEngine;
using System.Collections;

public class DebugConsoleController : MonoBehaviour {

	public GameObject	consoleObject;
	public UILabel		consoleLbl;
	public UIScrollView	scrollBar;

	public	UIButton 	showHideBtn;
	private UILabel	 	btnLbl;
	private bool		consoleVisible;

	public static System.Action<bool>	OnConsoleStateChanged;

	void Awake()
	{
		ConsoleCommands.AddCommands ();
		CustomConsole.consoleLbl = consoleLbl;
		scrollBar = consoleObject.GetComponentInChildren<UIScrollView>();
		showHideBtn.onClick.Add( new EventDelegate( ShowHideConsole ) );
		btnLbl = showHideBtn.GetComponentInChildren<UILabel>();
		consoleVisible = true;
		UIInput consolseInput = GetComponentInChildren<UIInput>();
		consolseInput.onSubmit.Add( new EventDelegate(  OnSubmit ) );
		ShowHideConsole();
	}

	private void ShowHideConsole()
	{
		consoleVisible = !consoleVisible;
		Time.timeScale = consoleVisible ? 0 : 1f;
		OnConsoleStateChanged( consoleVisible );
		consoleObject.SetActive( consoleVisible );
		//btnLbl.text = consoleVisible ? "Hide Console" : "Console" ;
		if (consoleVisible) {
			//CustomConsole.CheckCommand( "help" );
		}
	}

	public void OnSubmit( )
	{
		CustomConsole.CheckCommand( UIInput.current.value );
		UIInput.current.value = "";
	}

	void Update()
	{
		if( Input.GetKeyDown( KeyCode.BackQuote ) )
		{
			ShowHideConsole();
		}
	}
}
