﻿using UnityEngine;
using System.Collections;

public class DebugSplineAnimator : MonoBehaviour {
	
	public Spline spline;
	
	public float speed = 1f;
	public float offSet = 0f;
	public WrapMode wrapMode = WrapMode.Clamp;
	
	public float passedTime = 0f;
	public float angleRotation;
	
	//AutoRotate
	int angleTrigger = 0;
	int speedTrigger = 1;
	bool triggerRotate = false;
	bool right = false;
	
	RaycastHit hit;

	Vector3 rotateAroundPoint = Vector3.zero;
	Vector3 loopStartPos = Vector3.zero;
	float startAngle = 0;
	int checkLoopEndEnable = 3;//Skip frames ans enable check end loop
	
	public bool destroyAfterEnd = false;
	
	public bool kamikadze = false;
	private bool 			useSplineData;
	private SplineDataCopy	splineData;
	public delegate	void OnTragectoryEnd ();
	public OnTragectoryEnd	onTrajectoryEnd;
	
	
	private void Start() {
		//Player = PlayerScript.instance.gameObject;
		splineData = spline.GetComponent<SplineDataCopy> ();
		useSplineData = splineData != null;
		if (useSplineData) {
			//Destroy( spline );
		}
		speed *= 1.25f;
	}
	
	#if UNITY_EDITOR
	public bool emulateUpdate;
	public bool resetPosition;
	void OnDrawGizmosSelected()
	{
		if (emulateUpdate) {
			useSplineData = true;
		}
		if (resetPosition) {
			resetPosition = false;
			splineData = spline.GetComponent<SplineDataCopy> ();
			useSplineData = splineData != null;
			Reset();
		}
	}
	
	#endif
	void Update( )
	{
		#if UNITY_EDITOR
		if( emulateUpdate ){ return; }
		#endif
		if (useSplineData) {
			UseSplineData ();
		} else {
			UseSpline();
		}
	}
	
	public void Reset()
	{
		passedTime = 0;
	}
	
	private void CheckRotateTrigger()
	{
		if(right)
		{
			angleTrigger -= (speedTrigger * 5);
			if( angleTrigger <= 0 )
			{
				triggerRotate = false;
				angleTrigger = 0;
				speedTrigger = 0;
				right = false;
			}
		}
		else
		{
			angleTrigger += (speedTrigger * 5);
			if ( angleTrigger >= 0 )
			{
				triggerRotate = false;
				angleTrigger = 0;
				speedTrigger = 0;
				right = false;
			}
		}
	}
	
	float posOnSplne;
	Vector3 pos;
	const float ROTATION_SPEED = 3f;
	private void UseSplineData()
	{
		passedTime += Time.deltaTime * speed;
		posOnSplne = WrapValue( passedTime + offSet, 0f, 1f, wrapMode );
		
		splineData.CalculatePosAndRotation( posOnSplne );
		
		if( posOnSplne > 0.98f )
		{
			if( onTrajectoryEnd != null )
			{
				onTrajectoryEnd();
			}
		}
		
		pos = splineData.calculatedPosition;	
		Quaternion rot = splineData.calculatedRotation;
		Quaternion addHorizontalRot;
		/*if(triggerRotate)
		{	
			CheckRotateTrigger();
			addHorizontalRot = Quaternion.AngleAxis(angleTrigger, Vector3.forward);
			transform.rotation = Quaternion.Slerp(transform.rotation, ( rot * addHorizontalRot ), Time.deltaTime * 3f * speedTrigger);
		}
		else */
		{
				//Vector3 rotationDir = pos - transform.position;
			float angle = Vector3.Angle( pos - transform.position, transform.forward );	
			addHorizontalRot = Quaternion.AngleAxis( ( -angle * ROTATION_SPEED ), Vector3.forward );
			transform.rotation =  Quaternion.Slerp( transform.rotation, ( rot * addHorizontalRot ), Time.deltaTime * 5f );
		}
		//transform.rotation = Quaternion.Slerp(transform.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f);
		transform.position = pos;
	}

	private void UseSpline()
	{
		
	 passedTime += Time.deltaTime * speed;
		
		posOnSplne = WrapValue( passedTime + offSet, 0f, 1f, wrapMode );
		
		if(destroyAfterEnd && posOnSplne > 0.98f)
		{
			if( onTrajectoryEnd != null )
			{
				onTrajectoryEnd();
			}
			//Destroy(this.gameObject);
		}
		
		//if kamikadze - enable autopilot :-)
		if (kamikadze && posOnSplne > 0.98f && posOnSplne > 0)
		{
			gameObject.GetComponent<SplineAnimator>().enabled = false;
			gameObject.GetComponent<Kamikadze>().enabled = true;
			return;
		}
		
		pos =  spline.GetPositionOnSpline(posOnSplne);	
		
		//base rotation
		Quaternion rot = spline.GetOrientationOnSpline( WrapValue( passedTime + offSet, 0f, 1f, wrapMode ) );
		Quaternion addHorizontalRot;
			if(triggerRotate)
			{	
				CheckRotateTrigger();
				
				addHorizontalRot = Quaternion.AngleAxis(angleTrigger, Vector3.forward);
				transform.rotation = Quaternion.Slerp(transform.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f * speedTrigger);
			}
			else 
			{
				//Vector3 rotationDir = pos - transform.position;
				float angle = Vector3.Angle(pos - transform.position, transform.forward);			
				addHorizontalRot = Quaternion.AngleAxis((-angle*3f), Vector3.forward);
				transform.rotation = Quaternion.Slerp(transform.rotation, (rot * addHorizontalRot), Time.deltaTime * 3f);
			}
			transform.position = pos;
	}

	private float WrapValue( float v, float start, float end, WrapMode wMode )
	{
		switch( wMode )
		{
		case WrapMode.Clamp:
		case WrapMode.ClampForever:
			return Mathf.Clamp( v, start, end );
		case WrapMode.Default:
		case WrapMode.Loop:
			return Mathf.Repeat( v, end - start ) + start;
		case WrapMode.PingPong:
			return Mathf.PingPong( v, end - start ) + start;
		default:
			return v;
		}
	}
}
