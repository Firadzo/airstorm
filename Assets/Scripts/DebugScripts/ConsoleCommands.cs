﻿using UnityEngine;
using System.Collections;

public class ConsoleCommands
{
	private delegate bool ParamsPars<T>( string source, out T result );

    public static bool isActive { get; private set; }

	private static bool ParseCommandWithParams<T>( string command, string key, ParamsPars<T> paramsParser, ref T result )
	{
		string[] splited = command.Split(' ');
		if( splited.Length >= 2 )
		{
			if( splited [ 0 ].Trim() == key )
			{
				if( paramsParser( splited[ 1 ], out result ) )
				{
					return true;
				}
			}
		}
		return false;
	}

	private static bool SafeCommandExecution( System.Func< bool > coommandFunc )
	{
		try{
			return coommandFunc ();
		}
		catch( System.Exception e )
		{
			CustomConsole.DebugText( e.Message );
			return false;
		}
	}

	public static void AddCommands()
	{
		if( Application.isPlaying )
		{
			CustomConsole.AddCommand( Help );
			CustomConsole.AddCommand( AddCoins );
			CustomConsole.AddCommand( RaycastBullet );
			CustomConsole.AddCommand( Water );
			CustomConsole.AddCommand( Clear );
			CustomConsole.AddCommand( LoadLevel );
			CustomConsole.AddCommand( Invulnerability );
			CustomConsole.AddCommand( ScreenAD );
			CustomConsole.AddCommand( Banner );
			CustomConsole.AddCommand( TapjoyCoins );
			CustomConsole.AddCommand( TapjoyVideo );
			CustomConsole.AddCommand( ADColonyVideo );
			CustomConsole.AddCommand( AddLife );
			CustomConsole.AddCommand( KillPlayer );
			CustomConsole.AddCommand( UnityVideoAD );
			CustomConsole.AddCommand( RefreshStore );
			CustomConsole.AddCommand( EnableUI );
			CustomConsole.AddCommand( DeleteSaves );
			CustomConsole.AddCommand( SetAccelThreshold );
			CustomConsole.AddCommand( SetAccelOffset );
			DebugConsoleController.OnConsoleStateChanged += ( bool b ) => 
			{ 
		
                
			};
		}
	}

	private static bool EnableUI( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			bool enabled = false;
			if( ParseCommandWithParams( command, "ui", bool.TryParse, ref enabled ) )
			{
				GameObject obj = GameObject.Find( "GUI - Android(Clone)" );
				if( obj != null ) 
				{
					obj.GetComponent<UIPanel>().enabled = enabled;
					CustomConsole.DebugText( "UI "+ enabled );
				}
				else
				{
					CustomConsole.DebugText( "Can't find UI" );
				}
				return true;
			}
			return false;
		});
	}

	private static bool Help( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			command = command.Trim();
			if( command == "help" )
			{
				string s = "";
				s  += "help - показать все команды\n";
				s  += "coins int - добавить монет(количество монет просле пробела)\n";
				s  += "lvl int - загрузить уровень( номер уровня после пробела)\n";
				s  += "inv bool - неузявимость 0 или 1 через пробел\n";
				s  += "rays bool - enable | disable bullets raycasts\n";
				s  += "water 1 | 0 - enable | disable water\n";
				s  += "clear - очистить консоль\n";
				s  += "banner bool \n";
				s  += "tcoins int \n";
				s  += "uvideo tvideo advideo screenad addlife kill\n";
				s  += "rstore addlife kill\n";
				s  += "delsaves delete saves\n";
				s  += "accthr 0 - 1\n";
				CustomConsole.DebugText( s );
				return true;
			}
			return false;
		} );

	}

	private static bool SetAccelOffset ( string command )
	{
		return SafeCommandExecution( () => 
		                            {
			float result = 0;
			if( ParseCommandWithParams( command, "accoffs", float.TryParse, ref result ) )
			{
				PlayerAcceleratorController.accelerometerOffset.y =  result;
				CustomConsole.DebugText( "Accelerometer vertical offset now "+ PlayerAcceleratorController.accelerometerOffset.y );
				return true;
			}
			return false;
		});
	}

	private static bool SetAccelThreshold( string command )
	{
		return SafeCommandExecution( () => 
		                            {
			float result = 0;
			if( ParseCommandWithParams( command, "accthr", float.TryParse, ref result ) )
			{
				PlayerAcceleratorController.accelThreshold = Mathf.Clamp01( result );
				CustomConsole.DebugText( "Accelerometer threshold now "+ result );
				return true;
			}
			return false;
		});
	}

	private static bool DeleteSaves( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			if( command == "delsaves" )
			{
				PlayerPrefs.DeleteAll();
				CustomConsole.DebugText( "Saves deleted" );
				return true;
			}
			return false;
		});
	}

	private static bool RefreshStore( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			if( command == "rstore" )
			{
				Soomla.Store.StoreInventory.RefreshLocalInventory();
				CustomConsole.DebugText( "Soomla store refreshed" );
				return true;
			}
			return false;
		});
	}

	private static bool KillPlayer( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			if( command == "kill" )
			{
				PlayerScript.Kill();
				CustomConsole.DebugText( "player killed" );
				return true;
			}
			return false;
		});
	}

	private static bool AddLife( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			if( command == "addlife" )
			{
				EndLevelPopup.CallAddLife();
				CustomConsole.DebugText( "add life call" );
				return true;
			}
			return false;
		});
	}

	private static bool LoadLevel( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			int lvl = 0;
			if( ParseCommandWithParams( command, "lvl", int.TryParse, ref lvl ) )
			{
				lvl = Mathf.Clamp( lvl, 1, 5 );
				if( EndLevelPopup.instance != null )
				{
					EndLevelPopup.instance.LoadLevel( lvl );
				}
				else
				{
					MainMenuConroller menuController = GameObject.FindObjectOfType<MainMenuConroller>();
					menuController.LoadLevel( lvl );
				}
				CustomConsole.DebugText( "Loading level "+lvl );
				return true;
			}
			return false;
		});
	}

	private static bool Invulnerability( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			int inv = 0;
			if( ParseCommandWithParams( command, "inv", int.TryParse, ref inv ) )
			{
				PlayerScript.debugInvulnerability = inv > 0;
				CustomConsole.DebugText( "Игрок "+ ( PlayerScript.debugInvulnerability? "неуязвим":"уязвим" ) );
				return true;
			}
			return false;
		});
	}

	private static GameObject water;
	private static bool Water( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			int visible = 0;
			if( ParseCommandWithParams( command, "water", int.TryParse, ref visible ) )
			{
				if( water == null )
				{
					water = GameObject.FindWithTag("SmartWater");
				}
				if( water != null )
				{
					water.SetActive( visible > 0 );
					CustomConsole.DebugText( "Water "+ ( visible > 0 ? "enabled":"disabled" ) );
				}
				else
				{
					CustomConsole.DebugText( "Can't find water" );
				}
				return true;
			}
			return false;
		});
	}

	private static bool UnityVideoAD( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			if( command == "uvideo" )
			{
				if( MyTapjoy.ShowUnityADVideo( ()=>{ CustomConsole.DebugText("Sucess");}, ()=>{ CustomConsole.DebugText("Fail");} ) )
				{
					CustomConsole.DebugText( "Show fullscreen AD" );
				}
				else
				{
					CustomConsole.DebugText( "Video doesn't ready" );
				}
				return true;
			}
			return false;
		});
	}

	private static bool ScreenAD( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			if( command == "screenad" )
			{
				MyTapjoy.ShowInterstitial();
				CustomConsole.DebugText( "Show fullscreen AD" );
				return true;
			}
			return false;
		});
	}

	private static bool TapjoyVideo( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			if( command == "tvideo" )
			{
				MyTapjoy.ShowAddLifeVideo();
				CustomConsole.DebugText( "Show tapjoy video" );
				return true;
			}
			return false;
		});
	}

	private static bool ADColonyVideo( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			if( command == "advideo" )
			{
				AdColony.ShowVideoAd( ADCAdManager.VIDEO_ID );
				CustomConsole.DebugText( "Show ad colony video" );
				return true;
			}
			return false;
		});
	}

	private static bool Banner( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			bool flag = false;
			if( ParseCommandWithParams( command, "banner", bool.TryParse, ref flag ) )
			{
				if( flag )
				{
					MyAdMob.ShowBunner();
				}
				else
				{
					MyAdMob.HideBunner();
				}
				CustomConsole.DebugText( "Banner "+( flag ? "visible":"hidden" ) );
				return true;
			}
			return false;
		});
	}

	private static bool TapjoyCoins( string command )
	{	
		return SafeCommandExecution( () => 
		                            {  
			int coins = 0;
			if( ParseCommandWithParams( command, "tcoins", int.TryParse, ref coins ) )
			{
				if( coins > 0 )
				{
					TapjoyUnity.Tapjoy.AwardCurrency( coins );
				}
				else
				{
					TapjoyUnity.Tapjoy.SpendCurrency( coins * -1 );
				}
				CustomConsole.DebugText( "Tapjoy "+coins+" coins" );
				return true;
			}
			return false;
		});
	}

	private static bool Clear( string command )
	{
		return SafeCommandExecution( () => 
		{
			if( command == "clear" )
			{
			   CustomConsole.ClearText();
			   return true;
			}
			return false;
		} );
	}


	private static bool AddCoins( string command )
	{	
		return SafeCommandExecution( () => 
		{  
			int coins = 0;
			if( ParseCommandWithParams( command, "coins", int.TryParse, ref coins ) )
			{
				PlayerScript.instance.playerMoney += coins;
				CustomConsole.DebugText( "Added "+coins+" coins" );
				return true;
			}
			return false;
		});
	}


	private static bool RaycastBullet( string command )
	{
		return SafeCommandExecution( () => 
		                            {  
			bool raycast = false;
			if( ParseCommandWithParams( command, "rays", bool.TryParse, ref raycast ) )
			{
				Bullet.userRaycast = raycast;
				CustomConsole.DebugText( "Bullets raycasts "+( raycast ? "enabled":"disabled" ) );
				return true;
			}
			return false;
		});
	}
}
