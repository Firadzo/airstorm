﻿using UnityEngine;
using System.Collections;

public class DebugStatic : MonoBehaviour {

	private UILabel label;
	private bool	staticOn;
	
	void Awake () {
		label = GetComponent<UILabel> ();
		staticOn = true;
	}
	
	public void OnClick()
	{
		staticOn = !staticOn;
		label.text = staticOn ? "OFF static" : "On static";
		MeshRenderer[] renderers = FindObjectsOfType<MeshRenderer> ();
		foreach (MeshRenderer render in renderers) {
			if (render.gameObject.isStatic) {
				render.enabled = staticOn;
			}
		}
	}
}
