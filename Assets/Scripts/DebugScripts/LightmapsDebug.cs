﻿using UnityEngine;
using System.Collections;

public class LightmapsDebug : MonoBehaviour {

	private UILabel label;
	private bool	lightMapsON;
	private LightmapData[]	lightMaps;
	
	void Awake () {
		label = GetComponent<UILabel> ();
		lightMapsON = true;
		lightMaps = LightmapSettings.lightmaps;
		label.text = lightMapsON ? "OFF lightmaps" : "On lightmaps";
	}
	
	public void OnClick()
	{
		lightMapsON = !lightMapsON;
		label.text = lightMapsON ? "OFF lightmaps" : "On lightmaps";
		if (lightMapsON) {
			LightmapSettings.lightmaps = lightMaps;
		} 
		else {
			LightmapSettings.lightmaps = new LightmapData[0];
		}
	}
}
