﻿using UnityEngine;
using System.Collections;

public class DebugParticles : MonoBehaviour {

	private UILabel label;
	private bool	particlesON;
	
	void Awake () {
		label = GetComponent<UILabel> ();
		particlesON = true;
	}
	
	public void OnClick()
	{
		particlesON = !particlesON;
		label.text = particlesON ? "OFF particles" : "On particles";
		ParticleSystem[] particles = FindObjectsOfType<ParticleSystem> ();
		foreach (ParticleSystem render in particles) {
			render.GetComponent<Renderer>().enabled = particlesON;
		}
	}
}
