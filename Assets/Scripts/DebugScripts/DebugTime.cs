﻿using UnityEngine;
using System.Collections;

public class DebugTime : MonoBehaviour {
	private UILabel label;
	private bool	timePaused;

	void Awake () {
		label = GetComponent<UILabel> ();
		timePaused = false;
	}

	public void OnClick()
	{
		timePaused = !timePaused;
		Time.timeScale = timePaused ? 0 : 1;
		label.text = timePaused ? "Resume" : "Pause";
	}

}
