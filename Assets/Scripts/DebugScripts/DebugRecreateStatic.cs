﻿using UnityEngine;
using System.Collections;

public class DebugRecreateStatic : MonoBehaviour {

	IEnumerator OnClick()
	{
		MeshFilter[] renderers = FindObjectsOfType<MeshFilter> ();
		//Material[]	mats = new Material[ renderers.Length ];
		Mesh[]	meshes = new Mesh[ renderers.Length ];
		GameObject[]	objects = new GameObject[ renderers.Length ];
		for( int i = 0; i < renderers.Length; i++ )
		{
			if (renderers[i].gameObject.isStatic) {
				//mats[ i ] = renderers[i].GetComponent<MeshRenderer>().sharedMaterial;
				objects[ i ] = renderers[i].gameObject;
				meshes[ i ] = renderers[ i ].sharedMesh; 
				Destroy( renderers[i] );
				//MeshRenderer r = obj.AddComponent<MeshRenderer>();
				//r.sharedMaterial = tempMat;
			}
		}
		yield return new WaitForEndOfFrame();
		for (int i = 0; i < objects.Length; i++) {
			if( objects[ i ] != null )
			{
				//objects[ i ].AddComponent<MeshFilter>().sharedMesh = meshes[ i ] ;
			}
		}
	}
}
