﻿using UnityEngine;
using System.Collections;

public class DebugDynamic : MonoBehaviour {

	private UILabel label;
	private bool	dynamicOn;
	
	void Awake () {
		label = GetComponent<UILabel> ();
		dynamicOn = true;
	}
	
	public void OnClick()
	{
		dynamicOn = !dynamicOn;
		label.text = dynamicOn ? "OFF dynamic" : "On dynamic";
		MeshRenderer[] renderers = FindObjectsOfType<MeshRenderer> ();
		foreach (MeshRenderer render in renderers) {
			if( !render.gameObject.isStatic )
			{
			  render.enabled = dynamicOn;
			}
		}
	}
}
