﻿using UnityEngine;
using System.Collections;

public class DebugLight : MonoBehaviour {

	private UILabel label;
	private bool	lightEnabled;
	
	void Awake () {
		label = GetComponent<UILabel> ();
		lightEnabled = true;
	}
	
	public void OnClick()
	{
		lightEnabled = !lightEnabled;
		label.text = lightEnabled ? "Disable light" : "Enable light";
		FindObjectOfType<Light> ().enabled = lightEnabled;
	}
}
