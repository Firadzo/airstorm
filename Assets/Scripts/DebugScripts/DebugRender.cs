﻿using UnityEngine;
using System.Collections;

public class DebugRender : MonoBehaviour {

	private UILabel label;
	private bool	worldVisible;
	
	void Awake () {
		label = GetComponent<UILabel> ();
		worldVisible = true;
	}
	
	public void OnClick()
	{
		worldVisible = !worldVisible;
		label.text = worldVisible ? "Hide world" : "Show world";
		MeshRenderer[] renderers = FindObjectsOfType<MeshRenderer> ();
		foreach (MeshRenderer render in renderers) {
			render.enabled = worldVisible;
		}
	}
}
