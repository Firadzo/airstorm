﻿using UnityEngine;
using System.Collections;

public class ShowHideDebug : MonoBehaviour {

	private UILabel label;
	private bool	debugVisible;
	public GameObject	debug;
	
	void Awake () {
		label = GetComponent<UILabel> ();
		debugVisible = true;
		OnClick ();
	}
	
	public void OnClick()
	{
		debugVisible = !debugVisible;
		label.text = debugVisible ? "Hide debug" : "Show debug";
		debug.SetActive (debugVisible);
	}
}
