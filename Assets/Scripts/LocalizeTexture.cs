﻿using UnityEngine;
using System.Collections;

public class LocalizeTexture : MonoBehaviour {
	public Texture	engTexture;
	public Texture	ruTexture;

	private UITexture texture;
	// Use this for initialization
	void Awake () 
	{
		texture = GetComponent<UITexture>();
		Localization.onLocalize += Localize;
	}

	void Start()
	{

	}

	void OnDestroy()
	{
		Localization.onLocalize -= Localize;
	}

	// Update is called once per frame
	void Localize ( string lang ) 
	{
		if( lang == "Russian" )
		{
			texture.mainTexture = ruTexture;
		}
		else
		{
			texture.mainTexture = engTexture;
		}
	}
}
