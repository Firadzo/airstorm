﻿using UnityEngine;
using System.Collections;

public class ParentedEffect : GameEffectBase {

	private ParticleSystem	particles;
	private Transform		effectsParent;
	private bool			checkForDisable;
	// Use this for initialization
	override protected void Awake () {
		particles = GetComponent<ParticleSystem> ();
		base.Awake ();
		effectsParent = transf.parent;
	}

	override public void Play( Transform parent )
	{
		base.Play (parent);
		particles.enableEmission = true;
		particles.Play ();
	}

	public void Detach()
	{
		transf.parent = effectsParent;
		particles.enableEmission = false;
		checkForDisable = true;
	}

	// Update is called once per frame
	void Update () {
	if (checkForDisable) {
		
			if( particles.particleCount == 0 )
			{
				EffectEnd();
			}
		}
	}
}
