﻿using UnityEngine;
using System.Collections;

public class AtlasAndPlatformConroller : MonoBehaviour {
    public bool resolution800x480 = false;//Android telephones
    public bool resolution1024x768 = false;//Ipad 1-4, ipad mini 
    public bool resolution1136x640 = false;//Iphone 4, 5, HD Android telephones

    public int resulutionAndroid = 800;
    public int resulutionIpad = 1024;
    public int resoltionHD = 1136;

    //gui prefab (3 different)
    public GameObject androidGUI;
    public GameObject ipadGUI;
    public GameObject hdGUI;
	//Comixes
	public string[] 	Comixes800_480;
	public GameObject[] Comixes1024_768;
	public GameObject[] Comixes1136_600;

	private PlayerScript	player;

    void Awake()
    {
        SpawnPlayer plSpawn = GameObject.Find("LevelConroller").GetComponent<SpawnPlayer>();

		if (plSpawn.Player == null) {
			plSpawn.Player = GameObject.Find ("Player").transform;
		}
        plSpawn.CamPlayerMove = GameObject.Find("PlayerMove").transform;
		

        if (!resolution800x480 && !resolution1024x768 && !resolution1136x640)  //only for development!!!
        {
            int guiType = PlayerPrefs.GetInt("guiType", 1);

            switch (guiType)
            {
                case 1:
                    Instantiate(androidGUI, transform.position, Quaternion.identity);
					plSpawn.currComixes = Comixes800_480;
                    break;
//                case 2:
//                   	Instantiate(ipadGUI, transform.position, Quaternion.identity);
//					plSpawn.currComixes = Comixes1024_768;
//                    break;
//                case 3:
//                    Instantiate(hdGUI, transform.position, Quaternion.identity);
//					plSpawn.currComixes = Comixes1136_600;
//                    break;
            }
        } else {
            //only for development!!!
            if (resolution800x480)
            {
               	Instantiate(androidGUI, transform.position, Quaternion.identity);
				plSpawn.currComixes = Comixes800_480;
                PlayerPrefs.SetInt("guiType", 1);
            }
//            else if (resolution1024x768)
//            {
//                Instantiate(ipadGUI, transform.position, Quaternion.identity);
//				plSpawn.currComixes = Comixes1024_768;
//                PlayerPrefs.SetInt("guiType", 2);
//            }
//            else if (resolution1136x640)
//            {
//                Instantiate(hdGUI, transform.position, Quaternion.identity);
//				plSpawn.currComixes = Comixes1136_600;
//                PlayerPrefs.SetInt("guiType", 3);
//            }
            //end
        }    

        GameObject arhor = GameObject.Find("Anchor");
        if (arhor) {
            plSpawn.ComixArhor = arhor;
        }

        GameObject ingame = GameObject.Find("InGameMenu");
        if (ingame) {
            plSpawn.InGameMenu = ingame;
        }

		player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>();
		player.desantButton = GameObject.Find("DesantButton");
		setClickOnButtons( player.desantButton);
		
		player.superShield = GameObject.Find("SuperShield").GetComponent<UISlider>();
		setClickOnButtons( player.superShield.gameObject);
		
		player.superBomb = GameObject.Find("SuperBomb").GetComponent<UISlider>();
		setClickOnButtons( player.superBomb.gameObject);
		
		player.Lifebar 		= GameObject.Find("LifeBar").GetComponent<UISlider>();
		player.liveCounter 	= GameObject.Find("LifeCounter").GetComponent<UILabel>();
		player.scoreCounter 	=  GameObject.Find("Score").GetComponent<UILabel>();
		
		player.bomb = GameObject.Find("Bomb").GetComponent<UISlider>();
		setClickOnButtons( player.bomb.gameObject);
		
		player.torpedo = GameObject.Find("Torepedo").GetComponent<UISlider>();
		setClickOnButtons( player.torpedo.gameObject);
		
		player.shield = GameObject.Find("Shield").GetComponent<UISlider>();
		setClickOnButtons( player.shield.gameObject);
		
		player.repairePlane = GameObject.Find("RepairePlane").GetComponent<UISlider>();
		setClickOnButtons( player.repairePlane.gameObject);
        if(PlayerPrefs.GetInt("replay", -1) > 0)plSpawn.spawnPlayer();

     /*   GameObject store = GameObject.Find("GoogleIAB");
        if (store) {
            GoogleIABEventListener listener = store.GetComponent<GoogleIABEventListener>();
            GameObject moneylbl = GameObject.Find("MoneyLbLStore");
            if (moneylbl) {
                listener.storeMoneyLbl = moneylbl.GetComponent<UILabel>();
            }
            GameObject x1000 = GameObject.Find("1000");
            x1000.GetComponent<UIButtonMessage>().target = store;
            GameObject x2500 = GameObject.Find("2500");
            x2500.GetComponent<UIButtonMessage>().target = store;
            GameObject x5000 = GameObject.Find("5000");
            x5000.GetComponent<UIButtonMessage>().target = store;
            GameObject x30000 = GameObject.Find("30000");
            x30000.GetComponent<UIButtonMessage>().target = store;
            GameObject mig = GameObject.Find("BuyMig");
            mig.GetComponent<UIButtonMessage>().target = store;
        }*/
    }

    void setClickOnButtons(GameObject button ) 
	{
       // Transform tr = button.transform.FindChild("Collider");
		UIButtonMessage mess = button.GetComponentInChildren<UIButtonMessage>();
		mess.target = player.gameObject;
    }


}
