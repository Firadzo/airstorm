﻿using UnityEngine;
using System.Collections;

public class TextureAnimation : MonoBehaviour {

	private Material material;
	public float speedX;
	public float speedY;

	// Use this for initialization
	void Awake () {
		material = GetComponent<MeshRenderer>().material;
	}
	
	// Update is called once per frame
	private Vector2 offset;
	void Update () {
		offset = material.mainTextureOffset;
		offset.x += speedX * Time.deltaTime;
		offset.y += speedY * Time.deltaTime;
		material.mainTextureOffset = offset;
	}
}
