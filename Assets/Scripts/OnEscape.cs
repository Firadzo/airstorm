﻿using UnityEngine;
using System.Collections;

public class OnEscape : MonoBehaviour {
	public string message;
	
	private void Update() {
		if ( Input.GetKeyDown( KeyCode.Escape ) ) 
		{
			gameObject.SendMessage( message );
		}
	}
}
