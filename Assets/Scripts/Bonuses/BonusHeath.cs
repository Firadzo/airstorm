using UnityEngine;

public class BonusHeath : MonoBehaviour
{
	public float heathAddEasy = 5.0f;
	public float heathAddMiddle = 5.0f;
	public float heathAddMHard = 5.0f;
	
	void Update()
	{
		
		//gameObject.transform.position = gameObject.transform.position + Vector3.up * Mathf.Sin(Time.time * 2f) * 0.25f;
        gameObject.transform.rotation = gameObject.transform.rotation * Quaternion.Euler(Time.fixedDeltaTime * 100f, 0f, 0f);
		
	}
	
	void OnTriggerEnter (Collider col)
	{
		if(col.gameObject.tag == "Player")
		{
			PlayerScript player = col.transform.GetComponentInChildren<PlayerScript>();
		    float addLife = 0;
		
			switch(PlayerPrefs.GetInt(CONST.PlayerDifficultyKey))
			{
				case 0:
                    addLife += heathAddEasy;
				break;	
				case 1:
                    addLife += heathAddMiddle;
				break;	
				case 2:
                    addLife += heathAddMHard;
				break;
			}
				
		    if (player.Health + addLife > player.maxHeath) {
		        player.Health = player.maxHeath;
		    } else {
		        player.Health += addLife;
		    }

			Destroy(gameObject);
			
		}
	}
}
