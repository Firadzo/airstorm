using UnityEngine;
using System.Collections;

public class BonusCoin : MonoBehaviour {

    public 	int 			money = 0;
    public 	bool 			itIsBigCoin = false;
    public 	AudioClip 		collectSound;
    private bool 			addCoin = false;
	private bool			waitForSound;
	private MeshRenderer	meshRend;
	public	GameObject		haloObj;
	private Transform		transf;
	private GameObject		obj;
	const float				MAGNET_SPEED = 75f;
	private AudioSource		audio;

	public bool isActive
	{
		get{ return !obj.activeSelf; }
	}

	void Awake()
	{
		transf = transform;
		obj = gameObject;
		meshRend = GetComponent<MeshRenderer>();
		waitForSound = false;
		audio = GetComponent<AudioSource> ();
		if (audio != null) {
			audio.spatialBlend = 0;
		}
		obj.SetActive (false);
	}

	void Start() 
    {
	    money = (itIsBigCoin) ? PlayerPrefs.GetInt("bigCoin", 50) : PlayerPrefs.GetInt("smallCoin", 10);
        gameObject.AddComponent<AudioSource>();
        GetComponent<AudioSource>().clip = collectSound;
	}

	public void Spawn( Vector3 atPos )
	{
		waitForSound = false;
		addCoin = false;
		meshRend.enabled = true;
		atPos.y = PlayerScript.getPosition.y;
		transf.position = atPos;
		haloObj.SetActive (true);
		obj.SetActive (true);
	}


	float curDistToPlayer;
	void Update() 
    {        
        if ( !addCoin )
        {
			//transf.position = new Vector3( transf.position.x, PlayerScript.getPosition.y, transf.position.z );
			curDistToPlayer = Vector3.Distance( transf.position, PlayerScript.getPosition );
			if (  curDistToPlayer < 25 ) 
			{
				if( curDistToPlayer <= 10 )
				{
					meshRend.enabled = false;
					haloObj.SetActive( false );
					addCoin = true;
					audio.Play();
					PlayerScript.instance.playerMoney += money;
					waitForSound = true;
				}
				else
				{
					transf.Translate(( PlayerScript.getPosition - transf.position ).normalized * Time.deltaTime * MAGNET_SPEED, Space.World);
				}
            }

	    }
		else
		{
			transf.Rotate(Vector3.up, 100f * Time.deltaTime);
		}
		if( waitForSound && !GetComponent<AudioSource>().isPlaying )
		{
			waitForSound = false;
			obj.SetActive( false );
		}

	}
}
