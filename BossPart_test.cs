﻿using UnityEngine;
using System.Collections;

public class BossPart : MonoBehaviour {
    public bool avariable = false;
    public GameObject ExplosionEffect;
    public float Heath;
    private Color defaultColor;
	public GameObject SmokePref;
	// Use this for initialization
	void Start () {
        Transform[] root = GetComponentsInChildren<Transform>();
        for (int d = 0; d < root.Length; d++)
        {
            if (root[d].GetComponent<MeshRenderer>()) {
                 defaultColor = root[d].renderer.material.GetColor("_Color");
            }
        }
	}
	private Transform _smokeEffect;
	// Update is called once per frame
	void Update () {
		if(!avariable)
		{
			gameObject.GetComponent<BoxCollider>().enabled = false;
			return;
		}
		else 
		{
			gameObject.GetComponent<BoxCollider>().enabled = true;	
		}	
		
	    if (Heath < 0) {
            if (ExplosionEffect) {
                Transform eff = Instantiate(ExplosionEffect, transform.position + (new Vector3(0, 1.6f, 0)), transform.rotation) as Transform;
                if(eff)Destroy(eff.gameObject, 3);
            }
			if (SmokePref && !_smokeEffect) {
				_smokeEffect = ((GameObject)Instantiate(SmokePref)).transform;
				_smokeEffect.parent = transform;
				_smokeEffect.localPosition = Vector3.zero;
				_smokeEffect.localScale = Vector3.one;
				_smokeEffect.localRotation = Quaternion.identity;

			}
	        Destroy(gameObject);
	    }
	}

    //flashing after damage
    public void setDamageColor()
    {
		if(!avariable) return;
		
        if (Heath > 0)
        {
            Transform[] root = GetComponentsInChildren<Transform>();

            for (int d = 0; d < root.Length; d++) {
                 if (root[d].GetComponent<MeshRenderer>()) root[d].renderer.material.SetColor("_Color", new Color(255f, 255f, 255f, 255f));
            }

            StartCoroutine(setDefaultTextures());

        }
    }

    IEnumerator setDefaultTextures()
    {
        yield return new WaitForSeconds(0.1f);

        Transform[] root = GetComponentsInChildren<Transform>();

        for (int d = 0; d < root.Length; d++)
        {
            if (root[d].GetComponent<MeshRenderer>()) root[d].renderer.material.SetColor("_Color", defaultColor);
        }
    }

    public void applyDamage(float hp) {
		if(!avariable) return;
        Heath -= hp;
    }
}
